/// <reference path="./../../jquery.d.ts"/>
/// <reference path="./ajax.d.ts"/>

class Default_Penalty_Form
{
    form: AjaxForm;
    filter: Default_Penalty_FilterController;
    elements: Default_Penalty_Form_Elements;
    recordId: any;

    /**
     * Форма создания системы автоматического штрафования
     * @param filter
     */
    constructor(filter: Default_Penalty_FilterController) {
        this.filter = filter;

        var that = this;
        var formParams:AjaxFormOptions = {
            width: 500,
            style: 'normal',
            destroyOnHide: true
        };

        var form = this.form = new AjaxForm($('script[data-script-type="create-penalty-form"]').html(), formParams);

        form.setShowCallback(function(){
            that.recordId = that.filter.dataTable.selectedRecordId;

            form.ajax(
                {
                    'url': '/default/penalty/get-form',
                    'type': 'GET',
                    'dataType': 'json',
                    'data': {
                        'id': that.recordId
                    }
                },
                {
                    'done': function(data:Default_Penalty_Form_AjaxResponse_GetForm, form:AjaxForm) {
                        that.buildForm(data);
                    }
                }
            );
        });

        form.done(function (data, form:AjaxForm) {
            form.hide();

            if(window['filterController'] instanceof Default_Penalty_FilterController) {
                window['filterController'].dataTable.reset();
            }
        });

        this.elements = new Default_Penalty_Form_Elements(this);
    }

    /**
     * Создание формы создания системы оплаты
     * @param data
     */
    buildForm(data:Default_Penalty_Form_AjaxResponse_GetForm) {
        var that = this;
        var formDataDeferred = this.form.utils.chain();
        var formBuildDeferred = this.form.utils.chain();

        if(data) {
            this.deferredFillFormData(formDataDeferred, data);
        }

        this.deferredFormBuilder(formBuildDeferred, data);

        formBuildDeferred.run();
        formDataDeferred.run();
    }

    /**
     * Создание формы создания системы оплаты - deferred-контейнтер
     * @param deferred
     * @param dData
     * @returns {any}
     */
    deferredFormBuilder(deferred, dData) {
        var form = this;
        var filter = this.filter;

        deferred
            // reset watcher/scheme selectors
            .add(function() {
                form.elements.watcherSelect.find('option').remove();
                form.elements.schemeSelect.find('option').remove();
            })

            // setup watcher selectors
            .add(function(){
                for(var n in dData.repository.watchers) {
                    var record = dData.repository.watchers[n];

                    if(record.visible) {
                        form.elements.watcherSelect.append(
                            $('<option>').val(record.name)
                                .attr('title', record.description)
                                .attr('data-has-sub-form', record.subForm ? '1' : '0')
                                .text(record.title)
                        )
                    }
                }
            })

            // setup scheme selectors
            .add(function(){
                for(var n in dData.repository.schemes) {
                    var record = dData.repository.schemes[n];

                    if(record.visible) {
                        form.elements.schemeSelect.append(
                            $('<option>').val(record.name)
                                .attr('title', record.description)
                                .attr('data-has-sub-form', record.subForm ? '1' : '0')
                                .text(record.title)
                        )
                    }
                }
            })

            // setup sub-form builder and description behaviour
            .add(function(){
                $([form.elements.watcherSelect, form.elements.schemeSelect]).each(function(index, elem) {
                    var handler = function(){
                        if(parseInt($(this).val()) == 0) {
                            $(this).siblings('div.description').html('');
                        }else{
                            $(this).siblings('div.description').text($(this).find('option:selected').attr('title'));
                        }

                        if(parseInt($(this).find('option:selected').attr('data-has-sub-form')) == 1) {
                            form.buildSubForm($(this).attr('data-sub-form-trigger'), $(this).val());
                        }else{
                            form.resetSubForm($(this).attr('data-sub-form-trigger'));
                        }
                    };

                    $(elem).keyup(handler).change(handler).change();
                });
            })

            // setup checkbox
            .add(function(){
                form.elements.enabledCheckbox.change(function(){
                    form.elements.enabledHidden.val($(this).is(':checked') ? '1' : '0');
                }).change();
            })

            // decimalInput
            .add(function(){
                form.form.utils.decimalInput(form.form.domObject.find('input[data-float-input="1"]'));
            })

            // validation setup
            .add(function(){
                var elements = form.elements;

                form.form.validation.validateAll = true;
                form.form.validation.append(function(){
                    var title = elements.titleInput.val();

                    if(title.length == 0) {
                        return "Отсутствует название";
                    }

                    return true;
                });

                form.form.validation.append(function(){
                    var sumInput = $('#createFormSum');

                    if(sumInput.size()) {
                        var sum = parseFloat(sumInput.val());

                        if(isNaN(sum) || sum < 0.01) {
                            return "Не указана сумма штрафа";
                        }
                    }

                    return true;
                });
            })
        ;

        return deferred;
    }

    /**
     * Заполнение формы полученным AJAX-запросом данными
     * @param deferred
     * @param dData
     * @returns {any}
     */
    deferredFillFormData(deferred, dData) {
        var form = this;

        deferred
            .add(function() {
                if(dData.form.id) {
                    form.form.domObject.find('h1.window-title').text('Редактирование: #'+dData.form.id.toString());
                    form.form.domObject.find('input[type="submit"]').val('Сохранить');
                }else{
                    form.form.domObject.find('h1.window-title').text('Создание системы автоматического штрафования');
                    form.form.domObject.find('input[type="submit"]').val('Создать');
                }
            })

            .add([
                {name: 'id', value: dData.form.id},
                {name: 'name', value: dData.form.name},
                {name: 'watcher', value: dData.form.watcher},
                {name: 'scheme', value: dData.form.scheme},
                {name: 'period', value: dData.form.period},
                {name: 'enabled', value: dData.form.enabled}
            ])
            .add(function(){
                if(dData.form.enabled) {
                    form.elements.enabledCheckbox.attr('checked', 1);
                }else{
                    form.elements.enabledCheckbox.removeAttr('checked');
                }
            })
            .add(function(){
                form.elements.watcherSelect.change()
                form.elements.schemeSelect.change()
            })
            .add(function(){
                if(dData.form.options) {
                    for(var n in dData.form.options) {
                        console.log(n);
                        form.form.getDomObject().find('[name="options['+n+']"]').val(dData.form.options[n]);
                    }
                }
            })
            .add({name: 'sum', value: dData.form.sum})
        ;

        return deferred;
    }

    /**
     * Создает под-форму указанного типа (scheme, watcher)
     * @param type
     * @param template
     * @param noError
     */
    buildSubForm(type:string, template:string, noError = false) {
        var container = this.getSubForm(type).container;

        switch(type) {
            default:
                throw new Error('Unknown type');

            case 'watcher':
                var script = $('script[data-script-type="create-penalty-watcher-subform-'+template+'"]');
                break;

            case 'scheme':
                var script = $('script[data-script-type="create-penalty-scheme-subform-'+template+'"]');
                break;
        }

        if(!noError && script.size() == 0) {
            throw new Error('No template found for: ' + template);
        }

        this.resetSubForm(type);

        if(script.size() > 0) {
            container.append(script.html()).show();
        }
    }

    /**
     * Уничтожает саб-форму указанного типа (scheme, watcher)
     * @param type
     */
    resetSubForm(type:string) {
        this.getSubForm(type).container.html('').hide();
    }

    /**
     * Возвращает саб-форму указанного типа (scheme, watcher)
     * @param type
     * @returns {{container: JQuery, type: string}}
     */
    getSubForm(type:string):{container:JQuery; type:string} {
        switch(type) {
            default:
                throw new Error('Unknown type');

            case 'watcher':
                var container = $('#createFormWatcherSubForm');
                break;

            case 'scheme':
                var container = $('#createFormSchemeSubForm');
                break;
        }

        return {container: container, type:type};
    }
}

class Default_Penalty_Form_Elements
{
    form: Default_Penalty_Form;
    createButton: JQuery;
    editButton: JQuery;
    watcherSelect: JQuery;
    schemeSelect: JQuery;
    titleInput: JQuery;
    reasonTextarea: JQuery;
    enabledCheckbox: JQuery;
    enabledHidden: JQuery;

    constructor(form: Default_Penalty_Form) {
        this.form = form;
        this.createButton = $('input[data-filter-action="create"]');
        this.editButton = $('input[data-filter-action="edit"]');
        this.watcherSelect = $('#createFormWatcherSelect');
        this.schemeSelect = $('#createFormSchemeSelect');
        this.titleInput = $('#createFormTitle');
        this.reasonTextarea = $('#createFormReason');
        this.enabledCheckbox = $('#createFormEnabledCheckbox');
        this.enabledHidden = $('#createFormEnabledHidden');
    }
}

