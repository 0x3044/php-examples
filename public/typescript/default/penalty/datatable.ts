/// <reference path="./../../jquery.d.ts"/>
/// <reference path="./../../datatable/datatable.ts"/>
/// <reference path="./../../form/form.ts"/>

class Default_Penalty_FilterController
{
    dataTable: Default_Penalty_DataTable;
    elements: {
        createButton: JQuery;
        editButton: JQuery;
        removeButton: JQuery;
    };

    /**
     * Page Controller
     */
    constructor() {
        var that = this;

        // Элементы
        this.elements = {
            createButton: $('input[data-filter-action="create"]'),
            editButton: $('input[data-filter-action="edit"]'),
            removeButton: $('input[data-filter-action="remove"]')
        };

        // Создаем фабрику полей
        var fieldsFactory = new Default_Penalty_DataTable_FieldsFactory();

        // Создаем конфигурацию DataTable
        var dataTableParams:DataTable_Constructor_Params = {
            fields: ["id", "name", "scheme", "watcher", "period", "sum", "reason", "enabled"],
            fieldsFactory: fieldsFactory,
            selectable: {
                enabled: true,
                selectCallback: function(dataTable:DataTable) {
                    $('tr.datatable-record-id-'+that.dataTable.selectedRecordId.toString()).addClass('active');

                    that.elements.editButton.removeAttr('disabled');
                    that.elements.removeButton.removeAttr('disabled');
                },
                unselectCallback: function(dataTable:DataTable) {
                    $('tr.filter-grid-odd.active, tr.filter-grid-even.active').removeClass('active');

                    that.elements.editButton.attr('disabled', '1');
                    that.elements.removeButton.attr('disabled', '1');
                }
            },
            url: {
                resultUrl: "/default/penalty/getfilterresult",
                configUrl: "/default/penalty/getfilterconfig",
                maskUrl: "/default/penalty/getfiltermask"
            },
            gridParams: {
                containerId: "grid-container"
            },
            filterParams: {
                containerId: "filter-container",
                recordsPerPage: 15,
                defaultSortColumn: "id",
                defaultSortType: 'desc'
            }
        };

        // Создаем DataTable
        this.dataTable = new Default_Penalty_DataTable(dataTableParams);
    }

    /**
     * Бинд действия для кнопок "создать", "редактировать", "удалить"
     * @param form
     */
    setupButtons(form:Default_Penalty_Form)
    {
        var filter = this.dataTable;

        // Кнопка "создать"
        this.elements.createButton.click(function(){
            form.recordId = null;
            form.form.show();
        });

        // Кнопка "редактировать"
        this.elements.editButton.click(function(){
            form.recordId = filter.selectedRecordId;
            form.form.show();
        });

        // Кнопка "удалить"
        this.elements.removeButton.click(function(){
            var recordId = filter.selectedRecordId;

            if(confirm('Вы действительно хотите удалить стратегию автоматического штрафования?')) {
                $().ajaxHandler({
                    'domObject': $('body'),
                    'ajaxParams': {
                        'url': '/default/penalty/remove',
                        'type': 'POST',
                        'dataType': 'json',
                        'data': {
                            'id': recordId
                        }
                    },
                    'done': function(data) {
                        filter.reset();
                    }
                });
            }
        });
    }
}

class Default_Penalty_DataTable extends DataTable
{
}

class Default_Penalty_DataTable_FieldsFactory extends DataTable_FieldFactory
{
    getId():FinanceSupport_Fields_Element {
        return {
            caption: "#",
            align: "center",
            width: 60,
            sort: true,
            aliace: function (v, cnt, rowData) {
                var elem = $("<div><input type=\"hidden\" data-attr=\"record-id\" /><div></div></div>");

                elem.find('input').val(rowData['id'].toString());
                elem.find('div').text(rowData['id'].toString());

                $(cnt).parent().parent().addClass('datatable-record-id-'+rowData['id'].toString());

                return elem;
            }
        };
    }

    getName():DataTable_FieldFactory_Element {
        return {
            caption: "Название",
            align: "left",
            width: 180,
            sort: true
        }
    }

    getWatcher():DataTable_FieldFactory_Element {
        return {
            caption: "Тип штрафования",
            align: "center",
            width: 200,
            sort: true
        }
    }

    getScheme():DataTable_FieldFactory_Element {
        return {
            caption: "Схема штрафования",
            align: "center",
            width: 180,
            sort: true
        }
    }

    getSum():DataTable_FieldFactory_Element {
        return {
            caption: "Сумма",
            align: "center",
            width: 100,
            sort: true
        }
    }

    getReason():DataTable_FieldFactory_Element {
        return {
            caption: "Причина",
            align: "center",
            width: 250,
            sort: true,
            aliace: function(v, cnt, rowData) {
                if(v && v.length > 0) {
                    return v;
                }else{
                    return '—';
                }
            }
        }
    }

    getPeriod():DataTable_FieldFactory_Element {
        return {
            caption: "Период",
            align: "center",
            width: 70,
            sort: true
        }
    }

    getEnabled():DataTable_FieldFactory_Element {
        return {
            caption: "Включен?",
            align: "center",
            width: 80,
            sort: true,
            aliace: function(v, cnt, rowData) {
                if(parseInt(v)) {
                    return 'да'
                }else{
                    return 'нет';
                }
            }
        }
    }
}