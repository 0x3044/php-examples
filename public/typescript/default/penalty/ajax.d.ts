/// <reference path="./../../jquery.d.ts"/>

interface Default_Penalty_Form_AjaxResponse_GetForm
{
    form: {
        id: number;
        name: string;
        watcher: string;
        scheme: string;
        period: number;
        sum: number;
        enabled: boolean;
        options: {
            period?: number;
            multiplier?: number;
        }

    };
    repository: {
        schemes: {
            name: string;
            className: string;
            title: string;
            description: string;
            subForm: boolean;
            visible: boolean;
        }[];
        watchers: {
            name: string;
            className: string;
            title: string;
            description: string;
            subForm: boolean;
            visible: boolean;
        }[];
    };
}