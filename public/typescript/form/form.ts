/// <reference path="./../jquery.d.ts"/>

interface AjaxFormOptions
{
    /**
     * Длина формы
     */
    'width'?: any;

    /**
     * Включить/отключить дефолтный AJAX-сабмит
     */
    'disableAjaxHandler'?: boolean;

    /**
     * Флаг "уничтожать форму"
     * При true любая операция скрытия формы уничтожает, а не скрывает ее.
     */
    'destroyOnHide'?: boolean;

    /**
     * Флаг "скрывать форму при нажатии ESC"
     */
    'hideOnEscape'?: boolean;

    /**
     * Флаг "не назначать дефолтные css-классы форме"
     */
    'noDefaultCssClasses'?: boolean;

    /**
     * Стиль формы, два значения: normal (с загруглениями) и string(строгий, стандартный)
     */
    'style'?: string;

    /**
     * Коллбэк, вызываемый перед созданием формы
     */
    'preBuildCallback'?: (form:AjaxForm, builder:AjaxFormBuilder) => any;

    /**
     * Коллбэк, вызываемый после создания формы
     */
    'postBuildCallback'?: (form:AjaxForm, builder:AjaxFormBuilder) => any;

    /**
     * Использовать FormData для отправки запросов
     */
    'useFormData'?: boolean;

    /**
     * Цвет подложки
     * Значения:
     *  - css-совместимые значения(#ccc, magenta, ...)
     *  - Специальные значения:
     *      form-light - дефолтный, белый цвет
     *      form-medium - серый цвет
     *      form-dark - черный цвет
     *
     *
     */
    'bgColor'?: string;
}

/**
 * Всплывающая AJAX-форма
 *
 * Возможности:
 *
 *  - Добавьте кнопкам аттрибут data-form-button="{action}" для автоматической привязки определенных действий:
 *      - hide
 *      - destroy
 *      - show
 *      - submit
 */
class AjaxForm
{
    domObject:JQuery;
    options: AjaxFormOptions;
    submitBehaviour: (form:AjaxForm) => any;
    completeCallback: (form:AjaxForm) => any;
    doneCallback: (data:any, form:AjaxForm) => any;
    failCallback: (form:AjaxForm) => any;
    errorCallback: (data:any, form:AjaxForm) => any;
    showCallback: (form:AjaxForm) => any;
    hideCallback: (form:AjaxForm) => any;
    response: any;
    validation: AjaxFormValidation;
    utils: AjaxForm_Utils;

    /**
     * Создает AJAX-форму. Если в качестве параметра domObject передана строка, то она будет использована
     * как HTML-шаблон
     * @param domObject
     * @param options
     */
    constructor(domObject:any, options:AjaxFormOptions = {}) {
        // Конвертация HTML-шаблона в DOM-объект
        if(typeof domObject == "string") {
            domObject = $(domObject);
        }

        // Мердж дефолтной конфигурации с переданной
        this.domObject = domObject;
        this.validation = new AjaxFormValidation(this);
        this.utils = new AjaxForm_Utils(this);
        this.options = $.extend({
            'width': 'auto',
            'useFormData': false,
            'disableAjaxHandler': false,
            'style': 'normal',
            'destroyOnHide': false,
            'hideOnEscape': false,
            'noDefaultCssClasses': false,
            'bgColor': 'form-light',
            'preBuildCallback': function() { },
            'postBuildCallback': function() { }
        }, options);

        // Инициализация колбэков
        this.showCallback = function() { };
        this.hideCallback = function() { };

        // Передать создание формы билдеру
        var builder = new AjaxFormBuilder(this);
        builder.build();
    }

    /**
     * Возвращает DOM-объект формы (section)
     * @returns JQuery
     */
    getDomObject() {
        return this.domObject;
    }

    /**
     * Возвращает DOM-объект формы(section>form)
     * @returns JQuery
     */
    getFormDomObject() {
        return this.domObject.find('form[data-form-attr="1"]').first();
    }

    /**
     * Показывает пользователю форму
     */
    show() {
        this.getDomObject().show().transition("show");
    }

    /**
     * Скрывает от пользователя форму
     */
    hide() {
        if(this.options.destroyOnHide) {
            this.destroy();
        }else{
            this.getDomObject().transition("hide");
        }
    }

    /**
     * Установить колбэк на показ формы
     * @param callback
     */
    setShowCallback(callback:(form:AjaxForm) => any) {
        this.showCallback = callback;
    }

    /**
     * Установить колбэк на скрытие формы
     * @param callback
     */
    setHideCallback(callback:(form:AjaxForm) => any) {
        this.hideCallback = callback;
    }

    /**
     * Уничтожает форму и удаляет DOM-объект
     */
    destroy() {
        this.getDomObject().transition("destroy");
        this.getDomObject().remove();
    }

    /**
     * Выполнить AJAX-запрос с индикацией в форме
     * @param ajaxParams
     * @param options
     */
    ajax(ajaxParams:any, options:any) {
        options.ajaxParams = ajaxParams;

        if(!options.domObject) {
            options.domObject = this.getDomObject();
        }

        $().ajaxHandler(options);
    }

    /**
     * Установить текущий статус выполнения ajax-запроса (true/false)
     * @param status
     */
    ajaxStatus(status:boolean) {
        this.getFormDomObject().ajaxStatus(status);
    }

    /**
     * Отправить форму
     * @param callback
     */
    submit(callback?: (form:AjaxForm) => any):any {
        if(callback) {
            this.submitBehaviour = callback;
        }else{
            if(typeof this.submitBehaviour != 'function') {
                throw new Error('No submit behaviour available');
            }

            return this.submitBehaviour(this);
        }
    }

    /**
     * Установить либо вызвать колбэк на завершение AJAX-запроса
     * @param callback
     */
    complete(callback?: () => any) {
        if(callback) {
            this.completeCallback = callback;
        }else{
            if(typeof this.completeCallback == "function") {
                this.completeCallback(this);
            }
        }
    }

    /**
     * Установить либо вызвать колбэк на успешное завершение AJAX-запроса
     * @param callback
     */
    done(callback?: (data:any, form:AjaxForm) => any) {
        if(callback) {
            this.doneCallback = callback;
        }else{
            if(typeof this.doneCallback == "function") {
                this.doneCallback(this.response, this);
            }
        }
    }

    /**
     * Установить либо вызвать колбэк на успешное завершение AJAX-запроса,  которое, тем не менее закончилось ошибкой
     * @param callback
     */
    error(callback?: (data:any, form:AjaxForm) => any) {
        if(callback) {
            this.errorCallback = callback;
        }else{
            if(typeof this.errorCallback == "function") {
                this.errorCallback(this.response, this);
            }
        }
    }

    /**
     * Установить либо вызвать колбэк на неуспешное завершение AJAX-запроса
     * @param callback
     */
    fail(callback?: () => any) {
        if(callback) {
            this.failCallback = callback;
        }else{
            if(typeof this.failCallback == "function") {
                this.failCallback(this);
            }
        }
    }

    /**
     * Выполняет AJAX-отправку формы
     */
    sendAJAX() {
        if(this.options.useFormData) {
            this._sendAJAX_FormData();
        }else{
            this._sendAJAX_Simple();
        }
    }

    /**
     * Выполняет обычную AJAX-отправку формы
     * @private
     */
    _sendAJAX_Simple()
    {
        var self = this;
        var ajaxParams = {
            'url': this.getFormDomObject().attr('action'),
            'type': this.getFormDomObject().attr('method'),
            'dataType': 'json',
            'data': self.getFormDomObject().serialize()
        };

        this.ajax(ajaxParams, {
            'complete': function() {
                self.complete();
            },
            'done': function(data) {
                self.response = data;
                self.done();
            },
            'error': function(data) {
                self.response = data;
                self.error();
            },
            'fail': function() {
                self.fail();
            }
        });
    }

    /**
     * Выполняет AJAX-отправку формы с помощью FormData
     * @private
     */
    _sendAJAX_FormData()
    {
        var self = this;
        var fd = new FormData(<HTMLFormElement>this.getFormDomObject()[0]);
        var ajaxParams = {
            'url': this.getFormDomObject().attr('action'),
            'type': this.getFormDomObject().attr('method'),
            'processData': false,
            'contentType': false,
            'dataType': 'json',
            'data': fd
        };

        this.ajax(ajaxParams, {
            'complete': function() {
                self.complete();
            },
            'done': function(data) {
                self.response = data;
                self.done();
            },
            'error': function(data) {
                self.response = data;
                self.error();
            },
            'fail': function() {
                self.fail();
            }
        });
    }

    /**
     * Валидация формы
     */
    validate() {
        var validationResult = this.validation.validate();

        if(validationResult !== true) {
            for(var n in this.validation.validateTrace) {
                if(typeof this.validation.validateTrace[n] == 'string') {
                    alert(this.validation.validateTrace[n]);
                }
            }
        }

        return validationResult;
    }

    /**
     * Включает дефолтный обработчик формы
     */
    enableDefaultSubmitCallback() {
        var self = this;

        this.done(function(data:any, form:AjaxForm){
            self.hide();
        });

        this.submitBehaviour = function() {
            if(self.validate()) {
                self.sendAJAX();
            }

            return false;
        }
    }

    /**
     * Отключает дефолтный обработчик формы
     */
    disableDefaultSubmitCallback() {
        this.submitBehaviour = function() {
            return true;
        }
    }
}

/**
 * Билдер AJAX-формы
 * Класс отвечает за создание формы: прикрепление событий, загрузку css/js-скриптов, добавление css-классов etc
 */
class AjaxFormBuilder
{
    /**
     * Переданныя конструктору форма
     */
    form:AjaxForm;

    constructor(form:AjaxForm) {
        this.form = form;
    }

    build() {
        this.form.options.preBuildCallback(this.form, this);

        AjaxFormBuilder._setupCssStyle();
        AjaxFormBuilder._setupJsFiles();

        this._setupDomObject();
        this._setupTransition();
        this._setupButtons();
        this._setupSubmitBehaviour();
        this._setupEscBehaviour();

        this.form.options.postBuildCallback(this.form, this);
    }

    /**
     * Загрузка CSS-файлов
     * @private
     */
    static _setupCssStyle() {
        var cssUrl = window['linkPrefix'] + '/css/sass/form/form-abstract.css';

        if($('link[href*="'+cssUrl+'"]').size() == 0) {
            $('head').append('<link type="text/css" rel="stylesheet" href="'+cssUrl+'"/>');
        }
    }

    /**
     * Загрузка JS-файлов
     * @private
     */
    static _setupJsFiles() {
        var jsFiles = [
            window['linkPrefix'] + '/js/jquery/jquery.ajaxStatus.js',
            window['linkPrefix'] + '/js/jquery/jquery.ajaxHandler.js',
            window['linkPrefix'] + '/js/jquery/jquery.transition.js',
        ];

        for(var n in jsFiles) {
            var requireJs = jsFiles[n];

            if($('script[src*="'+requireJs+'"]').size() == 0) {
                $('head').append('<script src="'+requireJs+'"></script>');
            }
        }
    }

    /**
     * Добавление CSS-классов и донастройка формы
     * @private
     */
    _setupDomObject() {
        if(!(this.form.options.noDefaultCssClasses)) {
            this.form.getDomObject()
                .hide()
                .addClass('ajax-forms')
                .addClass('ajax-forms-' + this.form.options.style)
                .find('form').first()
                    .attr('data-form-attr', '1')
                    .addClass('form-abstract')
            ;
        }
    }

    /**
     * Настройка jquery.transition.js
     * @private
     */
    _setupTransition() {
        var form = this.form;
        var width = this.form.options.width;
        var domObject = this.form.getDomObject();

        var specialBgColors = {
            'form-light': 'white',
            'form-medium': 'gray',
            'form-dark': 'black'
        };

        if(specialBgColors[form.options.bgColor]) {
            var bgColor = specialBgColors[form.options.bgColor];

            domObject.addClass('form-transition-'+bgColor);
        }else{
            var bgColor:any = form.options.bgColor;
        }

        domObject.transition({
            "backgroundColor": bgColor,
            "width": typeof width == "number" ? width : 450,
            'showEvent': function() {
                if(typeof form.showCallback == 'function') {
                    form.showCallback(form);
                }
            },
            'hideEvent': function() {
                if(typeof form.hideCallback == 'function') {
                    form.hideCallback(form);
                }
            }
        });
    }

    /**
     * Прикрепление событий к кнопкам с аттрибутом data-form-button
     * @private
     */
    _setupButtons() {
        var form = this.form;

        form.getFormDomObject().children('footer').first().find('input[type="button"],input[type="submit"]').addClass('brownbutton');

        form.getDomObject().find('input[data-form-button="hide"]').click(function(){
            form.hide();
        });

        form.getDomObject().find('input[data-form-button="show"]').click(function(){
            form.show();
        });

        form.getDomObject().find('input[data-form-button="destroy"]').click(function(){
            form.destroy();
        });

        form.getDomObject().find('input[data-form-button="submit"]').click(function(){
            form.submit();
        });
    }

    /**
     * Настройка submit'а формы
     * @private
     */
    _setupSubmitBehaviour() {
        var self = this;

        this.form.getFormDomObject().submit(function(){
            return self.form.submit();
        });

        if(!(this.form.options.disableAjaxHandler)) {
            this.form.enableDefaultSubmitCallback();
        }
    }

    /**
     * Настройка скрытия формы по нажатию ESC
     * @private
     */
    _setupEscBehaviour()
    {
        var form = this.form;

        if(this.form.options.hideOnEscape) {
            $(document).keyup(function(e){
                if(e.keyCode === 27) {
                    form.hide();
                }
            });
        }
    }
}

/**
 * Валидация формы
 */
class AjaxFormValidation
{
    form: AjaxForm;

    validators = [];
    validateAll = false;
    validateTrace = [];

    constructor(form: AjaxForm) {
        this.form = form;
    }

    /**
     * Добавляет валидатор в коллекцию
     * @param validator
     */
    append(validator:() => any) {
        this.validators.push(validator);
    }

    /**
     * Удаляет валидатор из коллекции
     * @param validator
     */
    remove(validator:() => any) {
        for(var n in this.validators) {
            if(this.validators[n] == validator) {
                this.validators[n] = null;
            }
        }
    }

    /**
     * Производит валидацию
     * Все валидаторы должны вернуть true. Любой другой ответ считается за ошибку и возвращается
     * @returns {*}
     */
    validate():any {
        var validationResult = true;
        this.validateTrace = [];

        for(var n in this.validators) {
            var validator = this.validators[n];

            if(typeof validator == "function") {
                var result = validator(this.form);

                this.validateTrace.push(result);

                if(typeof result != "boolean" || result == false) {
                    if(this.validateAll) {
                        validationResult = false;
                    }else{
                        return false;
                    }
                }
            }
        }

        return validationResult;
    }
}

class AjaxForm_Utils
{
    form:AjaxForm;

    constructor(form: AjaxForm) {
        this.form = form;
    }

    chain() {
        return new (function(form:JQuery) {
            this.queue = [];
            this.form = form;

            this.add = function(action) {
                this.queue.push(action);

                return this;
            };

            this.reset = function() {
                this.queue = [];
            };

            this.run = function() {
                for(var n in this.queue) {
                    var action = this.queue[n];

                    if(typeof action == "function") {
                        action();
                    }else if(action instanceof Array) {
                        for(var k in action) {
                            var sAction = action[k];
                            this.form.find('[name="'+sAction.name+'"]').val(sAction.value);
                        }
                    }else if(typeof action == "object") {
                        this.form.find('[name="'+action.name+'"]').val(action.value);
                    }
                }
            };

            return this;
        })(this.form.getDomObject());
    }

    decimalInput(inputDomObject) {
        inputDomObject.bind('keyup', function(){
            console.log('type');
            var value = $(this).val();

            if(value != null && value.match(/,/)) {
                $(this).val(value.replace(/,/, '.'));
            }
        });
    }
}