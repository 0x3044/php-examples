<?php
use App_Filter_Controller_AbstractFilterController as AbstractFilterController;
use Default_Model_Filter_PenaltyFilter as Filter;

class PenaltyController extends AbstractFilterController
{
    public function indexAction()
    {
        Zend_Registry::set('statusString', 'Автоматические штрафы');

        $this->_getHeadScript()->appendJsFiles(array(
            '/js/default/penalty/datatable.js',
            '/js/default/penalty/form.js',
            '/js/form/form.js'
        ));

        $this->_getHeadLink()->appendCssFiles(array(
            '/css/sass/default/penalty/index.css'
        ));
    }

    /**
     * Возвращает данные для формы
     * @throws Exception
     */
    public function getFormAction()
    {
        $id = $this->_getParam('id');
        $id = $id > 0 ? $id : null;

        $ajaxForm = App_Form_AjaxForm_Factory::create(function() use ($id) {
            $service = App_Penalty_Service_Factory::getDbInstance();
            $repository = new App_Penalty_Form_Repository($service->getOptions());

            $form = new App_Penalty_Form($id);

            if($id) {
                $form->loadFromDb($id, $service);
            }

            return array(
                'form' => $form->toArray(),
                'repository' => array(
                    'schemes' => $repository->getSchemes(),
                    'watchers' => $repository->getWatchers()
                )
            );
        });

        $ajaxForm->process();
        $ajaxForm->sendJSONResponse();
    }

    /**
     * Сохраняет данные формы
     * @throws Exception
     */
    public function saveFormAction()
    {
        $id = $this->_getParam('id');
        $id = $id > 0 ? $id : null;

        $ajaxForm = App_Form_AjaxForm_Factory::create(function() use($id) {
            $service = App_Penalty_Service_Factory::getDbInstance();

            $form = new App_Penalty_Form($id);
            $form->loadFromPost($_POST);
            $form->save($service);
        });

        $ajaxForm->process();
        $ajaxForm->sendJSONResponse();
    }

    /**
     * Удаление системы автоматической регистрации
     * @throws Exception
     */
    public function removeAction()
    {
        $id = $this->_getParam('id');

        $ajaxForm = App_Form_AjaxForm_Factory::create(function() use ($id) {
            $service = App_Penalty_Service_Factory::getDbInstance();

            $penaltySystem = $service->getSystemFactory()->createSystemFromDb($id);
            $penaltySystem->unregister();

            return true;
        });

        $ajaxForm->process();
        $ajaxForm->sendJSONResponse();
    }

    /**
     * Возвращает фильтр контроллера
     * В этом методе также данный фильтр можно конфигурировать
     * @return \Default_Model_Filter_PenaltyFilter
     */
    protected function _createFilter()
    {
        return new Filter();
    }
}