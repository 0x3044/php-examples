<?php
class Finance_DebugController extends Zend_Controller_Action
{
    public function dispatcherAclCheckAction()
    {
        $allFields = array('car', 'carnumber', 'rentprice', 'transportdeliverytime');
        $pairFields = array('transportdeliverytime');

        $this->view->assign('config', array(
            'in' => array(
                'statuses' => array(0, 1, 2, 3, 6),
                'fields' => $allFields
            ),
            'inout' => array(
                'sub' => array(
                    'out' => array(
                        'statuses' => array(0, 9, 12, 13),
                        'fields' => $pairFields
                    ),
                    'in' => array(
                        'statuses' => array(0, 1, 2, 6),
                        'fields' => $allFields
                    )
                )
            ),
            'returnout' => array(
                'statuses' => array(0, 30, 31, 32, 33),
                'fields' => $allFields
            ),
            'supply' => array(
                'statuses' => array(0, 14, 15, 16, 17),
                'fields' => $allFields
            ),
            'insupply' => array(
                'sub' => array(
                    'in' => array(
                        'statuses' => array(0, 1, 2, 6),
                        'fields' => $allFields
                    ),
                    'supply' => array(
                        'statuses' => array(0, 14, 23, 17),
                        'fields' => $pairFields
                    )
                )
            ),
            'income' => array(
                'statuses' => array(0, 19, 20, 21, 22),
                'fields' => $allFields
            ),
            'returnsupply' => array(
                'statuses' => array(0, 24, 25, 26, 27),
                'fields' => $allFields
            ),
            'incomeout' => array(
                'sub' => array(
                    'income' => array(
                        'statuses' => array(0, 19, 22, 34, 35),
                        'fields' => $allFields
                    ),
                    'out' => array(
                        'statuses' => array(0, 9, 36, 37, 38),
                        'fields' => $pairFields
                    )
                )
            ),
            'repairout' => array(
                'statuses' => array(0, 39, 40, 41, 42),
                'fields' => $allFields
            ),
            'spares' => array(
                'statuses' => array(0, 43, 44, 45, 46),
                'fields' => $allFields
            )
        ));
    }
}