<?php
require_once realpath(__DIR__).'/AbstractController.php';
require_once APPLICATION_PATH.'/modules/claim/controllers/InoutController.php';

use App_Finance_Ticket_Component_Exception_AccessForbiddenException as AclException;
use App_Finance_Ticket_Logger_Logger as FinanceLogger;
use App_Client_CreditLimit as ClientCreditLimit;

class Finance_Ticket_ClaimController extends Finance_Ticket_AbstractController
{
    /**
     * Операции изменения данных в заявке/запросе для диспетчерского саппорта
     * /dispatcher?edit=[dispatcher|rent_process|date_to_process]
     */
    public function dispatcherAction()
    {
        $this->_validateDispatcherAccess();

        $jsonData = array("success" => true);

        try {
            $ticket = $this->getTicket();
            $claimId = $ticket->getClaimId();
            $editField = $this->_getParam("edit");
            $saveClaimChanges = false;

            $claimHelper = Finance_Model_Claim_Default::factory($claimId);

            if($this->_getParam('useConnectivityId')) {
                $claimHelper->enableUseConnectivityId();
            }

            switch($editField) {
                default:
                    throw new \Exception("Unknown field `{$editField}`");
                    break;

                case "dispatcher":
                    if(!($ticket->getAcl()->getDispatcherModule()->hasAccessClaimCarNumber())) {
                        throw new AclException("Вы не имеете прав на смену номера машины в заявке");
                    }

                    $dispatcherId = $this->_getParam('dispatcher_id');
                    $carNumber = $this->_getParam('car_number');

                    if($dispatcherId !== null && $carNumber === null) {
                        $claimData = App_Claim_Factory::getInstance()->getClaimData($claimId);

                        if($claimData->car_number) {
                            throw new \Exception("Невозможно сменить диспетчера у заявки, если по ней уже назначена машина.");
                        }
                    }

                    if($dispatcherId !== null) {
                        $claimHelper->setupDispatcherId($dispatcherId);
                    }

                    if($carNumber !== null) {
                        $claimHelper->setupCarNumber($carNumber);
                    }

                    $saveClaimChanges = true;

                    break;

                case "rent_price":
                    if(!($ticket->getAcl()->getDispatcherModule()->hasAccessClaimRentPrice())) {
                        throw new AclException("Вы не имеете прав на смену суммы найма в заявке");
                    }

                    if($rent_price = bcadd(0, (float) $this->_getParam('rent_price'), 2)) {
                        $claimHelper->setupRentPrice($rent_price);
                    }

                    $saveClaimChanges = true;

                    break;

                case "transport_delivery_time":
                    if(!($ticket->getAcl()->getDispatcherModule()->hasAccessClaimTransportDeliveryTime())) {
                        throw new AclException("Вы не имеете прав на смену время подачи в заявке");
                    }

                    if($transportDeliveryTime = $this->_getParam('transport_delivery_time')) {
                        $claimHelper->setupTransportDeliveryTime($transportDeliveryTime);
                    }

                    $saveClaimChanges = true;

                    break;

                case "date_to_process":
                    if(!($ticket->getAcl()->getDispatcherModule()->hasAccessClaimDateToProcess())) {
                        throw new AclException("Вы не имеете прав на смену даты подачи транспорта в запросе");
                    }

                    $logger = new FinanceLogger($ticket);
                    $logger->oldSnapshot();

                    $dateToProcess = $this->_getParam("dateToProcess");
                    $ticket->setDateToProcessFromMixed($dateToProcess);
                    $ticket->save();

                    $logger->newSnapshot();
                    $logger->writeLog();

                    break;
            }

            if($saveClaimChanges) {
                $claimHelper->save();
            }
        }
        catch(\Exception $e) {
            $jsonData = array(
                "success" => false,
                "error" => $e->getMessage(),
                "trace" => $e->getTrace()
            );
        }

        header('Content-Type: application/json');
        die(json_encode($jsonData));
    }

    /**
     * Редирект на страницу формирования акта сверки
     */
    public function reconciliationAction()
    {
        $this->_validateAccountingAccess();

        $view = Zend_Layout::getMvcInstance()->getView();
        $startDate = $this->_getParam('startDate');
        $endDate = $this->_getParam('endDate');
        $clientId = $this->_getParam('clientId');
        $showAll = $this->_getParam('showAll');

        if($showAll) {
            $url = "/analitics/balance/verify/client/{$clientId}/ap/2";
        } else {
            if(!(strlen($startDate) && strlen($endDate) && is_numeric($clientId) && $clientId > 0)) {
                throw new \Exception("Not enough parameters");
            }

            $startDate = new Zend_Date($startDate);
            $endDate = new Zend_Date($endDate);

            $url = $view->linkPrefix()."/analitics/balance/verify/datefrom/{$startDate->toString(App_Db::ZEND_DATE_RU_FORMAT)}/dateto/{$endDate->toString(App_Db::ZEND_DATE_RU_FORMAT)}/client/{$clientId}";
        }

        $this->_redirect($url);
    }

    /**
     * Возвращает информацию о кредитном лимите
     */
    public function creditAction()
    {
        $this->_validateAccountingAccess();

        try {
            $clientId = $this->_getParam('clientId');
            $claimId = $this->_getParam('claimId');

            $clientCreditLimit = new ClientCreditLimit($clientId, $claimId);
            $clientCreditLimitData = $clientCreditLimit->getExceedExtended();

            /** @var $clientDbTable App_Db_Clients */
            $clientDbTable = App_Db::get(DB_CLIENTS);
            if(!($clientData = $clientDbTable->find($clientCreditLimitData->getClientId()))) {
                throw new \Exception('Клиент не найден');
            }

            $jsonData = array(
                'success' => true,
                'invoicesSum' => (float) $clientCreditLimitData->getInvoicesSum(),
                'isExceeded' => $clientCreditLimitData->getIsExceeded(),
                'exceedValue' => (int) $clientCreditLimitData->getExceedValue(),
                'clientBalance' => (float) $clientCreditLimitData->getClientBalance(),
                'claimId' => $clientCreditLimitData->getClaimId(),
                'clientName' => $clientData->current()->offsetGet('u_title'),
                'clientCreditLimit' => (float) $clientCreditLimitData->getCreditLimit(),
                'clientHasDeferment' => $clientCreditLimitData->getClientHasDeferment(),
                'clientHasDefermentExpiration' => $clientCreditLimitData->getClientHasDefermentExpiration(),
                'trace' => $clientCreditLimitData->getTrace(),
                'explain' => $clientCreditLimitData->getStateExplain()
            );
        }
        catch(\Exception $e) {
            $jsonData = array(
                'success' => false,
                'error' => $e->getMessage()
            );
        }

        header('Content-Type: application/json');
        die(json_encode($jsonData));
    }

    /**
     * Пересчет кредитного лимита и сохранения значения в заявке
     */
    public function recalculateAction()
    {
        $this->_validateAccountingAccess();

        try {
            /** @var $claimTable App_Db_Claims */
            $claimTable = App_Db::get(DB_CLAIMS);

            $claimId = $this->_getParam('claimId');
            $clientId = $claimTable->getClaimClient($claimId);

            $clientCreditLimit = new ClientCreditLimit($clientId, $claimId);
            $clientCreditLimit->update();

            $jsonData = array(
                'success' => true,
                'credit_limit'
            );
        }catch(\Exception $e){
            $jsonData = array(
                'success' => false,
                'error' => $e->getMessage()
            );
        }

        header('Content-Type: application/json');
        die(json_encode($jsonData));
    }

    /**
     * Выбрасывает исключение, если пользователь не имеет доступа к бухгалтерскому саппорту
     * @throws Exception
     */
    protected function _validateAccountingAccess()
    {
        $accountingRecipient = App_Finance_Recipient_Factory::getInstance()->getAccountingRecipient();

        if(!($accountingRecipient->hasAuthorization())) {
            throw new \Exception('Вы не имеете доступа к бухгалтерскому саппорту');
        }
    }

    /**
     * Выбрасывает исключение, если пользователь не имеет доступа к бухгалтерскому саппорту
     * @throws Exception
     */
    protected function _validateDispatcherAccess()
    {
        $dispatcherRecipient = App_Finance_Recipient_Factory::getInstance()->getDispatcherRecipient();

        if(!($dispatcherRecipient->hasAuthorization())) {
            throw new \Exception('Вы не имеете доступа к диспетчерскому саппорту');
        }
    }
}