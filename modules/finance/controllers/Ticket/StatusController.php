<?php
require_once realpath(__DIR__).'/AbstractController.php';

use App_Finance_Ticket_Component_Exception_AccessForbiddenException as AclException;
use App_Finance_Ticket_Component_Status_Type_Default_Status as Status;

class Finance_Ticket_StatusController extends Finance_Ticket_AbstractController
{
    /**
     * При значении не FALSE проверяется текущий статус на указанный
     * @var bool|int|string
     */
    protected $_currentStatusCheck = false;

    /**
     * Включить проверку на указанный статус
     * @param $statusName
     */
    public function enableStatusCheck($statusName)
    {
        $this->_currentStatusCheck = $statusName;
    }

    /**
     * Отключить проверку на текущий статус
     */
    public function disableStatusCheck()
    {
        $this->_currentStatusCheck = false;
    }

    /**
     * Перевод запроса в статус "активный"
     */
    public function activeAction()
    {
        $this->_changeStatus("active");
    }

    /**
     * Перевод запроса в статус "ожидает отмены"
     */
    public function pendingCancelAction()
    {
        $this->_changeStatus("pending-cancel");
    }

    /**
     * Подтверждение запроса
     */
    public function archivedAction()
    {
        $this->_changeStatus("archived");
    }

    /**
     * Отмена запроса
     */
    public function canceledAction()
    {
        $this->_changeStatus("canceled");
    }

    /**
     * Удаление запроса
     */
    public function deletedAction()
    {
        $this->_changeStatus("deleted");
    }

    /**
     * Смена статуса тикета
     * @param $statusName
     */
    protected function _changeStatus($statusName)
    {
        $this->enableIgnoreAclRestrictions();

        try {
            $ticket = $this->getTicket();

            if($ticket->getStatusHandler()->equalsToString($statusName)) {
                throw new \Exception('Запрос уже находится в данном статусе');
            }

            $this->_checkCurrentTicketStatus();

            switch($statusName) {
                default:
                    throw new \Exception("Unknown status `{$statusName}`");

                case "active":
                    if($ticket->getStatusHandler()->isPendingCancel()) {
                        if(!($ticket->getAcl()->hasAccessActivePendingCancel())) {
                            throw new AclException("Вам не разрешено переводить этот запрос в статус `активные`");
                        }

                        $ticket->getStatusHandler()->setActive();
                        break;
                    } else {
                        throw new \Exception("Только ожидающие отмены запросы могут быть переведены в статус `активные`");
                    }

                    break;

                case "archived":
                    if(!($ticket->getAcl()->hasAccessArchive())) {
                        throw new AclException("Вам не разрешено переводить этот запрос в архив");
                    }

                    $ticket->getStatusHandler()->setArchived();
                    break;

                case "canceled":
                    if($ticket->getStatusHandler()->equalsToInt(Status::STATUS_ARCHIVED)) {
                        $access = $ticket->getAcl()->hasAccessCancelArchived();
                    } else {
                        $access = $ticket->getAcl()->hasAccessCancel();
                    }

                    if(!($access)) {
                        throw new AclException("Вам не разрешено отменять этот запрос");
                    }

                    $ticket->getStatusHandler()->setCanceled();
                    break;

                case "pending-cancel":
                    $canPendingCancel = $ticket->getAcl()->hasAccessPendingCancel();
                    $canCancelFromClaim = ($ticket->getAcl()->getClaimModule()->hasAccessCancel($ticket));

                    $hasAccess = $canPendingCancel || $canCancelFromClaim;

                    if(!($hasAccess)) {
                        throw new AclException("Вам не разрешено отменять запросы");
                    }

                    // Причина отмены запроса
                    if($this->_getParam('reason')) {
                        $helper = new App_Finance_Ticket_Component_CancellationReason($ticket);
                        $helper->setUpReason($this->_getParam('reason'));
                    }

                    $ticket->getStatusHandler()->setPendingCancel();
                    break;

                case "deleted":
                    if($ticket->getMainRecipient() != 'payment') {
                        throw new \Exception('Вы не имеете возможность удалить этот запрос');
                    }

                    $hasAccess = $ticket->getAcl()->hasAccessDestroy();

                    if(!($hasAccess)) {
                        throw new AclException("Вам не разрешено удалять запросы");
                    }

                    $ticket->getStatusHandler()->setDeleted();
                    break;
            }

            $ticket->save();

            $jsonData = array(
                "success" => true,
                "status_name" => $statusName
            );
        }
        catch(Exception $e) {
            $jsonData = array(
                "success" => false,
                "error" => $e->getMessage(),
                "trace" => $e->getTrace()
            );

            if(getenv('DEVELOPMENT') || getenv('STAGE') == 'DEVELOPMENT') {
                $jsonData['exception'] = array(
                    'message' => $e->getMessage(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine()
                );
            }
        }

        $this->disableIgnoreAclRestrictions();

        header('Content-Type: application/json');
        echo json_encode($jsonData);
        exit();
    }

    /**
     * Проверка текущего статуса запроса
     * @throws Exception
     */
    protected function _checkCurrentTicketStatus()
    {
        $ticket = $this->getTicket();

        if($currentStatus = $this->_getParam('ticketStatus')) {
            $this->enableStatusCheck($currentStatus);
        }

        if($currentStatus = $this->_currentStatusCheck) {
            if(is_numeric($currentStatus)) {
                $passed = $ticket->getStatusHandler()->equalsToInt((int) $currentStatus);
            } else {
                $passed = $ticket->getStatusHandler()->equalsToString($currentStatus);
            }

            if(!($passed)) {
                if($ticket->getStatusHandler()->equalsToString('canceled')) {
                    throw new \Exception("Операция отменена: запрос, статус которого вы хотите изменить, был отменен другим пользователем или другими действиями.");
                } else {
                    throw new \Exception("Операция отменена: запрос, статус которого вы хотите изменить, был изменен другим пользователем или другими действиями.");
                }
            }
        }
    }
}