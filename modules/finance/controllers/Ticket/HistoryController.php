<?php
use App_Finance_Ticket_TicketInterface as Ticket;
use App_Finance_Ticket_Factory as TicketFactory;
use App_Finance_Ticket_Component_Exception_AccessForbiddenException as AclException;

require_once realpath(__DIR__).'/AbstractController.php';

class Finance_Ticket_HistoryController extends Finance_Ticket_AbstractController
{
    public function getAction()
    {
        $ticket = $this->getTicket();

        $form = App_Form_AjaxForm_Factory::create(function() use ($ticket){
            $tickets = TicketFactory::getInstance()->createSameClaimIdTickets($ticket->getClaimId());
            $jsonData = array("success" => true, "tickets" => array(), 'claimId' => $ticket->getClaimId());

            if(!($ticket->getAcl()->hasAccessHistory())) {
                throw new AclException("Вы не имеете прав на просмотр истории по данному запросу");
            }

            if($tickets->size()) {
                /** @var $dbTicketType App_Db_TicketType */
                $dbTicketType = App_Db::get(DB_TICKET_TYPE);

                /** @var $ticket Ticket */
                foreach($tickets as $ticket) {
                    $dateToProcess = $ticket->getDateToProcess();
                    $dateProcessed = $ticket->getDateProcessed();

                    if($dbTicketType->getAccess($ticket->getTicketTypeName(), "access")) {
                        $jsonData["tickets"][] = array(
                            'id' => (int) $ticket->getId(),
                            'status' => $ticket->getStatusHandler()->toInt(),
                            'statusName' => $ticket->getStatusHandler()->toString(),
                            'dateCreated' => $ticket->getDateCreated()->toString(App_Db::ZEND_DATETIME_RU_FORMAT),
                            'dateCreatedTimestamp' => $ticket->getDateCreated()->toValue(),
                            'dateToProcess' => $dateToProcess instanceof Zend_Date ? $dateToProcess->toString(App_Db::ZEND_DATETIME_RU_FORMAT) : '-',
                            'dateToProcessTimestamp' => $dateToProcess instanceof Zend_Date ? $dateToProcess->toValue() : 0,
                            'dateProcessed' => $dateProcessed instanceof Zend_Date ? $dateProcessed->toString(App_Db::ZEND_DATETIME_RU_FORMAT) : '-',
                            'dateProcessedTimestamp' => $dateProcessed instanceof Zend_Date ? $dateProcessed->toValue() : 0,
                            'ticketTypeId' => (int) $ticket->getTicketTypeId(),
                            'ticketTypeName' => $ticket->getTicketTypeName(),
                            'ticketTypeDescription' => $dbTicketType->getTicketTypeDescription($ticket->getTicketTypeId()),
                            'url' => $ticket->getUrl()
                        );
                    }
                }
            }

            return $jsonData;
        });

        $form->process();
        $form->sendJSONResponse();
    }
}