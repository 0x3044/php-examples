<?php
require_once realpath(__DIR__).'/AbstractController.php';

use App_Finance_Ticket_Component_Attachment_ServiceNote_Document as ServiceNote;

class Finance_Ticket_ServiceNoteController extends Finance_Ticket_AbstractController
{
    public function goAction()
    {
        $this->_redirect(App_View_Helper_LinkPrefix::linkPrefix().'/finance/ticket/service-note/upload?ticketId='.$this->getTicket()->getId());
    }

    /**
     * Загрузка служебной записки
     * @throws Exception.
     */
    public function uploadAction()
    {
        $ticket = $this->getTicket();
        $serviceNote = new ServiceNote($ticket);

        if(!($serviceNote->isUploaded())) {
            throw new \Exception('Служебная записка не загружена для данного запроса');
        }

        $serviceNote->download();
    }
}