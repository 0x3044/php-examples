<?php
require_once realpath(__DIR__).'/AbstractController.php';

use App_Finance_Ticket_Component_Exception_AccessForbiddenException as AclException;

class Finance_Ticket_PrintController extends Finance_Ticket_AbstractController
{
    /**
     * Печать документа
     * @view json
     */
    public function indexAction()
    {
        $ticket = $this->getTicket();

        if(!($ticket->getAcl()->hasAccessPrint())) {
            throw new AclException("Вы не имеете прав на печать данного запроса");
        }

        switch($ticket->getTicketTypeName()) {
            default:
                throw new \Exception("Печать недоступна для запросов данного типа");

            case "dispatcher_verify":
                $this->_printDispatcherVerify();
                break;

            case "depot_verify":
                $this->_printDepotVerify();
                break;
        }
    }

    /**
     * Печать тикетов "подтверждение у диспетчера"
     */
    protected function _printDispatcherVerify()
    {
        $template = "dispatcher/dispatcher_verify.phtml";
        $data = $this->_getDispatcherVerifyViewData();

        $this->view->assign($data);
        $this->view->assign(array('date' => date('d.m.Y H:i')));

        Zend_Layout::getMvcInstance()->setLayout("print");
        $this->renderScript("ticket/print/".$template);
    }

    /**
     * Печать тикетов "подтверждение у начальника склада"
     */
    protected function _printDepotVerify()
    {
        $this->_redirect("/finance/diff/ticket/{$this->getTicket()->getId()}/");
    }

    /**
     * Данные для тикетов "Подтверждение диспетчером"
     * @throws Exception
     * @return array
     */
    protected function _getDispatcherVerifyViewData()
    {
        $ticket = $this->getTicket();

        $sqlQuery = <<<SQL
            SELECT
                t.id,
                claims.`id` AS claim_id,
                clients.`u_title` AS client_name,
                DATE_FORMAT(t.`date_to_process`, "%d.%m.%Y %H:%i") AS date_to_process,
                claims.unload_region,
                claims.unload_city,
                claims.unload_street,
                claims.unload_building,
                claims.unload_address,
                claims.`unload_work_time` AS unload_depot_worktime,
                claims.`unload_contact_name` AS unload_contact,
                claims.`unload_phone` AS unload_phone,
                u_dispatcher.`id` AS dispatcher_id,
                u_dispatcher.`name` AS dispatcher_name,
                u_dispatcher.`phone` AS dispatcher_phone
             FROM ticket t
            LEFT JOIN ticket_type tt ON t.`ticket_type_id` = tt.`id`
            LEFT JOIN claims ON t.`claim_id` = claims.`id`
            LEFT JOIN clients ON claims.`client_id` = clients.`id`
            LEFT JOIN users u ON claims.`manager_id` = u.`id`
            LEFT JOIN users u_dispatcher ON claims.`dispetcher` = u_dispatcher.`id`
            LEFT JOIN orgstructure_users ou ON claims.`manager_id` = ou.`userId`
            LEFT JOIN orgstructure o ON o.`id` = ou.`oId`
            WHERE t.`id`={$ticket->getId()}
SQL;

        $result = Zend_Db_Table_Abstract::getDefaultAdapter()->query($sqlQuery)->fetchAll(Zend_Db::FETCH_ASSOC);

        if(!(is_array($result) && count($result))) {
            throw new \Exception("Запрос #{$ticket->getId()} не найден");
        }

        $resultData = $result[0];
        $unload_address = array();

        foreach(array('unload_region', 'unload_city', 'unload_street', 'unload_building', 'unload_address') as $address_part) {
            $address_part = trim($resultData[$address_part]);

            if(strlen($address_part) > 0) {
                $unload_address[] = $address_part;
            }
        }

        if(count($unload_address)) {
            $resultData["unload_address"] = implode(', ', $unload_address);
        } else {
            $resultData["unload_address"] = "- Отсутствует -";
        }

        return $resultData;
    }
}