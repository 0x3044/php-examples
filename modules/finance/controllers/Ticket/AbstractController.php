<?php
use App_Finance_Ticket_Factory as TicketFactory;
use App_Finance_Ticket_TicketInterface as Ticket;
use App_Finance_Ticket_Component_Exception_AccessForbiddenException as AclException;

class Finance_Ticket_AbstractController extends Zend_Controller_Action
{
    /**
     * Id тикета, переданный по URL
     * @var int
     */
    protected $_ticketId;

    /**
     * Тикет, с которым работает текущий вызов контроллера
     * @var Ticket
     */
    protected $_ticket;

    /**
     * Флаг "игнорировать шлюз прав"
     * @var bool
     */
    protected $_ignoreAclAccess;

    /**
     * Устанавливает Id тикета, с которым работает текущий вызов контроллера
     */
    public function init()
    {
        $this->setTicketId($this->_getParam('ticketId'));
    }

    /**
     * Устанавливает ID тикета, с которым работает контроллер
     * @param int $ticketId
     * @throws InvalidArgumentException
     */
    public function setTicketId($ticketId)
    {
        if(!(is_numeric($ticketId)) || (int) $ticketId <= 0) {
            throw new \InvalidArgumentException("ticketId should be a numeric value, got ".gettype($ticketId));
        }

        $this->_ticketId = (int) $ticketId;
    }

    /**
     * Возвращает ID тикета
     * @return int
     */
    public function getTicketId()
    {
        return $this->_ticketId;
    }

    /**
     * Возвращает ID клиента у заявки
     * @return int
     */
    public function getClientId()
    {
        /** @var $dbTable App_Db_Claims */
        $dbTable = App_Db::get(DB_CLAIMS);

        return $dbTable->getClaimClient($this->getTicket()->getClaimId());
    }

    /**
     * Возвращает запрошенный тикет
     * @throws App_Finance_Ticket_Component_Exception_AccessForbiddenException
     * @return App_Finance_Ticket_Type_Abstract_Ticket
     */
    public function getTicket()
    {
        if(!($this->_ticket instanceof Ticket)) {
            $this->_ticket = TicketFactory::getInstance()->createFromTicketId($this->getTicketId());

            if(!($this->ignoreAclRestrictions() || $this->_ticket->getAcl()->hasAccessView())) {
                throw new AclException("Вам не разрешен доступ к данному запросу");
            }
        }

        return $this->_ticket;
    }

    /**
     * Включить флаг игнорирования прав доступа
     */
    public function enableIgnoreAclRestrictions()
    {
        $this->_ignoreAclAccess = true;
    }

    /**
     * Отключить флаг игнорирования прав доступа
     */
    public function disableIgnoreAclRestrictions()
    {
        $this->_ignoreAclAccess = false;
    }

    /**
     * Возвращает true, ксли флаг игнорирования прав доступа включен
     * @return bool
     */
    public function ignoreAclRestrictions()
    {
        return $this->_ignoreAclAccess;
    }
}