<?php
require_once realpath(__DIR__).'/AbstractController.php';

class Finance_Ticket_GatewayController extends Finance_Ticket_AbstractController
{
    public function goAction()
    {
        $this->_redirect($this->getTicket()->getUrl());
    }
}