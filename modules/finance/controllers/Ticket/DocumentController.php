<?php
use App_Finance_Ticket_Factory as TicketFactory;
use App_Finance_Ticket_Component_Document as Document;
use App_Finance_Ticket_Component_Exception_AccessForbiddenException as AclException;
use App_Finance_Ticket_Logger_Logger as FinanceLogger;
use App_Finance_Utils_DocumentViewRegister as DocumentViewRegister;

require_once realpath(__DIR__).'/AbstractController.php';

class Finance_Ticket_DocumentController extends Finance_Ticket_AbstractController
{
    /**
     * Возвращает список документов документа
     * @view json
     */
    public function listAction()
    {
        $ticketId = $this->getTicketId();

        try {
            $ticket = TicketFactory::getInstance()->createFromTicketId($ticketId);

            if(!($ticket->getAcl()->hasAccessListDocuments())) {
                throw new AclException("Вы не имеете прав к просмотру документов данного запроса");
            }

            $jsonData = array('ticketId' => $ticketId, 'documents' => array());

            $documents = $ticket->getDocumentsHandler()->getDocuments();

            /** @var $document Document */
            foreach($documents as $document) {
                $documentData = $document->toArray();

                if($document->fileExists()) {
                    $documentData['link'] = App_Filemanager_Preview_LinkFactory::createLink(
                        $document->getFilePath(),
                        $document->getPublicLink(),
                        $document->getVendorLink()
                    );
                }else{
                    $documentData['link'] = false;
                }

                $jsonData['documents'][] = $documentData;
            }

            $jsonData['success'] = true;
        }
        catch(\Exception $e) {
            $jsonData = array(
                'ticketId' => $ticketId,
                'success' => false,
                'error' => $e->getMessage()
            );
        }

        header('Content-Type: application/json');
        die(json_encode($jsonData));
    }

    /**
     * Загрузка документа с сервера
     * @throws Exception
     */
    public function downloadAction()
    {
        $ticket = $this->getTicket();

        $documentId = $this->_getParam('documentId');

        if(!(is_numeric($documentId) && $documentId > 0)) {
            throw new \Exception('Id документа должно быть числовым значением больше нуля');
        }

        if(!($ticket->getAcl()->hasAccessListDocuments())) {
            throw new AclException("Вы не имеете прав к просмотру документов данного запроса");
        }

        $documents = $ticket->getDocumentsHandler()->getDocuments();
        $document = $documents->getDocumentById($documentId);

        $document->download();
    }

    /**
     * Загружает все документы с сервера как один zip-архив
     */
    public function zipAction()
    {
        $ticket = $this->getTicket();

        if(!($ticket->getAcl()->hasAccessListDocuments())) {
            throw new AclException("Вы не имеете прав к просмотру документов данного запроса");
        }

        $documents = $ticket->getDocumentsHandler()->getDocuments();

        $docViewRegister = new DocumentViewRegister($ticket);

        if($documents->size() == 0) {
            throw new \Exception("В запросе нет прикрепленных документов");
        } else if($documents->size() == 1) {
            /** @var $document Document */
            $document = $documents[0];

            if(!($document->fileExists())) {
                throw new \Exception("Файл `{$document->getFilePath()}` не найден. Обратитесь к администратору");
            }

            $docViewRegister->register();
            $ticket->save();

            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header('Content-Disposition: attachment; filename='.$document->getFileName());
            header('Content-Length: '.filesize($document->getFilePath()));
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private", false);

            readfile($document->getFilePath());
            exit();
        } else {
            $zipFileName = tempnam("tmp", "fd");

            $zipArchive = new ZipArchive();
            $zipArchive->open($zipFileName, ZipArchive::OVERWRITE);

            if($documents->size()) {
                /** @var $document Document */
                foreach($documents as $document) {
                    if(!($document->fileExists())) {
                        throw new \Exception("Файл `{$document->getFilePath()}` не найден. Обратитесь к администратору");
                    }

                    $zipArchive->addFile($document->getFilePath(), $document->getFileName());
                }
            }

            $zipArchive->close();
            $docViewRegister->register();
            $ticket->save();

            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header('Content-Disposition: attachment; filename='.("TICKET{$ticket->getId()}_TASK{$ticket->getClaimId()}.zip"));
            header('Content-Length: '.filesize($zipFileName));
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private", false);

            readfile($zipFileName);
            unlink($zipFileName);
            exit();
        }
    }

    /**
     * Загрузка документа и прикрепление его к тикету
     */
    public function uploadAction()
    {
        $ticketId = $this->getTicketId();

        try {
            $ticket = TicketFactory::getInstance()->createFromTicketId($ticketId);
            $jsonData = array('ticketId' => $ticketId, 'documents' => array());

            $financeLogger = new FinanceLogger($ticket);
            $financeLogger->oldSnapshot();

            if(!($ticket->getAcl()->hasAccessAttachDocuments())) {
                throw new AclException("Вы не имеете прав к загрузке документов данного запроса");
            }

            $document = $ticket->getDocumentsHandler()->createDocument();
            $document->upload($_FILES['document']['tmp_name'], $_FILES['document']['name']);

            $financeLogger->newSnapshot();
            $financeLogger->writeLog();

            $jsonData['success'] = true;
        }
        catch(\Exception $e) {
            $jsonData = array(
                'ticketId' => $ticketId,
                'success' => false,
                'error' => $e->getMessage()
            );
        }

        header('Content-Type: application/json');
        die(json_encode($jsonData));
    }

    /**
     * Удаление документа тикета
     */
    public function removeAction()
    {
        $ticketId = $this->getTicketId();

        try {
            $ticket = TicketFactory::getInstance()->createFromTicketId($ticketId);

            $financeLogger = new FinanceLogger($ticket);
            $financeLogger->oldSnapshot();

            if(!($ticket->getAcl()->hasAccessAttachDocuments())) {
                throw new AclException("Вы не имеете прав к удалению документов данного запроса");
            }

            $jsonData = array('ticketId' => $ticketId, 'documents' => array());
            $documentId = $this->_getParam('documentId');

            if(!($documentId && is_numeric($documentId))) {
                throw new \Exception('documentId required');
            }

            $ticket->getDocumentsHandler()->removeDocument($documentId);

            $financeLogger->newSnapshot();
            $financeLogger->writeLog();

            $jsonData['success'] = true;
        }
        catch(\Exception $e) {
            $jsonData = array(
                'ticketId' => $ticketId,
                'success' => false,
                'error' => $e->getMessage()
            );
        }

        header('Content-Type: application/json');
        die(json_encode($jsonData));
    }
}
