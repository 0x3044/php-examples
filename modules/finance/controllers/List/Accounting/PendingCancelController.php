<?php
require realpath(__DIR__).'/AbstractController.php';

use Finance_List_Accounting_AbstractController as AccountingController;
use App_Finance_Ticket_Component_Status_Type_Default_Status as TicketStatus;
use App_Finance_Recipient_Factory as RecipientFactory;

class Finance_List_Accounting_PendingCancelController extends AccountingController
{
    /**
     * Возвращает конфигурацию для модели контроллера
     * @return mixed
     */
    protected function _getModelConfig()
    {
        return array_replace_recursive($this->_getDefaultModelConfig(), array(
            "workPeriodEnabled" => true,
            "fields" => array(
                'process_delay' => false,
                "credit_limit_exceed" => true,
                "date_processed_timestamp" => false
            ),
            "buttons" => array(
                "archive" => false,
                "history" => true,
                "active" => true,
                "cancel" => true,
                "print" => false,
            ),
            "forms" => array(
                "attachment" => false,
                "cancel" => true,
                "active" => true,
                "archive" => false,
            ),
            "filters" => array(
                "date_processed" => false,
                "process_delay" => false
            ),
            "excel" => array(
                "date_processed" => false,
                'process_delay' => false,
            )
        ));
    }

    /**
     * Пользовательский метод, подготавливает конфигурацию модели контроллера
     */
    protected function _setUpModel()
    {
        $configuration = $this->_getModel()->getConfiguration();

        $configuration->setRecipient(RecipientFactory::getInstance()->createFromRecipientName('accounting'));
        $configuration->setTicketStatusInt(TicketStatus::STATUS_PENDING_CANCEL);
    }
}