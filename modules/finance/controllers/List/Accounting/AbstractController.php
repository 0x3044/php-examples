<?php
require realpath(__DIR__).'/../AbstractController.php';

use Finance_Model_List_Type_Accounting_Model as Model;

abstract class Finance_List_Accounting_AbstractController extends Finance_List_AbstractController
{
    protected final function _createModel()
    {
        return new Model();
    }

    /**
     * Возвращает дефолтную конфигурацию для модели
     * @return array
     */
    protected function _getDefaultModelConfig()
    {
        return array(
            "workPeriodEnabled" => false,
            "doubleClickMainForm" => "main",
            "ticketTypes" => array("accounting_act_of_reconciliation", "accounting_waybill"),
            "forms" => array(
                "main" => true,
                "archive" => true,
                "cancel" => true,
                "details" => true,
                "attachment" => true,
                "history" => true,
                "documents" => true,
                "print" => false,
                "date-to-process" => false,
                "act-of-reconciliation" => true,
                "credit-limit" => true
            ),
            "buttons" => array(
                "excel" => true,
            ),
            "fields" => array(
                "id" => true,
                "orgstructure" => true,
                "manager_name" => true,
                "client_name" => true,
                "invoice_ids" => true,
                "claim_id" => true,
                "date_created_timestamp" => true,
                "date_to_process_timestamp" => true,
                "date_processed_timestamp" => true,
                "car" => true,
                "ticket_type" => true,
                "process_delay" => false,
                "credit_limit_exceed" => true,
                "num_ticket_documents" => true,
                "client_contracts" => true
            ),
            "excel" => array(
                "id" => true,
                "orgstructure" => true,
                "manager_name" => true,
                "client_name" => true,
                "invoice_ids" => true,
                "claim_id" => true,
                "date_created" => true,
                "date_to_process" => true,
                "date_processed" => true,
                "car" => true,
                "ticket_type" => true,
                "process_delay" => true,
                "credit_limit_exceed" => true,
                "num_ticket_documents" => true,
            ),
            "filters" => array(
                "id" => true,
                "orgstructure_id" => true,
                "client_id" => true,
                "manager_id" => true,
                "invoice_ids" => true,
                "connectivity_claim_id" => true,
                "date_created" => true,
                "date_to_process" => true,
                "date_processed" => true,
                "car" => true,
                "ticket_type_id" => true,
                "process_delay" => false,
                "credit_limit_exceed" => true,
                "num_ticket_documents" => true,
            ),
            "mainFormFields" => array(
                "claim_id", "orgstructure", "client_name", "manager_name", "invoice_ids", "list-documents"
            )
        );
    }
}