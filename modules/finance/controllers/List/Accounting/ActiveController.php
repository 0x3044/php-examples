<?php
require realpath(__DIR__).'/AbstractController.php';

use Finance_List_Accounting_AbstractController as AccountingController;
use App_Finance_Ticket_Component_Status_Type_Default_Status as TicketStatus;
use App_Finance_Recipient_Factory as RecipientFactory;

class Finance_List_Accounting_ActiveController extends AccountingController
{
    /**
     * Возвращает конфигурацию для модели контроллера
     * @return mixed
     */
    protected function _getModelConfig()
    {
        return array_replace_recursive($this->_getDefaultModelConfig(), array(
            "workPeriodEnabled" => false,
            "forms" => array(
                "main" => true,
                "archive" => true,
                "cancel" => true,
                "details" => true,
                "documents" => true
            ),
            "buttons" => array(
                "history" => true,
                "attachment" => true,
                "cancel" => true,
                "archive" => true,
            ),
            "fields" => array(
                "date_processed_timestamp" => false
            ),
            "filters" => array(
                "date_processed" => false,
                "process_delay" => false
            ),
            "excel" => array(
                "date_processed" => false,
                "process_delay" => false
            )
        ));
    }

    /**
     * Пользовательский метод, подготавливает конфигурацию модели контроллера
     */
    protected function _setUpModel()
    {
        $configuration = $this->_getModel()->getConfiguration();

        $configuration->setRecipient(RecipientFactory::getInstance()->createFromRecipientName('accounting'));
        $configuration->setTicketStatusInt(TicketStatus::STATUS_ACTIVE);
    }
}