<?php
require realpath(__DIR__).'/AbstractController.php';

use Finance_List_Accounting_AbstractController as AccountingController;
use App_Finance_Ticket_Component_Status_Type_Default_Status as TicketStatus;
use App_Finance_Recipient_Factory as RecipientFactory;

class Finance_List_Accounting_ArchivedController extends AccountingController
{
    /**
     * Возвращает конфигурацию для модели контроллера
     * @return mixed
     */
    protected function _getModelConfig()
    {
        return array_replace_recursive($this->_getDefaultModelConfig(), array(
            "workPeriodEnabled" => true,
            "fields" => array(
                'process_delay' => true,
                "credit_limit_exceed" => true
            ),
            "buttons" => array(
                "history" => true,
                "cancel" => true,
                'setviewed' => true,
                'setnotviewed' => true,
            ),
            "forms" => array(
                "main" => true,
                "archive" => false,
                "cancel" => true,
                "details" => true,
                "attachment" => false
            ),
            "excel" => array(
                "orgstructure" => false
            ),
            "filters" => array(
                "process_delay" => true
            )
        ));
    }

    /**
     * Пользовательский метод, подготавливает конфигурацию модели контроллера
     */
    protected function _setUpModel()
    {
        $configuration = $this->_getModel()->getConfiguration();

        $configuration->setRecipient(RecipientFactory::getInstance()->createFromRecipientName('accounting'));
        $configuration->setTicketStatusInt(TicketStatus::STATUS_ARCHIVED);
        $configuration->setFilterDefaultSortColumn('date_processed_timestamp');
        $configuration->setFilterDefaultSortType('DESC');
        $this->_getFilter()->registerResultPostProcess(function (&$result) {
            /** @var $ticketViewedDb App_Db_TicketViewed */
            $ticketViewedDb = App_Db::get(DB_TICKET_VIEWED);
            $viewedTicketsIds = $ticketViewedDb->getViewedTicketsIds('accounting');
            $checkIsNotEmpty = is_array($viewedTicketsIds) && !empty($viewedTicketsIds);
            foreach($result as $rowData) {
                $rowData->is_viewed = $checkIsNotEmpty && in_array($rowData->id, $viewedTicketsIds);
            }
        });
    }
}