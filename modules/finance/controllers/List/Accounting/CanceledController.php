<?php
require realpath(__DIR__).'/AbstractController.php';

use Finance_List_Accounting_AbstractController as AccountingController;
use App_Finance_Ticket_Component_Status_Type_Default_Status as TicketStatus;
use App_Finance_Recipient_Factory as RecipientFactory;

class Finance_List_Accounting_CanceledController extends AccountingController
{
    /**
     * Возвращает конфигурацию для модели контроллера
     * @return mixed
     */
    protected function _getModelConfig()
    {
        return array_replace_recursive($this->_getDefaultModelConfig(), array(
            "workPeriodEnabled" => true,
            "fields" => array(
                'process_delay' => true,
                "credit_limit_exceed" => true
            ),
            "buttons" => array(
                "history" => true,
                "excel" => true,
            ),
            "forms" => array(
                "attachment" => false,
                "cancel" => false,
                "archive" => false,
            ),
            "filters" => array(
                "process_delay" => true
            )
        ));
    }

    /**
     * Пользовательский метод, подготавливает конфигурацию модели контроллера
     */
    protected function _setUpModel()
    {
        $configuration = $this->_getModel()->getConfiguration();

        $configuration->setRecipient(RecipientFactory::getInstance()->createFromRecipientName('accounting'));
        $configuration->setTicketStatusInt(TicketStatus::STATUS_CANCELED);
    }
}