<?php
use Finance_Model_List_Type_AbstractType_Model as Model;
use App_Filter_Excel_Converter as FilterConverter;
use App_Filter_Controller_AbstractFilterController as FilterController;
use App_Filter_Excel_Component_Decorator_Header as HeaderDecorator;
use App_Filter_Excel_Component_Decorator_AlbumPageSetup as AlbumPageSetupDecorator;
use App_Filter_Excel_Converter as Converter;
use App_Finance_Ticket_Component_Status_Type_Default_Status as TicketStatus;
use Finance_Model_ExcelWidthDecorator as ExcelWidthDecorator;

abstract class Finance_List_AbstractController extends FilterController
{
    /**
     * @var Model
     */
    protected $_model;

    /**
     * Возвращает список доступных статусов
     * @return array
     */
    protected function _getAvailableStatuses()
    {
        return array(
            TicketStatus::STATUS_ACTIVE => array('name' => 'active', 'text' => 'Активные запросы'),
            TicketStatus::STATUS_CANCELED => array('name' => 'canceled', 'text' => 'Отмененные запросы'),
            TicketStatus::STATUS_PENDING_CANCEL => array('name' => 'pending-cancel', 'text' => 'Ожидающие отмены запросы'),
            TicketStatus::STATUS_ARCHIVED => array('name' => 'archived', 'text' => 'Архив')
        );
    }

    /**
     * Инициализация контроллера
     */
    public function init()
    {
        parent::init();

        $model = $this->_getModel();
        $recipient = $model->getConfiguration()->getRecipient();

        if(!($recipient->hasAuthorization())) {
            $this->_redirect('/');
        }

        if(!(App_Access::get('access', "finance>{$recipient->getName()}>status>{$this->_getModel()->getConfiguration()->getTicketStatusString()}"))) {
            $this->_redirect('/');
        }

        $statusList = $this->_getAvailableStatuses();

        $statusHeader = $statusList[$model->getConfiguration()->getTicketStatusInt()]['text'];
        Zend_Registry::set('statusString', "Финансовый саппорт ‒ {$recipient->getDescription()} ‒ {$statusHeader}");

        $this->_getHeadLink()->appendCssFiles(array(
            '/css/sass/finance/finance.css',
            '/css/sass/filter/form-filters.css',
            '/js/fancybox/jquery.fancybox.css',
            '/css/sass/form.css'
        ));

        $this->_getHeadScript()->appendJsFiles(array(
            '/js/jquery.damnUploader/js/jquery.damnUploader.js',
            '/js/jquery/jquery.ruDate.js',
            '/js/jquery/jquery.searchbox.js',
            '/js/jquery/jquery.editOnClick.js',
            '/js/jquery/jquery.fileSelect.js',
            '/js/datatable/fieldshider.js',
            '/js/form/form.js',
            '/js/finance/form/documents.js',
            '/js/finance/forms.js',
            '/js/finance/notify.js',
            '/js/finance/connectivity.js',
            '/js/finance/fields/abstract.js',
            '/js/finance/list/abstract.js',
            "/js/finance/fields/{$recipient->getName()}.js",
            "/js/finance/list/{$recipient->getName()}.js",
            '/js/finance/form/sorter.js',
            '/js/jquery/jquery.browsable.js',
            '/js/fancybox/jquery.fancybox.pack.js',
            '/js/filemanager/preview.js',
        ));

        $this->_includeAssets($this->_getHeadLink(), $this->_getHeadScript());

        $this->view->assign(array(
            'statusList' => $statusList,
            'acl' => array(
                'active' => App_Access::get("access", "finance>{$recipient->getName()}>status>active"),
                'archived' => App_Access::get("access", "finance>{$recipient->getName()}>status>archived"),
                'canceled' => App_Access::get("access", "finance>{$recipient->getName()}>status>canceled"),
                'pending-cancel' => App_Access::get("access", "finance>{$recipient->getName()}>status>pending-cancel"),
            )
        ));
    }

    /**
     * Наследуйте этот метод, чтобы включить специфичные для конкретного саппорта css/js-файлы
     * @param App_View_Helper_HeadLink $headLink
     * @param App_View_Helper_HeadScript $headScript
     */
    protected function _includeAssets(App_View_Helper_HeadLink $headLink, App_View_Helper_HeadScript $headScript) {}

    /**
     * Главная страница контроллера
     */
    public function indexAction()
    {
        $this->view->assign(array(
            "model" => $this->_getModel()
        ));
    }

    /**
     * Возвращает JSON-ответ с Id новых запросов в данном разделе саппорта
     */
    public function countAction()
    {
        try {
            $jsonData = array(
                "activeTicketIds" => $this->_getNewTicketsIds(),
                "success" => true
            );
        }
        catch(\Exception $e) {
            $jsonData = array(
                "success" => false,
                "error" => $e->getMessage()
            );
        }

        header('Content-Type: application/json');
        echo json_encode($jsonData);
        exit();
    }

    /**
     * Возвращает Id новых запросов в данный раздел саппорта
     * @throws Exception
     * @return array
     */
    protected function _getNewTicketsIds()
    {
        /** @var $dbTable App_Db_Ticket */
        $dbTable = App_Db::get(DB_TICKET);
        /** @var $typeDbTable App_Db_TicketType */
        $typeDbTable = App_Db::get(DB_TICKET_TYPE);

        $configuration = $this->_getModel()->getConfiguration();
        $ticketTypeIds = array();

        foreach($configuration->getTicketTypes() as $ticketType) {
            $testTicket = App_Finance_Ticket_Factory::getInstance()->createFromTicketTypeId($ticketType);

            if($testTicket->getAcl()->hasAccessView()) {
                $ticketTypeIds[] = $typeDbTable->getTicketTypeIdByName($ticketType);
            }
        }

        if(!(count($ticketTypeIds))) {
            throw new \Exception("У вас нет доступа ни к одному запроса данного саппорта");
        }

        return $dbTable->getTicketIdsByTicketTypeIds($ticketTypeIds, $configuration->getTicketStatusInt());
    }

    /**
     * Возвращает модель для данного контроллера
     * @return Model
     */
    abstract protected function _createModel();

    /**
     * Возвращает массив с параметрами для конфигурации модели
     * @return mixed
     */
    abstract protected function _getModelConfig();

    /**
     * Конфигурирует модель
     * @see Finance_List_AbstractController::_getModel
     */
    abstract protected function _setUpModel();

    /**
     * Возвращает фильтр контроллера
     * В этом методе также данный фильтр можно конфигурировать
     * @return \App_Filter_AbstractFilter
     */
    protected function _createFilter()
    {
        $filter = $this->_getModel()->getFilter();
        $recordId = $this->_getParam('ticketId');

        if($recordId > 0) {
            $filter->getSqlFormatter()->getHavingConditions()->add("id = ".(int) $recordId);
        }

        return $filter;
    }

    /**
     * Возврашает модель контроллера
     * @return \Finance_Model_List_Type_Accounting_Model
     */
    protected final function _getModel()
    {
        if(!($this->_model instanceof Model)) {
            $this->_model = $this->_createModel();
            $modelConfiguration = $this->_getModel()->getConfiguration();
            $modelConfiguration->setUp($this->_getModelConfig());
            $this->_setUpModel();
            $this->_getModel()->setUp();
            $this->_getModel()->getConfiguration()->setNotifyBaseIds($this->_getNewTicketsIds());

            if($modelConfiguration->getTicketStatusString() == 'active') {
                $recipient = $modelConfiguration->getRecipient();
                $recipient->getSessionHandler()->updateSessionData();
            }
        }

        return $this->_model;
    }

    /**
     * Метод, вызываемый при создании excel-конвертера
     * Вся конфигурация конвертера описывается здесь
     * @param App_Filter_AbstractFilter $filter Фильтр контроллера
     * @param App_Filter_Excel_Converter $excelConverter Конвертер Filter->.xls
     */
    protected function _setUpFilterExcelConverter(\App_Filter_AbstractFilter $filter, FilterConverter $excelConverter)
    {
        $model = $this->_getModel();

        $model->setUpExcel($excelConverter);
        $this->_filterExcelHeaderDecorator($excelConverter);
        $this->_filterExcelAlbumPageSetupDecorator($excelConverter);

        if(count($excelConverter->getHiddenFields()) == 0) {
            $excelWidthDecorator = new ExcelWidthDecorator($excelConverter);
            $excelWidthDecorator->decorate($model->getConfiguration()->getRecipient()->getName(), $model->getConfiguration()->getTicketStatusString());
        }
    }

    /**
     * Возвращает название excel-файла выгрузки
     * @throws Exception
     * @return string
     */
    protected function _getExcelFileName()
    {
        $model = $this->_getModel();
        $ticketTypes = $model->getConfiguration()->getTicketTypes();
        $ticketStatus = $model->getConfiguration()->getTicketStatusInt();

        switch($ticketStatus) {
            default:
                throw new \Exception("Unknown ticket status {$ticketStatus}");

            case TicketStatus::STATUS_ACTIVE:
                $ticketStatusString = "активные-запросы";
                break;

            case TicketStatus::STATUS_CANCELED:
                $ticketStatusString = "отмененные-запросы";
                break;

            case TicketStatus::STATUS_ARCHIVED:
                $ticketStatusString = "архивные-запросы";
                break;

            case TicketStatus::STATUS_PENDING_CANCEL:
                $ticketStatusString = "ожидающие-отмену";
                break;
        }

        if(!(count($ticketTypes))) {
            throw new \Exception('No ticketTypes available');
        }

        $recipientString = $model->getConfiguration()->getRecipient()->getDescription();

        return str_replace(' ', '-', "{$recipientString}-{$ticketStatusString}.xlsx");
    }

    /**
     * Добавляет HeaderDecorator, заголовок к excel-выгрузкам
     * @param App_Filter_Excel_Converter $converter
     */
    protected final function _filterExcelHeaderDecorator(Converter $converter)
    {
        if(Zend_Auth::getInstance()->hasIdentity()) {
            $userName = Zend_Auth::getInstance()->getIdentity()->name;
        } else {
            $userName = "<Неизвестно, кто создал данный файл>";
        }

        $title = $this->_getModel()->getConfiguration()->getRecipient()->getDescription();
        $generationDate = new Zend_Date();
        $generationDate = $generationDate->toString(App_Db::ZEND_DATETIME_RU_FORMAT);
        $description = "Сгенерировано пользователем {$userName}, дата генерации: {$generationDate}";

        $headerDecorator = new HeaderDecorator($converter);
        $headerDecorator->setTitle($title);
        $headerDecorator->setDescription($description);
        $headerDecorator->apply();
    }

    /**
     * @param App_Filter_Excel_Converter $excelConverter
     */
    protected function _filterExcelAlbumPageSetupDecorator(FilterConverter $excelConverter)
    {
        $albumPageDecorator = new AlbumPageSetupDecorator($excelConverter);
        $albumPageDecorator->apply();
    }

    /**
     * Отмечает тикет как "просмотренный"
     */
    public function setAsViewedAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        echo json_encode(App_Db::get(DB_TICKET_VIEWED)->setTicketAsViewed((int) $this->_getParam('ticketId')));
    }

    /**
     * Отмечает тикет как "НЕпросмотренный"
     */
    public function setAsNotViewedAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        echo json_encode(App_Db::get(DB_TICKET_VIEWED)->setTicketAsNotViewed((int) $this->_getParam('ticketId')));
    }
}