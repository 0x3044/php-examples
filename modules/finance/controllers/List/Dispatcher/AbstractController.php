<?php
require realpath(__DIR__).'/../AbstractController.php';

use Finance_Model_List_Type_Dispatcher_Model as Model;
use App_Filter_Component_Decorator_Type_Dispatchers_DispatchersNotAvailableException as DispatchersNotAvailableException;

abstract class Finance_List_Dispatcher_AbstractController extends Finance_List_AbstractController
{
    protected final function _createModel()
    {
        $model = new Model();

        try {
            $model->getFilter();
        }
        catch(DispatchersNotAvailableException $e) {
            $this->_redirect('/');
        }

        return $model;
    }

    /**
     * Возвращает дефолтную конфигурацию для модели
     * @return array
     */
    protected function _getDefaultModelConfig()
    {
        return array(
            "workPeriodEnabled" => false,
            "ticketTypes" => array("dispatcher_verify"),
            "doubleClickMainForm" => "main",
            "forms" => array(
                "main" => true,
                "attachment" => false,
                "documents" => true,
                "history" => true,
                "cancel" => true,
                "archive" => true,
                "details" => true,
                "date-to-process" => false
            ),
            "fields" => array(
                "id" => true,
                "date_created_timestamp" => true,
                "date_to_process_timestamp" => true,
                "date_processed_timestamp" => false,
                "manager_name" => true,
                "manager_phone" => true,
                "orgstructure" => true,
                "claim_id" => true,
                "claim_type_name" => true,
                "client_name" => true,
                "unload_address" => true,
                "has_client_directions" => true,
                "unload_depot_worktime" => true,
                "car_type" => true,
                "aprox_claim_weight" => true,
                "dispatcher_name" => true,
                "car_number" => true,
                "rent_price" => true,
                "transport_delivery_time" => true,
                "claim_annotation_dispatcher" => true,
            ),
            "buttons" => array(
                "excel" => true
            ),
            "excel" => array(
                "id" => true,
                "date_created" => true,
                "date_to_process" => true,
                "date_processed" => true,
                "manager_name" => true,
                "manager_phone" => true,
                "orgstructure" => true,
                "claim_id" => true,
                "claim_type_name" => true,
                "client_name" => true,
                "unload_address" => true,
                "unload_depot_worktime" => true,
                "car_type" => true,
                "aprox_claim_weight" => true,
                "dispatcher_name" => true,
                "car_number" => true,
                "rent_price" => true,
                "transport_delivery_time" => true,
                "claim_annotation_dispatcher" => true
            ),
            "filters" => array(
                "id" => true,
                "date_created" => true,
                "date_to_process" => true,
                "date_processed" => true,
                "manager_id" => true,
                "orgstructure_id" => true,
                "connectivity_claim_id" => true,
                "claim_type_id" => true,
                "client_id" => true,
                "unload_depot_worktime" => true,
                "car_type" => true,
                "aprox_claim_weight" => true,
                "dispatcher_id" => true,
                "car_number" => true,
                "rent_price" => true,
                "transport_delivery_time" => true,
                "has_client_directions" => true
            ),
            "mainFormFields" => array(
                "date_to_process_timestamp", "claim_id", "manager_name", "manager_phone", "orgstructure", "dispatcher_name",
                "car_number", "rent_price", "transport_delivery_time", "claim_annotation_dispatcher"
            )
        );
    }

    protected function _setUpFilterExcelConverter(\App_Filter_AbstractFilter $filter, \App_Filter_Excel_Converter $excelConverter)
    {
        parent::_setUpFilterExcelConverter($filter, $excelConverter);

        if($this->_getModel()->getConfiguration()->getTicketStatusString() != 'active') {
            $excelConverter->getMap()->find('client_name')->setWidth(10);
            $excelConverter->getMap()->find('manager_name')->setWidth(10);
        }
    }
}