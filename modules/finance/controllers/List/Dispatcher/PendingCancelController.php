<?php
require realpath(__DIR__).'/AbstractController.php';

use Finance_List_Dispatcher_AbstractController as DispatcherController;
use App_Finance_Ticket_Component_Status_Type_Default_Status as TicketStatus;
use App_Finance_Recipient_Factory as RecipientFactory;

class Finance_List_Dispatcher_PendingCancelController extends DispatcherController
{
    /**
     * Возвращает конфигурацию для модели контроллера
     * @return mixed
     */
    protected function _getModelConfig()
    {
        return array_replace_recursive($this->_getDefaultModelConfig(), array(
            "editable" => false,
            "forms" => array(
                "active" => true,
                "cancel" => true,
                "archive" => false,
            ),
            "buttons" => array(
                "history" => true,
                "active" => true,
                "cancel" => true,
                "print" => true,
            ),
            "fields" => array(),
            "filters" => array(
                "date_processed" => false
            ),
            "excel" => array(
                "date_processed" => false
            )
        ));
    }

    /**
     * Пользовательский метод, подготавливает конфигурацию модели контроллера
     */
    protected function _setUpModel()
    {
        $configuration = $this->_getModel()->getConfiguration();

        $configuration->setRecipient(RecipientFactory::getInstance()->createFromRecipientName('dispatcher'));
        $configuration->setTicketStatusInt(TicketStatus::STATUS_PENDING_CANCEL);
    }
}