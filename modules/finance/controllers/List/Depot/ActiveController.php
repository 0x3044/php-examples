<?php
require realpath(__DIR__).'/AbstractController.php';

use Finance_List_Depot_AbstractController as DepotController;
use App_Finance_Ticket_Component_Status_Type_Default_Status as TicketStatus;
use App_Finance_Recipient_Factory as RecipientFactory;

class Finance_List_Depot_ActiveController extends DepotController
{
    public function init()
    {
        parent::init();
        /** @var $ticketViewedDb App_Db_TicketViewed */
        $ticketViewedDb = App_Db::get(DB_TICKET_VIEWED);
        $ticketViewedDb->syncWithCookies();
    }

    /**
     * Возвращает конфигурацию для модели контроллера
     * @return mixed
     */
    protected function _getModelConfig()
    {
        return array_replace_recursive($this->_getDefaultModelConfig(), array(
            "doubleClickMainForm" => "print",
            "forms" => array(),
            "fields" => array(),
            "filters" => array(),
            "buttons" => array(
                "history" => true,
                "cancel" => true,
                "archive" => true,
                "print" => true,
            ),
            "excel" => array(
                "date_created" => false,
                "date_processed" => false,
            )
        ));
    }

    /**
     * Пользовательский метод, подготавливает конфигурацию модели контроллера
     */
    protected function _setUpModel()
    {
        $configuration = $this->_getModel()->getConfiguration();

        $configuration->setRecipient(RecipientFactory::getInstance()->createFromRecipientName('depot'));
        $configuration->setTicketStatusInt(TicketStatus::STATUS_ACTIVE);
        $configuration->setRecordsPerPage(30);
    }

    /**
     * Возвращает конфиг для формы сортировки
     * @return array
     */
    protected function _getSorterConfig()
    {
        /* Берем поля из модели */
        $defaultModelConfig = $this->_getDefaultModelConfig();
        $fieldsNames = array_keys($defaultModelConfig['fields']);
        $fieldsNames[] = 'is_viewed';
        /* Тип сортировки по умолчанию */
        $defaultSortElement = array(
            'type' => 'Default',
            'orderTypes' => array(
                'ASC' => 'По возр.', 'DESC' => 'По убыв.'
            )
        );
        /* Замены типов сортировки для полей */
        $typeReplaces = array(
            'cash_planned_state' => array(
                'type' => 'Sortable',
                'orderTypes' => array(
                    array('title' => 'не обр', 'class' => 'gray', 'value' => '-999,-1'),
                    array('title' => 'откл', 'class' => 'red', 'value' => 'null'),
                    array('title' => 'отлож', 'class' => 'yellow', 'value' => '0,5'),
                    array('title' => 'обр', 'class' => 'green', 'value' => '1,2,3,4')
                )
            ),
            'car_number' => array(
                'type' => 'Sortable',
                'orderTypes' => array(
                    array('title' => 'не наша', 'class' => 'gray', 'value' => 'not_own'),
                    array('title' => 'не выбр.', 'class' => 'red', 'value' => 'own_not_selected'),
                    array('title' => 'с номером', 'class' => 'green', 'value' => 'own_selected')
                )
            ),
            'is_viewed' => array('type' => 'Default', 'orderTypes' => array('ASC' => 'Жирн. сверху', 'DESC' => 'Жирн. снизу')),
            'claim_changes' => array('type' => 'Default', 'orderTypes' => array('ASC' => 'Изменённые снизу', 'DESC' => 'Изменённые сверху'))
        );
        /* Итоговый массив полей */
        $sorterFields = array();
        foreach($fieldsNames as $fieldName) {
            if(isset($typeReplaces[$fieldName])) {
                $sorterField = $typeReplaces[$fieldName];
            } else {
                $sorterField = $defaultSortElement;
            }
            $sorterField['name'] = $fieldName;
            $sorterFields[] = $sorterField;
        }

        return array('fields' => $sorterFields);
    }

    /**
     * Главная страница контроллера
     */
    public function indexAction()
    {
        parent::indexAction();
        $this->removeScript('/js/jqueryui/jquery-ui.js');
        $this->_getHeadScript()->appendFile('/js/jqueryui/jquery-ui-1.10.4.js');
        $this->view->assign(array('sorterConfig' => $this->_getSorterConfig()));
    }

    /**
     * Удаляет ранее добавленный скрипт
     * @param string $src The source path of the script file.
     * @return boolean Returns TRUE, if the removal has been a success.
     */
    public function removeScript($src)
    {
        $headScriptContainer = Zend_View_Helper_Placeholder_Registry::getRegistry()
            ->getContainer("Zend_View_Helper_HeadScript");
        $iter = $headScriptContainer->getIterator();
        $success = false;
        foreach($iter as $k => $value) {
            if(strpos($value->attributes["src"], $src) !== false) {
                $iter->offsetUnset($k);
                $success = true;
            }
        }
        Zend_View_Helper_Placeholder_Registry::getRegistry()
            ->setContainer("Zend_View_Helper_HeadScript", $headScriptContainer);

        return $success;
    }
}