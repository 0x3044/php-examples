<?php
require realpath(__DIR__).'/AbstractController.php';

use Finance_List_Depot_AbstractController as DepotController;
use App_Finance_Ticket_Component_Status_Type_Default_Status as TicketStatus;
use App_Finance_Recipient_Factory as RecipientFactory;

class Finance_List_Depot_ArchivedController extends DepotController
{
    /**
     * Возвращает конфигурацию для модели контроллера
     * @return mixed
     */
    protected function _getModelConfig()
    {
        return array_replace_recursive($this->_getDefaultModelConfig(), array(
            "workPeriodEnabled" => true,
            "forms" => array(
                "cancel" => true,
                "archive" => false,
            ),
            "fields" => array(
                "date_created_timestamp" => true,
                "date_processed_timestamp" => true
            ),
            "filters" => array(),
            "excel" => array(),
            "buttons" => array(
                "history" => true,
                "cancel" => true,
                "print" => true,
            )
        ));
    }

    /**
     * Пользовательский метод, подготавливает конфигурацию модели контроллера
     */
    protected function _setUpModel()
    {
        $configuration = $this->_getModel()->getConfiguration();

        $configuration->setRecipient(RecipientFactory::getInstance()->createFromRecipientName('depot'));
        $configuration->setTicketStatusInt(TicketStatus::STATUS_ARCHIVED);
    }
}