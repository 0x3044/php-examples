<?php
require realpath(__DIR__).'/../AbstractController.php';

use Finance_Model_List_Type_Depot_Model as Model;

abstract class Finance_List_Depot_AbstractController extends Finance_List_AbstractController
{
    protected final function _createModel()
    {
        return new Model();
    }

    /**
     * Возвращает дефолтную конфигурацию для модели
     * @return array
     */
    protected function _getDefaultModelConfig()
    {
        return array(
            "workPeriodEnabled" => false,
            "doubleClickMainForm" => "main",
            "ticketTypes" => array("depot_verify"),
            "forms" => array(
                "main" => true,
                "attachment" => false,
                "documents" => true,
                "history" => true,
                "cancel" => true,
                "archive" => true,
                "details" => true,
                "date-to-process" => false
            ),
            "fields" => array(
                "id" => true,
                "claim_id" => true,
                "claim_type_name" => true,
                "date_created_timestamp" => true,
                "date_to_process_timestamp" => true,
                "date_processed_timestamp" => false,
                "manager_name" => true,
                "client_name" => true,
                "cash_planned_state" => true,
                "claim_status" => true,
                "car_number" => true,
                "transport_delivery_time" => true,
                "claim_annotation_depot" => true,
                "claim_changes" => true,
            ),
            "buttons" => array(
                "excel" => true,
            ),
            "excel" => array(
                "id" => true,
                "claim_id" => true,
                "claim_type_name" => true,
                "date_created" => true,
                "date_to_process" => true,
                "date_processed" => true,
                "manager_name" => true,
                "client_name" => true,
                "cash_planned_state" => true,
                "claim_status" => true,
                "car_number" => true,
                "transport_delivery_time" => true,
                "claim_annotation_depot" => true,
                "claim_changes" => true
            ),
            "filters" => array(
                "id" => true,
                "connectivity_claim_id" => true,
                "claim_type_id" => true,
                "date_created" => true,
                "date_to_process" => true,
                "date_processed" => true,
                "manager_id" => true,
                "client_id" => true,
                "cash_planned_state" => true,
                "claim_status_id" => true,
                "car_number" => true,
                "transport_delivery_time" => true,
                "claim_changes" => true
            ),
            "mainFormFields" => array(
                "claim_id", "claim_type_name", "date_to_process_timestamp", "manager_name", "client_name", "cash_planned_state",
                "claim_status", "car_number", "transport_delivery_time", "claim_changes", "claim_annotation_depot"
            )
        );
    }
}