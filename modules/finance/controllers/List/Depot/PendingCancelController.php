<?php
require realpath(__DIR__).'/AbstractController.php';

use Finance_List_Depot_AbstractController as DepotController;
use App_Finance_Ticket_Component_Status_Type_Default_Status as TicketStatus;
use App_Finance_Recipient_Factory as RecipientFactory;

class Finance_List_Depot_PendingCancelController extends DepotController
{
    /**
     * Возвращает конфигурацию для модели контроллера
     * @return mixed
     */
    protected function _getModelConfig()
    {
        return array_replace_recursive($this->_getDefaultModelConfig(), array(
            "doubleClickMainForm" => "print",
            "forms" => array(
                "active" => true,
                "cancel" => true,
                "archive" => false,
            ),
            "fields" => array(),
            "filters" => array(),
            "excel" => array(
                "date_created" => false,
                "date_processed" => false,
            ),
            "buttons" => array(
                "history" => true,
                "active" => true,
                "cancel" => true,
                "archive" => false,
                "print" => true,
            )
        ));
    }

    /**
     * Пользовательский метод, подготавливает конфигурацию модели контроллера
     */
    protected function _setUpModel()
    {
        $configuration = $this->_getModel()->getConfiguration();

        $configuration->setRecipient(RecipientFactory::getInstance()->createFromRecipientName('depot'));
        $configuration->setTicketStatusInt(TicketStatus::STATUS_PENDING_CANCEL);

        $this->_getFilter()->registerResultPostProcess(function (&$result) {
            /** @var $ticketViewedDb App_Db_TicketViewed */
            $ticketViewedDb = App_Db::get(DB_TICKET_VIEWED);
            $viewedTicketsIds = $ticketViewedDb->getViewedTicketsIds('depot');
            $checkIsNotEmpty = is_array($viewedTicketsIds) && !empty($viewedTicketsIds);
            foreach($result as $rowData) {
                $rowData->is_viewed = $checkIsNotEmpty && in_array($rowData->id, $viewedTicketsIds);
            }
        });
    }
}