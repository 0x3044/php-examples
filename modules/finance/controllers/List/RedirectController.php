<?php

/**
 * Редирект на активные запросы
 */
class Finance_List_RedirectController extends Zend_Controller_Action
{
    public function indexAction()
    {
        $recipient = $this->_getParam("recipient");

        if(!(strlen($recipient))) {
            throw new \InvalidArgumentException("Invalid recipient");
        }

        foreach(array('active', 'canceled', 'archived') as $status) {
            if(App_Access::get("access", "finance>{$recipient}>status>{$status}")) {
                $this->_redirect("/finance/list/{$recipient}/{$status}");
            }
        }

        $this->_redirect('/');
    }
}