<?php
require realpath(__DIR__).'/../AbstractController.php';

use Finance_Model_List_Type_Payment_Model as Model;
use App_Finance_Ticket_Component_Status_Type_Default_Status as TicketStatus;

abstract class Finance_List_Payment_AbstractController extends Finance_List_AbstractController
{
    /**
     * {@inheritdoc}
     * @param App_View_Helper_HeadLink $headLink
     * @param App_View_Helper_HeadScript $headScript
     */
    protected function _includeAssets(App_View_Helper_HeadLink $headLink, App_View_Helper_HeadScript $headScript)
    {
        $headScript->appendJsFiles(array(
            '/js/finance/searchbox.js'
        ));
    }

    /**
     * Возвращает список доступных статусов
     * @return array
     */
    protected function _getAvailableStatuses()
    {
        return array(
            TicketStatus::STATUS_ACTIVE => array('name' => 'active', 'text' => 'Активные запросы'),
            TicketStatus::STATUS_PENDING_CANCEL => array('name' => 'pending-cancel', 'text' => 'Отмененные запросы'),
            TicketStatus::STATUS_ARCHIVED => array('name' => 'archived', 'text' => 'Архив')
        );
    }

    /**
     * {@inheritdoc}
     * @return Finance_Model_List_Type_AbstractType_Model|Finance_Model_List_Type_Payment_Model
     */
    protected final function _createModel()
    {
        return new Model();
    }

    /**
     * Возвращает дефолтную конфигурацию для модели
     * @return array
     */
    protected function _getDefaultModelConfig()
    {
        return array(
            "workPeriodEnabled" => false,
            "doubleClickMainForm" => "",
            "ticketTypes" => array("payment_request"),
            "forms" => array(
                "main" => true,
                "archive" => true,
                "pending-cancel-extended" => true,
                "details" => true,
                "attachment" => true,
                "history" => false,
                "documents" => false,
                "print" => false,
                "date-to-process" => false,
                "act-of-reconciliation" => true,
                "credit-limit" => true,
            ),
            "buttons" => array(
                "excel" => true,
                "history" => false,
                "attachment" => false
            ),
            "fields" => array(
                "id" => true,
                "orgstructure" => true,
                "author_user_name" => true,
                "date_created_timestamp" => true,
                "date_processed_timestamp" => true,
                "process_delay" => false,
                'client_name' => true,
                'payment_type_name' => true,
                'payment_document_number' => true,
                'ru_payment_from_date' => true,
                'payment_sum' => true,
                'payment_annotation' => true,
                'cancellation_reason' => false,
                'deleted_flag' => false,
                "num_ticket_documents" => true,
            ),
            "excel" => array(
                "id" => true,
                "orgstructure" => true,
                "author_user_name" => true,
                "date_created" => true,
                "date_processed" => true,
                'client_name' => true,
                'payment_type' => true,
                'payment_document_number' => true,
                'ru_payment_from_date' => true,
                'payment_sum' => true,
                'payment_annotation' => true,
                "num_ticket_documents" => true,
            ),
            "filters" => array(
                "id" => true,
                "author_user_id" => true,
                "date_created" => true,
                "date_processed" => true,
                "process_delay" => false,
                "orgstructure_id" => true,
                'client_id' => true,
                'payment_type' => true,
                'payment_document_number' => true,
                'payment_from_date' => true,
                'payment_sum' => true,
                "num_ticket_documents" => true,
            ),
            "mainFormFields" => array(
                "orgstructure",
                "author_user_name",
                "date_created_timestamp",
                'client_name',
                'payment_type_name',
                'payment_sum',
                'payment_annotation'
            )
        );
    }
}