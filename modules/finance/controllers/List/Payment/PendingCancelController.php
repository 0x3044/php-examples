<?php
require realpath(__DIR__).'/AbstractController.php';

use Finance_List_Payment_AbstractController as PaymentController;
use App_Finance_Ticket_Component_Status_Type_Default_Status as TicketStatus;
use App_Finance_Recipient_Factory as RecipientFactory;

class Finance_List_Payment_PendingCancelController extends PaymentController
{
    /**
     * Возвращает конфигурацию для модели контроллера
     * @return mixed
     */
    protected function _getModelConfig()
    {
        return array_replace_recursive($this->_getDefaultModelConfig(), array(
            "workPeriodEnabled" => true,
            "fields" => array(
                'cancellation_reason' => true,
                "date_processed_timestamp" => true
            ),
            "buttons" => array(
                "destroy" => true,
                "active" => true
            ),
            "forms" => array(
                "attachment" => false,
                "cancel" => true,
                "active" => true,
                "archive" => false,
                "destroy" => true,
                "cancellation-reason" => true
            ),
            "filters" => array(
                "date_processed" => true,
            ),
            "excel" => array(
                "date_processed" => false,
                'process_delay' => false,
            )
        ));
    }

    /**
     * Пользовательский метод, подготавливает конфигурацию модели контроллера
     */
    protected function _setUpModel()
    {
        $configuration = $this->_getModel()->getConfiguration();

        $configuration->setRecipient(RecipientFactory::getInstance()->createFromRecipientName('payment'));
        $configuration->setTicketStatusInt(TicketStatus::STATUS_PENDING_CANCEL);
    }
}