<?php
require realpath(__DIR__).'/AbstractController.php';

use Finance_List_Payment_AbstractController as PaymentController;
use App_Finance_Ticket_Component_Status_Type_Default_Status as TicketStatus;
use App_Finance_Recipient_Factory as RecipientFactory;

class Finance_List_Payment_ArchivedController extends PaymentController
{
    /**
     * Возвращает конфигурацию для модели контроллера
     * @return mixed
     */
    protected function _getModelConfig()
    {
        return array_replace_recursive($this->_getDefaultModelConfig(), array(
            "workPeriodEnabled" => true,
            "fields" => array(
                'cancellation_reason' => true,
                'deleted_flag' => true,
                "date_processed_timestamp" => true
            ),
            "buttons" => array(
                "cancel" => false,
                "setviewed" => false,
                "setnotviewed" => false
            ),
            "forms" => array(
                "main" => true,
                "archive" => false,
                "cancel" => false,
                "details" => true,
                "attachment" => false,
                "cancellation-reason" => true
            ),
            "excel" => array(
                "orgstructure" => false
            ),
            "filters" => array(
                "deleted_flag" => true
            )
        ));
    }

    /**
     * Пользовательский метод, подготавливает конфигурацию модели контроллера
     */
    protected function _setUpModel()
    {
        $configuration = $this->_getModel()->getConfiguration();

        $configuration->setRecipient(RecipientFactory::getInstance()->createFromRecipientName('payment'));
        $configuration->setTicketStatuses(array(
            TicketStatus::STATUS_ARCHIVED,
            TicketStatus::STATUS_DELETED
        ));
        $configuration->setFilterDefaultSortColumn('date_processed_timestamp');
        $configuration->setFilterDefaultSortType('DESC');
        $this->_getFilter()->registerResultPostProcess(function (&$result) {
            /** @var $ticketViewedDb App_Db_TicketViewed */
            $ticketViewedDb = App_Db::get(DB_TICKET_VIEWED);
            $viewedTicketsIds = $ticketViewedDb->getViewedTicketsIds('payment');
            $checkIsNotEmpty = is_array($viewedTicketsIds) && !empty($viewedTicketsIds);
            foreach($result as $rowData) {
                $rowData->is_viewed = $checkIsNotEmpty && in_array($rowData->id, $viewedTicketsIds);
            }
        });
    }
}