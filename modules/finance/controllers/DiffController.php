<?php
use App_Finance_Ticket_Factory as TicketFactory;
use App_Finance_Service as FinanceService;

class Finance_DiffController extends Zend_Controller_Action
{
    public function init()
    {
        $this->_getHeadScript()->appendJsFiles(array(
            '/js/finance/diff.js',
            '/js/jquery/jquery.ajaxStatus.js',
            '/js/jquery/jquery.ajaxHandler.js'
        ));

        $this->_getHeadLink()->appendCssFiles(array(
            '/css/sass/finance/finance.css'
        ));
    }

    /**
     * Форма "было-стало" либо редирект на заявку, если в заявке не было изменений с момента создания запроса
     * в складской саппорт/даты последнего просмотра формы начальником склада
     */
    public function indexAction()
    {
        $this->_helper->layout->setLayout('no-menu');
        $ticketId = (int) $this->_getParam('ticketId');
        $ticket = TicketFactory::getInstance()->createFromTicketId($ticketId);

        /** @var $claimDbRepository App_Db_Claims */
        $claimDbRepository = App_Db::get(DB_CLAIMS);
        $claimUrl = $claimDbRepository->getUrl($ticket->getClaimId());

        $modificationHandler = $ticket->getModificationHandler()->getClaim();
        $hasModifications = $modificationHandler->hasModifications();

        if(!($hasModifications)) {
            if(!($modificationHandler->hasFixedEventId())) {
                $modificationHandler->updateFixedEventId(true);
            }

            $this->_redirect($claimUrl);
        }

        if(is_null($modificationHandler->getFixedEventId())) {
            $modificationHandler->updateFixedEventId();
        }

        $timeOrig = $modificationHandler->getFixedTimestamp();
        $timeHead = $modificationHandler->getActualTimestamp();

        $timeOrigString = new Zend_Date($timeOrig, Zend_Date::TIMESTAMP);
        $timeHeadString = new Zend_Date($timeHead, Zend_Date::TIMESTAMP);

        $this->view->assign(array(
            'diffModel' => array(
                'ticketId' => $ticketId,
                'claimId' => $ticket->getClaimId(),
                'claimUrl' => $claimUrl,
                'claimUrlOrig' => $claimUrl.'/?diff=1&version=orig',
                'claimUrlHead' => $claimUrl.'/?diff=1&version=head',
                'timeOrig' => $timeOrig,
                'timeHead' => $timeHead,
                'timeOrigString' => $timeOrigString->toString(App_Db::ZEND_DATETIME_RU_FORMAT),
                'timeHeadString' => $timeHeadString->toString(App_Db::ZEND_DATETIME_RU_FORMAT),
            )
        ));
    }

    /**
     * Обновляет номер версии у заявки
     */
    public function updateAction()
    {
        try {
            $claimId = (int) $this->_getParam('claimId');
            $ticket = FinanceService::getInstance()->getUtil()->getActiveDepotTicket($claimId);

            if($ticket) {
                $ticket->getModificationHandler()->getClaim()->updateFixedEventId(true);
            }

            $jsonData = array('success' => true);
        }
        catch(\Exception $e) {
            $jsonData = array(
                "success" => false,
                "error" => $e->getMessage()
            );
        }

        header('Content-Type: application/json');
        die(json_encode($jsonData));
    }

    /**
     * Подтвердает запрос в складской саппорт
     */
    public function approuveAction()
    {
        try {
            $claimId = (int) $this->_getParam('claimId');
            $ticket = FinanceService::getInstance()->getUtil()->getActiveDepotTicket($claimId);

            if($ticket) {
                $ticket->getStatusHandler()->setArchived();
                $ticket->save();
            }

            $jsonData = array('success' => true);
        }
        catch(\Exception $e) {
            $jsonData = array(
                "success" => false,
                "error" => $e->getMessage()
            );
        }

        header('Content-Type: application/json');
        die(json_encode($jsonData));
    }
}