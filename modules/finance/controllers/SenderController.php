<?php
use App_Finance_Sender_FormGateway as SenderFormGateway;

class Finance_SenderController extends Zend_Controller_Action
{
    /**
     * Шлюз
     * @var SenderFormGateway
     */
    protected $_senderFormGateway;

    /**
     * Возвращает список маршрутов
     */
    public function routesAction()
    {
        $formGateway = $this->getSenderFormGateway();
        $jsonData = $formGateway->getRoutes($this->_getParam('claimId'));

        header('Content-Type: application/json');
        exit(json_encode($jsonData));
    }

    /**
     * Рассылает тикеты по отмеченным маршрутам
     * Также отправляет информацию по заблокированным маршрутам
     */
    public function performAction()
    {
        $formGateway = $this->getSenderFormGateway();
        $jsonData = $formGateway->sendRoutes($this->_getParam('claimId'), $_POST);

        header('Content-Type: application/json');
        exit(json_encode($jsonData));
    }

    /**
     * Возвращает шлюз
     * @return App_Finance_Sender_FormGateway
     */
    public function getSenderFormGateway()
    {
        if(!($this->_senderFormGateway instanceof SenderFormGateway)) {
            $this->_senderFormGateway = new SenderFormGateway();
        }

        return $this->_senderFormGateway;
    }
}