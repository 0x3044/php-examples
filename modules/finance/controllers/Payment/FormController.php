<?php
use Finance_Model_Payment_FormHelper as FormHelper;
use App_Finance_Ticket_Type_Payment_Form_Data_Form as Form;

class Finance_Payment_FormController extends Zend_Controller_Action
{
    /**
     * Возвразает JSON-ответом данные для формы
     * Опционально можно передать Id запроса для загрузки данных запроса
     */
    public function getAction()
    {
        $jsonData = array();
        $ticketId = (int) $this->_getParam('ticketId');

        try {
            $formHelper = new FormHelper($ticketId);
            $form = $formHelper->getPaymentForm();

            $jsonData['ticketData'] = $form->toArray(true);

            $jsonData = array_merge($jsonData, array(
                'success' => true,
                'acl' => array(
                    'documents' => App_Finance_Ticket_Type_Payment_Form_DataSource::getInstance()->getDocumentsAcl($form->getTicket()),
                    'waste' => App_Finance_Ticket_Type_Payment_Form_DataSource::getInstance()->getWasteAcl($form->getTicket())
                )
            ));
        }catch(\Exception $e){
            $jsonData = array(
                'success' => false,
                'error' => $e->getMessage(),
                'trace' => $e->getTrace()
            );
        }

        header('Content-Type: application/json');
        die(json_encode($jsonData));
    }

    /**
     * Возвращает список клиентов для указанного типа оплаты
     */
    public function getClientsAction()
    {
        $paymentTypeId = (int) $this->_getParam('paymentType');

        try {
            $jsonData = array(
                'success' => true,
                'clients' => App_Finance_Ticket_Type_Payment_Form_DataSource::getInstance()->getAvailableClients($paymentTypeId)
            );
        }catch(\Exception $e){
            $jsonData = array(
                'success' => false,
                'error' => $e->getMessage(),
                'trace' => $e->getTrace()
            );
        }

        header('Content-Type: application/json');
        die(json_encode($jsonData));
    }

    /**
     * Возвращает список заявок
     */
    public function getClaimsAction()
    {
        $paymentTypeId = $this->_getParam('paymentType');
        $clientId = $this->_getParam('clientId');
        $dataSource = new App_Finance_Ticket_Type_Payment_Form_DataSource();

        try {
            $jsonData = array(
                'success' => true,
                'html' => $dataSource->getClaims($paymentTypeId, $clientId, $this->_getAllParams())
            );
        }catch(\Exception $e){
            $jsonData = array(
                'success' => false,
                'error' => $e->getMessage()
            );
        }

        header('Content-Type: application/json');
        die(json_encode($jsonData));
    }

    /**
     * Возвращает похожие запросы
     * @return array|bool
     */
    public function getSimilarTicketsAction()
    {
        $jsonData = array(
            'records' => array(),
            'success' => true
        );

        try {
            /** @var $ticketTypeDbTable App_Db_TicketType */
            $ticketTypeDbTable = App_Db::get(DB_TICKET_TYPE);

            $documentNumber = $this->_getParam('documentNumber');
            $fromDate = $this->_getParam('fromDate');
            $paymentType = $this->_getParam('paymentType');
            $clientId = (int) $this->_getParam('clientId');
            $ticketTypeIds = $ticketTypeDbTable->getTicketTypeIdByName('payment_request');
            $excludeTicketId = (int) $this->_getParam('ticketId');

            if($clientId > 0 && strlen($fromDate) && strlen($documentNumber) && $paymentType > 0) {
                $fromDate = new Zend_Date($fromDate, App_Db::ZEND_DATE_RU_FORMAT);

                $bind = array(
                    'fromDate' => $fromDate->toString(App_Db::ZEND_DATE_FORMAT),
                    'paymentType' => $paymentType,
                    'clientId' => $clientId,
                    'documentNumber' => $documentNumber,
                );

                $sqlQuery = <<<SQL
                      SELECT
                        ticket.id,
                        UNIX_TIMESTAMP(ticket.date_created) as date,
                        ticket_payment.sum as sum,
                        users.name as author
                      FROM ticket
                      LEFT JOIN ticket_payment ON ticket.id = ticket_payment.ticket_id
                      LEFT JOIN users ON users.id = ticket.author_user_id
                      WHERE ticket.ticket_type_id IN ({$ticketTypeIds})
                         AND ticket_payment.from_date = :fromDate
                         AND ticket_payment.client_id = :clientId
                         AND ticket_payment.payment_type = :paymentType
                         AND ticket_payment.document_number = :documentNumber
SQL;

                if($excludeTicketId > 0) {
                    $sqlQuery .= ' AND ticket.id != :excludeTicketId';
                    $bind['excludeTicketId'] = $excludeTicketId;
                }

                if($result = App_Db::get()->query($sqlQuery, $bind)->fetchAll(Zend_Db::FETCH_ASSOC)) {
                    $total = 0;

                    // Постобработка - дата из формата TIMESTAMP в формат RU
                    foreach($result as &$row) {
                        $total = bcadd($total, (float) $row['sum'], 2);

                        $zendDate = new Zend_Date((int) $row['date']);
                        $row['date'] = $zendDate->toString(App_Db::ZEND_DATE_RU_FORMAT);
                        $row['link'] = '/finance/ticket/gateway/go?ticketId='.$row['id'];
                        $row['sum'] = number_format($row['sum'], 2);
                    }

                    $jsonData = array(
                        'records' => $result,
                        'total' => number_format($total, 2),
                        'success' => true
                    );
                }
            }
        }catch(\Exception $e){
            $jsonData = array(
                'success' => false,
                'error' => $e->getMessage()
            );
        }

        header('Content-Type: application/json');
        die(json_encode($jsonData));
    }

    /**
     * Сохранение формы
     */
    public function postAction()
    {
        try {
            $formHelper = new FormHelper(0);

            $form = $formHelper->getPaymentForm();
            $form->enableAcl();

            $formReader = new App_Finance_Ticket_Type_Payment_Form_Reader();
            $formReader->formFromArray($form, $this->_getAllParams());

            $formHelper->createTicket();

            $jsonData = array(
                'ticketId' => $form->getTicket()->getId(),
                'success' => true
            );
        }catch(\Exception $e){
            $jsonData = array(
                'success' => false,
                'error' => $e->getMessage(),
                'trace' => $e->getTrace()
            );
        }

        header('Content-Type: application/json');
        die(json_encode($jsonData));
    }
}