<?php

class Finance_IndexController extends Zend_Controller_Action
{
    /**
     * Список доступных саппортов
     */
    public function indexAction()
    {
        $recipientListHelper = new Finance_View_Helper_RecipientIndexList();

        $this->view->assign(array(
            'recipientList' => $recipientListHelper->getRecipients()
        ));
    }

    /**
     * Тестовый экшен
     */
    public function testAction()
    {
    }

    public function testSubmitAction()
    {
        $raiseError = $this->_getParam('raise-error');

        $formHandler = App_Form_AjaxForm_Factory::create(function(App_Form_AjaxForm_Form_AbstractForm $form) use($raiseError){
            sleep(1);

            if($raiseError) {
                $form->error("[Ошибка: вы чекнули флаг]");
            }

            return true;
        });

        $formHandler->process();
        $formHandler->sendJSONResponse();
    }
}