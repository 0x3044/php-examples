<?php
use App_Finance_Recipient_Factory as RecipientFactory;

class Finance_Bootstrap extends Zend_Application_Module_Bootstrap
{
    protected function _initRoutes() {
        $this->bootstrap('frontController');

        /** @var $frontController Zend_Controller_Front */
        $frontController = $this->getResource('frontController');

        /** @var $router Zend_Controller_Router_Rewrite */
        $router = $frontController->getRouter();

        // add /finance/list route
        $router->addRoute("finance_list", new Zend_Controller_Router_Route(
            "/finance/list",
            array(
                'module' => 'finance',
                'controller' => "index",
                'action' => 'index'
            )
        ));

        // add /finance/diff route
        $router->addRoute("finance_diff", new Zend_Controller_Router_Route(
            "/finance/diff/ticket/:ticketId",
            array(
                'module' => 'finance',
                'controller' => "diff",
                'action' => 'index'
            )
        ));

        // add service note routes
        $router->addRoute("finance_service_note", new Zend_Controller_Router_Route(
            "/finance/ticket/service-note/:action",
            array(
                'module' => 'finance',
                'controller' => "ticket_service-note",
            )
        ));

        // add ticket routes
        foreach(array('document', 'status', 'history', 'gateway', 'print', 'claim', 'destroy') as $action) {
            $router->addRoute("finance_ticket_{$action}", new Zend_Controller_Router_Route(
                "/finance/ticket/{$action}/:action",
                array(
                    'module' => 'finance',
                    'controller' => "ticket_{$action}"
                )
            ));
        }

        // add list routes
        foreach(RecipientFactory::getInstance()->getRecipients() as $recipient) {
            // add /finance/list/recipient route
            $router->addRoute("finance_list_{$recipient}", new Zend_Controller_Router_Route(
                "/finance/list/{$recipient}/",
                array(
                    'module' => 'finance',
                    'controller' => "list_redirect",
                    'action' => 'index',
                    'recipient' => $recipient
                )
            ));

            // add /finance/list/recipient/status/[...] routes
            foreach(array('active', 'archived', 'canceled', 'pending-cancel') as $status) {
                $router->addRoute("finance_list_{$recipient}_{$status}", new Zend_Controller_Router_Route(
                    "/finance/list/{$recipient}/{$status}/:action",
                    array(
                        'module' => 'finance',
                        'controller' => "list_{$recipient}_{$status}",
                        'action' => 'index'
                    )
                ));
            }
        }

        // add payment request form
        $router->addRoute('finance_payment_form', new Zend_Controller_Router_Route(
            '/finance/payment/form/:action',
            array(
                'module' => 'finance',
                'controller' => "payment_form",
            )
        ));
    }
}