<?php
use App_Finance_Recipient_Factory as RecipientFactory;

class Finance_View_Helper_RecipientIndexList
{
    public function getRecipients()
    {
        $recipientViewList = array();
        $recipientList = RecipientFactory::getInstance()->getRecipients();

        foreach($recipientList as $recipientName) {
            $recipient = RecipientFactory::getInstance()->createFromRecipientName($recipientName);

            if($recipient->hasAuthorization()) {
                $recipientViewList[$recipientName] = $recipient;
            }
        }

        return $recipientViewList;
    }
}