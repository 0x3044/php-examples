<?php
class Finance_Model_Filter_Factory
{
    /**
     * Возвращает фильтр в зависимости от получателя
     * @param $recipient
     * @param array $options
     * @throws Exception
     * @return Finance_Model_Filter_Abstract
     */
    public static final function getFilter($recipient, array $options = null) {
        switch(strtolower($recipient)) {
            default:
                throw new \Exception("Недоступен фильтр для получателя `{$recipient}`");

            case 'accounting':
                return new Finance_Model_Filter_Accounting($options);

            case 'dispatcher':
                return new Finance_Model_Filter_Dispatcher($options);

            case 'depot':
                return new Finance_Model_Filter_Depot($options);

            case 'payment':
                return new Finance_Model_Filter_Payment($options);
        }
    }
}