<?php

/**
 * Замена ORDER-выражений
 */
class Finance_Model_Filter_Decorator_MultiSort extends App_Filter_Component_Decorator_Type_AbstractType_Decorator
{
    protected $_orderFields = array();

    public function __construct($orderFields = array())
    {
        $this->_orderFields = $orderFields;
    }

    public function onBeforeSqlFormat()
    {
        if(is_array($this->_orderFields) && !empty($this->_orderFields)) {
            $orderExpression = $this->getFilter()->getSqlFormatter()->getOrderExpression();
            $orderExpression->clear();
            foreach($this->_orderFields as $orderFieldName => $orderFieldType) {
                $method_name = $orderFieldName;
                $method_name[0] = strtoupper($method_name[0]);
                $func = create_function('$c', 'return strtoupper($c[1]);');
                $method_name = 'add'.preg_replace_callback('/_([a-z])/', $func, $method_name).'Expression';
                if(method_exists($this, $method_name)) {
                    $this->$method_name($orderExpression, $orderFieldName, $orderFieldType);
                } else {
                    $orderExpression->add($orderFieldName, $orderFieldType);
                }
            }
        }
    }
}