<?php

class Finance_Model_Filter_Decorator_MultiSort_Depot extends Finance_Model_Filter_Decorator_MultiSort
{
    /**
     * Подменяет выражение ORDER для поля cash_planned_state
     * @param App_Filter_Component_SqlFormatter_OrderExpression $orderExpression
     * @param type $orderFieldName Имя поля сортировки
     * @param type $orderFieldType Тип сортировки
     */
    public function addCashPlannedStateExpression($orderExpression, $orderFieldName, $orderFieldType)
    {
        if($orderFieldType == 'ASC' || $orderFieldType == 'DESC') {
            $orderExpression->add($orderFieldName, $orderFieldType);
        } else if(is_array($orderFieldType)) {
            foreach($orderFieldType as $key => $val) {
                $cashStates = explode(',', $val);
                $orders = array();
                foreach($cashStates as $cashState) {
                    $orders[] = $orderFieldName.'='.$cashState;
                }
                $orderExpression->add(implode(' OR ', $orders), 'ASC');
                $orderExpression->add('FIELD('.$orderFieldName.','.$val.')', 'ASC');
            }
        }
    }

    /**
     * Подменяет выражение ORDER для поля client_name
     * @param App_Filter_Component_SqlFormatter_OrderExpression $orderExpression
     * @param type $orderFieldName Имя поля сортировки
     * @param type $orderFieldType Тип сортировки
     */
    public function addClientNameExpression($orderExpression, $orderFieldName, $orderFieldType)
    {
        $orderExpression->add('client_u_title', $orderFieldType);
        $orderExpression->add('client_s_title', $orderFieldType);
    }

    /**
     * Подменяет выражение ORDER для поля date_created_timestamp
     * @param App_Filter_Component_SqlFormatter_OrderExpression $orderExpression
     * @param type $orderFieldName Имя поля сортировки
     * @param type $orderFieldType Тип сортировки
     */
    public function addDateCreatedTimestampExpression($orderExpression, $orderFieldName, $orderFieldType)
    {
        $orderExpression->add('DATE_FORMAT(date_created, "%Y-%m-%d")', $orderFieldType);
    }

    /**
     * Подменяет выражение ORDER для поля date_to_process_timestamp
     * @param App_Filter_Component_SqlFormatter_OrderExpression $orderExpression
     * @param type $orderFieldName Имя поля сортировки
     * @param type $orderFieldType Тип сортировки
     */
    public function addDateToProcessTimestampExpression($orderExpression, $orderFieldName, $orderFieldType)
    {
        $orderExpression->add('DATE_FORMAT(date_to_process, "%Y-%m-%d")', $orderFieldType);
    }

    /**
     * Подменяет выражение ORDER для поля car_number
     * @param App_Filter_Component_SqlFormatter_OrderExpression $orderExpression
     * @param type $orderFieldName Имя поля сортировки
     * @param type $orderFieldType Тип сортировки
     */
    public function addCarNumberExpression($orderExpression, $orderFieldName, $orderFieldType)
    {
        if($orderFieldType == 'ASC' || $orderFieldType == 'DESC') {
            $orderExpression->add($orderFieldName, $orderFieldType);
        } else if(is_array($orderFieldType)) {
            foreach($orderFieldType as $key => $val) {
                switch($val) {
                    case 'own_selected':
                        $orderExpression->add('claim_has_car = 1 AND car_number IS NOT NULL', 'ASC');
                        $orderExpression->add('car_number', 'ASC');
                        break;
                    case 'own_not_selected':
                        $orderExpression->add('claim_has_car = 1 AND car_number IS NULL', 'ASC');
                        break;
                    case 'not_own':
                        $orderExpression->add('claim_has_car = 0', 'ASC');
                        break;
                    default:
                        break;
                }
            }
        }
    }
}