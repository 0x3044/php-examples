<?php
use App_Filter_Component_Decorator_Type_AbstractType_Decorator as FilterDecorator;

class Finance_Model_Filter_Decorator_CarManager extends FilterDecorator
{
    /**
     * Фильтр для машины "менеджер"
     */
    public function onBeforeSqlFormat()
    {
        $whereConditions = $this->getFilter()->getSqlFormatter()->getWhereConditions();

        foreach($whereConditions->getItems() as $condition) {
            if(preg_match('/\bcar in \(/', $condition)) {
                preg_match_all('/\d/', $condition, $carChoices);
                $carChoices = $carChoices[0];

                if(in_array(2, $carChoices)) {
                    if(!(in_array(0, $carChoices))) {
                        $whereConditions->add('car_manager = 1');
                    }

                    $carChoices[] = 0;
                }else{
                    $whereConditions->add('car_manager = 0');
                }

                $carChoices = implode(',', array_unique($carChoices));

                $whereConditions->remove($condition);
                $whereConditions->add("car in ({$carChoices})");
            }
        }
    }
}