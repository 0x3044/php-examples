<?php
use Finance_Model_Filter_Abstract as Filter;
use App_Filter_Component_Decorator_Type_Search_Decorator as SearchDecorator;

class Finance_Model_Filter_Depot extends Filter
{
    /**
     * Инициализация фильтра. Данный метод можно наследовать для настройки масок, конфигов, пост-процессинга
     * результатов, etc
     */
    public function init()
    {
        parent::init();

        $this->_initDecorators();

        $postProcessLibrary = $this->getPostProcessLibrary();

        if(!($this->isCountOnly())) {
            $this->_getResultBehaviour()->appendPostProcessCallback(function(&$result) use ($postProcessLibrary){
                $postProcessLibrary->postProcessCashPlanned($result);
            });
        }
    }

    /**
     * Инициализация декораторов
     */
    protected function _initDecorators()
    {
        $orderFields = Zend_Controller_Front::getInstance()->getRequest()->getParam('sortFields', array());

        if(!empty($orderFields)) {
            $multiSortDecorator = new Finance_Model_Filter_Decorator_MultiSort_Depot($orderFields);
            $carManagerDecorator = new Finance_Model_Filter_Decorator_CarManager();
            $searchDecorator = new SearchDecorator('annotation_depot');

            $this->addDecorator($multiSortDecorator);
            $this->addDecorator($carManagerDecorator);
            $this->addDecorator($searchDecorator);
        }
    }

    /**
     * Возвращает основной SQL-запрос
     * @return string
     */
    protected function _getSqlQuery()
    {
        $noStateId = Cash_Model_Planned_State::STATE_NO_STATE;

        /** @var $ticketViewedDb App_Db_TicketViewed */
        $ticketViewedDb = App_Db::get(DB_TICKET_VIEWED);
        $viewedTicketsIds = $ticketViewedDb->getViewedTicketsIds('depot');

        if(empty($viewedTicketsIds)) {
            $viewedTicketsIds = array(0);
        }
        $viewedTicketsIds = implode(',', $viewedTicketsIds);

        if($this->isCountOnly()) {
            return <<<SQL
                SELECT
                    t.`id`,
                    t.`author_user_id`,
                    t.`ticket_type_id`,
                    t.`status` AS status,
                    '' as client_s_title,
                    '' as client_u_title
                FROM
                    ticket t
                LEFT JOIN ticket_type tt ON t.`ticket_type_id` = tt.`id`
SQL;
        }else{
            return <<<SQL
                SELECT
                    t.`id`,
                    u.`name` AS manager_name,
                    claims.`manager_id` AS manager_id,
                    o.`title` AS orgstructure,
                    o.`id` AS orgstructure_id,
                    t.`claim_id`,
                    t.`date_created`,
                    t.`date_to_process`,
                    t.`date_processed`,
                    claims.`url` AS claim_url,
                    claims.`full_id` AS claim_full_id,
                    claims.`payment` AS cashType,
                    claims.`claim_status` AS claim_status_id,
                    claims.`car_manager`,
                    claims.`annotation_depot` AS claim_annotation_depot,
                    st.`status_title` AS claim_status,
                    CASE claims.connectivity
                      WHEN 0 THEN claims.id
                      WHEN NULL THEN claims.id
                      ELSE concat(claims.id, ',', claims.connectivity)
                    END AS connectivity_claim_id,
                    ctp.show_connectivity,
                    DATE_FORMAT(t.`date_created`, "%d.%m.%Y %H:%i") AS ru_date_created,
                    DATE_FORMAT(t.`date_to_process`, "%d.%m.%Y %H:%i") AS ru_date_to_process,
                    DATE_FORMAT(t.`date_processed`, "%d.%m.%Y %H:%i") AS ru_date_processed,
                    clients.`id` AS client_id,
                    clients.`u_title` AS client_u_title,
                    clients.`s_title` AS client_s_title,
                    CASE LENGTH(clients.`u_title`)
                          WHEN 0 THEN clients.`s_title`
                          ELSE clients.`u_title`
                    END as client_name,
                    UNIX_TIMESTAMP(t.`date_to_process`) AS date_to_process_timestamp,
                    UNIX_TIMESTAMP(t.`date_created`) AS date_created_timestamp,
                    UNIX_TIMESTAMP(t.`date_processed`) AS date_processed_timestamp,
                    UNIX_TIMESTAMP(NOW()) AS mysql_current_timestamp,
                    t.`status`,
                    t.`ticket_type_id`,
                    t.`is_completed`,
                    claims.`type_id` AS claim_type_id,
                    ctp.`title` AS claim_type_name,
                    claims.`car_number` AS car_number_id,
                    claims.`car` claim_has_car,
                    CASE claims.`car`
                      WHEN 1 THEN drivers.`carNumber`
                      ELSE NULL
                    END AS car_number,
                    CASE COUNT(cp.`cash_type`)
                      WHEN 0 THEN {$noStateId}
                      ELSE cp.`state`
                    END AS cash_planned_state,
                    claims.`transport_delivery_time`,
                    IF (tmf.`has_claim_modifications` IS NOT NULL, tmf.`has_claim_modifications`, 0) AS claim_changes,
                    IF(t.id IN ({$viewedTicketsIds}), 1, 0) AS is_viewed
                  FROM
                    ticket t
                LEFT JOIN ticket_type tt ON t.`ticket_type_id` = tt.`id`
                LEFT JOIN claims ON t.`claim_id` = claims.`id`
                LEFT JOIN clients ON claims.`client_id` = clients.`id`
                LEFT JOIN users u ON claims.`manager_id` = u.`id`
                LEFT JOIN orgstructure_users ou ON claims.`manager_id` = ou.`userId`
                LEFT JOIN orgstructure o ON o.`id` = ou.`oId`
                LEFT JOIN drivers ON drivers.`id`= claims.`car_number`
                LEFT JOIN cash_planned cp ON cp.`reason` = claims.`id`
                LEFT JOIN log_main_latest_event lmle ON lmle.`object_id` = t.`claim_id` AND lmle.`event_module_id` = 2
                LEFT JOIN ticket_modified_fields tmf ON tmf.`ticket_id` = t.`id`
                LEFT JOIN status_type AS st ON st.`id` = claims.`claim_status`
                LEFT JOIN claim_type_pair ctp ON ctp.`type_id` = claims.`type_id`
                GROUP BY t.`id`
SQL;
        }
    }

    /**
     * Возвращает SQL-запрос для маски
     * @return string
     */
    public function getMaskSqlQuery()
    {
        $sqlQuery = <<<SQL
            SELECT DISTINCT result.expr_fields, result.date_created FROM (
              {$this->_getSqlQuery()}
            ) result
SQL;

        return str_replace("expr_fields", "{EXPR_FIELD}", $sqlQuery.' {WHERE} {HAVING}');
    }

    /**
     * Возвращает основной SQL-запрос
     * @return string
     */
    public function getResultSqlQuery()
    {
        $sqlQuery = <<<SQL
          SELECT result.* FROM (
            {$this->_getSqlQuery()}
        ) result
SQL;

        return $sqlQuery."{WHERE} {HAVING} {ORDER} {LIMIT}";
    }

    /**
     * Возвращает SQL-запрос для подсчета количества строк в результате
     * @return string
     */
    public function getResultCountSqlQuery()
    {
        $sqlCountQuery = "SELECT count(*) FROM (RESULT) s";

        return str_replace("RESULT", "{RESULT}", $sqlCountQuery);
    }

    /**
     * Возвращает массив с типами тикетов, с которыми данный фильтр работает
     * @return array
     */
    function getTicketTypes()
    {
        return array('depot_verify');
    }
}