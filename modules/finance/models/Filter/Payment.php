<?php
use Finance_Model_Filter_Abstract as Filter;
use App_Filter_Component_Decorator_Type_AclManagers_Decorator as AclManagersDecorator;
use App_Filter_Component_Decorator_Type_Search_Decorator as SearchDecorator;

class Finance_Model_Filter_Payment extends Filter
{
    /**
     * Фильтр используется только для посчета строк
     * @var bool
     */
    protected $_countOnly = false;

    /**
     * Инициализация фильтра. Данный метод можно наследовать для настройки масок, конфигов, пост-процессинга
     * результатов, etc
     */
    public function init() {
        parent::init(array(
            'useLegalClientName' => true
        ));

        $clientDecorator = new AclManagersDecorator();
        $clientDecorator->setAclPath('finance>payment>payment_request>managers');
        $clientDecorator->setFilterFieldManager('author_user_id');
        $clientDecorator->setFilterFieldDepartment('orgstructure_id');
        $this->addDecorator($clientDecorator);

        $searchDecorator = new SearchDecorator('payment_annotation');
        $this->addDecorator($searchDecorator);

        if(!($this->isCountOnly())) {
            $postProcessLibrary = $this->getPostProcessLibrary();

            $this->_getResultBehaviour()->appendPostProcessCallback(function(&$result) use ($postProcessLibrary){
                $postProcessLibrary->postProcessServiceNote($result);
                $postProcessLibrary->postProcessClientName($result);
                $postProcessLibrary->postProcessPaymentType($result);
                $postProcessLibrary->postProcessMaxLength($result, 'payment_annotation', 100);
                $postProcessLibrary->postProcessCancellationReason($result);
                $postProcessLibrary->postProcessAppendFirstFileBrowsableFlag($result);
            });
        }
    }

    /**
     * Возвращает основной SQL-запрос
     * @return string
     */
    protected function _getSqlQuery()
    {
        $deletedStatus = App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_DELETED;

        if($this->isCountOnly()) {
            return <<<SQL
                SELECT
                    t.`id`,
                    t.`ticket_type_id`,
                    t.`status` AS STATUS,
                    t.`author_user_id`,
                    o.`id` AS orgstructure_id,
                    '' as client_s_title,
                '' as client_u_title
                FROM
                    ticket t
                LEFT JOIN ticket_type tt ON t.`ticket_type_id` = tt.`id`
                LEFT JOIN orgstructure_users ou ON t.`author_user_id` = ou.`userId`
                LEFT JOIN orgstructure o ON o.`id` = ou.`oId`
                GROUP BY t.`id`
SQL;
        }else{
            return <<<SQL
                SELECT
                    t.`id`,
                    t.`ticket_type_id`,
                    t.`status` AS STATUS,
                    t.`json_data` AS ticket_json_data,
                    COUNT(DISTINCT td.`id`) AS num_ticket_documents,
                    t.`date_created`,
                    t.`date_to_process`,
                    t.`date_processed`,
                    t.`is_completed`,
                    t.`author_user_id`,
                    u.`name` as author_user_name,
                    t.`json_data` as `json_data`,
                    o.`id` AS orgstructure_id,
                    o.`title` AS orgstructure,
                    DATE_FORMAT(t.`date_created`, "%d.%m.%Y %H:%i") AS ru_date_created,
                    DATE_FORMAT(t.`date_to_process`, "%d.%m.%Y %H:%i") AS ru_date_to_process,
                    DATE_FORMAT(t.`date_processed`, "%d.%m.%Y %H:%i") AS ru_date_processed,
                    CASE
                        WHEN t.date_processed IS NULL THEN 0
                        WHEN t.`date_processed` > t.`date_to_process` THEN UNIX_TIMESTAMP(t.`date_processed`)-UNIX_TIMESTAMP(t.`date_to_process`)
                        ELSE 0
                    END AS process_delay,
                    UNIX_TIMESTAMP(t.`date_to_process`) AS date_to_process_timestamp,
                    UNIX_TIMESTAMP(t.`date_created`) AS date_created_timestamp,
                    UNIX_TIMESTAMP(t.`date_processed`) AS date_processed_timestamp,
                    UNIX_TIMESTAMP(NOW()) AS mysql_current_timestamp,
                    tt.`description` AS ticket_type,
                    tp.`sum` as payment_sum,
                    tp.`annotation` as payment_annotation,
                    tp.`payment_type` as payment_type,
                    tp.`payment_type` as payment_type_name,
                    tp.`document_number` as payment_document_number,
                    tp.`from_date` as payment_from_date,
                    DATE_FORMAT(tp.from_date, "%d.%m.%Y") AS ru_payment_from_date,
                    tp.`client_id` as client_id,
                    c.`u_title` AS client_u_title,
                    c.`s_title` AS client_s_title,
                    tsn.`filename` AS ticket_service_note_file,
                    CASE t.`status`
                      WHEN {$deletedStatus} THEN 1
                      ELSE 0
                    END AS deleted_flag
                FROM
                    ticket t
                LEFT JOIN ticket_type tt ON t.`ticket_type_id` = tt.`id`
                LEFT JOIN ticket_document td ON td.`ticket_id` = t.`id`
                LEFT JOIN ticket_service_note tsn ON tsn.`ticket_id` = t.`id`
                LEFT JOIN ticket_payment tp ON tp.`ticket_id` = t.`id`
                LEFT JOIN clients c ON c.`id` = tp.`client_id`
                LEFT JOIN orgstructure_users ou ON t.`author_user_id` = ou.`userId`
                LEFT JOIN orgstructure o ON o.`id` = ou.`oId`
                LEFT JOIN users u ON t.`author_user_id` = u.`id`
                GROUP BY t.`claim_id`, t.`id`
SQL;
        }
    }

    /**
     * Возвращает SQL-запрос для маски
     * @return string
     */
    public function getMaskSqlQuery()
    {
        $sqlQuery = <<<SQL
            SELECT DISTINCT result.expr_fields, result.date_created FROM (
              {$this->_getSqlQuery()}
            ) result
SQL;

        return str_replace("expr_fields", "{EXPR_FIELD}", $sqlQuery).' {WHERE} {HAVING}';
    }

    /**
     * Возвращает основной SQL-запрос
     * @return string
     */
    public function getResultSqlQuery()
    {
        $sqlQuery = <<<SQL
          SELECT result.* FROM (
            {$this->_getSqlQuery()}
        ) result
SQL;

        return $sqlQuery." {WHERE} {HAVING} {ORDER} {LIMIT}";
    }

    /**
     * Возвращает SQL-запрос для подсчета количества строк в результате
     * @return string
     */
    public function getResultCountSqlQuery()
    {
        $sqlCountQuery = "SELECT count(*) FROM (RESULT) s";

        return str_replace("RESULT", "{RESULT}", $sqlCountQuery);
    }

    /**
     * Возвращает массив с типами тикетов, с которыми данный фильтр работает
     * @return array
     */
    function getTicketTypes()
    {
        return array('accounting_act_of_reconciliation', 'accounting_waybill');
    }
}