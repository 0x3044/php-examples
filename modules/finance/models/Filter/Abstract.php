<?php
use App_Filter_AbstractFilter as Filter;
use Finance_Model_Filter_Component_PostProcessLibrary as PostProcessLibrary;

abstract class Finance_Model_Filter_Abstract extends Filter
{
    /**
     * @var PostProcessLibrary
     */
    protected $_postProcessLibrary;

    /**
     * Фильтр используется только для посчета строк
     * @var bool
     */
    protected $_countOnly = false;

    /**
     * Инициализация фильтра. Данный метод можно наследовать для настройки масок, конфигов, пост-процессинга
     * результатов, etc
     */
    public function init($options = array())
    {
        $this->disableDynamicFilter();

        $options = $this->getOptions();
        $this->_countOnly = isset($options['countOnly']) ? (bool) $options['countOnly'] : false;

        $this->_postProcessLibrary = new PostProcessLibrary();
        $useClientLegalName = isset($options['useLegalClientName']) ? $options['useLegalClientName'] : false;
        $postProcessLibrary = $this->getPostProcessLibrary();

        $this->_getResultBehaviour()->appendPostProcessCallback(function (&$result) use ($useClientLegalName, $postProcessLibrary) {
            $postProcessLibrary->postProcessTicketAcl($result);
            $postProcessLibrary->postProcessClientName($result, $useClientLegalName);
        });
    }

    /**
     * Возвращает true, если запрос используется только для посчета строк
     * @return bool
     */
    public function isCountOnly()
    {
        return $this->_countOnly;
    }


    /**
     * Возвращает библиотеку постпроцесс-обработки
     * @return \Finance_Model_Filter_Component_PostProcessLibrary
     */
    public function getPostProcessLibrary()
    {
        return $this->_postProcessLibrary;
    }

    /**
     * Возвращает массив с типами тикетов, с которыми данный фильтр работает
     * @return array
     */
    abstract public function getTicketTypes();

    /**
     * Возвращает SQL-запрос для маски
     * @return string
     */
    public function getMaskSqlQuery()
    {
        $sqlQuery = <<<SQL
            SELECT DISTINCT result.expr_fields, result.claim_date FROM (
              {$this->_getSqlQuery()}
            ) result
SQL;

        return str_replace("expr_fields", "{EXPR_FIELD}", $sqlQuery).' {WHERE} {HAVING}';
    }
}