<?php
use Finance_Model_Filter_Abstract as Filter;

class Finance_Model_Filter_Accounting extends Filter
{
    /**
     * Инициализация фильтра. Данный метод можно наследовать для настройки масок, конфигов, пост-процессинга
     * результатов, etc
     */
    public function init() {
        parent::init(array(
            'useLegalClientName' => true
        ));

        if(!($this->isCountOnly())) {
            $carManagerDecorator = new Finance_Model_Filter_Decorator_CarManager();
            $this->addDecorator($carManagerDecorator);

            $postProcessLibrary = $this->getPostProcessLibrary();

            $this->_getResultBehaviour()->appendPostProcessCallback(function(&$result) use ($postProcessLibrary){
                $postProcessLibrary
                    ->postProcessInvoices($result)
                    ->postProcessClientCard($result)
                    ->postProcessDocumentsViewed($result)
                    ->postProcessServiceNote($result)
                    ->postProcessDelay($result)
                    ->postProcessClientDeliveryFact($result)
                    ->postProcessExplanationNote($result)
                    ->postProcessClientContracts($result)
                ;
            });
        }
    }

    /**
     * Возвращает основной SQL-запрос
     * Сделан статичным и публичным для фильтра в аналитике
     * @return string
     */
    public static function getSqlQuery()
    {
        return <<<SQL
            SELECT
                t.`id`,
                t.`author_user_id`,
                t.`ticket_type_id`,
                t.`status` AS status,
                u.`name` AS manager_name,
                claims.`manager_id` AS manager_id,
                claims.`url` AS claim_url,
                claims.`full_id` AS claim_full_id,
                claims.`claim_status` AS claim_status,
                claims.`car` AS car,
                claims.`car_manager`,
                claims.`tk_docs_required`,
                CASE claims.connectivity
                  WHEN 0 THEN claims.id
                  WHEN NULL THEN claims.id
                  ELSE concat(claims.id, ',', claims.connectivity)
                END AS connectivity_claim_id,
                ctp.show_connectivity,
                t.`json_data` AS ticket_json_data,
                COUNT(DISTINCT td.`id`) AS num_ticket_documents,
                DATE(FROM_UNIXTIME(claims.`date`)) AS claim_date,
                claims.date AS claim_date_timestamp,
                CASE claims.`date_closed`
                  WHEN NULL THEN NULL
                  WHEN 0 THEN NULL
                  ELSE FROM_UNIXTIME(claims.`date_closed`)
                END AS claim_date_closed,
                clients.`u_title` AS client_u_title,
                clients.`s_title` AS client_s_title,
                CASE LENGTH(clients.`u_title`)
                      WHEN 0 THEN clients.`s_title`
                      ELSE clients.`u_title`
                END as client_name,
                t.`claim_id`,
                t.`date_created`,
                t.`date_to_process`,
                t.`date_processed`,
                claims.`credit_limit_exceed`,
                t.`is_completed`,
                t.`json_data`,
                DATE_FORMAT(t.`date_created`, "%d.%m.%Y %H:%i") AS ru_date_created,
                DATE_FORMAT(t.`date_to_process`, "%d.%m.%Y %H:%i") AS ru_date_to_process,
                DATE_FORMAT(t.`date_processed`, "%d.%m.%Y %H:%i") AS ru_date_processed,
                CASE
                    WHEN t.date_processed IS NULL THEN 0
                    WHEN t.`date_processed` > t.`date_to_process` THEN UNIX_TIMESTAMP(t.`date_processed`)-UNIX_TIMESTAMP(t.`date_to_process`)
                    ELSE 0
                END AS process_delay,
                clients.`id` AS client_id,
                clients.`client_card` AS client_card,
                UNIX_TIMESTAMP(t.`date_to_process`) AS date_to_process_timestamp,
                UNIX_TIMESTAMP(t.`date_created`) AS date_created_timestamp,
                UNIX_TIMESTAMP(t.`date_processed`) AS date_processed_timestamp,
                UNIX_TIMESTAMP(NOW()) AS mysql_current_timestamp,
                tt.`description` AS ticket_type,
                o.`title` AS orgstructure,
		        o.`id` AS orgstructure_id,
		        GROUP_CONCAT(DISTINCT i.id ORDER BY i.id ASC) AS invoice_ids,
		        tsn.`filename` AS ticket_service_note_file,
		        ten.`filename` AS ticket_explanation_note_file
            FROM
                ticket t
            LEFT JOIN ticket_type tt ON t.`ticket_type_id` = tt.`id`
            LEFT JOIN ticket_document td ON td.`ticket_id` = t.`id`
            LEFT JOIN claims ON t.`claim_id` = claims.`id`
            LEFT JOIN clients ON claims.`client_id` = clients.`id`
            LEFT JOIN users u ON claims.`manager_id` = u.`id`
            LEFT JOIN orgstructure_users ou ON claims.`manager_id` = ou.`userId`
            LEFT JOIN orgstructure o ON o.`id` = ou.`oId`
            LEFT JOIN claim_invoices ci ON ci.`claim_id` = t.`claim_id`
            LEFT JOIN invoice i ON ci.`invoice_id` = i.`prim_id`
            LEFT JOIN claim_type_pair ctp ON ctp.`type_id` = claims.`type_id`
            LEFT JOIN ticket_service_note tsn ON tsn.`ticket_id` = t.`id`
            LEFT JOIN ticket_explanatory_note ten ON ten.`ticket_id` = t.`id`
            GROUP BY t.`claim_id`, t.`id`
SQL;
    }

    /**
     * Возвращает основной SQL-запрос
     * @return string
     */
    protected function _getSqlQuery()
    {
        if($this->isCountOnly()) {
            return <<<SQL
            SELECT
                t.`id`,
                t.`author_user_id`,
                t.`ticket_type_id`,
                t.`status` AS status,
                '' as client_s_title,
                '' as client_u_title
            FROM
                ticket t
            LEFT JOIN ticket_type tt ON t.`ticket_type_id` = tt.`id`
SQL;
        }else{
            return self::getSqlQuery();
        }
    }

    /**
     * Возвращает SQL-запрос для маски
     * @return string
     */
    public function getMaskSqlQuery()
    {
        $sqlQuery = <<<SQL
            SELECT DISTINCT result.expr_fields, result.date_created FROM (
              {$this->_getSqlQuery()}
            ) result
SQL;

        return str_replace("expr_fields", "{EXPR_FIELD}", $sqlQuery).' {WHERE} {HAVING}';
    }

    /**
     * Возвращает основной SQL-запрос
     * @return string
     */
    public function getResultSqlQuery()
    {
        $sqlQuery = <<<SQL
          SELECT result.* FROM (
            {$this->_getSqlQuery()}
        ) result
SQL;

        return $sqlQuery." {WHERE} {HAVING} {ORDER} {LIMIT}";
    }

    /**
     * Возвращает SQL-запрос для подсчета количества строк в результате
     * @return string
     */
    public function getResultCountSqlQuery()
    {
        $sqlCountQuery = "SELECT count(*) FROM (RESULT) s";

        return str_replace("RESULT", "{RESULT}", $sqlCountQuery);
    }

    /**
     * Возвращает массив с типами тикетов, с которыми данный фильтр работает
     * @return array
     */
    function getTicketTypes()
    {
        return array('accounting_act_of_reconciliation', 'accounting_waybill');
    }
}