<?php
use Finance_Model_Filter_Component_ConnectivityFields as ConnectivityFields;

class Finance_Model_Filter_Component_ConnectivityConfiguration
{
    /**
     * @var ConnectivityFields
     */
    protected $_fields;

    /**
     * Id связанной заявки
     * @var int
     */
    protected $_connectivityId;

    /**
     * Флаг "включена/отключена поддержка"
     * @var bool
     */
    protected $_enabled;

    /**
     * Заголовок для верхней части
     * @var string
     */
    protected $_topTitle;

    /**
     * Заголовок для нижней части
     * @var string
     */
    protected $_bottomTitle;

    /**
     * Реверс верхней/нижней части
     * @var bool
     */
    protected $_reverse;

    /**
     * Компонент для фильтра
     * Содержит список полей, которые нужно просматривать/редактировать кроме основного
     * @param $connectivityId
     */
    public function __construct($connectivityId)
    {
        App_Spl_TypeCheck::getInstance()->numeric($connectivityId);

        $this->_connectivityId = $connectivityId;
        $this->_fields = new ConnectivityFields();
    }

    /**
     * Возвращает Id связанной заявки
     * @return int
     */
    public function getConnectivityId()
    {
        return $this->_connectivityId;
    }

    /**
     * Возвращает список полей
     * @return \Finance_Model_Filter_Component_ConnectivityFields
     */
    public function getFields()
    {
        return $this->_fields;
    }

    public function enable()
    {
        $this->_enabled = true;
    }

    public function disable()
    {
        $this->_enabled = false;
    }

    public function isEnabled()
    {
        return $this->_enabled;
    }

    /**
     * @param string $topTitle
     */
    public function setTopTitle($topTitle)
    {
        $this->_topTitle = $topTitle;
    }

    /**
     * @return string
     */
    public function getTopTitle()
    {
        return $this->_topTitle;
    }

    /**
     * @param string $bottomTitle
     */
    public function setBottomTitle($bottomTitle)
    {
        $this->_bottomTitle = $bottomTitle;
    }

    /**
     * @return string
     */
    public function getBottomTitle()
    {
        return $this->_bottomTitle;
    }

    public function enableReverse() {
        $this->_reverse = true;
    }

    public function disableReverse() {
        $this->_reverse = false;
    }

    public function isReverseEnabled() {
        return $this->_reverse;
    }

    /**
     * Сериализация в массив
     * @internal param bool $fieldsAsObject
     * @return array
     */
    public function toArray()
    {
        return array(
            'enabled' => $this->isEnabled(),
            'reversed' => $this->isReverseEnabled(),
            'connectivityId' => $this->getConnectivityId(),
            'topTitle' => $this->getTopTitle(),
            'bottomTitle' => $this->getBottomTitle(),
            'fields' => $this->getFields()->toArray()
        );
    }

    public function toObject()
    {
        return (object) array(
            'enabled' => $this->isEnabled(),
            'reversed' => $this->isReverseEnabled(),
            'connectivityId' => $this->getConnectivityId(),
            'topTitle' => $this->getTopTitle(),
            'bottomTitle' => $this->getBottomTitle(),
            'fields' => (object) $this->getFields()->toArray()
        );
    }
}