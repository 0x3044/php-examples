<?php

class Finance_Model_Filter_Component_ConnectivityFields
{
    /**
     * Поля для редактирования
     * @var array
     */
    protected $_fields = array();

    public function addField($fieldName, $value)
    {
        $this->_fields[$fieldName] = $value;
    }

    public function removeField($fieldName)
    {
        if(array_key_exists($fieldName, $this->_fields)) {
            unset($this->_fields[$fieldName]);
        }
    }

    public function hasField($fieldName)
    {
        return isset($this->_fields[$fieldName]);
    }

    public function getFieldOptions($fieldName)
    {
        if(!($this->hasField($fieldName))) {
            throw new \Exception("Field `{$fieldName}` not found");
        }

        return $this->_fields[$fieldName];
    }

    public function listFields()
    {
        return array_keys($this->_fields);
    }

    public function clear()
    {
        $this->_fields = array();
    }

    public function toArray()
    {
        return $this->_fields;
    }
}