<?php
class Finance_Model_Filter_Component_PostProcessLibrary_Connectivity
{
    /**
     * Пост-процесс
     * @var Finance_Model_Filter_Component_PostProcessLibrary
     */
    protected $_postProcessLibrary;

    /**
     * Постпроцесс - просрочка по выполнению запросов в финансовый саппорт
     * @param $result
     * @param Finance_Model_Filter_Component_PostProcessLibrary $postProcess
     * @throws Exception
     */
    public function run(&$result, Finance_Model_Filter_Component_PostProcessLibrary $postProcess)
    {
        $this->_postProcessLibrary = $postProcess;

        if(is_array($result) && count($result)) {
            $connectivityIds = $this->_getConnectivityMap($result);
            $sourceIds = array_flip($connectivityIds);
            $indexMap = $this->_getIndexMap($result);

            if(count($connectivityIds)) {
                $connectivityResult = $this->_getConnectivityInfo($connectivityIds);

                if(is_array($connectivityResult) && count($connectivityIds)) {
                    foreach($connectivityResult as $connectivityResultRow) {
                        $connectivityId = $connectivityResultRow['claim_id'];
                        $destinationIndex = $indexMap[$sourceIds[$connectivityId]];

                        if(!isset($result[$destinationIndex])) {
                            throw new \Exception('Connectivity failure');
                        }

                        $result[$destinationIndex]->connectivity_configuration = $this->_getConnectivityConfiguration($connectivityResultRow);
                    }
                }
            }
        }
    }

    /**
     * Возвращает карту claim_id => index
     * @param array $result
     * @return array
     */
    protected function _getIndexMap(array &$result)
    {
        $map = array();

        foreach($result as $index => $rowData) {
            if((int) $rowData->connectivity > 0) {
                $map[(int) $rowData->claim_id] = $index;
            }
        }

        return $map;
    }

    /**
     * Возвращает карту claim_id => connectivity
     * @param array $result
     * @return array
     */
    protected function _getConnectivityMap(array &$result)
    {
        $map = array();

        foreach($result as $index => $rowData) {
            if((int) $rowData->connectivity > 0) {
                $map[(int) $rowData->claim_id] = (int) $rowData->connectivity;
            }
        }

        return $map;
    }

    /**
     * Возвращает connectivity-информацию из базы данных
     * @param array $connectivityIds
     * @return array
     */
    protected function _getConnectivityInfo(array $connectivityIds) {
        $connectivityIdsStr = implode(',', $connectivityIds);

        $sqlQuery = <<<SQL
                    SELECT
                        claims.id AS claim_id,
                        claims.connectivity,
                        claims.type_id as claim_type_id,
                        claims.`transport_delivery_time`,
                        claims.`aprox_claim_weight`,
                        claims.`annotation_dispatcher`,
                        clients.`u_title` AS client_u_title,
                        clients.`s_title` AS client_s_title,
                        clients.id AS client_id,
                        claims.unload_region,
                        claims.unload_city,
                        claims.unload_street,
                        claims.unload_building,
                        claims.unload_address,
                        claims.`unload_contact_name` AS unload_contact,
                        clients.`unload_work_time` AS unload_depot_worktime,
                        claims.annotation_dispatcher as claim_annotation_dispatcher
                    FROM claims
                    LEFT JOIN clients ON claims.`client_id` = clients.`id`
                    WHERE claims.id IN ({$connectivityIdsStr})
SQL;

        return Zend_Db_Table_Abstract::getDefaultAdapter()->query($sqlQuery)->fetchAll(Zend_Db::FETCH_ASSOC);
    }

    /**
     * Возвращает connectivity_configuration для строки результата
     * @param $rowData
     * @return bool|object
     */
    protected function _getConnectivityConfiguration($rowData)
    {
        $typeId = $rowData['claim_type_id'];
        $claimId = $rowData['connectivity'];

        $configuration = new Finance_Model_Filter_Component_ConnectivityConfiguration($claimId);
        $postProcessLibrary = $this->_postProcessLibrary;

        switch($typeId) {
            case App_Claim_Factory::TYPE_ID_IN_OUT:
                $configuration->enable();
                $configuration->enableReverse();
                $configuration->setTopTitle('Приход');
                $configuration->setBottomTitle('Отгрузка');

                foreach($rowData as $fieldName => $fieldValue) {
                    $configuration->getFields()->addField($fieldName, $fieldValue);
                }

                break;

            case App_Claim_Factory::TYPE_ID_INCOME_OUT:
                $configuration->enable();
                $configuration->setTopTitle('Приход от переработчика');
                $configuration->setBottomTitle('Отгрузка');
                $configuration->enableReverse();

                foreach($rowData as $fieldName => $fieldValue) {
                    $configuration->getFields()->addField($fieldName, $fieldValue);
                }

                break;

            case App_Claim_Factory::TYPE_ID_IN_SUPPLY:
                $configuration->enable();
                $configuration->setTopTitle('Приход');
                $configuration->setBottomTitle('Поставка');

                foreach($rowData as $fieldName => $fieldValue) {
                    $configuration->getFields()->addField($fieldName, $fieldValue);
                }

                break;
        }

        if($configuration->isEnabled()) {
            $configurationSubResult = $configuration->toObject();
            $configurationArrayRepresentation = array(0 => $configurationSubResult->fields);

            $postProcessLibrary
                ->postProcessClientName($configurationArrayRepresentation)
                ->postProcessUnloadAddress($configurationArrayRepresentation)
            ;

            return $configurationSubResult;
        }

        return false;
    }
}