<?php
class Finance_Model_Filter_Component_PostProcessLibrary_ProcessDelay
{
    /**
     * Постпроцесс - просрочка по выполнению запросов в финансовый саппорт
     * @param $result
     */
    public function run(&$result)
    {
        if(is_array($result) && count($result)) {
            foreach($result as $rowData) {
                $rowData->process_delay = $this->_getProcessDelay(isset($rowData->process_delay) ? $rowData->process_delay : 0);
            }
        }
    }

    /**
     * Возвращает просрочку тикета
     * @param $processDelay
     * @return string
     */
    public function _getProcessDelay($processDelay)
    {
        if($processDelay == 0) {
            return '—';
        } else {
            $pdStr = "";
            $processDelay = floor($processDelay / 60);

            if($processDelay >= 60 * 24) {
                $days = floor($processDelay / (60 * 24));
                $pdStr .= $this->_ruDay($days).' ';
                $processDelay -= $days * 60 * 24;
            };

            if($processDelay >= 60) {
                $pdStr .= $this->_ruHour(floor($processDelay / 60)).' ';
                $processDelay = $processDelay % 60;
            }

            if($processDelay) {
                $pdStr .= $processDelay.' мин.';
            }

            return $pdStr;
        }
    }

    /**
     * Возвращает количество дней в русскоязычном формате
     * @param $day
     * @return string
     */
    protected final function _ruDay($day)
    {
        if($day >= 10 && $day <= 19) {
            return $day.' дней';
        } else {
            $lastNumber = substr($day, -1, 1);

            switch((int) $lastNumber) {
                default:
                    return $day.' дней';

                case 1:
                    return $day.' день';

                case 2:
                case 3:
                case 4:
                    return $day.' дня';
            }
        }
    }

    /**
     * Возвращает количество часов в русскоязычном формате
     * @param $hour
     * @throws Exception
     * @return string
     */
    protected final function _ruHour($hour)
    {
        if($hour >= 10 && $hour <= 19) {
            return $hour.' часов';
        } else {
            $lastNumber = substr($hour, -1, 1);

            switch((int) $lastNumber) {
                default:
                    throw new \Exception("Invalid hour `{$hour}`");

                case 0:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    return $hour.' часов';

                case 1:
                    return $hour.' час';

                case 2:
                case 3:
                case 4:
                    return $hour.' часа';
            }
        }
    }
}