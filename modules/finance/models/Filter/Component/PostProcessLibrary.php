<?php
use App_Finance_Ticket_Factory as TicketFactory;
use Cash_Model_Planned_State_Manager as StateManager;

class Finance_Model_Filter_Component_PostProcessLibrary
{
    /**
     * array_map по всем строкам результатов
     * @param $result
     * @param closure $callback
     */
    public function each(&$result, Closure $callback)
    {
        if((is_array($result) || $result instanceof stdClass) && count($result)) {
            /** @var $rowData stdClass */
            foreach($result as $rowData) {
                $callback($rowData);
            }
        }
    }

    /**
     * Постпроцесс - добавление информации о правах по данному запросу
     * @param array $result
     */
    public function postProcessTicketAcl(&$result)
    {
        $this->each($result, function(stdClass $rowData) {
            $rowData->acl = TicketFactory::getInstance()->createFromTicketTypeId($rowData->ticket_type_id)->getAcl()->toArray();
        });
    }

    /**
     * Постпроцесс - добавлеят поле "client_name" с названием клиента
     * @param array $result
     * @param bool $useClientLegalName При true показывать полное юридическое имя
     * @return $this
     */
    public function postProcessClientName(&$result, $useClientLegalName = true)
    {
        $this->each($result, function(stdClass $rowData) use($useClientLegalName) {
            if($useClientLegalName) {
                $client_u_title = trim($rowData->client_u_title);
                $client_s_title = trim($rowData->client_s_title);
            }else{
                $client_u_title = trim($rowData->client_s_title);
                $client_s_title = trim($rowData->client_u_title);
            }

            if(strlen($client_u_title)) {
                $rowData->client_name = $client_u_title;
            }else if(strlen($client_s_title)){
                $rowData->client_name = $client_s_title;
            }else{
                $rowData->client_name = '-- нет клиента --';
            }
        });

        return $this;
    }

    /**
     * Постпроцесс - добавлен поле invoices_data с информацией по прикрепленным к заявке счета
     * @param array $result
     * @return $this
     */
    public function postProcessInvoices(&$result) {
        if(is_array($result) && count($result)) {
            $claimIds = array();

            /** @var $rowData stdClass */
            foreach($result as $rowData) {
                $claimIds[] = $rowData->claim_id;
            }

            /** @var $claimInvoicesDbTable App_Db_ClaimInvoices */
            $claimInvoicesDbTable = App_Db::get(DB_CLAIM_INVOICES);

            $sqlQuery = " SELECT
                                  ci.claim_id, ci.invoice_id, i.`id` AS account_number, i.`period`, i.`revision`
                              FROM claim_invoices ci
                              LEFT JOIN invoice i ON i.`prim_id` = ci.`invoice_id`";

            if(count($claimIds) > 0) {
                $sqlQuery .= ' WHERE claim_id IN ('.implode(',', $claimIds).')';
            }

            if($claimInvoicesRows = $claimInvoicesDbTable->getAdapter()->query($sqlQuery)) {
                $invoiceRelations = array();

                foreach($claimInvoicesRows as $invoiceRow) {
                    $claimId = $invoiceRow->claim_id;
                    $invoiceId = $invoiceRow->invoice_id;

                    if(!(isset($invoiceRelations[$claimId]))) {
                        $invoiceRelations[$claimId] = array();
                    }

                    if(is_numeric($invoiceId) && $invoiceId > 0) {
                        $invoiceRelations[$claimId][] = $invoiceRow;
                    }
                }
            } else {
                $invoiceRelations = array();
            }

            if(count($invoiceRelations) > 0) {
                /** @var $rowData stdClass */
                foreach($result as $rowData) {
                    if(isset($invoiceRelations[$rowData->claim_id])) {
                        $rowData->invoices_data = json_encode($invoiceRelations[$rowData->claim_id]);
                    } else {
                        $rowData->invoices_data = NULL;
                    }
                }
            } else {
                /** @var $rowData stdClass */
                foreach($result as $rowData) {
                    $rowData->invoices_data = NULL;
                }
            }
        }

        return $this;
    }

    /**
     * Постпроцесс - просрочка по выполнению запросов в финансовый саппорт
     * @param array $result
     * @return $this
     */
    public function postProcessDelay(&$result)
    {
        $helper = new Finance_Model_Filter_Component_PostProcessLibrary_ProcessDelay();
        $helper->run($result);

        return $this;
    }

    /**
     * Постпроцесс - добавляет информацию о служебных записках запроса
     * @param array $result
     * @return $this
     */
    public function postProcessServiceNote(&$result)
    {
        $this->each($result, function(stdClass $rowData) {
            if($rowData->ticket_service_note_file) {
                $rowData->ticket_service_note_url = App_Finance_Ticket_Component_Attachment_ServiceNote_Document::getStaticLink($rowData->id);

                $ticket = TicketFactory::getInstance()->getRegistry()->get($rowData->id);
                $serviceNote = new App_Finance_Ticket_Component_Attachment_ServiceNote_Document($ticket);

                if($serviceNote->isUploaded()) {
                    $browsableHelper = new App_Filemanager_Preview_FileTypeDetector($serviceNote->getPath());

                    $rowData->service_note = array(
                        'type' => $browsableHelper->getBrowsableType(),
                        'fileName' => $serviceNote->getFileName(),
                        'publicLink' => $serviceNote->getLink(),
                        'vendorLink' => '/filemanager/preview/service-note/?ticketId='.$ticket->getId()
                    );
                }
            }else{
                $rowData->service_note = array();
            }
        });

        return $this;
    }

    /**
     * Постпроцесс - добавляет информацию о карточке клиента
     * @param array $result
     * @return $this
     */
    public function postProcessClientCard(&$result)
    {
        $this->each($result, function(stdClass $rowData) {
            if($rowData->client_card) {
                $rowData->client_card_url = Client_Model_Client_Card::getStaticLink($rowData->client_id);
            }
        });

        return $this;
    }

    /**
     * Постпроцесс - добавляет информаци о статусе просмотра прикрепленных к запросу документов
     * @param array $result
     * @return $this
     */
    public function postProcessDocumentsViewed(&$result)
    {
        $this->each($result, function(stdClass $rowData) {
            $currentUserId = Zend_Auth::getInstance()->getIdentity()->id;
            $ticketJsonData = json_decode($rowData->ticket_json_data, true);

            if(isset($ticketJsonData['document_views']) && isset($ticketJsonData['document_views'][$currentUserId])) {
                $rowData->documents_viewed = true;
            } else {
                $rowData->documents_viewed = false;
            }
        });

        return $this;
    }

    /**
     * Постпроцес - адрес выгрузки
     * @param  array $result
     * @return $this
     */
    public function postProcessUnloadAddress(&$result)
    {
        $address_parts = array('unload_region', 'unload_city', 'unload_street', 'unload_building', 'unload_address');

        $this->each($result, function(stdClass $rowData) use($address_parts) {
            $unload_address = array();

            foreach($address_parts as $address_part) {
                $address_part = trim($rowData->$address_part);

                if(strlen($address_part) > 0) {
                    $unload_address[] = $address_part;
                }
            }

            if(count($unload_address)) {
                $rowData->unload_address = implode(', ', $unload_address);
            } else {
                $rowData->unload_address = null;
            }
        });

        return $this;
    }

    /**
     * Постпроцес - тип оплаты
     * @param  array $result
     * @return $this
     */
    public function postProcessPaymentType(&$result)
    {
        $paymentTypes = App_Finance_Ticket_Type_Payment_Form_DataSource::getInstance()->getPaymentTypes()->getAvailablePaymentTypes();

        $this->each($result, function(stdClass $rowData) use($paymentTypes) {
            $rowData->payment_type_name = $paymentTypes[$rowData->payment_type]['title'];
        });

        return $this;
    }

    /**
     * Постпроцесс - ограничить длину
     * @param $result
     * @param $fieldName
     * @param int $length
     * @return $this
     */
    public function postProcessMaxLength(&$result, $fieldName, $length = 300)
    {
        $this->each($result, function(stdClass $rowData) use($fieldName, $length) {
            if(strlen($rowData->$fieldName) > $length) {
                $rowData->$fieldName = substr($rowData->$fieldName, 0, 300).'...';
            }
        });

        return $this;
    }

    /**
     * Добавляет флаг "По факту приезда клиента"
     * @param $result
     * @return $this
     */
    public function postProcessClientDeliveryFact(&$result)
    {
        $this->each($result, function(stdClass $rowData) {
            $jsonData = json_decode($rowData->json_data, true);

            if(isset($jsonData['clientDeliveryFact'])) {
                $rowData->clientDeliveryFact = (bool) $jsonData['clientDeliveryFact'];
            }else{
                $rowData->clientDeliveryFact = false;
            }
        });

        return $this;
    }

    /**
     * Постпроцесс - схема проезда
     * @param $result
     */
    public function postProcessClientDirections(&$result)
    {
        $this->each($result, function(stdClass $rowData) {
            if($rowData->has_client_directions) {
                $rowData->client_directions_url = Client_Model_Client_Directions_Definition::getUrlFromFileName(
                    $rowData->client_id,
                    Client_Model_Client_Directions_Definition::ADDRESS_TYPE_UNLOAD,
                    $rowData->client_directions_file_name);
            }
        });
    }

    /**
     * Постпроцесс - статус документов
     * @param $result
     */
    public function postProcessCashPlanned(&$result)
    {
        $stateManager = new StateManager();

        $this->each($result, function(stdClass $rowData) use($stateManager) {
            // cash planned state
            $stateHandler = new Cash_Model_Planned_State_Handler((int) $rowData->claim_id);
            $stateHandler->calculateFictive();

            // cash_planned_state
            $cashPlannedState = $stateHandler->getResultStateId();

            $claimStatus = $rowData->claim_status_id;
            $claimTypeId = $rowData->claim_type_id;

            if((int) $claimStatus == (int) App_Claim_Factory::getInstance()->getCreateStatus($claimTypeId)) {
                $state = $stateManager->createStateByStateId(Cash_Model_Planned_State::STATE_NO_STATE);
            }else{
                $state = $stateManager->createStateByStateId($cashPlannedState);
            }


            $rowData->cash_planned_claim_state = $state->getClaimState();
            $rowData->cash_planned_claim_state_title = $state->getClaimStateTitle();
            $rowData->cash_planned_claim_state_color = $state->getColor();

            if($state->getStateId() === Cash_Model_Planned_State::STATE_NO_STATE) {
                $rowData->cash_planned_url = NULL;
            } else {
                if($rowData->cashType == Claim_Model_Abstract::PAYMENT_TYPE_BANK) {
                    $rowData->cash_planned_url = "/cash/planned/account/view/trId/{$rowData->claim_id}";
                } else {
                    $rowData->cash_planned_url = "/cash/planned/money/view/trId/{$rowData->claim_id}";
                }
            }
        });
    }

    /**
     * Постпроцесс - причина удаления
     * @param $result
     */
    public function postProcessCancellationReason(&$result)
    {
        /** @var $userTable App_Db_Users */
        $userTable = App_Db::get(DB_USERS);

        $this->each($result, function(stdClass $rowData) use($userTable) {
            $jsonData = json_decode($rowData->json_data);

            if(isset($jsonData->cancellation_reason)) {
                $rowData->cancellation_reason = $jsonData->cancellation_reason;

                if(isset($jsonData->cancellation_reason_user_id) && (int) $jsonData->cancellation_reason_user_id > 0) {
                    $user = $userTable->getUser((int) $jsonData->cancellation_reason_user_id);
                    $rowData->cancellation_reason_user_name = $user->name;
                }
            }else{
                $rowData->cancellation_reason = '';
            }
        });
    }

    /**
     * Постпроцесс - если по запросу прикреплен только один документ, то добавляем информацию о возможности/невозможности
     * его просмотра
     * @param $result
     * @return $this
     */
    public function postProcessAppendFirstFileBrowsableFlag(&$result)
    {
        $this->each($result, function(stdClass $rowData) {
            if((string) $rowData->num_ticket_documents == '1') {
                $ticket = TicketFactory::getInstance()->getRegistry()->get($rowData->id);
                $ticket->getDocumentsHandler()->pull();

                /** @var $document App_Finance_Ticket_Component_Document */
                $document = $ticket->getDocumentsHandler()->getDocuments()->offsetGet(0);

                $fileHelper = new App_Filemanager_Preview_FileTypeDetector($document->getFilePath());
                $rowData->file_browsable_flag = $fileHelper->isBrowsable();
            }
        });

        return $this;
    }

    /**
     * Постпроцесс - добавляет информацию о пояснительной записке
     * @param string $result
     * @return $this
     */
    public function postProcessExplanationNote(&$result)
    {
        $this->each($result, function(stdClass $rowData) {
            if($rowData->ticket_explanation_note_file) {
                $ticket = TicketFactory::getInstance()->getRegistry()->get($rowData->id);
                $explanationNote = new App_Finance_Ticket_Component_Attachment_ExplanatoryNote_Document($ticket);

                if($explanationNote->isUploaded()) {
                    $browsableHelper = new App_Filemanager_Preview_FileTypeDetector($explanationNote->getPath());

                    $rowData->explanatory_note = array_merge($explanationNote->toArray(), array(
                        'type' => $browsableHelper->getBrowsableType(),
                        'fileName' => $explanationNote->getFileName(),
                        'publicLink' => $explanationNote->getLink(),
                        'vendorLink' => '/filemanager/preview/explanatory/?ticketId='.$ticket->getId()
                    ));
                }else{
                    $rowData->explanatory_note = array();
                }
            }else{
                $rowData->explanatory_note = array();
            }
        });

        return $this;
    }

    /**
     * Постпроцесс - добавляет информацию о договорах клиента
     * @param $result
     * @return $this
     */
    public function postProcessClientContracts(&$result)
    {
        /** @var $dbTable App_Db_ClientTreaty */
        $dbTable = App_Db::get(DB_CLIENT_TREATY);

        $this->each($result, function(stdClass $rowData) use($dbTable) {
            $rowData->client_contracts = array();

            foreach($dbTable->getClientTreatyFiles($rowData->client_id) as $filename) {
                $browsableHelper = new App_Filemanager_Preview_FileTypeDetector($dbTable->getFilePath($filename, $rowData->client_id));

                $rowData->client_contracts[] = array(
                    'name' => $filename,
                    'type' => $browsableHelper->getBrowsableType(),
                    'publicLink' => $dbTable->getFileUrl($filename, $rowData->client_id),
                    'vendorLink' => '/filemanager/preview/client-contract/?clientId='.$rowData->client_id.'&file='.urlencode($filename)
                );
            };
        });

        return $this;
    }
}