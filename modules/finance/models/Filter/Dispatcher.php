<?php
use Finance_Model_Filter_Abstract as Filter;
use App_Filter_Component_Decorator_Type_Dispatchers_Decorator as DispatchersDecorator;
use App_Filter_Component_Decorator_Type_OrderReplace_Decorator as OrderReplaceDecorator;
use App_Finance_Recipient_Factory as RecipientFactory;
use App_Filter_Component_Decorator_Type_Search_Decorator as SearchDecorator;

class Finance_Model_Filter_Dispatcher extends Filter
{
    /**
     * Инициализация фильтра. Данный метод можно наследовать для настройки масок, конфигов, пост-процессинга
     * результатов, etc
     */
    public function init()
    {
        parent::init();

        $this->_initDecorators();
        $this->_postProcessConnectivity();
    }

    /**
     * Инициализация декораторов
     * @throws App_Filter_Component_Decorator_Type_Dispatchers_DispatchersNotAvailableException
     */
    protected function _initDecorators()
    {
        $postProcessLibrary = $this->getPostProcessLibrary();

        $address_parts = array('unload_region', 'unload_city', 'unload_street', 'unload_building', 'unload_address');

        $dispatcherDecorator = new DispatchersDecorator();
        $dispatcherDecorator->setDispatcherIds(array_keys(RecipientFactory::getInstance()->getDispatcherRecipient()->getAvailableDispatcherIds()));

        $orderReplaceDecorator = new OrderReplaceDecorator();
        $orderReplaceDecorator->replace("unload_address", $address_parts);

        $carManagerDecorator = new Finance_Model_Filter_Decorator_CarManager();
        $searchDecorator = new SearchDecorator('annotation_dispatcher');

        $this->addDecorator($searchDecorator);
        $this->addDecorator($dispatcherDecorator);

        if(!($this->isCountOnly())) {
            $this->addDecorator($orderReplaceDecorator);
            $this->addDecorator($carManagerDecorator);

            $this->_getResultBehaviour()->appendPostProcessCallback(function (&$result) use ($postProcessLibrary) {
                $postProcessLibrary->postProcessUnloadAddress($result);
                $postProcessLibrary->postProcessClientDirections($result);
            });
        }
    }

    /**
     * Пост-процесс - коннективити-конфигурация
     */
    protected function _postProcessConnectivity()
    {
        $postProcessLibrary = $this->getPostProcessLibrary();

        $this->_getResultBehaviour()->appendPostProcessCallback(function(&$result) use($postProcessLibrary) {
            $postProcess = new Finance_Model_Filter_Component_PostProcessLibrary_Connectivity();
            $postProcess->run($result, $postProcessLibrary);
        });
    }

    /**
     * Возвращает основной SQL-запрос
     * @return string
     */
    protected function _getSqlQuery()
    {
        $unloadAddressType = Client_Model_Client_Directions_Definition::ADDRESS_TYPE_UNLOAD;

        if($this->isCountOnly()) {
            return <<<SQL
                SELECT
                    t.`id`,
                    t.`ticket_type_id`,
                    t.`status` AS STATUS,
                    t.`author_user_id`,
                    '' as client_s_title,
                    '' as client_u_title
                    o.`id` AS orgstructure_id
                FROM
                    ticket t
                LEFT JOIN ticket_type tt ON t.`ticket_type_id` = tt.`id`
                LEFT JOIN orgstructure_users ou ON t.`author_user_id` = ou.`userId`
                LEFT JOIN orgstructure o ON o.`id` = ou.`oId`
                GROUP BY t.`id`
SQL;
        }else{
            return <<<SQL
                SELECT
                    t.`id`,
                    u.`name` AS manager_name,
                        claims.`manager_id` AS manager_id,
                        o.`title` AS orgstructure,
                    o.`id` AS orgstructure_id,
                    t.`claim_id`,
                    t.`status`,
                    t.`ticket_type_id`,
                    t.`date_created`,
                    t.`date_to_process`,
                    t.`date_processed`,
                    t.`is_completed`,
                    claims.`car_manager`,
                    claims.`type_id` as claim_type_id,
                    ctp.`title` AS claim_type_name,
                    claims.connectivity,
                    CASE claims.connectivity
                      WHEN 0 THEN claims.id
                      WHEN NULL THEN claims.id
                      ELSE concat(claims.id, ',', claims.connectivity)
                    END AS connectivity_claim_id,
                    ctp.show_connectivity,
                    DATE_FORMAT(t.`date_created`, "%d.%m.%Y %H:%i") AS ru_date_created,
                    DATE_FORMAT(t.`date_to_process`, "%d.%m.%Y %H:%i") AS ru_date_to_process,
                    DATE_FORMAT(t.`date_processed`, "%d.%m.%Y %H:%i") AS ru_date_processed,
                    clients.`id` AS client_id,
                    clients.`u_title` AS client_u_title,
                    clients.`s_title` AS client_s_title,
                    CASE LENGTH(clients.`u_title`)
                          WHEN 0 THEN clients.`s_title`
                          ELSE clients.`u_title`
                    END as client_name,
                    u_dispatcher.`id` AS dispatcher_id,
                    u_dispatcher.`name` AS dispatcher_name,
                    claims.`url` AS claim_url,
                    claims.`full_id` AS claim_full_id,
                    claims.transport_delivery_time AS transport_delivery_time,
                    claims.aprox_claim_weight AS aprox_claim_weight,
                    claims.`car_type` AS car_type,
                    drivers.`carNumber` AS car_number,
                    claims.`rent_price` AS rent_price,
                    claims.`date` AS claim_date,
                    claims.`annotation_dispatcher` AS claim_annotation_dispatcher,
                    clients.`f_region`,
                    clients.`f_gorod`,
                    clients.`f_street`,
                    clients.`f_building`,
                    clients.`f_adress`,
                    DATE_FORMAT(FROM_UNIXTIME(claims.`date`), "%d.%m.%Y") AS ru_claim_date,
                    clients.`unload_work_time` AS unload_depot_worktime,
                    UNIX_TIMESTAMP(t.`date_to_process`) AS date_to_process_timestamp,
                    UNIX_TIMESTAMP(t.`date_created`) AS date_created_timestamp,
                    UNIX_TIMESTAMP(t.`date_processed`) AS date_processed_timestamp,
                    UNIX_TIMESTAMP(NOW()) AS mysql_current_timestamp,
                    claims.unload_region,
                    claims.unload_city,
                    claims.unload_street,
                    claims.unload_building,
                    claims.unload_address,
                    claims.`unload_contact_name` AS unload_contact,
                    claims.`car` claim_has_car,
                    COUNT(ue.`id`) AS user_has_emails,
                    CASE
                      WHEN client_directions.`client_id` IS NULL THEN 0
                      ELSE 1
                    END as has_client_directions,
                    client_directions.mime_type AS client_directions_mime_type,
                    client_directions.file_name AS client_directions_file_name,
                    client_directions.browsable AS client_directions_browsable,
                    GROUP_CONCAT(up.phone) AS manager_phone
                FROM
                    ticket t
                LEFT JOIN ticket_type tt ON t.`ticket_type_id` = tt.`id`
                LEFT JOIN claims ON t.`claim_id` = claims.`id`
                LEFT JOIN clients ON claims.`client_id` = clients.`id`
                LEFT JOIN client_directions ON clients.`id` = client_directions.`client_id` AND client_directions.`address_type` = {$unloadAddressType}
                LEFT JOIN users u ON claims.`manager_id` = u.`id`
                LEFT JOIN users u_dispatcher ON claims.`dispetcher` = u_dispatcher.`id`
                LEFT JOIN orgstructure_users ou ON claims.`manager_id` = ou.`userId`
                LEFT JOIN orgstructure o ON o.`id` = ou.`oId`
                LEFT JOIN drivers ON drivers.`id` = claims.`car_number`
                LEFT JOIN user_email ue ON ue.`user_id` = claims.`manager_id`
                LEFT JOIN claim_type_pair ctp ON ctp.`type_id` = claims.`type_id`
                LEFT JOIN user_phone up ON up.user_id = u.id
                GROUP BY t.`id`
SQL;
        }
    }

    /**
     * Возвращает SQL-запрос для маски
     * @return string
     */
    public function getMaskSqlQuery()
    {
        $sqlQuery = <<<SQL
            SELECT DISTINCT result.expr_fields, result.date_created FROM (
              {$this->_getSqlQuery()}
            ) result
SQL;

        return str_replace("expr_fields", "{EXPR_FIELD}", $sqlQuery.' {WHERE} {HAVING}');
    }

    /**
     * Возвращает основной SQL-запрос
     * @return string
     */
    public function getResultSqlQuery()
    {
        $sqlQuery = <<<SQL
          SELECT result.* FROM (
            {$this->_getSqlQuery()}
        ) result
SQL;

        return $sqlQuery." {WHERE} {HAVING} {ORDER} {LIMIT}";
    }

    /**
     * Возвращает SQL-запрос для подсчета количества строк в результате
     * @return string
     */
    public function getResultCountSqlQuery()
    {
        $sqlCountQuery = "SELECT count(*) FROM (RESULT) s";

        return str_replace("RESULT", "{RESULT}", $sqlCountQuery);
    }

    /**
     * Возвращает массив с типами тикетов, с которыми данный фильтр работает
     * @return array
     */
    function getTicketTypes()
    {
        return array('dispatcher_verify');
    }
}