<?php
use App_Finance_Ticket_Type_Payment_Request_Ticket as PaymentRequestTicket;
use App_Finance_Ticket_Logger_Logger as FinanceLogger;
use App_Finance_Ticket_Factory as TicketFactory;
use App_Finance_Ticket_Type_Payment_Form_Data_Form as PaymentForm;

class Finance_Model_Payment_FormHelper
{
    /**
     * @var PaymentForm
     */
    protected $_paymentForm;

    /**
     * Хелпер для контроллера
     * @param $ticketId
     */
    public function __construct($ticketId)
    {
        App_Spl_TypeCheck::getInstance()->numeric($ticketId);

        if($ticketId) {
            $ticket = TicketFactory::getInstance()->createFromTicketId($ticketId);
        }else{
            $ticket = new PaymentRequestTicket();
        }

        $this->_paymentForm = new PaymentForm($ticket);
    }

    /**
     * Возвращает форму запроса
     * @return \App_Finance_Ticket_Type_Payment_Form_Data_Form
     */
    public function getPaymentForm()
    {
        return $this->_paymentForm;
    }

    /**
     * Создает запрос с помощью данных $_POST данных
     * @throws Exception
     * @return App_Finance_Ticket_Type_Payment_Request_Ticket
     */
    public function createTicket()
    {
        try {
            Zend_Db_Table_Abstract::getDefaultAdapter()->beginTransaction();

            $newTicket = $this->getPaymentForm()->getTicket();
            $newTicket->setDateToProcess(new Zend_Date());
            $newTicket->save(array(
                'enableTransaction' => false
            ));

            $this->_handleDocuments();
            $this->_handlePaymentData();

            Zend_Db_Table_Abstract::getDefaultAdapter()->commit();
        }
        catch(\Exception $e) {
            Zend_Db_Table_Abstract::getDefaultAdapter()->rollBack();
            throw $e;
        }
    }

    /**
     * Загрузка и прикрепление документов к запросу
     * @throws Exception
     */
    protected function _handleDocuments()
    {
        $ticket = $this->getPaymentForm()->getTicket();
        $fileCounter = 0;

        if(isset($_FILES['documents']) && count($_FILES['documents'])) {
            $financeLogger = new FinanceLogger($ticket);
            $financeLogger->oldSnapshot();

            foreach(array_keys($_FILES['documents']['name']) as $index) {
                if(strlen($_FILES['documents']['tmp_name'][$index])) {
                    if($this->getPaymentForm()->getTicket()->getAcl()->hasAccessAttachDocuments()) {
                        $document = $ticket->getDocumentsHandler()->createDocument();
                        $document->upload($_FILES['documents']['tmp_name'][$index], $_FILES['documents']['name'][$index]);

                        $fileCounter++;
                    }else{
                        throw new \Exception("Вы не имеете права прикреплять файлы к запросу");
                    }
                }
            }

            if($fileCounter > 0) {
                $financeLogger->newSnapshot();
                $financeLogger->writeLog();
            }else{
                $userCanIgnoreRestricts = (int) $ticket->getAcl()->hasAccess('nodocumentsrequired') == App_Access_Field_Type1::ACCESS_ALLOWED;

                if(!($userCanIgnoreRestricts)) {
                    throw new \Exception("Запрос не может быть создан без прикрепления документов");
                }
            }
        }
    }

    /**
     * Сохранение расшифровки
     */
    protected function _handlePaymentData()
    {
        $paymentWriter = new App_Finance_Ticket_Type_Payment_Form_Writer($this->getPaymentForm());
        $paymentWriter->save();
    }
}