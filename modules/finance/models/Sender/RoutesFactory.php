<?php
use Finance_Model_Sender_Form as SenderForm;
use App_Finance_Sender_Form_Route_Element as Route;
use App_Finance_Sender_Form_Route_Collection as Routes;
use App_Finance_Recipient_Factory as RecipientFactory;
use App_Finance_Ticket_Factory as TicketFactory;
use App_Finance_Ticket_Component_Validation_Exception as ValidationException;
use App_Finance_Ticket_Component_Validation_Type_UniqueTicket_Exception as SentException;
use App_Finance_Ticket_Component_Validation_Type_WasCompleted_Exception as WasCompletedException;
use App_Finance_Ticket_Component_Validation_Type_OnlyOwnCar_Exception as OnlyOwnCarException;
use App_Finance_Ticket_Component_Validation_Type_CashAccountOnly_Exception as CashAccountOnlyException;
use App_Finance_Ticket_Component_Validation_Type_InvoicesAvailable_Exception as InvoicesAvailableException;
use App_Finance_Ticket_Component_Validation_Type_HasPendingCancelTicket_Exception as HasPendingCancelTicketException;
use App_Finance_Ticket_Component_Validation_Type_ClaimTypeSupported_Exception as ClaimTypeNotSupportedException;
use App_Finance_Ticket_Component_Validation_Type_ExplanatoryNote_Exception as ExplanatoryNoteException;

class Finance_Model_Sender_RoutesFactory
{
    /**
     * Переданная конструктору форму отправки
     * @var SenderForm
     */
    protected $_senderForm;

    /**
     * @param Finance_Model_Sender_Form $form
     */
    public final function __construct(SenderForm $form)
    {
        $this->_senderForm = $form;
    }

    /**
     * Возвращает переданную конструктору фомру отправки
     * @return \Finance_Model_Sender_Form
     */
    public final function getSenderForm()
    {
        return $this->_senderForm;
    }

    /**
     * Заполняет форму доступными маршрутами отправки запросов в саппорт.
     *
     * Пожалуйста, прочтите: личное обращание основателя финансового саппорта Д.:
     *
     *      "Шестнадцатого декабря 2014 года появилась новая доработка, связанная с рассылкой MailingPenaltyForCarWaste_Observer.
     *       Данная рассылка основывается на факте наличия запроса в бухгалтерский саппорт на момент создания запроса к диспетчеру,
     *       поэтому данная фабрика теперь обязазывает к тому, чтобы опция "бухгалтерский саппорт" находилась выше и первее по списку,
     *       чем диспетчерский".
     *
     * @param App_Finance_Sender_Form_Routes $routes
     */
    public function createRoutes(\App_Finance_Sender_Form_Routes $routes)
    {
        /** @var $ticketTypeDbTable App_Db_TicketType */
        $ticketTypeDbTable = App_Db::get(DB_TICKET_TYPE);
        $recipientFactory = RecipientFactory::getInstance();

        foreach(array('accounting', 'dispatcher', 'depot') as $recipientName) {
            $recipientRoutes = new Routes($this->getSenderForm()->getClaimId(), RecipientFactory::getInstance()->createFromRecipientName($recipientName));
            $subRoutes = $recipientFactory->createFromRecipientName($recipientName)->getTicketTypes();

            foreach($subRoutes as $ticketType) {
                $ticketTypeId = $ticketTypeDbTable->getTicketTypeIdByName($ticketType);

                $route = new Route(uniqid());
                $route->setTicketTypeId($ticketTypeId);

                $testTicket = TicketFactory::getInstance()->createFromTicketTypeId($ticketTypeId);
                $testTicket->setClaimId($this->getSenderForm()->getClaimId());

                if($testTicket->getAcl()->hasAccessCreate() || $testTicket->getAcl()->getClaimModule()->hasAccessCreate($testTicket)) {
                    try {
                        $validationObserver = $testTicket->getValidationObserver();
                        $validationObserver->getValidationHandler()->validateByTicket($testTicket);
                        $route->getStateHandler()->setActive();
                    }
                    catch(OnlyOwnCarException $e) {
                        $route->getStateHandler()->setHidden();
                    }
                    catch(CashAccountOnlyException $e) {
                        $route->getStateHandler()->setHidden();
                    }
                    catch(InvoicesAvailableException $e) {
                        $route->getStateHandler()->setHidden();
                    }
                    catch(HasPendingCancelTicketException $e) {
                        $route->getStateHandler()->setSent();
                        $route->getMessageHandler()->infoMessage($e->getMessage());
                    }
                    catch(WasCompletedException $e) {
                        $route->getStateHandler()->setCompleted();
                    }
                    catch(SentException $e) {
                        $route->getStateHandler()->setSent();
                    }
                    catch(ClaimTypeNotSupportedException $e) {
                        $route->getMessageHandler()->errorMessage($e->getMessage());
                        $route->getStateHandler()->setIsAvailable(false);
                        $route->getStateHandler()->setHidden();
                    }
                    catch(ExplanatoryNoteException $e) {
                        $route->getStateHandler()->setDisabled();
                        $route->getMessageHandler()->infoMessage($e->getMessage());
                        $this->getSenderForm()->enableRequireShow();
                    }
                    catch(ValidationException $e) {
                        $route->getStateHandler()->setDisabled();
                        $route->getMessageHandler()->errorMessage($e->getMessage());
                    }

                    if($recipientName == 'accounting') {
                        $this->_setUpServiceNote($route, $testTicket);

                        if($testTicket->getAcl()->hasAccessExplanatoryNote()) {
                            $this->_setUpExplanatoryNote($route, $testTicket);
                        }

                        $route->setDateClientDeliveryFactEnabled(true);
                    }

                    $recipientRoutes->add($route);
                }
            }

            $routes->add($recipientRoutes);
        }
    }

    /**
     * В случае, если это запрос в бухсаппорт и есть отмененный запрос(который когда-то находился в архиве), то
     * должна показываться форма прикрепления служебной записки
     * @param App_Finance_Sender_Form_Route_Element $route
     * @param App_Finance_Ticket_TicketInterface $ticket
     */
    protected function _setUpServiceNote(Route $route, App_Finance_Ticket_TicketInterface $ticket)
    {
        $serviceNotePreconditions = new App_Finance_Ticket_Component_Observer_Type_ServiceNote_Preconditions();

        if($serviceNotePreconditions->isServiceNoteRequired($ticket)) {
            $route->getServiceNoteHandler()->enable();
        }

        // Загрузка служебной записки, если таковая уже существует
        $finder = new App_Finance_Ticket_Finder();
        $finder->setClaimIds($this->getSenderForm()->getClaimId());
        $finder->setTicketStatus(App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_ACTIVE);
        $finder->setRecipients('accounting');
        $finder->sortBy('id', 'desc');

        if($activeTicket = $finder->findOne()) {
            $serviceNote = new App_Finance_Ticket_Component_Attachment_ServiceNote_Document($activeTicket);

            if($serviceNote->isUploaded()) {
                $route->getServiceNoteHandler()->setTicketId($activeTicket->getId());
            }
        }
    }

    /**
     * В случае, если выполняется следующее условие: с 1 августа 2014 года, есть 1 и более архивных запросов в
     * бухгалтерский саппорт по клиенту, а договора с данным клиентом до сих пор нет
     * Включается необходимость прикрепления служебной записки
     * (https://ns25.ru/analitics/sheets)
     * @param App_Finance_Sender_Form_Route_Element $route
     * @param App_Finance_Ticket_TicketInterface $ticket
     */
    protected function _setUpExplanatoryNote(Route $route, App_Finance_Ticket_TicketInterface $ticket)
    {
        $serviceNotePreconditions = new App_Finance_Ticket_Component_Observer_Type_ExplanatoryNote_Preconditions();

        if($serviceNotePreconditions->isExplanationNoteRequired($ticket)) {
            $route->getExplanatoryNoteHandler()->enable();
        }

        // Загрузка пояснительной записки, если таковая уже существует
        $finder = new App_Finance_Ticket_Finder();
        $finder->setClaimIds($this->getSenderForm()->getClaimId());
        $finder->setTicketStatus(App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_ACTIVE);
        $finder->setRecipients('accounting');
        $finder->sortBy('id', 'desc');

        if($activeTicket = $finder->findOne()) {
            $explanatoryNote = new App_Finance_Ticket_Component_Attachment_ExplanatoryNote_Document($activeTicket);

            if($explanatoryNote->isUploaded()) {
                $route->getExplanatoryNoteHandler()->setTicketId($activeTicket->getId());
            }
        }
    }
}