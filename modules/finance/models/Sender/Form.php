<?php
use App_Finance_Sender_Form as SenderForm;

class Finance_Model_Sender_Form extends SenderForm
{
    /**
     * Конфигурация списка маршрутов
     * ЕСЛИ ВЫ СЮДА ЗАШЛИ ДЛЯ ТОГО, ЧТОБЫ ВКЛЮЧИТЬ/ОТКЛЮЧИТЬ ПОДДЕРЖКУ ЗАПРОСОВ ДЛЯ ЗАЯВОК:
     * @see App_Finance_Ticket_Component_Validation_Type_ClaimTypeSupported_Validator
     * @param App_Finance_Sender_Form_Routes $routes
     * @throws Exception
     */
    protected function _setUpRoutes(\App_Finance_Sender_Form_Routes $routes)
    {
        $senderRoutesGenerator = new Finance_Model_Sender_RoutesFactory($this);
        $senderRoutesGenerator->createRoutes($routes);
    }

    /**
     * Заполняет форму/маршрут данными, исходя из существующех для указанной заявки тикетов
     */
    public function createFromClaimId()
    {
        if(!($this->getClaimId())) {
            throw new \Exception('Не указан ID заявки');
        }

        $this->getRoutes()->createFromClaimId();
    }

    /**
     * Возвращает URL для получения данных о марщрутах в json-формате
     * @return string
     */
    public function getDataUrl()
    {
        return '/finance/sender/routes';
    }

    /**
     * Возвращает URL для отправки тикетов по отмеченным маршрутам
     * @return string
     */
    public function getPostUrl()
    {
        return '/finance/sender/perform';
    }
}