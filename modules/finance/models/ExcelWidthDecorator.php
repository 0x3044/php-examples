<?php
use App_Filter_Excel_Converter as ExcelConverter;
use App_Filter_Excel_Component_Map_Abstract as MapRecord;

class Finance_Model_ExcelWidthDecorator
{
    /**
     * @var ExcelConverter
     */
    protected $_excelConverter;

    /**
     * Декоратор, устанавливает ширину полей у Excel-конвертера в зависимости от получателя/статуса
     * Конфигурация декоратора находится в @see Finance_Model_ExcelWidthDecorator::_getWidths
     * @param App_Filter_Excel_Converter $excelConverter
     */
    public function __construct(ExcelConverter $excelConverter)
    {
        $this->_excelConverter = $excelConverter;
    }

    /**
     * Настроить длины колонок
     * @param $recipient
     * @param $status
     */
    public function decorate($recipient, $status)
    {
        $widths = $this->_getWidths($recipient, $status);
        $count = 0;

        /** @var $excelField MapRecord */
        foreach($this->getExcelConverter()->getMap()->getItems() as $n => $excelField) {
            if(isset($widths[$count])) {
                $excelField->setWidth($widths[$count]);
            }

            $count++;
        }
    }

    /**
     * Возвращает переданный конструктору excel-конвертер
     * @return \App_Filter_Excel_Converter
     */
    public function getExcelConverter()
    {
        return $this->_excelConverter;
    }

    /**
     * Возврашает конфигурацию декоратора для указанного получателя/статуса
     * Для получения длин используйте инструмент tests/suites/tools/excel-width
     * @param $recipient
     * @param $status
     * @return mixed
     * @throws Exception
     */
    protected function _getWidths($recipient, $status)
    {
        $config = array(
            'accounting' => array(
                'active' => array(
                    5, 14.28, 24.28, 27.28, 8.28, 9.85, 10.85, 10.85, 12.57, 8, 9, 3.6
                ),
                'canceled' => array(
                    5, 10.71, 17.14, 20.57, 6.42, 9.28, 10.85, 10.85, 10.85, 8.85, 10, 8.42, 7.57, 3.57
                ),
                'archived' => array(
                    5, 10.71, 17.14, 20.57, 6.42, 9.28, 10.85, 10.85, 10.85, 8.85, 10, 8.42, 7.57, 3.57
                ),
                'pending-cancel' => array(
                    5, 14.28, 24.28, 27.28, 8.28, 9.85, 10.85, 10.85, 12.57, 8, 9, 3.6
                )
            ),
            'dispatcher' => array(
                'active' => array(
                    4.42, 10.85, 10.85, 13.28, 5.71, 7.00, 15.57, 12.00, 7.00, 10.00, 8.00, 7.85, 6.42, 6.14, 9.42, 8.71
                ),
                'canceled' => array(
                    4.42, 9.57, 10.00, 9.42, 10.00, 5.71, 8.14, 10.00, 14.57, 7.00, 10.00, 8.00, 6.00, 7.00, 6.00, 7.00, 9.71
                ),
                'archived' => array(
                    4.42, 9.57, 10.00, 9.42, 10.00, 5.71, 8.14, 10.00, 14.57, 7.00, 10.00, 8.00, 6.00, 7.00, 6.00, 7.00, 9.71
                ),
                'pending-cancel' => array(
                    4.42, 10.85, 10.85, 13.28, 5.71, 7.00, 15.57, 12.00, 7.00, 10.00, 8.00, 7.85, 6.42, 6.14, 9.42, 8.71
                ),
            ),
            'depot' => array(
                'active' => array(
                    5.00, 9.28, 8.71, 10.85, 15.85, 17.00, 14.00, 14.28, 11.28, 9.71, 10.28, 15.14
                ),
                'canceled' => array(
                    5.00, 8.42, 8.14, 11.57, 10.85, 10.14, 10.85, 15.00, 12.28, 13.71, 10.42, 8.00, 7.28, 10.14
                ),
                'archived' => array(
                    5.00, 8.42, 8.14, 11.57, 10.85, 10.14, 10.85, 15.00, 12.28, 13.71, 10.42, 8.00, 7.28, 10.14
                ),
                'pending-cancel' => array(
                    5.00, 9.28, 8.71, 10.85, 15.85, 17.00, 14.00, 14.28, 11.28, 9.71, 10.28, 15.14
                ),
            ),
            'payment' => array(
                'active' => array(
                    5.42, 13.57, 12.42, 12.42, 17.14, 12.57, 7.71, 20.42, 4.57
                ),
                'archived' => array(
                    5.42, 13.57, 12.42, 12.42, 17.14, 12.57, 7.71, 20.42, 4.57
                ),
                'canceled' => array(
                    5.42, 13.57, 12.42, 12.42, 17.14, 12.57, 7.71, 20.42, 4.57
                ),
                'pending-cancel' => array(
                    5.42, 13.57, 12.42, 12.42, 17.14, 12.57, 7.71, 20.42, 4.57
                )
            )
        );

        if(!(isset($config[$recipient]) && isset($config[$recipient][$status]))) {
            throw new \Exception("Unknown recipient/status `{$recipient}/{$status}` for ExcelWidthDecorator");
        }

        return $config[$recipient][$status];
    }
}