<?php
class Finance_Model_Claim_InSupply extends Finance_Model_Claim_Default
{
    /**
     * @var Claim_Model_InSupply_Supply
     */
    protected $_claimSupply;

    /**
     * @var Claim_Model_InSupply_In
     */
    protected $_claimIn;

    /**
     * @var App_Claim_Mapper_InSupply_Supply
     */
    protected $_mapperSupply;

    /**
     * @var App_Claim_Mapper_InSupply_In
     */
    protected $_mapperIn;

    public function __construct($claimId)
    {
        /** @var $mapper App_Claim_Mapper_Abstract */
        /** @var $claim Claim_Model_Abstract */
        list($mapperIncome, $claimIncome) = App_Claim_Factory::getInstance()->createMapperByClaimId($claimId);

        if(is_null($claimIncome)) {
            throw new \Exception("Claim `{$claimId}` not found");
        }

        $connectivityClaimId = $claimIncome->connectivity;
        list($mapperOut, $claimOut) = App_Claim_Factory::getInstance()->createMapperByClaimId($connectivityClaimId);

        $this->_claimId = $claimId;
        $this->_mapperSupply = $mapperOut;
        $this->_mapperIn = $mapperIncome;
        $this->_claimSupply = $claimOut;
        $this->_claimIn = $claimIncome;
    }

    protected function _getClaim($purpose = NULL)
    {
        switch($purpose) {
            default:
                return $this->_claimIn;

            case 'carNumber':
                return $this->_claimSupply;

            case 'transportDeliveryTime':
                if($this->useConnectivityId()) {
                    return $this->_claimSupply;
                }else{
                    return $this->_claimIn;
                }
        }
    }

    public function save()
    {
        $this->_prepareClaims();

        $this->_saveSupplyClaim();
        $this->_saveInClaim();
        $this->_validate();

    }

    protected function _prepareClaims()
    {
        $claimOut = $this->_claimSupply;
        $claimIncome = $this->_claimIn;

        $claimOut->dispetcher = $claimIncome->dispetcher;
        $claimIncome->driverName = $claimOut->driverName;
        $claimIncome->driverPhone = $claimOut->driverPhone;
        $claimIncome->car_number = $claimOut->car_number;
        $claimOut->rent_price = $claimIncome->rent_price;
    }

    protected function _saveInClaim()
    {
        $claim = $this->_claimSupply;
        $mapper = $this->_mapperSupply;

        $this->_triggerLogger($claim);
        $mapper->save($claim, $this->_claimIn);
    }

    protected function _saveSupplyClaim()
    {
        $claim = $this->_claimIn;
        $mapper = $this->_mapperIn;

        $this->_triggerLogger($claim);
        $mapper->save($claim, $this->_claimSupply);
    }
}