<?php

class Finance_Model_Claim_IncomeOut extends Finance_Model_Claim_Default
{
    /**
     * @var Claim_Model_IncomeOut_Out
     */
    protected $_claimOut;

    /**
     * @var Claim_Model_IncomeOut_Income
     */
    protected $_claimIncome;

    /**
     * @var App_Claim_Mapper_IncomeOut_Out
     */
    protected $_mapperOut;

    /**
     * @var App_Claim_Mapper_IncomeOut_Income
     */
    protected $_mapperIncome;

    public function __construct($claimId)
    {
        /** @var $mapper App_Claim_Mapper_Abstract */
        /** @var $claim Claim_Model_Abstract */
        list($mapperIncome, $claimIncome) = App_Claim_Factory::getInstance()->createMapperByClaimId($claimId);

        if(is_null($claimIncome)) {
            throw new \Exception("Claim `{$claimId}` not found");
        }

        $connectivityClaimId = $claimIncome->connectivity;
        list($mapperOut, $claimOut) = App_Claim_Factory::getInstance()->createMapperByClaimId($connectivityClaimId);

        $this->_claimId = $claimId;
        $this->_mapperOut = $mapperOut;
        $this->_mapperIncome = $mapperIncome;
        $this->_claimOut = $claimOut;
        $this->_claimIncome = $claimIncome;
    }

    protected function _getClaim($purpose = NULL)
    {
        switch($purpose) {
            default:
                return $this->_claimIncome;

            case 'carNumber':
                return $this->_claimOut;

            case 'transportDeliveryTime':
                if($this->useConnectivityId()) {
                    return $this->_claimOut;
                }else{
                    return $this->_claimIncome;
                }
        }
    }

    public function save()
    {
        $this->_prepareClaims();

        $this->_saveOutClaim();
        $this->_saveInClaim();
        $this->_validate();

    }

    protected function _prepareClaims()
    {
        $claimOut = $this->_claimOut;
        $claimIncome = $this->_claimIncome;

        $claimOut->dispetcher = $claimIncome->dispetcher;
        $claimIncome->driverName = $claimOut->driverName;
        $claimIncome->driverPhone = $claimOut->driverPhone;
        $claimIncome->car_number = $claimOut->car_number;
        $claimOut->rent_price = $claimIncome->rent_price;
    }

    protected function _saveInClaim()
    {
        $claim = $this->_claimOut;
        $mapper = $this->_mapperOut;

        $this->_triggerLogger($claim);
        $mapper->disableSaveProducts();
        $mapper->save($claim, $this->_claimIncome);
    }

    protected function _saveOutClaim()
    {
        $claim = $this->_claimIncome;
        $mapper = $this->_mapperIncome;

        $this->_triggerLogger($claim);
        $mapper->disableSaveProducts();
        $mapper->save($claim, $this->_claimOut);
        $this->_triggerCashPlanned();
    }
}