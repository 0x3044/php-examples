<?php

class Finance_Model_Claim_Inout extends Finance_Model_Claim_Default
{
    /**
     * @var Claim_Model_InOut_In
     */
    protected $_claimIn;

    /**
     * @var Claim_Model_InOut_Out
     */
    protected $_claimOut;

    /**
     * @var App_Claim_Mapper_InOut_In
     */
    protected $_mapperIn;

    /**
     * @var App_Claim_Mapper_InOut_Out
     */
    protected $_mapperOut;

    public function __construct($claimId)
    {
        /** @var $mapper App_Claim_Mapper_Abstract */
        /** @var $claim Claim_Model_Abstract */
        list($mapperOut, $claimOut) = App_Claim_Factory::getInstance()->createMapperByClaimId($claimId);

        if(is_null($claimOut)) {
            throw new \Exception("Claim `{$claimId}` not found");
        }

        $connectivityClaimId = $claimOut->connectivity;
        list($mapperIn, $claimIn) = App_Claim_Factory::getInstance()->createMapperByClaimId($connectivityClaimId);

        $this->_claimId = $claimId;
        $this->_mapperIn = $mapperIn;
        $this->_mapperOut = $mapperOut;
        $this->_claimIn = $claimIn;
        $this->_claimOut = $claimOut;
    }

    protected function _getClaim($purpose = NULL)
    {
        switch($purpose) {
            default:
                return $this->_claimOut;

            case 'carNumber':
                return $this->_claimIn;

            case 'transportDeliveryTime':
                if($this->useConnectivityId()) {
                    return $this->_claimIn;
                }else{
                    return $this->_claimOut;
                }
        }
    }

    public function save()
    {
        $this->_prepareClaims();

        $this->_saveOutClaim();
        $this->_saveInClaim();
        $this->_validate();

    }

    protected function _prepareClaims()
    {
        $claimIn = $this->_claimIn;
        $claimOut = $this->_claimOut;

        $claimIn->dispetcher = $claimOut->dispetcher;
        $claimOut->driverName = $claimIn->driverName;
        $claimOut->driverPhone = $claimIn->driverPhone;
        $claimOut->car_number = $claimIn->car_number;
        $claimIn->rent_price = $claimOut->rent_price;
    }

    protected function _saveInClaim()
    {
        $claim = $this->_claimIn;
        $mapper = $this->_mapperIn;

        $this->_triggerLogger($claim);
        $mapper->save($claim);
    }

    protected function _saveOutClaim()
    {
        $claim = $this->_claimOut;
        $mapper = $this->_mapperOut;

        $this->_triggerLogger($claim);
        $mapper->save($claim, $this->_claimIn);
        $this->_triggerCashPlanned();
    }
}