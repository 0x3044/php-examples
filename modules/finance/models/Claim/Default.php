<?php
use Cash_Model_Planned_State_Handler as StateHandler;

class Finance_Model_Claim_Default
{
    protected $_claimId;

    /**
     * @var App_Claim_Mapper_Abstract
     */
    protected $_mapper;

    /**
     * @var Claim_Model_Abstract
     */
    protected $_claim;

    /**
     * Флаг "использовать connectivity Id"
     * @var bool
     */
    protected $_useConnectivityId = false;

    protected $_validateProfiles = array();
    protected $_expectedValues = array();

    /**
     * Возвращает обработчик в зависимости от типа заявки
     * @param $claimId
     * @return Finance_Model_Claim_Default|Finance_Model_Claim_Inout
     * @throws Exception
     */
    public static function factory($claimId)
    {
        App_Spl_TypeCheck::getInstance()->positiveNumeric($claimId);

        $typeId = App_Claim_Factory::getInstance()->getTypeId($claimId);

        switch($typeId) {
            default:
                throw new \Exception("Unsupported claim type `{$typeId}`");

            case App_Claim_Factory::TYPE_ID_IN:
            case App_Claim_Factory::TYPE_ID_OUT:
            case App_Claim_Factory::TYPE_ID_RETURN_OUT:
            case App_Claim_Factory::TYPE_ID_SUPPLY:
            case App_Claim_Factory::TYPE_ID_INCOME:
            case App_Claim_Factory::TYPE_ID_RETURN_SUPPLY:
            case App_Claim_Factory::TYPE_ID_REPAIR_OUT:
            case App_Claim_Factory::TYPE_ID_SPARES:
                return new Finance_Model_Claim_Default($claimId);

            case App_Claim_Factory::TYPE_ID_IN_OUT:
                return new Finance_Model_Claim_Inout($claimId);

            case App_Claim_Factory::TYPE_ID_IN_SUPPLY:
                return new Finance_Model_Claim_InSupply($claimId);

            case App_Claim_Factory::TYPE_ID_INCOME_OUT:
                return new Finance_Model_Claim_IncomeOut($claimId);
        }
    }

    /**
     * Хелпер по сохранению данных в заявке через диспетчерский саппорт
     * @param $claimId
     * @throws Exception
     */
    public function __construct($claimId)
    {
        /** @var $mapper App_Claim_Mapper_Abstract */
        /** @var $claim Claim_Model_Abstract */
        list($mapper, $claim) = App_Claim_Factory::getInstance()->createMapperByClaimId($claimId);

        if(is_null($claim)) {
            throw new \Exception("Claim `{$claimId}` not found");
        }

        $this->_claimId = $claimId;
        $this->_mapper = $mapper;
        $this->_claim = $claim;

        if(method_exists($this->_mapper, 'disableSaveProducts')) {
            $this->_mapper->disableSaveProducts();
        }
    }

    /**
     * Включить флаг "использовать connectivity"
     */
    public function enableUseConnectivityId()
    {
        $this->_useConnectivityId = true;
    }

    /**
     * Отключить флаг "использовать connectivity"
     */
    public function disableUseConnectivityId()
    {
        $this->_useConnectivityId = false;
    }

    /**
     * Возвращает true, если включен флаг "использовать connectivity"
     * @return bool
     */
    public function useConnectivityId()
    {
        return $this->_useConnectivityId;
    }

    /**
     * Сохраняет изменения в заявке
     */
    public function save()
    {
        $claim = $this->_getClaim();

        $this->_triggerLogger($claim);
        $this->_getMapper()->save($claim);
        $this->_validate();

        $enabledTypeIds = array(
            App_Claim_Factory::TYPE_ID_OUT,
            App_Claim_Factory::TYPE_ID_IN_OUT,
            App_Claim_Factory::TYPE_ID_INCOME_OUT,
            App_Claim_Factory::TYPE_ID_SPARES
        );

        if(in_array($claim->type_id, $enabledTypeIds)) {
            $this->_triggerCashPlanned($claim->id);
        }
    }

    /**
     * Возвращает данные заявки
     * @param null $purpose
     * @return Claim_Model_Abstract
     */
    protected function _getClaim($purpose = NULL)
    {
        return $this->_claim;
    }

    /**
     * Возвращает переданный конструктору ID заявки
     * @return int
     */
    public function getClaimId()
    {
        return $this->_claimId;
    }

    /**
     * Возвращает Mapper заявки
     * @return App_Claim_Mapper_Abstract
     */
    protected function _getMapper()
    {
        return $this->_mapper;
    }

    /**
     * Смена диспетчера
     * @param $dispatcherId
     * @throws Exception
     */
    public function setupDispatcherId($dispatcherId)
    {
        $claim = $this->_getClaim("dispatcher");

        if($dispatcherId < 0) {
            throw new \Exception("No dispatcher_id available");
        }

        $dispatchers = App_Finance_Recipient_Factory::getInstance()->getDispatcherRecipient()->getAvailableDispatcherIds();

        if(!(is_array($dispatchers) && count($dispatchers) && in_array($dispatcherId, array_keys($dispatchers)))) {
            throw new \Exception("Вы не имеете доступа назначить этого диспетчера для этой заявки");
        }

        $claim->dispetcher = $dispatcherId;
        $this->_validateProfiles[] = 'dispatcherId';
        $this->_expectedValues['dispatcherId'] = $dispatcherId;
    }

    /**
     * Смена номера машины
     * @param $carNumber
     */
    public function setupCarNumber($carNumber)
    {
        $claim = $this->_getClaim("carNumber");

        $claim->car_number = $carNumber > 0 ? $carNumber : null;

        /** @var $driversDbTable App_Db_Drivers */
        $driversDbTable = App_Db::get(DB_DRIVERS);

        if($carNumber) {
            $carInfo = $driversDbTable->getCar($carNumber);

            if($carInfo) {
                $claim->driverName = $carInfo->name;
                $claim->driverPhone = $carInfo->phone;
            }
        }else{
            $claim->driverName = null;
            $claim->driverPhone = null;
        }

        $this->_validateProfiles[] = 'carNumber';
        $this->_expectedValues['carNumber'] = $carNumber;
    }

    /**
     * Смена стоимости найма
     * @param $rentPrice
     * @throws Exception
     */
    public function setupRentPrice($rentPrice)
    {
        $claim = $this->_getClaim("rentPrice");

        if($rentPrice < 0) {
            throw new \Exception("Invalid rent price");
        }

        $claim->rent_price = $rentPrice;
        $this->_validateProfiles[] = 'rentPrice';
        $this->_expectedValues['rentPrice'] = $rentPrice;
    }

    /**
     * Смена времени подачи транспорта
     * @param $transportDeliveryTime
     */
    public function setupTransportDeliveryTime($transportDeliveryTime)
    {
        $claim = $this->_getClaim("transportDeliveryTime");

        $claim->transport_delivery_time = $transportDeliveryTime;
        // TODO: восстановить валидацию
    }

    /**
     * Триггерит логгер при изменении полей в заявке
     * @param Claim_Model_Abstract $newClaim
     * @throws Exception
     */
    protected function _triggerLogger(Claim_Model_Abstract $newClaim)
    {
        /** @var $mapper App_Claim_Mapper_Abstract */
        /** @var $oldClaim Claim_Model_Abstract */
        list($mapper, $oldClaim) = App_Claim_Factory::getInstance()->createMapperByClaimId($newClaim->id);
        $adapter = App_Claim_Factory::getInstance()->createLoggerAdapter($newClaim);

        if($adapter) {
            $adapter->setObjectId($newClaim->id);
            $adapter->setOldObject($oldClaim);
            $adapter->setNewObject($newClaim);

            $adapter->writeLog();
        }
    }

    /**
     * Вызов cash_planned триггера
     */
    protected function _triggerCashPlanned()
    {
        $claimId = $this->getClaimId();

        /** @var $dbCashPlanned App_Db_CashPlanned */
        $dbCashPlanned = App_Db::get(DB_CASH_PLANNED);
        $dbCashPlanned->trigger($claimId);

        $stateHandler = new StateHandler($claimId);
        $stateHandler->update();
    }

    /**
     * Проверка, сохранились ли данные или нет
     * @throws Exception
     */
    protected function _validate()
    {
        $throwException = false;
        $claimData = App_Claim_Factory::getInstance()->getClaimData($this->getClaimId());

        foreach($this->_validateProfiles as $profile) {
            switch($profile) {
                case "dispatcherId":
                    if($claimData->dispetcher != $this->_expectedValues['dispatcherId']) {
                        $throwException = true;
                    }

                    break;

                case "carNumber":
                    if((int) $claimData->car_number != (int) $this->_expectedValues['carNumber']) {
                        $throwException = true;
                    }

                    break;

                case "transportDeliveryTime":
                    if($claimData->transport_delivery_time != $this->_expectedValues['transportDeliveryTime']) {
                        $throwException = true;
                    }

                    break;

                case "rentPrice":
                    if($claimData->rent_price != $this->_expectedValues['rentPrice']) {
                        $throwException = true;
                    }

                    break;
            }
        }

        if($throwException) {
            throw new \Exception("Ваши изменения по неизвестным причинам не были сохранены в заявке. Обратитесь к системному администратору.");
        }
    }
}