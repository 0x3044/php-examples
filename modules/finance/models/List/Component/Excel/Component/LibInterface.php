<?php
use App_Filter_Excel_Converter as ExcelConverter;

interface Finance_Model_List_Component_Excel_Component_LibInterface
{
    public function __construct(ExcelConverter $excelConverter);

    /**
     * @return ExcelConverter
     */
    public function getExcel();

    /**
     * Возвращает true, если конфигуратор имеет возможность создать поле с данным названием
     * @param string $excelFieldName
     * @return bool
     */
    public function has($excelFieldName);

    /**
     * Создает поле
     * @param string $excelFieldName
     */
    public function setUpField($excelFieldName);
}