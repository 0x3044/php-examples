<?php

interface Finance_Model_List_Component_Excel_Component_ConfiguratorInterface
{
    public function addField($fieldName);

    public function addFieldsFromArray(array $fields);

    public function removeField($fieldName);

    public function removeFieldsFromArray(array $fields);

    public function clear();

    public function setUp();
}