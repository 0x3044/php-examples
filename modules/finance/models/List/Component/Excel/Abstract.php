<?php
use App_Filter_Excel_Converter as ExcelConverter;
use App_Filter_Excel_Component_Map as ExcelMap;
use App_Spl_Collection_Abstract as SplCollection;
use Finance_Model_List_Component_Excel_Component_FieldCollection as FieldCollection;
use Finance_Model_List_Component_Excel_Component_ConfiguratorInterface as ConfiguratorInterface;
use Finance_Model_List_Component_Excel_Component_LibInterface as LibInterface;

abstract class Finance_Model_List_Component_Excel_Abstract implements LibInterface, ConfiguratorInterface
{
    /**
     * @var ExcelConverter
     */
    protected $_excelConverter;

    /**
     * @var FieldCollection
     */
    protected $_fields;

    public function __construct(ExcelConverter $excelConverter)
    {
        $this->_excelConverter = $excelConverter;
        $this->_fields = new FieldCollection();
    }

    public final function setUp()
    {
        $this->getExcel()->setMap($this->getExcelMap());

        foreach($this->getFields() as $fieldName) {
            $this->setUpField($fieldName);
        }
    }

    public function addField($fieldName)
    {
        $this->getFields()->add($fieldName);
    }

    public function addFieldsFromArray(array $fields)
    {
        if(is_array($fields) && count($fields)) {
            foreach($fields as $fieldName) {
                $this->addField($fieldName);
            }
        }
    }

    public function removeField($fieldName)
    {
        $this->getFields()->remove($fieldName);
    }

    public function removeFieldsFromArray(array $fields)
    {
        if(is_array($fields) && count($fields)) {
            foreach($fields as $fieldName) {
                $this->getFields()->remove($fieldName);
            }
        }
    }

    public function clear()
    {
        $this->getFields()->clear();
    }

    /**
     * @return ExcelConverter
     */
    public final function getExcel()
    {
        return $this->_excelConverter;
    }

    /**
     * @return SplCollection
     */
    public final function getFields()
    {
        return $this->_fields;
    }

    /**
     * @param \App_Filter_Excel_Component_Map $excelMap
     */
    public function setExcelMap($excelMap)
    {
        $this->_excelMap = $excelMap;
    }

    /**
     * @return \App_Filter_Excel_Component_Map
     */
    public function getExcelMap()
    {
        return $this->getExcel()->getMap();
    }
}