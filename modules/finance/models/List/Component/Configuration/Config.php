<?php
use App_Spl_Hash_Abstract as SplHash;

class Finance_Model_List_Component_Configuration_Config extends SplHash
{
    /**
     * Return true if key => item is allowed for this collection
     * @param string $key
     * @param mixed $item
     * @return bool
     */
    public function isItemAllowed($key, $item)
    {
        return is_bool($item);
    }
}