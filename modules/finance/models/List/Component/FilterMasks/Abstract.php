<?php
use Finance_Model_Filter_Abstract as Filter;
use App_Finance_Recipient_RecipientInterface as Recipient;
use Finance_Model_List_Component_FilterMasks_Component_MaskCollection as MaskCollection;
use Finance_Model_List_Component_FilterMasks_Component_ConfiguratorInterface as ConfiguratorInterface;

abstract class Finance_Model_List_Component_FilterMasks_Abstract implements ConfiguratorInterface
{
    /**
     * Переданный конструктору фильтр
     * @var Filter
     */
    protected $_filter;

    /**
     * Переданный конструктору получатель
     * @var Recipient
     */
    protected $_recipient;

    /**
     * Переданные конструктору список статусов запросов
     * @var array
     */
    protected $_ticketStatus = array();

    /**
     * Коллекция масок
     * @var MaskCollection
     */
    protected $_masks;

    /**
     * Библиотека масок для фильтров в финансовом саппорте
     * Маски автоматически ограничивают свой выбор в соответствии с типам запросов, указанными в переданном получателе
     * @param Finance_Model_Filter_Abstract $filter
     * @param App_Finance_Recipient_RecipientInterface $recipient
     * @param array $ticketStatus
     */
    public final function __construct(Filter $filter, Recipient $recipient, array $ticketStatus)
    {
        $this->_filter = $filter;
        $this->_recipient = $recipient;
        $this->_ticketStatus = $ticketStatus;
        $this->_masks = new MaskCollection();
    }

    /**
     * {@inheritdoc}
     */
    public final function setUp()
    {
        foreach($this->getMasks() as $maskName) {
            $this->setUpMask($maskName);
        }
    }

    /**
     * {@inheritdoc}
     * @param string $maskName
     */
    public function addMask($maskName)
    {
        $this->getMasks()->add($maskName);
    }

    /**
     * {@inheritdoc}
     * @param array $masks
     */
    public function addMasksFromArray(array $masks)
    {
        if(is_array($masks) && count($masks)) {
            foreach($masks as $maskName) {
                $this->addMask($maskName);
            }
        }
    }

    /**
     * {@inheritdoc}
     * @param string $maskName
     * @throws Exception
     */
    public function removeMask($maskName)
    {
        $this->getMasks()->remove($maskName);
    }

    /**
     * {@inheritdoc}
     * @param array $masks
     * @throws Exception
     */
    public function removeMasksFromArray(array $masks)
    {
        if(is_array($masks) && count($masks)) {
            foreach($masks as $maskName) {
                $this->getMasks()->remove($maskName);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function clear()
    {
        $this->getMasks()->clear();
    }

    /**
     * {@inheritdoc}
     * @return \Finance_Model_Filter_Abstract
     */
    public final function getFilter()
    {
        return $this->_filter;
    }

    /**
     * {@inheritdoc}
     * @return MaskCollection
     */
    public final function getMasks()
    {
        return $this->_masks;
    }

    /**
     * Возвращает переданный конструктору получатель
     * @return App_Finance_Recipient_RecipientInterface
     */
    public function getRecipient()
    {
        return $this->_recipient;
    }

    /**
     * Возвращает переданный конструктору список статусов запросов
     * @return array
     */
    public function getTicketStatus()
    {
        return $this->_ticketStatus;
    }

    /**
     * Возврашает ID типов запросов в виде строки для SQL-запроса
     * @return array
     */
    protected function _getTicketTypeIdsAsString()
    {
        return implode(',', $this->getRecipient()->getTicketTypeIds());
    }

    /**
     * Возврашает список статусов запросов в виде строки для SQL-запроса
     * @return array
     */
    protected function _getTicketStatusAsString()
    {
        return implode(',', $this->_ticketStatus);
    }
}