<?php
use Finance_Model_List_Component_FilterMasks_Component_LibInterface as LibInterface;

interface Finance_Model_List_Component_FilterMasks_Component_ConfiguratorInterface extends LibInterface
{
    public function addMask($maskName);

    public function addMasksFromArray(array $masks);

    public function removeMask($maskName);

    public function removeMasksFromArray(array $masks);

    public function clear();

    public function setUp();
}
