<?php
use Finance_Model_Filter_Abstract as Filter;

interface Finance_Model_List_Component_FilterMasks_Component_LibInterface
{
    /**
     * @return \Finance_Model_Filter_Abstract
     */
    public function getFilter();

    /**
     * Возвращает true, если конфигуратор имеет возможность создать маску с данным названием
     * @param string $maskName
     * @return string
     */
    public function has($maskName);

    /**
     * Создает маску и конфиг для фильтра
     * @param string $maskName
     */
    public function setUpMask($maskName);
}