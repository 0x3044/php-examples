<?php
use App_Spl_Collection_Abstract as SplCollection;

class Finance_Model_List_Component_FilterMasks_Component_MaskCollection extends SplCollection
{
    /**
     * Return true if item is allowed for this collection
     * @param $item
     * @return bool
     */
    public function isItemAllowed($item)
    {
        return is_string($item) && strlen($item) > 0;
    }
}