<?php
use App_Finance_Recipient_RecipientInterface as Recipient;
use Finance_Model_List_Component_Configuration_Config as Hash;

class Finance_Model_List_Component_Configuration
{
    /**
     * Дефолтное количество записей в результате фильтра
     */
    const DEFAULT_RECORDS_PER_PAGE = 15;

    /**
     * Получатель
     * @var Recipient
     */
    protected $_recipient;

    /**
     * Список полей
     * @var Hash
     */
    protected $_fields;

    /**
     * Список форм
     * @var Hash
     */
    protected $_forms;

    /**
     * Список кнопок
     * @var Hash
     */
    protected $_buttons;

    /**
     * Список фильтров
     * @var Hash
     */
    protected $_filters;

    /**
     * Список полей в excel-выгрузке
     * @var Hash
     */
    protected $_excelFields;

    /**
     * Список полей в главной форме
     * @var array
     */
    protected $_mainFormFields = array();

    /**
     * Количество результатов в выгрузке
     * @var int
     */
    protected $_recordsPerPage = self::DEFAULT_RECORDS_PER_PAGE;

    /**
     * URL конфига фильтра
     * @var string
     */
    protected $filterConfigUrl;

    /**
     * URL маски фильтра
     * @var string
     */
    protected $filterMaskUrl;

    /**
     * URL результата фильтра
     * @var string
     */
    protected $filterResultUrl;

    /**
     * URL excel-выгрузки
     * @var string
     */
    protected $filterExcelUrl;

    /**
     * URL получения информации о новых тикетах
     * @var string
     */
    protected $notifyUrl;

    /**
     * Id новых записей в саппорте(базовое)
     * @var array
     */
    protected $notifyBaseIds;

    /**
     * Дефолтное поле для сортировки
     * @var string
     */
    protected $filterDefaultSortColumn;

    /**
     * Дефолтный порядок сортировки
     * @var string desc, asc
     */
    protected $filterDefaultSortType;

    /**
     * Статус(int)
     * @var int
     */
    protected $_ticketStatusInt;

    /**
     * Статус (int[])
     * @var int[]
     */
    protected $_ticketStatuses;

    /**
     * Статус(string)
     * @var string
     */
    protected $_ticketStatusString;

    /**
     * Типы тикетов в выборке фильтра
     * @var array
     */
    protected $_ticketTypes = array();

    /**
     * Включен/не включен декоратор "рабочий диапазон"
     * @var bool
     */
    protected $_workPeriodFilterEnabled = false;

    /**
     * Редактируемы/не редактируемы поля
     * @var bool
     */
    protected $_editable = false;

    /**
     * Форма для открытия по даблклику
     * @var bool
     */
    protected $_doubleClickMainForm = "main";

    /**
     * @construct
     */
    public function __construct()
    {
        $this->_fields = new Hash();
        $this->_forms = new Hash();
        $this->_buttons = new Hash();
        $this->_filters = new Hash();
        $this->_excelFields = new Hash();
    }

    /**
     * Заполнить конфиг значениями из массива
     * @param array $modelConfig ["forms" => (..), "fields" => (..), "buttons" => (..)]
     * @throws Exception
     */
    public function setUp(Array $modelConfig)
    {
        foreach(array('ticketTypes', 'workPeriodEnabled') as $requiredParams) {
            if(!(isset($modelConfig[$requiredParams]))) {
                throw new \Exception("Config `{$requiredParams}` is required");
            }
        }

        $this->setTicketTypes($modelConfig['ticketTypes']);
        $this->setWorkPeriodFilterEnabled((bool) $modelConfig['workPeriodEnabled']);

        if(isset($modelConfig['mainFormFields'])) {
            $this->setMainFormFields($modelConfig['mainFormFields']);
        }

        if(isset($modelConfig['editable'])) {
            $this->setEditable((bool) $modelConfig['editable']);
        }

        if(isset($modelConfig['doubleClickMainForm'])) {
            $this->setDoubleClickMainForm($modelConfig['doubleClickMainForm']);
        }

        foreach(array('fields', 'forms', 'buttons', 'filters', 'excel', 'excelFields') as $config) {
            if(isset($modelConfig[$config])) {
                switch($config) {
                    default:
                        throw new \Exception("Unknown config");
                        break;

                    case 'fields':
                        $hash = $this->getFields();
                        break;

                    case 'forms':
                        $hash = $this->getForms();
                        break;

                    case 'buttons':
                        $hash = $this->getButtons();
                        break;

                    case 'filters':
                        $hash = $this->getFilters();
                        break;

                    case "excel":
                    case "excelFields":
                        $hash = $this->getExcelFields();
                        break;
                }
                $hash->addHashArray($modelConfig[$config]);
            }
        }
    }

    /**
     * Возвращает конфигурацию модели в виде массива
     * @return array
     */
    public function toArray()
    {
        $fields = $this->getFields()->toArray();
        $enabledFields = array();

        foreach($fields as $fieldName => $isEnabled) {
            if($isEnabled) {
                $enabledFields[] = $fieldName;
            }
        }

        return array(
            'recipient' => $this->getRecipient()->toArray(),
            'statusInt' => $this->getTicketStatusInt(),
            'statusString' => $this->getTicketStatusString(),
            'fields' => $enabledFields,
            'forms' => $this->getForms()->toArray(),
            'buttons' => $this->getButtons()->toArray(),
            'ticketTypes' => $this->_ticketTypes,
            'mainFormFields' => $this->getMainFormFields(),
            'editable' => $this->getEditable(),
            'urlTicketId' => isset($_GET['ticketId']) ? $_GET['ticketId'] : 0,
            'doubleClickMainForm' => $this->getDoubleClickMainForm(),
            'notifyUrl' => $this->getNotifyUrl(),
            'notifyBaseIds' => $this->getNotifyBaseIds(),
            'filter' => array(
                'configUrl' => $this->getFilterConfigUrl(),
                'maskUrl' => $this->getFilterMaskUrl(),
                'resultUrl' => $this->getFilterResultUrl(),
                'excelUrl' => $this->getFilterExcelUrl(),
                'recordsPerPage' => $this->getRecordsPerPage(),
                'defaultSortColumn' => $this->getFilterDefaultSortColumn(),
                'defaultSortType' => $this->getFilterDefaultSortType()
            ),
            'workPeriod' => array(
                "url" => $this->getWorkPeriodUrl(),
                'uniqueId' => $this->getWorkPeriodUniqueId(),
                'enabled' => $this->getWorkPeriodFilterEnabled(),
                'startDate' => $this->getWorkPeriodStartDate(),
                'endDate' => $this->getWorkPeriodEndDate()
            )
        );
    }

    /**
     * @param \Finance_Model_List_Component_Configuration_Config $buttons
     */
    public function setButtons($buttons)
    {
        $this->_buttons = $buttons;
    }

    /**
     * @return \Finance_Model_List_Component_Configuration_Config
     */
    public function getButtons()
    {
        return $this->_buttons;
    }

    /**
     * @param \Finance_Model_List_Component_Configuration_Config $fields
     */
    public function setFields($fields)
    {
        $this->_fields = $fields;
    }

    /**
     * @return \Finance_Model_List_Component_Configuration_Config
     */
    public function getFields()
    {
        return $this->_fields;
    }

    /**
     * @param \Finance_Model_List_Component_Configuration_Config $forms
     */
    public function setForms($forms)
    {
        $this->_forms = $forms;
    }

    /**
     * @return \Finance_Model_List_Component_Configuration_Config
     */
    public function getForms()
    {
        return $this->_forms;
    }

    /**
     * @param int $recordsPerPage
     * @throws InvalidArgumentException
     */
    public function setRecordsPerPage($recordsPerPage)
    {
        if(!(is_numeric($recordsPerPage) && $recordsPerPage > 0 && $recordsPerPage < 100)) {
            throw new \InvalidArgumentException("Invalid recordsPerPage, required int(0..100), got ".var_export($recordsPerPage));
        }

        $this->_recordsPerPage = (int) $recordsPerPage;
    }

    /**
     * @return int
     */
    public function getRecordsPerPage()
    {
        return $this->_recordsPerPage;
    }

    /**
     * @param string $filterConfigUrl
     */
    public function setFilterConfigUrl($filterConfigUrl)
    {
        $this->filterConfigUrl = $filterConfigUrl;
    }

    /**
     * @return string
     */
    public function getFilterConfigUrl()
    {
        return $this->filterConfigUrl;
    }

    /**
     * @param string $filterMaskUrl
     */
    public function setFilterMaskUrl($filterMaskUrl)
    {
        $this->filterMaskUrl = $filterMaskUrl;
    }

    /**
     * @return string
     */
    public function getFilterMaskUrl()
    {
        return $this->filterMaskUrl;
    }

    /**
     * @param string $filterResultUrl
     */
    public function setFilterResultUrl($filterResultUrl)
    {
        $this->filterResultUrl = $filterResultUrl;
    }

    /**
     * @return string
     */
    public function getFilterResultUrl()
    {
        return $this->filterResultUrl;
    }

    /**
     * @param string $filterExcelUrl
     */
    public function setFilterExcelUrl($filterExcelUrl)
    {
        $this->filterExcelUrl = $filterExcelUrl;
    }

    /**
     * @return string
     */
    public function getFilterExcelUrl()
    {
        return $this->filterExcelUrl;
    }

    /**
     * @param string $notifyUrl
     */
    public function setNotifyUrl($notifyUrl)
    {
        $this->notifyUrl = $notifyUrl;
    }

    /**
     * @return string
     */
    public function getNotifyUrl()
    {
        return $this->notifyUrl;
    }

    /**
     * @array int $notifyBaseIds
     */
    public function setNotifyBaseIds($baseCount)
    {
        $this->notifyBaseIds = $baseCount;
    }

    /**
     * @return array
     */
    public function getNotifyBaseIds()
    {
        return $this->notifyBaseIds;
    }

    /**
     * @param string $filterDefaultSortColumn
     */
    public function setFilterDefaultSortColumn($filterDefaultSortColumn)
    {
        $this->filterDefaultSortColumn = $filterDefaultSortColumn;
    }

    /**
     * @return string
     */
    public function getFilterDefaultSortColumn()
    {
        return $this->filterDefaultSortColumn;
    }

    /**
     * @param string $filterDefaultSortType
     */
    public function setFilterDefaultSortType($filterDefaultSortType)
    {
        $this->filterDefaultSortType = $filterDefaultSortType;
    }

    /**
     * @return string
     */
    public function getFilterDefaultSortType()
    {
        return $this->filterDefaultSortType;
    }

    /**
     * @param int $ticketStatusInt
     * @throws InvalidArgumentException
     */
    public function setTicketStatusInt($ticketStatusInt)
    {
        if(!(is_int($ticketStatusInt))) {
            throw new \InvalidArgumentException("_ticketStatusInt should be int, got ".var_export($ticketStatusInt));
        }

        $this->_ticketStatusInt = $ticketStatusInt;
        $this->_ticketStatuses = array($ticketStatusInt);
    }

    /**
     * @return int
     */
    public function getTicketStatusInt()
    {
        return $this->_ticketStatusInt;
    }

    /**
     * @param \int[] $ticketStatuses
     */
    public function setTicketStatuses($ticketStatuses)
    {
        $this->_ticketStatuses = $ticketStatuses;
        $this->_ticketStatusInt = $ticketStatuses[0];
    }

    /**
     * @return \int[]
     */
    public function getTicketStatuses()
    {
        return $this->_ticketStatuses;
    }

    /**
     * @param string $ticketStatusString
     */
    public function setTicketStatusString($ticketStatusString)
    {
        $this->_ticketStatusString = $ticketStatusString;
    }

    /**
     * @return string
     */
    public function getTicketStatusString()
    {
        if(is_null($this->_ticketStatusString) && $this->_ticketStatusInt) {
            $this->_ticketStatusString = App_Finance_Ticket_Component_Status_Type_Default_Status::getStatusNameById($this->_ticketStatusInt);
        }

        return $this->_ticketStatusString;
    }

    /**
     * @param array $ticketTypes
     */
    public function setTicketTypes($ticketTypes)
    {
        $this->_ticketTypes = $ticketTypes;
    }

    /**
     * @return array
     */
    public function getTicketTypes()
    {
        return $this->_ticketTypes;
    }

    /**
     * @param boolean $workPeriodFilterEnabled
     */
    public function setWorkPeriodFilterEnabled($workPeriodFilterEnabled)
    {
        $this->_workPeriodFilterEnabled = $workPeriodFilterEnabled;
    }

    /**
     * @return string
     */
    public function getWorkPeriodUniqueId()
    {
        return $this->getRecipient()->getName().'_'.$this->getTicketStatusString();
    }

    /**
     * @return string
     */
    public function getWorkPeriodUrl()
    {
        return "/finance/list/{$this->getRecipient()->getName()}/{$this->getTicketStatusString()}/setworkperiod";
    }

    /**
     * @return boolean
     */
    public function getWorkPeriodFilterEnabled()
    {
        return $this->_workPeriodFilterEnabled;
    }

    public function getWorkPeriodStartDate()
    {
        return isset($_SESSION['filterWorkPeriod'][$this->getWorkPeriodUniqueId()]['startDateTimestamp'])
            ? $_SESSION['filterWorkPeriod'][$this->getWorkPeriodUniqueId()]['startDateTimestamp']
            : false;
    }

    public function getWorkPeriodEndDate()
    {
        return isset($_SESSION['filterWorkPeriod'][$this->getWorkPeriodUniqueId()]['endDateTimestamp'])
            ? $_SESSION['filterWorkPeriod'][$this->getWorkPeriodUniqueId()]['endDateTimestamp']
            : false;
    }

    /**
     * @return \Finance_Model_List_Component_Configuration_Config
     */
    public function getExcelFields()
    {
        return $this->_excelFields;
    }

    /**
     * @return \Finance_Model_List_Component_Configuration_Config
     */
    public function getFilters()
    {
        return $this->_filters;
    }

    /**
     * @param array $mainFormFields
     */
    public function setMainFormFields(array $mainFormFields)
    {
        $this->_mainFormFields = $mainFormFields;
    }

    /**
     * @return array
     */
    public function getMainFormFields()
    {
        return $this->_mainFormFields;
    }

    /**
     * @param boolean $editable
     */
    public function setEditable($editable)
    {
        $this->_editable = $editable;
    }

    /**
     * @return boolean
     */
    public function getEditable()
    {
        return $this->_editable;
    }

    /**
     * @param boolean $doubleClickMainForm
     */
    public function setDoubleClickMainForm($doubleClickMainForm)
    {
        $this->_doubleClickMainForm = $doubleClickMainForm;
    }

    /**
     * @return boolean
     */
    public function getDoubleClickMainForm()
    {
        return $this->_doubleClickMainForm;
    }

    /**
     * @param \App_Finance_Recipient_RecipientInterface $recipient
     */
    public function setRecipient($recipient)
    {
        $this->_recipient = $recipient;
    }

    /**
     * @throws Exception
     * @return \App_Finance_Recipient_RecipientInterface
     */
    public function getRecipient()
    {
        if(!($this->_recipient instanceof Recipient)) {
            throw new \Exception("No recipient available");
        }

        return $this->_recipient;
    }
}