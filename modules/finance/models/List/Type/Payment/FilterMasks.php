<?php
use Finance_Model_List_Type_AbstractType_FilterMasks as FilterMasks;

class Finance_Model_List_Type_Payment_FilterMasks extends FilterMasks
{
    /**
     * Payment Sum
     * @return App_Filter_Component_Config
     */
    public function createMaskPaymentSum()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "payment_sum", "payment_sum");

        return $config->create(array(
            'caption' => 'Сумма',
            'fieldName' => 'payment_sum',
            'filterUse' => 'select',
            'filterType' => 'integer',
            'options' => "
                SELECT DISTINCT
                  tp.`sum` AS v,
                  tp.`sum` AS t
                FROM ticket AS t
                LEFT JOIN ticket_payment AS tp ON tp.`ticket_id` = t.`id`
                WHERE t.`ticket_type_id` IN ({$this->_getTicketTypeIdsAsString()}) AND t.`status` IN ({$this->_getTicketStatusAsString()})
            "
        ));
    }

    /**
     * Payment Type
     * @return App_Filter_Component_Config
     */
    public function createMaskPaymentType()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "payment_type", "payment_type");

        $options = array();

        foreach(App_Finance_Ticket_Type_Payment_Form_DataSource::getInstance()->getPaymentTypes()->getAvailablePaymentTypes() as $paymentType) {
            $options[] = array(
                'v' => $paymentType['id'],
                't' => $paymentType['title']
            );
        }

        return $config->create(array(
            'caption' => 'Тип оплаты',
            'fieldName' => 'payment_type',
            'filterUse' => 'select',
            'filterType' => 'integer',
            'options' => $options
        ));
    }

    /**
     * Payment Document Number
     * @return App_Filter_Component_Config
     */
    public function createMaskPaymentDocumentNumber()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "payment_document_number", "payment_document_number");

        return $config->create(array(
            'caption' => 'Номер документа',
            'fieldName' => 'payment_document_number',
            'filterUse' => 'select',
            'filterType' => 'string',
            'options' => "
                SELECT DISTINCT
                  tp.`document_number` AS v,
                  tp.`document_number` AS t
                FROM ticket AS t
                LEFT JOIN ticket_payment AS tp ON tp.`ticket_id` = t.`id`
                WHERE t.`ticket_type_id` IN ({$this->_getTicketTypeIdsAsString()}) AND t.`status` IN ({$this->_getTicketStatusAsString()})
            "
        ));
    }

    /**
     * Payment From Date
     * @return App_Filter_Component_Config
     */
    public function createMaskPaymentFromDate()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("date_day_range", "payment_from_date", "payment_from_date");

        return $config->create(array(
            'caption' => 'От даты',
            'fieldName' => 'payment_from_date',
            'filterUse' => 'date',
            'filterType' => 'integer',
            'options' => array()
        ));
    }

    /**
     * Client Id
     * @return App_Filter_Component_Config
     */
    public function createMaskClientId()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "client_id", "client_id");

        return $config->create(array(
            'caption' => 'Клиент',
            'fieldName' => 'client_id',
            'options' => "
                SELECT DISTINCT
                  ticket_payment.client_id as v,
                    CASE LENGTH(clients.`u_title`)
                      WHEN 0 THEN clients.`s_title`
                      ELSE clients.`u_title`
                    END AS t
                FROM ticket_payment
                LEFT JOIN clients ON clients.id = ticket_payment.client_id
                LEFT JOIN ticket ON ticket.id = ticket_payment.ticket_id
            "
        ));
    }

    /**
     * Orgstructure Id
     * @return App_Filter_Component_Config
     */
    public function createMaskOrgstructureId()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "orgstructure_id", "orgstructure_id");

        return $config->create(array(
            'caption' => 'Отдел',
            'fieldName' => 'orgstructure_id',
            'filterUse' => 'string',
            'options' => "
                SELECT DISTINCT
                    o.`id` AS v,
                    o.`title` AS t
                FROM ticket t
                LEFT JOIN orgstructure_users ou ON t.`author_user_id` = ou.`userId`
                LEFT JOIN orgstructure o ON o.`id` = ou.`oId`
                WHERE t.`ticket_type_id` IN ({$this->_getTicketTypeIdsAsString()}) AND t.`status` IN ({$this->_getTicketStatusAsString()})
            "
        ));
    }
}