<?php
use Finance_Model_List_Type_AbstractType_Excel as AbstractExcel;

class Finance_Model_List_Type_Payment_Excel extends AbstractExcel
{
    /**
     * Payment Type
     */
    public function createExcelFieldPaymentType()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "payment_type_name",
            "title" => "Тип оплаты",
            "align" => "center",
            "width" => 10
        ));
    }

    /**
     * Payment Sum
     */
    public function createExcelFieldPaymentSum()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "payment_sum",
            "title" => "Сумма",
            "align" => "center",
            "width" => 10
        ));
    }

    /**
     * Payment Annotation
     */
    public function createExcelFieldPaymentAnnotation()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "payment_annotation",
            "title" => "Примечание",
            "align" => "center",
            "width" => 15
        ));
    }

    /**
     * Payment Document Number
     */
    public function createExcelFieldPaymentDocumentNumber()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "payment_document_number",
            "title" => "Номер документа",
            "align" => "center",
            "width" => 15
        ));
    }

    /**
     * Ru Payment From Date
     */
    public function createExcelFieldRuPaymentFromDate()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "ru_payment_from_date",
            "title" => "От даты",
            "align" => "center",
            "width" => 15
        ));
    }
}