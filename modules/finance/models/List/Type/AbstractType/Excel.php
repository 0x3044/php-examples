<?php
use Finance_Model_List_Component_Excel_Abstract as AbstractExcel;
use Cash_Model_Planned_State_Manager as StateManager;
use Cash_Model_Planned_State as State;

abstract class Finance_Model_List_Type_AbstractType_Excel extends AbstractExcel
{
    /**
     * Менеджер статусов заявок
     * @var StateManager
     */
    private $_stateManager;

    /**
     * Возвращает менеджер статусов заявков
     * @return Cash_Model_Planned_State_Manager
     */
    protected function _getStateManager()
    {
        if(!($this->_stateManager instanceof StateManager)) {
            $this->_stateManager = new StateManager();
        }

        return $this->_stateManager;
    }

    /**
     * Возвращает connectivity-коллбэк
     * @param $connectivityFieldName
     * @param string|null $connectivityPairFieldName
     * @param bool $filterHtml
     * @return callable
     */
    protected function _connectivityField($connectivityFieldName, $connectivityPairFieldName = null, $filterHtml = false)
    {
        return function (PHPExcel_Worksheet $sheet, $cellName, $fieldName, stdClass $rowData) use ($connectivityFieldName, $connectivityPairFieldName, $filterHtml) {
            if(!($connectivityPairFieldName)) {
                $connectivityPairFieldName = $connectivityFieldName;
            }

            if(isset($rowData->connectivity_configuration) && is_object($rowData->connectivity_configuration)) {
                if(isset($rowData->connectivity_configuration->fields->$connectivityFieldName)) {
                    $reversed = $rowData->connectivity_configuration->reversed;
                    $topTitle = $rowData->connectivity_configuration->topTitle;
                    $bottomTitle = $rowData->connectivity_configuration->bottomTitle;

                    $topValue = $rowData->$connectivityFieldName;
                    $bottomValue = $rowData->connectivity_configuration->fields->$connectivityPairFieldName;

                    if($filterHtml) {
                        $topValue = preg_replace('/\<br(\s*)?\/?\>/i', "\n", stripslashes($topValue));
                        $bottomValue = preg_replace('/\<br(\s*)?\/?\>/i', "\n", stripslashes($bottomValue));
                    }

                    if(!strlen($topValue)) { $topValue = '—'; }
                    if(!strlen($bottomValue)) { $bottomValue = '—'; }

                    if($reversed) {
                        $rowData->$connectivityFieldName = $topTitle .":\n".$bottomValue."\n\n{$bottomTitle}:\n".$topValue;
                    }else{
                        $rowData->$connectivityFieldName = $topTitle .":\n".$topValue."\n\n{$bottomTitle}:\n".$bottomValue;
                    }
                }
            }
        };
    }

    /**
     * Возвращает true, если конфигуратор имеет возможность создать поле с данным названием
     * @param string $excelFieldName
     * @return bool
     */
    public function has($excelFieldName)
    {
        return method_exists($this, $this->_excelFieldNameToMethodName($excelFieldName));
    }

    /**
     * Создает поле
     * @param string $excelFieldName
     * @throws Exception
     */
    public function setUpField($excelFieldName)
    {
        $methodName = $this->_excelFieldNameToMethodName($excelFieldName);

        if(!($this->has($excelFieldName))) {
            throw new \Exception("ExcelField configuration for field `{$excelFieldName}` not found");
        }

        $this->$methodName();
    }

    /**
     * Camel case
     * @param string $excelFieldName
     * @return string
     */
    protected function _excelFieldNameToMethodName($excelFieldName)
    {
        $words = explode('_', strtolower($excelFieldName));

        $return = '';
        foreach($words as $word) {
            $return .= ucfirst(trim($word));
        }

        return "createExcelField".$return;
    }

    /**
     * Id
     */
    public function createExcelFieldId()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "id",
            "title" => "#",
            "align" => "center",
            "width" => 5
        ));
    }

    /**
     * Orgstructure
     */
    public function createExcelFieldOrgstructure()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "orgstructure",
            "title" => "Отдел",
            "align" => "left",
            "width" => 10.71
        ));
    }

    /**
     * Client Name
     */
    public function createExcelFieldClientName()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "client_name",
            "title" => "Название компании",
            "align" => "left",
            "width" => 17.5,
            "cellCallback" => $this->_connectivityField('client_name')
        ));
    }

    /**
     * Manager Name
     */
    public function createExcelFieldManagerName()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "manager_name",
            "title" => "Менеджер",
            "align" => "left",
            "width" => 16
        ));
    }

    /**
     * Manager Phone
     */
    public function createExcelFieldManagerPhone()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "manager_phone",
            "title" => "Телефон",
            "align" => "left",
            "width" => 20,
            "cellCallback" => function (PHPExcel_Worksheet $sheet, $cellName, $fieldName, stdClass $rowData) {
                    $rowData->manager_phone = str_replace(',', "\n", $rowData->manager_phone);
                }
        ));
    }

    /**
     * Claim Id
     */
    public function createExcelFieldClaimId()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("link", array(
            "field" => "claim_id",
            "title" => "Заявка",
            "align" => "center",
            "width" => 9.28,
            "urlCallback" => function ($rowData) {
                    return 'http://'.$_SERVER["HTTP_HOST"].'/'.$rowData->claim_url;
                },
            "cellCallback" => function (PHPExcel_Worksheet $sheet, $cellName, $fieldName, stdClass $rowData) {
                    if(is_numeric($rowData->claim_id)) {
                        $rowData->claim_id = 'TASK'.$rowData->claim_id;
                    }

                    if(isset($rowData->connectivity_configuration) && is_object($rowData->connectivity_configuration)) {
                        if(isset($rowData->connectivity_configuration->fields->claim_id)) {
                            $connectivityClaimId = $rowData->connectivity_configuration->fields->claim_id;

                            if(is_numeric($connectivityClaimId)) {
                                $connectivityClaimId = 'TASK'.$connectivityClaimId;
                            }

                            $rowData->claim_id = $rowData->claim_id."\n".$connectivityClaimId;

                            $cellStyle = $sheet->getStyle($cellName);
                            $cellStyle->getAlignment()->setWrapText(true);
                        }
                    }
                }
        ));
    }

    /**
     * Claim Date
     */
    public function createExcelFieldClaimDate()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("date", array(
            "field" => "claim_date",
            "title" => "Дата исполнения заявки",
            "align" => "center",
            "width" => 25,
            "dateFormat" => 'dd/mm/yyyy hh:mm'
        ));
    }

    /**
     * Date Created
     */
    public function createExcelFieldDateCreated()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("date", array(
            "field" => "date_created",
            "title" => "Дата создания",
            "align" => "center",
            "width" => 10.86,
            "dateFormat" => 'dd/mm/yyyy hh:mm'
        ));
    }

    /**
     * Date To Process
     */
    public function createExcelFieldDateToProcess()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("date", array(
            "field" => "date_to_process",
            "title" => "Выполнить до",
            "align" => "center",
            "width" => 10.86,
            "dateFormat" => 'dd/mm/yyyy hh:mm',
            "cellCallback" => function (PHPExcel_Worksheet $sheet, $cellName, $fieldName, stdClass $rowData) {
                    $status = $rowData->status;
                    $isCompleted = (bool) $rowData->is_completed;

                    if(!($isCompleted) && $status == App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_ACTIVE) {
                        $bgColor = false;
                        $bgColorSoon = 'FFFFA500';
                        $bgColorImmediately = 'FFA52A2A';

                        $currentDate = new Zend_Date();
                        $dateToProcess = new Zend_Date($rowData->date_to_process_timestamp, Zend_Date::TIMESTAMP);

                        /** @var $difference Zend_Date */
                        $difference = $dateToProcess->sub($currentDate);

                        $measure = new Zend_Measure_Time($difference->toValue(), Zend_Measure_Time::SECOND);
                        $measure->convertTo(Zend_Measure_Time::MINUTE);
                        $differenceInMinutes = $measure->getValue();

                        if($differenceInMinutes < 15) {
                            $bgColor = $bgColorImmediately;
                        } else if($differenceInMinutes >= 15 && $differenceInMinutes <= 30) {
                            $bgColor = $bgColorSoon;
                        }

                        if($bgColor) {
                            /** @var $cellStyle PHPExcel_Style */
                            $cellStyle = $sheet->getStyle($cellName);
                            $cellStyle->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));
                            $cellStyle->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                            $cellStyle->getFill()->setStartColor(new PHPExcel_Style_Color($bgColor));
                        }
                    }
                }
        ));
    }

    /**
     * Invoice Ids
     */
    public function createExcelFieldInvoiceIds()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "invoice_ids",
            "title" => "№ счета",
            "align" => "center",
            "width" => 6.5,
            "cellCallback" => function (PHPExcel_Worksheet $sheet, $cellName, $fieldName, stdClass $rowData) {
                    if(!(strlen($rowData->invoice_ids))) {
                        $rowData->invoice_ids = '-';
                    }
                }
        ));
    }

    /**
     * Date Processed
     */
    public function createExcelFieldDateProcessed()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("date", array(
            "field" => "date_processed",
            "title" => "Дата обработки",
            "align" => "center",
            "width" => 10.85,
            "dateFormat" => 'dd/mm/yyyy hh:mm'
        ));
    }

    /**
     * Process Delay
     */
    public function createExcelFieldProcessDelay()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "process_delay",
            "title" => "Просрочка",
            "align" => "center",
            "width" => 10,
        ));
    }

    /**
     * Ticket Type
     */
    public function createExcelFieldTicketType()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "ticket_type",
            "title" => "Тип документа",
            "align" => "left",
            "width" => 16.57
        ));
    }

    /**
     * Credit Limit Exceed
     */
    public function createExcelFieldCreditLimitExceed()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "credit_limit_exceed",
            "title" => "Кредитный лимит",
            "align" => "center",
            "width" => 8,
            "cellCallback" => function (PHPExcel_Worksheet $sheet, $cellName, $fieldName, stdClass $rowData) {
                    $credit_limit_exceed = $rowData->credit_limit_exceed;

                    if($credit_limit_exceed) {
                        $rowData->credit_limit_exceed = "ПРЕВЫШЕН";

                        /** @var $cellStyle PHPExcel_Style */
                        $cellStyle = $sheet->getStyle($cellName);
                        $cellStyle->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));
                        $cellStyle->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                        $cellStyle->getFill()->setStartColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_RED));
                    } else {
                        $rowData->credit_limit_exceed = "ОК";
                    }
                }
        ));
    }

    /**
     * Unload Depot Worktime
     */
    public function createExcelFieldUnloadDepotWorktime()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "unload_depot_worktime",
            "title" => "Время работы выгрузки",
            "align" => "center",
            "width" => 12,
            "cellCallback" => $this->_connectivityField('unload_depot_worktime')
        ));
    }

    /**
     * Unload Address
     */
    public function createExcelFieldUnloadAddress()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "unload_address",
            "title" => "Адрес выгрузки",
            "align" => "center",
            "width" => 12,
            "cellCallback" => $this->_connectivityField('unload_address')
        ));
    }

    /**
     * Car Type
     */
    public function createExcelFieldCarType()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "car_type",
            "title" => "Тип машины",
            "align" => "center",
            "width" => 12
        ));
    }

    /**
     * Aprox Claim Weight
     */
    public function createExcelFieldAproxClaimWeight()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "aprox_claim_weight",
            "title" => "Ориентировочный вес",
            "align" => "center",
            "width" => 10,
            "cellCallback" => $this->_connectivityField("aprox_claim_weight", "aprox_claim_weight", true)
        ));
    }

    /**
     * Dispatcher Name
     */
    public function createExcelFieldDispatcherName()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "dispatcher_name",
            "title" => "Диспетчер",
            "align" => "center",
            "width" => 10
        ));
    }

    /**
     * Car
     */
    public function createExcelFieldCar()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "car",
            "title" => "Машина",
            "align" => "center",
            "width" => 10,
            "cellCallback" => function (PHPExcel_Worksheet $sheet, $cellName, $fieldName, stdClass $rowData) {
                    $rowData->car = $rowData->car ? 'Наша' : 'Самовывоз';
                }
        ));
    }

    /**
     * Car Number
     */
    public function createExcelFieldCarNumber()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "car_number",
            "title" => "Номер машины",
            "align" => "center",
            "width" => 10,
            "cellCallback" => function (PHPExcel_Worksheet $sheet, $cellName, $fieldName, stdClass $rowData) {
                    if(!(isset($rowData->claim_has_car)) || $rowData->claim_has_car) {
                        if(!(strlen($rowData->car_number))) {
                            $rowData->car_number = 'Не указана!';

                            /** @var $cellStyle PHPExcel_Style */
                            $cellStyle = $sheet->getStyle($cellName);
                            $cellStyle->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));
                            $cellStyle->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                            $cellStyle->getFill()->setStartColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_DARKRED));
                        }
                    } else {
                        $rowData->car_number = '(не наша)';
                    }
                }
        ));
    }

    /**
     * Rent Price
     */
    public function createExcelFieldRentPrice()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "rent_price",
            "title" => "Сумма найма",
            "align" => "center",
            "width" => 10
        ));
    }

    /**
     * Transport Delivery Time
     */
    public function createExcelFieldTransportDeliveryTime()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "transport_delivery_time",
            "title" => "Время подачи",
            "align" => "center",
            "width" => 10,
            "cellCallback" => $this->_connectivityField('transport_delivery_time')
        ));
    }

    /**
     * Claim Type Name
     */
    public function createExcelFieldClaimTypeName()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "claim_type_name",
            "title" => "Тип заявки",
            "align" => "center",
            "width" => 13.28
        ));
    }

    /**
     * Cash Planned State
     */
    public function createExcelFieldCashPlannedState()
    {
        $excelMap = $this->getExcelMap();
        $stateManager = $this->_getStateManager();

        $excelMap->create("plain", array(
            "field" => "cash_planned_state",
            "title" => "Статус оплаты/документов",
            "align" => "center",
            "width" => 20.28,
            "cellCallback" => function (PHPExcel_Worksheet $sheet, $cellName, $fieldName, stdClass $rowData) use ($stateManager) {
                    $state = $stateManager->createStateByStateId($rowData->cash_planned_state);
                    $rowData->cash_planned_state = $state->getClaimStateTitle();

                    switch($state->getColor()) {
                        default:
                            $bgColor = "FFF0F0F0";
                            $color = PHPExcel_Style_Color::COLOR_BLACK;
                            break;

                        case State::COLOR_YELLOW:
                            $bgColor = PHPExcel_Style_Color::COLOR_YELLOW;
                            $color = PHPExcel_Style_Color::COLOR_BLACK;
                            break;

                        case State::COLOR_RED:
                            $bgColor = PHPExcel_Style_Color::COLOR_DARKRED;
                            $color = PHPExcel_Style_Color::COLOR_WHITE;
                            break;

                        case State::COLOR_GREEN:
                            $bgColor = PHPExcel_Style_Color::COLOR_GREEN;
                            $color = PHPExcel_Style_Color::COLOR_BLACK;
                            break;
                    }

                    /** @var $cellStyle PHPExcel_Style */
                    $cellStyle = $sheet->getStyle($cellName);
                    $cellStyle->getFont()->setColor(new PHPExcel_Style_Color($color));
                    $cellStyle->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                    $cellStyle->getFill()->setStartColor(new PHPExcel_Style_Color($bgColor));
                }
        ));
    }

    /**
     * Claim Changes
     */
    public function createExcelFieldClaimChanges()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "claim_changes",
            "title" => "Изменения",
            "align" => "center",
            "width" => 20,
            "cellCallback" => function (PHPExcel_Worksheet $sheet, $cellName, $fieldName, stdClass $rowData) {
                    if($rowData->claim_changes) {
                        $rowData->claim_changes = 'Проведены изменения';

                        /** @var $cellStyle PHPExcel_Style */
                        $cellStyle = $sheet->getStyle($cellName);
                        $cellStyle->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                        $cellStyle->getFill()->setStartColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_YELLOW));
                    } else {
                        $rowData->claim_changes = 'Изменений не проводилось';
                    }
                }
        ));
    }

    /**
     * Num Ticket Documents
     */
    public function createExcelFieldNumTicketDocuments()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "num_ticket_documents",
            "title" => "✐",
            "align" => "center",
            "width" => 3.57,
            "cellCallback" => function (PHPExcel_Worksheet $sheet, $cellName, $fieldName, stdClass $rowData) {
                    $rowData->num_ticket_documents = $rowData->num_ticket_documents > 0 ? "Да" : "Нет";
                }
        ));
    }

    /**
     * Claim Status
     */
    public function createExcelFieldClaimStatus()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "claim_status",
            "title" => "Статус заявки",
            "align" => "center",
            "width" => 10
        ));
    }

    /**
     * Claim Annotation Dispatcher
     */
    public function createExcelFieldClaimAnnotationDispatcher()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "claim_annotation_dispatcher",
            "title" => "Примечание",
            "align" => "center",
            "width" => 20,
            "cellCallback" => $this->_connectivityField('claim_annotation_dispatcher', 'claim_annotation_dispatcher', true)
        ));
    }

    /**
     * Claim Annotation Depot
     */
    public function createExcelFieldClaimAnnotationDepot()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "claim_annotation_depot",
            "title" => "Примечание",
            "align" => "center",
            "width" => 20
        ));
    }

    /**
     * Author User Name
     */
    public function createExcelFieldAuthorUserName()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "author_user_name",
            "title" => "Автор",
            "align" => "center",
            "width" => 15
        ));
    }
}