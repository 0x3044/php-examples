<?php
use App_Filter_Excel_Converter as ExcelConverter;
use Finance_Model_List_Component_Configuration as Configuration;
use Finance_Model_List_Component_Excel_Abstract as ExcelConfiguration;
use Finance_Model_List_Component_FilterMasks_Abstract as FilterMasks;
use App_Filter_Component_Decorator_Type_DefaultWhereConditions_Decorator as DefaultWhereConditionsDecorator;
use App_Filter_Component_Decorator_Type_WorkPeriod_Decorator as WorkPeriodDecorator;
use Finance_Model_Filter_Abstract as Filter;

abstract class Finance_Model_List_Type_AbstractType_Model
{
    /**
     * Конфигурация модели
     * @var Configuration
     */
    protected $_configuration;

    /**
     * Фильтр модели
     * @var Filter
     */
    protected $_filter;

    /**
     * Маски/конфиги к фильтру
     * @var FilterMasks
     */
    protected $_filterMasks;

    /**
     * Конвертер фильтра в excel-файла
     * @var ExcelConverter
     */
    protected $_excelConverter;

    /**
     * Конфигурация для excel-конвертера
     * @var ExcelConfiguration
     */
    protected $_excelConfiguration;

    /**
     * Модель контроллера
     * Включает в себя фильтр, конфигурацию(передающаяся во вьюху js-скрипту через JSON), конвертер и конфигурации
     * фильтра и конвертера.
     */
    public final function __construct()
    {
        $this->_configuration = new Configuration();
    }

    /**
     * Настройка модели
     */
    public final function setUp()
    {
        $configuration = $this->getConfiguration();
        $recipient = $configuration->getRecipient()->getName();
        $status = $configuration->getTicketStatusString();

        if(!($status) || !($recipient)) {
            throw new \Exception("Status/recipient required");
        }

        $configuration->setFilterConfigUrl("/finance/list/{$recipient}/{$status}/getfilterconfig");
        $configuration->setFilterMaskUrl("/finance/list/{$recipient}/{$status}/getfiltermask");
        $configuration->setFilterResultUrl("/finance/list/{$recipient}/{$status}/getfilterresult");
        $configuration->setFilterExcelUrl("/finance/list/{$recipient}/{$status}/getfilterexcel");
        $configuration->setNotifyUrl("/finance/list/{$recipient}/{$status}/count");

        if(!($configuration->getFilterDefaultSortColumn())) {
            if($configuration->getTicketStatusInt() == App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_ACTIVE) {
                $configuration->setFilterDefaultSortColumn("date_to_process_timestamp");
                $configuration->setFilterDefaultSortType("asc");
            } else {
                $configuration->setFilterDefaultSortColumn("id");
                $configuration->setFilterDefaultSortType("desc");
            }
        }

        $this->getFilter()->setRecordsPerPage($configuration->getRecordsPerPage());

        $this->_setUpFilterDecorators();
        $this->_setUpFilterMasks();
    }

    /**
     * Настройка конвертера и конфигурации конвертера
     * @param App_Filter_Excel_Converter $excelConverter
     */
    public final function setUpExcel(ExcelConverter $excelConverter)
    {
        $excelConverter->setFilter($this->getFilter());
        $this->setExcelConverter($excelConverter);

        $excelConfiguration = $this->getExcelConfiguration();

        foreach($this->getConfiguration()->getExcelFields()->toArray() as $fieldName => $fieldEnabled) {
            if($fieldEnabled) {
                $excelConfiguration->addField($fieldName);
            }
        }

        $this->getExcelConfiguration()->setUp();
    }

    /**
     * Настройка масок и конфигураций фильтра
     */
    protected final function _setUpFilterMasks()
    {
        $configuration = $this->getConfiguration();
        $filterMasks = $this->getFilterMasks();

        foreach($configuration->getFilters()->toArray() as $filterName => $filterEnabled) {
            if($filterEnabled) {
                $filterMasks->addMask($filterName);
            }
        }

        $filterMasks->setUp();
    }

    /**
     * Настройка декораторов к фильтру
     */
    protected final function _setUpFilterDecorators()
    {
        $this->_setUpFilterDefaultWhereConditionsDecorator();

        if($this->getConfiguration()->getWorkPeriodFilterEnabled()) {
            $this->_setUpFilterWorkPeriodDecorator();
        }
    }

    /**
     * Настройка дефолтных условий к фильтру
     * @throws Exception
     */
    protected function _setUpFilterDefaultWhereConditionsDecorator()
    {
        $wheres = array();
        $filter = $this->getFilter();

        // Status filtration
        $wheres[] = sprintf("status in (%s)", implode(',', $this->getConfiguration()->getTicketStatuses()));

        /** @var $dbTicketTypes App_Db_TicketType */
        $dbTicketTypes = App_Db::get(DB_TICKET_TYPE);
        $ticketTypes = $this->getConfiguration()->getTicketTypes();

        if(is_array($ticketTypes) && count($ticketTypes)) {
            $ticketTypeIds = array();

            foreach($ticketTypes as $ticketType) {
                $ticketTypeIds[] = $dbTicketTypes->getTicketTypeIdByName($ticketType);
            }

            $wheres[] = "ticket_type_id IN (".implode(',', $ticketTypeIds).")";
        }

        $decorator = new DefaultWhereConditionsDecorator();
        $decorator->getWhereConditions()->applyFromArray($wheres);

        $filter->addDecorator($decorator);
    }

    /**
     * Настройка декоратора "рабочий диапазон"
     */
    protected function _setUpFilterWorkPeriodDecorator()
    {
        $this->getFilter()->addDecorator(new WorkPeriodDecorator($this->getConfiguration()->getWorkPeriodUniqueId(), 'date_created'));
    }

    /**
     * Создание масок и конфигураций к фильтру
     * @internal param \Finance_Model_Filter_Abstract $filter
     * @return FilterMasks
     */
    abstract protected function _createFilterMasks();

    /**
     * Создание фильтра
     * @return Filter
     */
    abstract protected function _createFilter();

    /**
     * Создание конфигурации excel-конвертера
     * @return ExcelConfiguration
     */
    abstract protected function _createExcelConfiguration();

    /**
     * Возвращает JSON-представление модели
     * @return string
     */
    public function toJson()
    {
        return json_encode($this->getConfiguration()->toArray());
    }

    /**
     * Возвращает конфигурацию модели
     * @return \Finance_Model_List_Component_Configuration
     */
    public final function getConfiguration()
    {
        return $this->_configuration;
    }

    /**
     * Возвращает фильтр
     * @return \Finance_Model_Filter_Abstract
     */
    public final function getFilter()
    {
        if(!($this->_filter instanceof Filter)) {
            $this->_filter = $this->_createFilter();
        }

        return $this->_filter;
    }

    /**
     * Возвращает конфигурацию фильтра
     * @return Finance_Model_List_Component_FilterMasks_Abstract
     */
    public final function getFilterMasks()
    {
        if(!($this->_filterMasks instanceof FilterMasks)) {
            $this->_filterMasks = $this->_createFilterMasks();
        }

        return $this->_filterMasks;
    }

    /**
     * Устаноить excel-конвертер
     * @param \App_Filter_Excel_Converter $excelConverter
     */
    public function setExcelConverter($excelConverter)
    {
        $this->_excelConverter = $excelConverter;
    }

    /**
     * Возвращает excel-конвертер для данной модели
     * @return App_Filter_Excel_Converter
     * @throws Exception
     */
    public final function getExcelConverter()
    {
        if(!($this->_excelConverter instanceof ExcelConverter)) {
            throw new \Exception("No excel converter available");
        }

        return $this->_excelConverter;
    }

    /**
     * Возвращает конфигурацию excel-конвертера
     * @return \Finance_Model_List_Component_Excel_Abstract
     */
    public function getExcelConfiguration()
    {
        if(!($this->_excelConfiguration instanceof ExcelConfiguration)) {
            $this->_excelConfiguration = $this->_createExcelConfiguration();
        }

        return $this->_excelConfiguration;
    }
}