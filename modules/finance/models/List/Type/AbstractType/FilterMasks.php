<?php
use Finance_Model_List_Component_FilterMasks_Abstract as FilterMasks;
use Cash_Model_Planned_State_Manager as StateManager;
use Cash_Model_Planned_State as State;

abstract class Finance_Model_List_Type_AbstractType_FilterMasks extends FilterMasks
{
    /**
     * Значение маски для машины менеджера
     * @const int
     */
    const CAR_MANAGER_CHOICE = 2;

    /**
     * Возвращает true, если конфигуратор имеет возможность создать маску с данным названием
     * @param string $maskName
     * @return string
     */
    public function has($maskName)
    {
        return method_exists($this, $this->_maskNameToMethodName($maskName));
    }

    /**
     * Создает маску и конфиг для фильтра
     * @param string $maskName
     * @throws Exception
     */
    public function setUpMask($maskName)
    {
        $methodName = $this->_maskNameToMethodName($maskName);

        if(!($this->has($maskName))) {
            throw new \Exception("Mask configuration for field `{$maskName}` not found");
        }

        $this->$methodName();
    }

    /**
     * Camel case
     * @param string $maskName
     * @return string
     */
    protected function _maskNameToMethodName($maskName)
    {
        $words = explode('_', strtolower($maskName));

        $return = '';
        foreach($words as $word) {
            $return .= ucfirst(trim($word));
        }

        return "createMask".$return;
    }

    /**
     * Id
     * @return App_Filter_Component_Config
     */
    public function createMaskId()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "id", "id");

        return $config->create(array(
            'caption' => 'Номер запроса',
            'fieldName' => 'id',
            'filterUse' => 'id',
            'filterType' => 'integer',
            'options' => "
                SELECT DISTINCT
                  ticket.`id` AS v,
                  ticket.`id` AS t
                FROM ticket
                WHERE ticket.`ticket_type_id` IN ({$this->_getTicketTypeIdsAsString()}) AND ticket.`status` IN ({$this->_getTicketStatusAsString()})
            "
        ));
    }

    /**
     * Orgstructure Id
     * @return App_Filter_Component_Config
     */
    public function createMaskOrgstructureId()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "orgstructure_id", "orgstructure_id");

        return $config->create(array(
            'caption' => 'Отдел',
            'fieldName' => 'orgstructure_id',
            'filterUse' => 'string',
            'options' => "
                SELECT DISTINCT
                    o.`id` AS v,
                    o.`title` AS t
                FROM ticket t
                LEFT JOIN claims ON t.`claim_id` = claims.`id`
                LEFT JOIN clients ON claims.`client_id` = clients.`id`
                LEFT JOIN orgstructure_users ou ON claims.`manager_id` = ou.`userId`
                LEFT JOIN orgstructure o ON o.`id` = ou.`oId`
                WHERE t.`ticket_type_id` IN ({$this->_getTicketTypeIdsAsString()}) AND t.`status` IN ({$this->_getTicketStatusAsString()})
            "
        ));
    }

    /**
     * Client Id
     * @return App_Filter_Component_Config
     */
    public function createMaskClientId()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "client_id", "client_id");

        return $config->create(array(
            'caption' => 'Клиент',
            'fieldName' => 'client_id',
            'options' => "
                SELECT DISTINCT
                    clients.id AS v,
                    CASE LENGTH(clients.`u_title`)
                      WHEN 0 THEN clients.`s_title`
                      ELSE clients.`u_title`
                    END as t
                FROM ticket
                LEFT JOIN claims ON ticket.claim_id = claims.id
                LEFT JOIN clients ON claims.`client_id` = clients.`id`
                WHERE ticket.`ticket_type_id` IN ({$this->_getTicketTypeIdsAsString()}) AND ticket.`status` IN ({$this->_getTicketStatusAsString()})
            "
        ));
    }

    /**
     * Manager Id
     * @return App_Filter_Component_Config
     */
    public function createMaskManagerId()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "manager_id", "manager_id");

        return $config->create(array(
            'caption' => 'Менеджер',
            'fieldName' => 'manager_id',
            'filterUse' => 'string',
            'options' => "
                SELECT DISTINCT
                  claims.`manager_id` AS v,
                  users.name AS t
                FROM ticket
                LEFT JOIN claims ON ticket.claim_id = claims.id
                LEFT JOIN users ON claims.`manager_id` = users.id
                WHERE ticket.`ticket_type_id` IN ({$this->_getTicketTypeIdsAsString()}) AND ticket.`status` IN ({$this->_getTicketStatusAsString()})
            "
        ));
    }

    /**
     * Claim Id
     * @return App_Filter_Component_Config
     */
    public function createMaskConnectivityClaimId()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "connectivity_claim_id", "connectivity_claim_id");

        return $config->create(array(
            'caption' => 'Заявка',
            'fieldName' => 'connectivity_claim_id',
            'options' => "
                SELECT DISTINCT * FROM (
                    SELECT
                        CASE claims.`connectivity`
                            WHEN 0 THEN claims.`id`
                            WHEN NULL THEN claims.`id`
                            ELSE CONCAT(claims.id, ',', claims.connectivity)
                        END AS v,
                        CASE claims.`connectivity`
                            WHEN 0 THEN claims.`id`
                            WHEN NULL THEN claims.`id`
                            ELSE CONCAT(claims.id, ', ', claims.connectivity)
                        END AS t
                    FROM claims
                    RIGHT JOIN ticket ON ticket.`claim_id` = claims.`id`
                    WHERE ticket.`ticket_type_id` IN ({$this->_getTicketTypeIdsAsString()}) AND ticket.`status` IN ({$this->_getTicketStatusAsString()})
                    UNION
                    SELECT
                        CASE claims.`connectivity`
                            WHEN 0 THEN claims.`id`
                            WHEN NULL THEN claims.`id`
                            ELSE CONCAT(claims.id, ',', claims.connectivity)
                        END AS v,
                        CASE claims.`connectivity`
                            WHEN 0 THEN claims.`id`
                            WHEN NULL THEN claims.`id`
                            ELSE CONCAT(claims.id, ', ', claims.connectivity)
                        END AS t
                    FROM claims
                    RIGHT JOIN ticket ON ticket.`claim_id` = claims.`connectivity`
                    WHERE ticket.`ticket_type_id` IN ({$this->_getTicketTypeIdsAsString()}) AND ticket.`status` IN ({$this->_getTicketStatusAsString()})
                ) t
                ORDER BY v DESC
            "
        ));
    }

    /**
     * Claim Date
     * @return App_Filter_Component_Config
     */
    public function createMaskClaimDate()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("date_day_range", "claim_date", "claim_date");

        return $config->create(array(
            'caption' => 'Дата обработки заявки',
            'fieldName' => 'claim_date',
            'filterType' => 'integer',
            'filterUse' => 'date',
            'options' => array()
        ));
    }

    /**
     * Ticket Type Id
     * @return App_Filter_Component_Config
     */
    public function createMaskTicketTypeId()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "ticket_type_id", "ticket_type_id");

        return $config->create(array(
            'caption' => 'Тип запроса',
            'fieldName' => 'ticket_type_id',
            'filterType' => 'integer',
            'filterUse' => 'select',
            'options' => "
                SELECT
                    `id` AS v,
                    `description` AS t
                FROM ticket_type
            "
        ));
    }

    /**
     * Process Delay
     * @return App_Filter_Component_Config
     */
    public function createMaskProcessDelay()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("minutes_range", "process_delay", "process_delay");

        return $config->create(array(
            'caption' => 'Просрочка',
            'fieldName' => 'process_delay',
            'filterType' => 'integer',
            'filterUse' => 'select',
            'options' => array(
                array("v" => 0.5 * 60, "t" => "До 30 минут"),
                array("v" => 1 * 60, "t" => "До 1 часа"),
                array("v" => 2 * 60, "t" => "До 2 часов"),
                array("v" => 5 * 60, "t" => "До 5 часов"),
                array("v" => 24 * 60, "t" => "До суток"),
                array("v" => 0, "t" => "Более суток"),
            )
        ));
    }

    /**
     * Date Created
     * @return App_Filter_Component_Config
     */
    public function createMaskDateCreated()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("date_day_range", "date_created", "date_created");

        return $config->create(array(
            'caption' => 'Дата создания',
            'fieldName' => 'date_created',
            'filterType' => 'integer',
            'filterUse' => 'date',
            'options' => array()
        ));
    }

    /**
     * Date To Process
     * @return App_Filter_Component_Config
     */
    public function createMaskDateToProcess()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("date_day_range", "date_to_process", "date_to_process");

        return $config->create(array(
            'caption' => 'Выполнить до',
            'fieldName' => 'date_to_process',
            'filterType' => 'integer',
            'filterUse' => 'date',
            'options' => array()
        ));
    }

    /**
     * Date Processed
     * @return App_Filter_Component_Config
     */
    public function createMaskDateProcessed()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("date_day_range", "date_processed", "date_processed");

        return $config->create(array(
            'caption' => 'Дата обработки',
            'fieldName' => 'date_processed',
            'filterType' => 'integer',
            'filterUse' => 'date',
            'options' => array()
        ));
    }

    /**
     * Credit Limit Exceed
     * @return App_Filter_Component_Config
     */
    public function createMaskCreditLimitExceed()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("sign", "credit_limit_exceed", "credit_limit_exceed");

        return $config->create(array(
            'caption' => 'Кредитный лимит',
            'fieldName' => 'credit_limit_exceed',
            'filterType' => 'string',
            'filterUse' => 'select',
            'options' => array(
                array("v" => -1, "t" => "Не превышен"),
                array("v" => 1, "t" => "Превышен"),
            )
        ));
    }

    /**
     * Invoice Ids
     * @return App_Filter_Component_Config
     */
    public function createMaskInvoiceIds()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "invoice_ids", "invoice_ids");

        return $config->create(array(
            'caption' => '№ счета',
            'fieldName' => 'invoice_ids',
            'filterType' => 'string',
            'filterUse' => 'select',
            'options' => "
                SELECT DISTINCT
                    GROUP_CONCAT(DISTINCT i.id ORDER BY i.id ASC) AS v,
                    GROUP_CONCAT(DISTINCT i.id ORDER BY i.id ASC) AS t
                FROM ticket t
                LEFT JOIN claim_invoices ci ON t.`claim_id` = ci.`claim_id`
                LEFT JOIN invoice i ON ci.`invoice_id` = i.`prim_id`
                WHERE t.`ticket_type_id` IN ({$this->_getTicketTypeIdsAsString()}) AND t.`status` IN ({$this->_getTicketStatusAsString()})
                GROUP BY t.`claim_id`
                HAVING v IS NOT NULL
            "
        ));
    }

    /**
     * Unload Depot Worktime
     * @return App_Filter_Component_Config
     */
    public function createMaskUnloadDepotWorktime()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "unload_depot_worktime", "unload_depot_worktime");

        return $config->create(array(
            'caption' => 'Время работы выгрузки',
            'fieldName' => 'unload_depot_worktime',
            'filterType' => 'string',
            'filterUse' => 'select',
            'options' => "
                SELECT DISTINCT
                    clients.`unload_work_time` AS t,
                    clients.`unload_work_time` AS v
                FROM ticket
                LEFT JOIN claims ON claims.id = ticket.claim_id
                LEFT JOIN clients ON clients.id = claims.client_id
                WHERE ticket.`ticket_type_id` IN ({$this->_getTicketTypeIdsAsString()}) AND ticket.`status` IN ({$this->_getTicketStatusAsString()})
            "
        ));
    }

    /**
     * Car Type
     * @return App_Filter_Component_Config
     */
    public function createMaskCarType()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "car_type", "car_type");

        return $config->create(array(
            'caption' => 'Тип машины',
            'fieldName' => 'car_type',
            'filterType' => 'string',
            'filterUse' => 'select',
            'options' => "
                SELECT DISTINCT
                    claims.`car_type` AS t,
                    claims.`car_type` AS v
                FROM ticket
                LEFT JOIN claims ON claims.id = ticket.claim_id
                WHERE ticket.`ticket_type_id` IN ({$this->_getTicketTypeIdsAsString()}) AND ticket.`status` IN ({$this->_getTicketStatusAsString()})
            "
        ));
    }

    /**
     * Aprox Claim Weight
     * @return App_Filter_Component_Config
     */
    public function createMaskAproxClaimWeight()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "aprox_claim_weight", "aprox_claim_weight");

        return $config->create(array(
            'caption' => 'Ориентировочный вес',
            'fieldName' => 'aprox_claim_weight',
            'filterType' => 'string',
            'filterUse' => 'select',
            'options' => "
                SELECT DISTINCT
                    claims.`aprox_claim_weight` AS t,
                    claims.`aprox_claim_weight` AS v
                FROM ticket
                LEFT JOIN claims ON claims.id = ticket.claim_id
                WHERE ticket.`ticket_type_id` IN ({$this->_getTicketTypeIdsAsString()}) AND ticket.`status` IN ({$this->_getTicketStatusAsString()})
            "
        ));
    }

    /**
     * Dispatcher Id
     * @return App_Filter_Component_Config
     */
    public function createMaskDispatcherId()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "dispatcher_id", "dispatcher_id");

        $options = array();
        $dispatchers = App_Helper_Driver::getAllDispetchers();

        foreach($dispatchers as $v => $t) {
            $options[] = array(
                'v' => (int) $v,
                't' => $t
            );
        }

        return $config->create(array(
            'caption' => 'Диспетчер',
            'fieldName' => 'dispatcher_id',
            'filterType' => 'string',
            'filterUse' => 'select',
            'options' => $options
        ));
    }

    /**
     * Car Number
     * @return App_Filter_Component_Config
     */
    public function createMaskCarNumber()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "car_number", "car_number");

        return $config->create(array(
            'caption' => 'Номер машины',
            'fieldName' => 'car_number',
            'filterType' => 'string',
            'filterUse' => 'select',
            'options' => "
                SELECT DISTINCT
                    drivers.`carNumber` AS v,
                    drivers.`carNumber` AS t
                FROM ticket
                LEFT JOIN claims ON claims.id = ticket.claim_id
                RIGHT JOIN drivers ON drivers.id = claims.car_number
                WHERE ticket.`ticket_type_id` IN ({$this->_getTicketTypeIdsAsString()}) AND ticket.`status` IN ({$this->_getTicketStatusAsString()})
            "
        ));
    }

    /**
     * Rent Price
     * @return App_Filter_Component_Config
     */
    public function createMaskRentPrice()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "rent_price", "rent_price");

        return $config->create(array(
            'caption' => 'Сумма найма',
            'fieldName' => 'rent_price',
            'filterType' => 'string',
            'filterUse' => 'select',
            'options' => "
                SELECT DISTINCT
                  claims.`rent_price` AS v,
                  claims.`rent_price` AS t
                FROM ticket
                LEFT JOIN claims ON claims.id = ticket.claim_id
                WHERE ticket.`ticket_type_id` IN ({$this->_getTicketTypeIdsAsString()}) AND ticket.`status` IN ({$this->_getTicketStatusAsString()})
            "
        ));
    }

    /**
     * Transport Delivery Time
     * @return App_Filter_Component_Config
     */
    public function createMaskTransportDeliveryTime()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "transport_delivery_time", "transport_delivery_time");

        return $config->create(array(
            'caption' => 'Время подачи',
            'fieldName' => 'transport_delivery_time',
            'filterType' => 'string',
            'filterUse' => 'select',
            'options' => "
                SELECT DISTINCT
                  claims.`transport_delivery_time` AS v,
                  claims.`transport_delivery_time` AS t
                FROM ticket
                LEFT JOIN claims ON claims.id = ticket.claim_id
                WHERE ticket.`ticket_type_id` IN ({$this->_getTicketTypeIdsAsString()}) AND ticket.`status` IN ({$this->_getTicketStatusAsString()})
            "
        ));
    }

    /**
     * Claim Type Id
     * @return App_Filter_Component_Config
     */
    public function createMaskClaimTypeId()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "claim_type_id", "claim_type_id");

        return $config->create(array(
            'caption' => 'Тип заявки',
            'fieldName' => 'claim_type_id',
            'filterType' => 'string',
            'filterUse' => 'select',
            'options' => "
                SELECT
                    claim_type_pair.`type_id` AS v,
                    claim_type_pair.`title` AS t
                FROM claim_type_pair
            "
        ));
    }

    /**
     * Cash Planned State
     * @return App_Filter_Component_Config
     */
    public function createMaskCashPlannedState()
    {
        $options = array();
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("cash_planned_state", "cash_planned_state", "cash_planned_state");

        $stateManager = new StateManager();
        $stateManager->getAvailableClaimStates();

        foreach($stateManager->getEQClaimStates() as $states) {
            if(is_array($states) && count($states) > 0) {
                /** @var $state State */
                $state = $states[0];
                $stateId = $state->getStateId();
            } else {
                $stateId = State::STATE_NO_STATE;
            }

            $state = $stateManager->getPrototypeStateByStateId($stateId);
            $options[] = array(
                'v' => $state->getClaimState(),
                't' => $state->getClaimState() == 'not_processed'
                        ? $stateManager->getPrototypeStateByName('not_processed')->getClaimStateTitle()
                        : $state->getClaimStateTitle()
            );
        }

        return $config->create(array(
            'caption' => 'Статус оплаты/док-в',
            'fieldName' => 'cash_planned_state',
            'filterType' => 'string',
            'filterUse' => 'select',
            'options' => $options
        ));
    }

    /**
     * Claim Changes
     * @return App_Filter_Component_Config
     */
    public function createMaskClaimChanges()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "claim_changes", "claim_changes");

        return $config->create(array(
            'caption' => 'Изменения в заявке',
            'fieldName' => 'claim_changes',
            'filterType' => 'string',
            'filterUse' => 'select',
            'options' => array(
                array("v" => 0, "t" => "Заявки без изменений"),
                array("v" => 1, "t" => "Заявки с изменениями"),
            )
        ));
    }

    /**
     * Num Ticket Documents
     * @return App_Filter_Component_Config
     */
    public function createMaskNumTicketDocuments()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "num_ticket_documents", "num_ticket_documents");

        return $config->create(array(
            'caption' => 'Наличие документов',
            'fieldName' => 'num_ticket_documents',
            'filterType' => 'string',
            'filterUse' => 'select',
            'options' => array(
                array("v" => 0, "t" => "Отсутствие документов"),
                array("v" => 1, "t" => "Наличие документов"),
            )
        ));
    }

    /**
     * Car
     * @return App_Filter_Component_Config
     */
    public function createMaskCar()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "car", "car");

        return $config->create(array(
            'caption' => 'Машина',
            'fieldName' => 'car',
            'filterType' => 'string',
            'filterUse' => 'select',
            'options' => array(
                array("v" => 0, "t" => "Самовывоз"),
                array("v" => 1, "t" => "Наша"),
                array("v" => self::CAR_MANAGER_CHOICE, "t" => "Менеджер")
            )
        ));
    }

    /**
     * Claim Status Id
     * @return App_Filter_Component_Config
     */
    public function createMaskClaimStatusId()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "claim_status_id", "claim_status_id");

        return $config->create(array(
            'caption' => 'Статус заявки',
            'fieldName' => 'claim_status_id',
            'filterType' => 'string',
            'filterUse' => 'select',
            'options' => "
                SELECT
                    id AS v,
                    status_title AS t
                FROM status_type
            "
        ));
    }

    /**
     * Has Client Directions
     * @return App_Filter_Component_Config
     */
    public function createMaskHasClientDirections()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "has_client_directions", "has_client_directions");

        return $config->create(array(
            'caption' => 'Схема проезда',
            'fieldName' => 'has_client_directions',
            'filterType' => 'string',
            'filterUse' => 'select',
            'options' => array(
                array("v" => 0, "t" => "Нет схемы проезда"),
                array("v" => 1, "t" => "Есть схема проезда"),
            )
        ));
    }

    /**
     * Author User Id
     * @return App_Filter_Component_Config
     */
    public function createMaskAuthorUserId()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "author_user_id", "author_user_id");

        return $config->create(array(
            'caption' => 'Автор',
            'fieldName' => 'author_user_id',
            'filterType' => 'integer',
            'filterUse' => 'select',
            'options' => "
                SELECT DISTINCT
                    t.author_user_id AS v,
                    u.name AS t
                FROM ticket t
                LEFT JOIN users u ON u.id = t.author_user_id
                WHERE t.`ticket_type_id` IN ({$this->_getTicketTypeIdsAsString()}) AND t.`status` IN ({$this->_getTicketStatusAsString()})
                ORDER BY u.name ASC
            "
        ));
    }

    /**
     * Deleted Flag
     * @return App_Filter_Component_Config
     */
    public function createMaskDeletedFlag()
    {
        $config = $this->getFilter()->getConfig();
        $mask = $this->getFilter()->getMask();

        $mask->create("select", "deleted_flag", "deleted_flag");

        return $config->create(array(
            'caption' => 'Запрос удален?',
            'fieldName' => 'deleted_flag',
            'filterType' => 'string',
            'filterUse' => 'select',
            'options' => array(
                array("v" => 1, "t" => "Да"),
                array("v" => 0, "t" => "Нет"),
            )
        ));
    }
}