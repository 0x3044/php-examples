<?php
use Finance_Model_Filter_Dispatcher as Filter;
use Finance_Model_List_Type_AbstractType_Model as AbstractModel;
use Finance_Model_List_Type_Dispatcher_Excel as ExcelConfiguration;
use Finance_Model_List_Type_Dispatcher_FilterMasks as FilterMasks;

class Finance_Model_List_Type_Dispatcher_Model extends AbstractModel
{
    /**
     * @return Filter
     */
    protected function _createFilter()
    {
        return new Filter();
    }

    /**
     * @internal param \Finance_Model_Filter_Abstract $filter
     * @return \Finance_Model_List_Component_FilterMasks_Abstract
     */
    protected function _createFilterMasks()
    {
        return new FilterMasks($this->getFilter(), App_Finance_Recipient_Factory::getInstance()->getDispatcherRecipient(), $this->getConfiguration()->getTicketStatuses());
    }

    /**
     * @return ExcelConfiguration
     */
    protected function _createExcelConfiguration()
    {
        return new ExcelConfiguration($this->getExcelConverter());
    }
}