<?php
use Finance_Model_List_Type_AbstractType_Excel as AbstractExcel;

class Finance_Model_List_Type_Accounting_Excel extends AbstractExcel
{
    public function createExcelFieldTicketType()
    {
        $excelMap = $this->getExcelMap();

        $excelMap->create("plain", array(
            "field" => "ticket_type",
            "title" => "Тип документа",
            "align" => "left",
            "width" => 16.57,
            "cellCallback" => function (PHPExcel_Worksheet $sheet, $cellName, $fieldName, stdClass $rowData) {
                    if($rowData->tk_docs_required) {
                        $rowData->ticket_type .= '(требуются документы ТТН)';
                    }
                }
        ));
    }
}