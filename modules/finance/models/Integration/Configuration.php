<?php
use App_Claim_Reference as ClaimTypeReference;
use App_Finance_Ticket_Component_Acl_Module_ClaimStatus as ClaimPermissions;
use Finance_Model_Sender_Form as FinanceSenderForm;

class Finance_Model_Integration_Configuration
{
    const DIFF_FORM_ORIG = 'orig';

    const DIFF_FORM_HEAD = 'head';

    /**
     * @var Zend_View_Interface
     */
    protected $_zendView;

    /**
     * Режим "только для чтения"
     * @var bool
     */
    protected $_readOnly;

    /**
     * Тип заявки
     * @var ClaimTypeReference
     */
    protected $_claimTypeReference;

    /**
     * Статус создания для данного типа заявков
     * @var int
     */
    protected $_claimCreateStatus;

    /**
     * Текущий статус заявки
     * @var int
     */
    protected $_claimCurrentStatus;

    /**
     * Права заявки
     * @var stdClass
     */
    protected $_claimPermissions;

    /**
     * Права пользователя на финансовый саппорт относительно заявки
     * @var ClaimPermissions
     */
    protected $_financePermissions;

    /**
     * Форма отправки запросов
     * @var FinanceSenderForm
     */
    protected $_financeSenderForm;

    /**
     * Версия заявки
     * @var string
     */
    protected $_diffBlockVersion;

    /**
     * Конфигурация интеграций финансового саппорта
     * @param Zend_View_Interface $view
     * @param App_Claim_Reference $claimTypeReference
     */
    public function __construct(Zend_View_Interface $view, ClaimTypeReference $claimTypeReference)
    {
        $this->_zendView = $view;
        $this->_claimTypeReference = $claimTypeReference;

        $this->_fetchClaimData();
    }

    /**
     * Получение различной сервисной информации о заявке
     */
    protected function _fetchClaimData()
    {
        $claimFactory = App_Claim_Factory::getInstance();
        $claimId = $this->getClaimId();
        $typeId = $this->getTypeId();

        $this->_setClaimCreateStatus($this->_getCreateStatus($typeId));

        if($claimId) {
            $claimData = $claimFactory->getClaimData($claimId);
            $this->_setClaimCurrentStatus($claimData->claim_status);
        } else {
            $this->_setClaimCurrentStatus($this->getClaimCreateStatus());
        }
    }

    /**
     * Возвращает статус создания для данного типа заявок
     * @param $typeId
     * @return int
     * @throws Exception
     */
    protected function _getCreateStatus($typeId)
    {
        return App_Claim_Factory::getInstance()->getCreateStatus($typeId);
    }

    /**
     * @return \Zend_View
     */
    public function getZendView()
    {
        return $this->_zendView;
    }

    /**
     * Возвращает описание типа заявки
     * @return \App_Claim_Reference
     */
    public function getClaimTypeReference()
    {
        return $this->_claimTypeReference;
    }

    /**
     * Возвращает ID заявки
     * @return int
     */
    public function getClaimId()
    {
        return $this->getClaimTypeReference()->getClaimId();
    }

    /**
     * Возвращает тип заявки (парный)
     * @return int
     */
    public function getTypeId()
    {
        return $this->getClaimTypeReference()->getTypeId();
    }

    /**
     * Возвращает тип заявки
     * @return int
     */
    public function getClaimTypeId()
    {
        return $this->getClaimTypeReference()->getTypeId();
    }

    /**
     * Возвращает true, если конфигурации был указан ID заявки
     * @return bool
     */
    public function hasClaimId()
    {
        return $this->getClaimId() > 0;
    }

    /**
     * Возвращает статус создания заявки
     * @return int
     */
    public function getClaimCreateStatus()
    {
        return $this->_claimCreateStatus;
    }

    /**
     * Установить статус создания
     * @param int $claimStatus
     */
    protected function _setClaimCreateStatus($claimStatus)
    {
        $this->_claimCreateStatus = $claimStatus;
    }

    /**
     * Установить текущий статус заявки
     * @param int $claimCurrentStatus
     */
    protected function _setClaimCurrentStatus($claimCurrentStatus)
    {
        $this->_claimCurrentStatus = $claimCurrentStatus;
    }

    /**
     * Возвращает текущий статус заявки
     * @return int
     */
    public function getClaimCurrentStatus()
    {
        return $this->_claimCurrentStatus;
    }

    /**
     * Включить режим "только для чтения"
     */
    public function enableReadOnly()
    {
        $this->_readOnly = true;
    }

    /**
     * Отключить режим "только для чтения"
     */
    public function disableReadOnly()
    {
        $this->_readOnly = false;
    }

    /**
     * Установить/отключить режим "только для чтения"
     * @param boolean $readOnly
     */
    public function setReadOnly($readOnly)
    {
        $this->_readOnly = (bool) $readOnly;
    }

    /**
     * Возвращает true, если
     * @return bool
     */
    public function isReadOnly()
    {
        return $this->_readOnly;
    }

    /**
     * Установить права финансового саппорта
     * @param \App_Finance_Ticket_Component_Acl_Module_ClaimStatus $financePermissions
     */
    public function setFinancePermissions($financePermissions)
    {
        $this->_financePermissions = $financePermissions;
    }

    /**
     * Возвращает права финансого саппорта
     * @return \App_Finance_Ticket_Component_Acl_Module_ClaimStatus
     */
    public function getFinancePermissions()
    {
        return $this->_financePermissions;
    }

    /**
     * Указать форму отправки запросов
     * @param \Finance_Model_Sender_Form $financeSenderForm
     */
    public function setFinanceSenderForm(FinanceSenderForm $financeSenderForm)
    {
        $this->_financeSenderForm = $financeSenderForm;
    }

    /**
     * Возвращает форму отправки запросов
     * @return \Finance_Model_Sender_Form
     */
    public function getFinanceSenderForm()
    {
        return $this->_financeSenderForm;
    }

    /**
     * Включить режим отображения формы "было-стало"
     * @param $version
     */
    public function enableDiffForm($version)
    {
        $this->_diffBlockVersion = $version;
    }

    /**
     * Отключить режим отображения формы "было-стало"
     */
    public function disableDiffForm()
    {
        $this->_diffBlockVersion = false;
    }

    /**
     * Возврашает true, если режим отображения формы "было-стало" включен
     * @return bool
     */
    public function isDiffFormEnabled()
    {
        return (bool) $this->_diffBlockVersion;
    }

    /**
     * Возвращает версию для формы "было-стало"
     * @return string
     */
    public function getDiffFormVersion()
    {
        return $this->_diffBlockVersion;
    }
}