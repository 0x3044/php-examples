<?php
use Finance_Model_Integration_IntegrationPack_ViewIntegration as ViewIntegration;
use Finance_Model_Integration_IntegrationPack_ControllerIntegration as ControllerIntegration;
use Finance_Model_Integration_Configuration as FinanceIntegrationConfiguration;

class Finance_Model_Integration_Register
{
    /**
     * @var Finance_Model_Integration_Register
     */
    protected static $_instance;

    /**
     * Конфигурация интеграции финансового саппорта
     * @var FinanceIntegrationConfiguration
     */
    protected $_configuration;

    /**
     * Интеграция финансового саппорта (View)
     * @var ViewIntegration
     */
    protected $_viewIntegration;

    /**
     * Интеграция финансового саппорта (Controller)
     * @var ControllerIntegration
     */
    protected $_controllerIntegration;

    /**
     * Синглтон
     * @return Finance_Model_Integration_Register
     */
    public static function getInstance()
    {
        if(is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * Конфигурация интеграции финансового саппорта
     * @param Finance_Model_Integration_Configuration $configuration
     * @return $this
     * @throws Exception
     */
    public function setUp(FinanceIntegrationConfiguration $configuration)
    {
        $this->_configuration = $configuration;

        $senderForm = new Finance_Model_Sender_Form($configuration->getClaimId());

        if($configuration->hasClaimId()) {
            $senderForm->createFromClaimId();
        }

        $configuration->setFinanceSenderForm($senderForm);
        $configuration->setFinancePermissions(new App_Finance_Ticket_Component_Acl_Module_ClaimStatus($configuration->getClaimTypeReference())
        );

        $viewIntegration = new ViewIntegration($configuration);
        $controllerIntegration = new ControllerIntegration($configuration);

        $this->registerViewIntegration($viewIntegration);
        $this->registerControllerIntegration($controllerIntegration);

        return $this;
    }

    /**
     * Возвращает true, если есть возможность интегрировать финансовый саппорт
     * @return bool
     */
    public function isReady()
    {
        return !(is_null($this->_configuration));
    }

    /**
     * Возвращает конфигурацию
     * @return Finance_Model_Integration_Configuration
     */
    public function getConfiguration()
    {
        return $this->_configuration;
    }

    /**
     * Регистрирует интергратор финансового саппорта (View)
     * @param Finance_Model_Integration_IntegrationPack_ViewIntegration $viewIntegrationInstance
     */
    public function registerViewIntegration(ViewIntegration $viewIntegrationInstance)
    {
        $this->_viewIntegration = $viewIntegrationInstance;
    }

    /**
     * Регистрирует интегратор финансового саппорта (Controller)
     * @param Finance_Model_Integration_IntegrationPack_ControllerIntegration $controllerIntegrationInstance
     */
    public function registerControllerIntegration(ControllerIntegration $controllerIntegrationInstance)
    {
        $this->_controllerIntegration = $controllerIntegrationInstance;
    }

    /**
     * Возвращает зарегистрированный интергратор финансового саппорта (View)
     * @return Finance_Model_Integration_IntegrationPack_ViewIntegration
     * @throws Exception
     */
    public function getViewIntegration()
    {
        if(is_null($this->_viewIntegration)) {
            throw new \Exception('FinanceViewIntegration instance is not available');
        }

        return $this->_viewIntegration;
    }

    /**
     * Возвращает зарегестрированный интегратор финансового саппорта (Controller)
     * @return Finance_Model_Integration_IntegrationPack_ControllerIntegration
     * @throws Exception
     */
    public function getControllerIntegration()
    {
        if(is_null($this->_controllerIntegration)) {
            throw new \Exception('FinanceControllerIntegration instance is not available');
        }

        return $this->_controllerIntegration;
    }
}