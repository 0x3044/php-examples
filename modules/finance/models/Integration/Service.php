<?php
use Finance_Model_Integration_Configuration as FinanceIntegrationConfiguration;
use Finance_Model_Integration_Register as FinanceIntegrationRegister;
use App_Claim_Reference as ClaimTypeReference;

class Finance_Model_Integration_Service
{
    /**
     * Синглтон
     * @var Finance_Model_Integration_Service
     */
    protected static $_instance;

    /**
     * Возвращает сервис
     * @return Finance_Model_Integration_Service
     */
    public static function getInstance()
    {
        if(is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * Возвращает интеграцию финансового саппорта
     * @return Finance_Model_Integration_Register
     * @throws Exception
     */
    public function getFinanceSupportIntegration()
    {
        $register = Finance_Model_Integration_Register::getInstance();

        if(!($register->isReady())) {
            throw new \Exception("Интеграция финансового саппорта не сконфигурирована");
        }

        return $register;
    }

    /**
     * Настройка интеграции финансового саппорта
     * @param App_Claim_Reference $claimTypeReference
     */
    public function setUp(ClaimTypeReference $claimTypeReference)
    {
        $configuration = new FinanceIntegrationConfiguration(Zend_Layout::getMvcInstance()->getView(), $claimTypeReference);

        FinanceIntegrationRegister::getInstance()->setUp($configuration);
    }
}