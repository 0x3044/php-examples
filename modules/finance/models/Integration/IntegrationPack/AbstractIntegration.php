<?php
use Finance_Model_Integration_Configuration as IntegrationConfiguration;

class Finance_Model_Integration_IntegrationPack_AbstractIntegration
{
    /**
     * Конфигурация интегрирования финансового саппорта
     * @var IntegrationConfiguration
     */
    protected $_integrationConfiguration;

    /**
     * Интеграция финансового саппорта
     * @see Finance_Model_Integration_ViewIntegration::integrateAssets
     * @see Finance_Model_Integration_ViewIntegration::integrateFinanceSenderForm
     */
    public function __construct(IntegrationConfiguration $integration_ConfigurationInstance)
    {
        $this->_integrationConfiguration = $integration_ConfigurationInstance;
    }

    /**
     * Возвращает конфигурацию интеграции финансового саппорта
     * @return Finance_Model_Integration_Configuration
     */
    public function getConfiguration()
    {
        return $this->_integrationConfiguration;
    }
}