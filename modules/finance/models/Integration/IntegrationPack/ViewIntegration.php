<?php
use Finance_Model_Integration_IntegrationPack_AbstractIntegration as AbstractIntegration;
use App_Claim_BlockService_Fields_Handler as BlockedFieldsHandler;

class Finance_Model_Integration_IntegrationPack_ViewIntegration extends AbstractIntegration
{
    /**
     * Интеграция финансового саппорта (View)
     */
    public function integrate()
    {
        $this->integrateAssets();
        $this->integrateFinanceSenderForm();
        $this->integrateDiff();
    }

    /**
     * Подключение css/js-файлов
     */
    public function integrateAssets()
    {
        /** @var $headLink App_View_Helper_HeadLink */
        $headLink = $this->getConfiguration()->getZendView()->getHelper('headLink');
        /** @var $headScript App_View_Helper_HeadScript */
        $headScript = $this->getConfiguration()->getZendView()->getHelper('headScript');

        $headLink->appendCssFiles(array(
            "/css/sass/finance/finance.css",
            "/css/sass/form/form-abstract.css",
        ));

        $headScript->appendJsFiles(array(
            '/js/jquery/jquery.ajaxStatus.js',
            '/js/jquery/jquery.ajaxHandler.js'
        ));
    }

    /**
     * Подключение формы отправки запросов
     * Ребинд кнопки "отправить"
     * @throws Exception
     * @return string|null
     */
    public function integrateFinanceSenderForm()
    {
        $zendView = $this->getConfiguration()->getZendView();

        if(!($zendView->financeSenderForm instanceof Finance_Model_Sender_Form)) {
            if(!($this->getConfiguration()->getFinanceSenderForm())) {
                $this->getConfiguration()->setFinanceSenderForm(new Finance_Model_Sender_Form($this->getConfiguration()->getClaimId()));
            }

            $zendView->financeSenderForm = $this->getConfiguration()->getFinanceSenderForm();
        }

        echo $this->getConfiguration()->getZendView()->render("abstract/claim-form/finance/sender.phtml");
    }

    /**
     * Блок статусов запросов в заявке
     * @param Claim_View_Helper_DisplayField_Factory $displayFieldsHelper
     */
    public function integrateFinanceStatus(Claim_View_Helper_DisplayField_Factory $displayFieldsHelper)
    {
        if($this->getConfiguration()->hasClaimId() && !($this->getConfiguration()->isDiffFormEnabled())) {
            $displayFieldsHelper->create('finance', 'access', $this->getConfiguration()->getFinancePermissions()->hasAccessStatus())->render();
        }
    }

    /**
     * Интеграция заголовка для diff-формы заявок
     */
    public function integrateDiff()
    {
        if($this->getConfiguration()->hasClaimId()) {
            $diffEnabled = $this->getConfiguration()->isDiffFormEnabled();
            $diffVersion = $this->getConfiguration()->getDiffFormVersion();

            $this->getConfiguration()->getZendView()->assign(array(
                'diff' => (bool) $diffEnabled,
                'diffVersion' => $diffVersion
            ));

            if($diffVersion) {
                /** @var $headScript App_View_Helper_HeadScript */
                $headScript = $this->getConfiguration()->getZendView()->getHelper('headScript');
                $headScript->appendJsFiles(array(
                    '/js/finance/claim.js'
                ));

                echo $this->getConfiguration()->getZendView()->render('abstract/claim-form/diff/highlight.phtml');
                echo $this->getConfiguration()->getZendView()->render('abstract/claim-form/diff/header.phtml');
            }
        }
    }

    /**
     * Интеграция блокировки полей в заявке
     * @param int $claimType
     * @param object $permissions
     * @throws Exception
     */
    public function integrateFieldsBlocks($claimType, $permissions)
    {
        $closeStatus = 0;

        if($this->getConfiguration()->hasClaimId() && $this->getConfiguration()->getClaimCurrentStatus() != $closeStatus) {
            $blockedFieldsHandler = new BlockedFieldsHandler($this->getConfiguration()->getClaimId());

            if(!($permissions)) {
                throw new \Exception('Отсутствует информация о правах доступа для интеграции финансового саппорта');
            }

            $blockedFields = $blockedFieldsHandler->getBlockedFields($this->getConfiguration()->getClaimId(), $claimType);

            foreach($blockedFields as $blockedField) {
                if(isset($permissions->{$blockedField}) && $permissions->{$blockedField} > 3) {
                    $permissions->{$blockedField} = 3;
                }
            }
        }
    }

    /**
     * Кнопка отправки запросов
     */
    public function integrateFinanceSupportButton()
    {
        echo $this->getConfiguration()->getZendView()->render('abstract/claim-form/finance/finance-support-button.phtml');
    }

}