<?php
use Finance_Model_Integration_IntegrationPack_AbstractIntegration as AbstractIntegration;
use App_Finance_Utils_Claim_ProductObserver as FinanceProductObserver;
use App_Finance_Utils_Claim_ProductObserver_Exception as FinanceProductObserverException;
use App_Diff_Type_Claim_Factory as ClaimDiffFactory;
use App_Finance_Utils_Claim_ProductObserver as ProductObserver;
use App_Finance_Utils_Claim_ProductObserver_Exception as ProductObserverException;
use App_Finance_Ticket_TicketInterface as Ticket;
use App_Finance_Service as FinanceService;

class Finance_Model_Integration_IntegrationPack_ControllerIntegration extends AbstractIntegration
{
    /**
     * Предопределенная базовая интеграция финансового саппорта
     */
    public function integrate()
    {
        $this->integrateFinanceIntegrationRegister();
        $this->integrateRequestParams();
        $this->integrateViewOnly();
        $this->integrateSenderForm();
    }

    /**
     * Регистрирует во View объект интеграции финансового саппорта
     */
    public function integrateFinanceIntegrationRegister()
    {
        $this->getConfiguration()->getZendView()->assign(array(
            'financeIntegration' => Finance_Model_Integration_Register::getInstance()
        ));
    }

    /**
     * Конфигурация интеграции из $_REQUEST
     */
    public function integrateRequestParams()
    {
        if($this->getConfiguration()->hasClaimId()) {
            $zendView = $this->getConfiguration()->getZendView();
            $request = Zend_Controller_Front::getInstance()->getRequest();

            $paramDiff = $request->getParam('diff');
            $paramDiffVersion = strtolower($request->getParam('version'));

            if($paramDiff) {
                $this->getConfiguration()->enableDiffForm($paramDiffVersion);
            }

            $zendView->assign(array(
                'diff' => $paramDiff,
                'diffVersion' => $paramDiffVersion
            ));
        }
    }

    /**
     * Интеграция защиты от нежелательного перевода со статуса на статус при двойном нажатии на кнопку "отправить"
     * @param object|stdClass $formData
     */
    public function integratePostFormStatusCheck($formData)
    {
        $currentClaimStatus = $this->getConfiguration()->getClaimCurrentStatus();
        $fixedClaimStatus = isset($formData->claimFixedStatus) ? $formData->claimFixedStatus : NULL;

        if(!(isset($formData->changeStatusAvaleble))) {
            $formData->changeStatusAvaleble = 0;
        }

        if($fixedClaimStatus) {
            if($currentClaimStatus != $fixedClaimStatus) {
                $formData->changeStatusAvaleble = 0;
            }
        }
    }

    /**
     * Включает флаг onlyview в случаи, если включен режим отображения Diff-формы
     * Включает режим ReadOnly в интеграции финансового саппорта, если флаг onlyview установлен в true
     */
    public function integrateViewOnly()
    {
        $zendView = $this->getConfiguration()->getZendView();

        if($this->getConfiguration()->isReadOnly()) {
            $zendView->assign("onlyview", true);
        } else if($zendView->onlyview) {
            $this->getConfiguration()->setReadOnly(true);
        } else {
            $this->getConfiguration()->setReadOnly(false);
        }

        if($zendView->onlyview) {
            $this->getConfiguration()->enableReadOnly();
        }
    }

    /**
     * Интеграция формы отправки запросов
     */
    public function integrateSenderForm()
    {
        $this->getConfiguration()->getZendView()->assign(array(
            'financeSenderForm' => $this->getConfiguration()->getFinanceSenderForm()
        ));
    }

    /**
     * Интеграция формы "было-стало"
     * Роллбак данных по заявке, если пользователем была запрошена версия "orig"
     * @param $claimData
     */
    public function integrateDiffForm($claimData)
    {
        $config = $this->getConfiguration();
        $claimId = $config->getClaimId();
        $zendView = $config->getZendView();

        if($config->hasClaimId() && $config->isDiffFormEnabled()) {
            $config->enableReadOnly();
            $zendView->onlyview = true;

            Zend_Layout::getMvcInstance()->setLayout('no-menu');

            if($config->getDiffFormVersion() == $config::DIFF_FORM_ORIG) {
                if($mapperRollback = App_Finance_Utils_Claim_MapperRollback::create($claimId, $claimData)) {
                    $mapperRollback->rollback();
                } else {
                    $config->enableDiffForm($config::DIFF_FORM_HEAD);
                }
            }

            $ticket = FinanceService::getInstance()->getUtil()->getActiveDepotTicket($claimId);

            if($ticket) {
                $modificationHandler = $ticket->getModificationHandler()->getClaim();
                $hasModifications = App_Access::get('access', 'finance>depot>depot_verify>diff') && $modificationHandler->hasModifications();

                if($hasModifications) {
                    $claimDiff = $modificationHandler->getDiff()->getClaimChanges()->getClaimFieldsChanges()->getDiff();
                    $productDiff = $modificationHandler->getDiff()->getClaimChanges()->getProductChanges();

                    $claimFactory = App_Claim_Factory::getInstance();
                    $claimData = $claimFactory->getClaimData($claimId);
                    $fieldNamePrefix = $claimFactory->typeIdToString($claimData->type_id);

                    $zendView->assign(array('diffModifications' => array(
                        'version' => $config->getDiffFormVersion(),
                        'hasModifications' => true,
                        'timeOrig' => $modificationHandler->getFixedTimestamp(),
                        'timeHead' => $modificationHandler->getActualTimestamp(),
                        'modifiedFields' => array(
                            'titles' => $claimDiff->getListOfModifiedTitles(),
                            'fields' => $claimDiff->getListOfModified()
                        ),
                        'fieldNamePrefix' => $fieldNamePrefix,
                        'addedProducts' => $productDiff->getProductsAdded()->getItemIds(),
                        'modifiedProducts' => $productDiff->getProductsModified()->getItemIds(),
                        'removedProducts' => $productDiff->getProductsRemoved()->getItemIds(),
                        'modifiedProductsFields' => $productDiff->getProductsModified()->getModifiedFields()
                    )));
                } else {
                    Zend_Controller_Front::getInstance()->getResponse()->setRedirect($claimData->url);
                }
            }
        }
    }

    /**
     * Интеграция запрета удалять товар при действующем запросе в складской саппорт
     * @param $itemId
     * @see Finance_Model_Integration_IntegrationPack_ControllerIntegration::getStartEventId
     */
    public function integrateValidateDropProduct($itemId = null)
    {
        $claimId = $this->getConfiguration()->getClaimId();

        if($itemId) {
            App_Spl_TypeCheck::getInstance()->positiveNumeric($claimId);
        }

        try {
            $productObserver = new FinanceProductObserver($claimId, $itemId);
            $productObserver->validateRemove();
        }
        catch(FinanceProductObserverException $e) {
            exit(json_encode(array('error' => $e->getMessage())));
        }
    }

    /**
     * Интеграция запрета уменьшать товар при действующем запросе в складской саппорт
     * @param $startEventId
     * @param bool $throwException
     * @throws App_Finance_Utils_Claim_ProductObserver_Exception
     * @throws Exception
     */
    public function integrateSetBlockItem($startEventId, $throwException = false)
    {
        $claimId = $this->getConfiguration()->getClaimId();

        App_Spl_TypeCheck::getInstance()->positiveNumeric($startEventId, $claimId);

        try {
            $productObserver = new ProductObserver($claimId);
            $productObserver->validateSetBlockByStartEventId($startEventId);
        }
        catch(ProductObserverException $e) {
            if($throwException) {
                throw $e;
            }else{
                Zend_Db_Table_Abstract::getDefaultAdapter()->rollBack();
                die($e->getMessage());
            }
        }
    }

    /**
     * Интеграция валидации статуса запроса
     * @param $newStatus
     */
    public function integrateValidateStatus($newStatus)
    {
        $claimId = $this->getConfiguration()->getClaimId();

        App_Spl_TypeCheck::getInstance()->positiveNumeric($claimId);

        $financeStatusHub = new App_Finance_Utils_Claim_StatusHub($claimId);
        $financeStatusHub->validate($newStatus);
    }

    /**
     * Интеграция отката/удаления запросов при выполнении каскада
     */
    public function integrateClaimCascadeRollback()
    {
        if($claimId = $this->getConfiguration()->getClaimId()) {
            $finder = new App_Finance_Ticket_Finder();
            $finder->setClaimIds($claimId);

            $tickets = $finder->find();

            if($tickets->size()) {
                /** @var $ticket Ticket */
                foreach($tickets as $ticket) {
                    $ticket->getObserversHandler()->on('onAfterClaimCascade');
                }
            }
        }
    }

    /**
     * Обновление eventId в запросах в складской саппорт
     * Действие необходимо для drop_product/set_block_item
     */
    public function updateDepotTicketModified()
    {
        $claimId = $this->getConfiguration()->getClaimId();

        App_Spl_TypeCheck::getInstance()->positiveNumeric($claimId);

        if($depotTicket = FinanceService::getInstance()->getUtil()->getActiveDepotTicket($claimId)) {
            $depotTicket->getModificationHandler()->getClaim()->updateFixedEventId();
        }
    }

    /**
     * Возвращает последний eventId заявки (для использования в integrateValidateDropProduct)
     * @return int|null
     */
    public function getStartEventId()
    {
        $claimId = $this->getConfiguration()->getClaimId();
        App_Spl_TypeCheck::getInstance()->positiveNumeric($claimId);
        $claimDiffFactory = new ClaimDiffFactory();

        return $claimDiffFactory->getRepository()->getLatestEventId($claimId);
    }
}