<?php
require_once (__DIR__).'/application.php';

use App_Cron_CronServiceFactory as CronServiceFactory;

$cronService = CronServiceFactory::create(array(
    'verbose' => true,
    'logger' => 'file'
));
$cronService->runCronServices();