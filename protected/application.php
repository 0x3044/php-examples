<?php
defined('APPLICATION_PATH')
|| define('APPLICATION_PATH', realpath(dirname(__FILE__).'/../application'));

// Define application environment
defined('APPLICATION_ENV')
|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : (isset($argv[1]) ? $argv[1] : false)));

if(!(APPLICATION_ENV)) {
    throw new \Exception('Отсутствует APPLICATION_ENV');
}

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH.'/../library'),
    realpath(APPLICATION_PATH.'/../library/PEAR'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    'development',
    APPLICATION_PATH.'/configs/application.ini'
);
$application->bootstrap();