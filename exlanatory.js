/**
 * Explanatory notes list
 * (today's code)
 * @param container
 * @constructor
 */
function Finance_SenderForm_ExplanatoryDetails(container)
{
    this.containers = {
        main: container,
        link: container.find('a[data-action="expand"]'),
        list: container.find('table[data-container="list"]'),
        records: container.find('tbody[data-container="records"]')
    };

    // link behaviour
    (function(explanatory){
        explanatory.hideDetails();

        explanatory.containers.link.unbind('click').click(function(e){
            e.preventDefault();
            explanatory.toggleDetails();
        });
    })(this);
}

Finance_SenderForm_ExplanatoryDetails.prototype = {
    buildDetails: function(details) {
        this.containers.records.html('');

        if(details.length == 0) {
            this.containers.link.hide();
        }else{
            this.containers.link.show();

            for(var n in details) {
                var record = details[n];
                var template = $($('script[data-template="sender-form-explanatory-details-record"]').html());

                template.find('span[data-attr="date"]').text(record.date);
                template.find('a[data-attr="claim-id"]').text(record.claimId).attr('href', window['linkPrefix'] + record.claimUrl);

                (function(link, dRecord) {
                    link.text(dRecord.link.fileName).click(function(e){
                        e.preventDefault();
                        Filemanager_PreviewHelper.open(dRecord.link);
                    });
                })(template.find('a[data-attr="preview-link"]'), record);

                this.containers.records.append(template);
            }
        }
    },

    showDetails: function() {
        this.containers.link.addClass('active');
        this.containers.list.show();
    },

    hideDetails: function() {
        this.containers.link.removeClass('active');
        this.containers.list.hide();
    },

    toggleDetails: function() {
        this.containers.list.is(':hidden') ? this.showDetails() : this.hideDetails();
    }
};
