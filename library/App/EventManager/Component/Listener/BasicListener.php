<?php
use App_EventManager_EventAwareInterface as EventAwareListener;

abstract class App_EventManager_Component_Listener_BasicListener implements EventAwareListener
{
    /**
     * Запуск события
     * @param string $eventId
     * @param mixed $source
     * @param array $options
     * @return mixed
     */
    public function fire($eventId, $source = NULL, array $options = array())
    {
        $filter = new Zend_Filter();
        $filter->addFilter(new Zend_Filter_Word_SeparatorToCamelCase('-'));
        $filter->addFilter(new Zend_Filter_Word_SeparatorToCamelCase('_'));

        $eventId = $filter->filter($eventId);
        $methodName = 'on'.ucfirst($eventId);

        if(method_exists($this, $methodName)) {
            $this->$methodName($options);
        }
    }
}