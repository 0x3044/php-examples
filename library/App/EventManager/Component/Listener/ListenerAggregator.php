<?php
use App_EventManager_EventAwareInterface as EventAwareInterface;

class App_EventManager_Component_Listener_ListenerAggregator
{
    /**
     * Подписчики на события
     * @var EventAwareInterface[]
     */
    protected $_subscribers = array();

    /**
     * Запуск события
     * @param $listeners
     * @param string|null $eventId
     * @param mixed $source
     * @param array $options
     * @return mixed|void
     */
    public function fire($listeners, $eventId, $source = NULL, array $options = array())
    {
        if($listeners != '*' && !(is_array($listeners))) {
            $listeners = array($listeners);
        }

        foreach($this->_subscribers as $subscriber) {
            if($listeners == '*' || in_array($subscriber->getListenerId(), $listeners)) { // filter by listener
                $subscriber->fire($eventId, $source, $options);
            }
        }
    }

    /**
     * Зарегистрировать подписчика в EventManager
     * @param EventAwareInterface $subscriber
     */
    public function register(EventAwareInterface $subscriber)
    {
        $this->_subscribers[] = $subscriber;
    }

    /**
     * Остановить обработку событий для указанного подписчика
     * @param EventAwareInterface $subscriber
     */
    public function unRegister(EventAwareInterface $subscriber)
    {
        foreach($this->_subscribers as $key => $iSubscriber) {
            if($iSubscriber == $subscriber) {
                unset($this->_subscribers[$key]);
            }
        }
    }
}