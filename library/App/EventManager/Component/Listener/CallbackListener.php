<?php
use App_EventManager_EventAwareInterface as EventAwareListener;

class App_EventManager_Component_Listener_CallbackListener implements EventAwareListener
{
    /**
     * Коллекция [eventId => [callbacks]]
     * @var Closure[]
     */
    protected $_callbacks = array();

    /**
     * Listener Id
     * @var string
     */
    protected $_listenerId;

    /**
     * Коллбэковая версия наблюдателя
     * @param $listenerId
     */
    public function __construct($listenerId)
    {
        $this->_listenerId = $listenerId;
    }

    /**
     * Возвращает ID обработчика событий
     * @return string
     */
    public function getListenerId()
    {
        return $this->_listenerId;
    }

    /**
     * Добавить обработчик событий  для указанного $eventName
     * @param $eventId
     * @param callable $callback
     */
    public function add($eventId, Closure $callback)
    {
        if(!(isset($this->_callbacks[$eventId]))) {
            $this->_callbacks[$eventId] = array();
        }

        $this->_callbacks[$eventId][] = $callback;
    }

    /**
     * Удаляет все коллбэеки для указанного $eventName
     * @param $eventId
     */
    public function destroy($eventId)
    {
        if(isset($this->_callbacks[$eventId])) {
            unset($this->_callbacks[$eventId]);
        }
    }

    /**
     * Возвращает массив из обработчиков событий для указанного $eventName
     * @param $eventId
     * @return callable
     */
    public function &getCallbacks($eventId)
    {
        if(!($this->hasCallbacksForEvent($eventId))) {
            $this->_callbacks[$eventId] = array();
        }

        return $this->_callbacks[$eventId];
    }

    /**
     * Возвращает true, если в наблюдателе есть зарегистрированные обработчики событий для указанного $eventName
     * @param $eventId
     * @return bool
     */
    public function hasCallbacksForEvent($eventId)
    {
        return isset($this->_callbacks[$eventId]);
    }

    /**
     * Запуск события
     * @param string $eventId
     * @param mixed $source
     * @param array $options
     */
    public function fire($eventId, $source = NULL, array $options = array())
    {
        if(isset($this->_callbacks[$eventId])) {
            foreach($this->_callbacks[$eventId] as $callback) {
                $callback($eventId, $options, $source);
            }
        }
    }
}