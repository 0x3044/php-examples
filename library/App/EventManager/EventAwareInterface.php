<?php

interface App_EventManager_EventAwareInterface
{
    /**
     * Возвращает ID обработчика событий
     * @return string
     */
    public function getListenerId();

    /**
     * Запуск события
     * @param string $eventId
     * @param null $source
     * @param array $options
     * @return
     */
    public function fire($eventId, $source = NULL, array $options = array());
}