<?php
use App_EventManager_Component_Listener_ListenerAggregator as Listeners;

class App_EventManager_EventManager
{
    /**
     * Менеджер событий
     * @var Listeners
     */
    protected $_listeners;

    /**
     * Сервис автоматического выписывания штрафов
     * @param Listeners $listeners
     */
    public function __construct(Listeners $listeners)
    {
        $this->_listeners = $listeners;
    }

    /**
     * Запуск события
     * @param $listeners
     * @param string $eventId
     * @param mixed $source
     * @param array $options
     * @return mixed
     */
    public function fire($listeners, $eventId, $source = NULL, array $options = array())
    {
        return $this->getListeners()->fire($listeners, $eventId, $source, $options);
    }

    /**
     * Возвращает менеджер событий
     * @return Listeners
     */
    public function getListeners()
    {
        return $this->_listeners;
    }
}