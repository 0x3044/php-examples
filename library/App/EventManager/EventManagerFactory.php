<?php
class App_EventManager_EventManagerFactory
{
    /**
     * Создает и возвращает EventManager
     * @return App_EventManager_EventManager
     */
    public static function create()
    {
        $eventManager = new App_EventManager_EventManager(
            new App_EventManager_Component_Listener_ListenerAggregator()
        );

        return $eventManager;
    }

    /**
     * Возвращает дефолтный глобальный зарегистрированный в системе EventManager
     * @return App_EventManager_EventManager
     * @throws Zend_Exception
     */
    public static function getDefaultEventManager()
    {
        return Zend_Registry::get('eventManager');
    }
}