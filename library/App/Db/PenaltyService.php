<?php
use App_Penalty_Service_RepositoryInterface as PenaltyRepositoryInterface;

class App_Db_PenaltyService extends App_Db_Abstract implements PenaltyRepositoryInterface
{
    /**
     * {@inheritdoc}
     * @var string
     */
    protected $_name = DB_PENALTY_SERVICE;

    /**
     * {@inheritdoc}
     * @var string
     */
    protected $_primary = 'id';

    /**
     * Возвращает все записи из payment_service
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function fetchPenaltySystems()
    {
        return $this->fetchAll();
    }

    /**
     * Возвращает параметры автоматической системы штрафования
     * @param int $id
     * @return array
     */
    public function getPenaltyParams($id)
    {
        App_Spl_TypeCheck::getInstance()->id($id);

        if(!($record = $this->fetchRow($this->select()->where('id = '.(int) $id), Zend_Db::FETCH_ASSOC))) {
            throw new \OutOfBoundsException(sprintf('Запись `%s` не найдена', $id));
        }

        return $record;
    }

    /**
     * Регистрирует новую схему автоматического штрафования
     * @param array $params
     */
    public function registerPenaltySystem(array $params)
    {
        $this->insert($this->unpackParams($params));
    }

    /**
     * Обновляет параметры уже существующей системы автоматического штрафования
     * @param int $id
     * @param array $params
     */
    public function updatePenaltySystem($id, $params)
    {
        App_Spl_TypeCheck::getInstance()->id($id);

        $this->update($params, array('id = ?' => $id));
    }

    /**
     * Валидирует и возвращает массов параметров для вставки/обновления записи в БД
     * @param $params
     * @return array
     */
    private function unpackParams($params)
    {
        foreach(array('name', 'scheme', 'watcher', 'period', 'sum', 'reason', 'enabled', 'options') as $field) {
            if(!(isset($params[$field]))) {
                throw new \InvalidArgumentException(sprintf('Отсутствуют данные для поля `%s`', $field));
            }
        }

        $data = array(
            'name' => trim($params['name']),
            'watcher' => trim($params['watcher']),
            'scheme' => trim($params['scheme']),
            'period' => trim($params['period']),
            'sum' => round((float) $params['sum'], 2),
            'reason' => trim($params['reason']),
            'enabled' => (int) (bool) $params['enabled'],
            'options' => json_encode($params['options'])
        );

        foreach(array('name', 'watcher', 'scheme', 'sum') as $notEmpty) {
            if(is_int($data[$notEmpty]) || is_float($data[$notEmpty])) {
                if($data[$notEmpty] <= 0) {
                    throw new \InvalidArgumentException(sprintf('Значение поля `%s` должно быть больше нуля', $notEmpty));
                }
            }else if(is_string($data[$notEmpty])){
                if(strlen($data[$notEmpty]) == 0) {
                    throw new \InvalidArgumentException(sprintf('Значение поля `%s` не должны быть пустым', $notEmpty));
                }
            }
        }

        return $data;
    }

    /**
     * Удаляет схему автоматического штрафования
     * @param $id
     */
    public function unregisterPenaltySystemById($id)
    {
        if(!(is_numeric($id))) {
            throw new \InvalidArgumentException(sprintf('Невозможно удалить стратегию автоматического штрафования `%s`', var_export($id, true)));
        }

        $this->delete(array('id = ?' => (int) $id));
    }
}