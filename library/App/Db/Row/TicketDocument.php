<?php
class App_Db_Row_TicketDocument extends App_Db_Row_Abstract
{
    /**
     * @var int
     */
    protected $_id;

    /**
     * @var string
     */
    protected $_date_created;

    /**
     * @var int
     */
    protected $_ticket_id;

    /**
     * @var string
     */
    protected $_file_name;

    /**
     * @var string
     */
    protected $_dir_name;

    /**
     * Возвращает список полей записи
     * @return array
     */
    public function getFields()
    {
        return array('id', 'date_created', 'ticket_id', 'file_name', 'dir_name');
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param int $ticket_id
     */
    public function setTicketId($ticket_id)
    {
        $this->_ticket_id = $ticket_id;
    }

    /**
     * @return int
     */
    public function getTicketId()
    {
        return $this->_ticket_id;
    }

    /**
     * @param string $file_name
     */
    public function setFileName($file_name)
    {
        $this->_file_name = $this->_filterFileName($file_name);
    }

    /**
     * @param $file_name
     * @return string
     */
    protected function _filterFileName($file_name) {

        $appTranslit = new App_Translit();

        return $appTranslit->filterFileName($file_name);
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->_file_name;
    }

    /**
     * @param string $dir_name
     */
    public function setDirName($dir_name)
    {
        $this->_dir_name = $dir_name;
    }

    /**
     * @return string
     */
    public function getDirName()
    {
        return $this->_dir_name;
    }

    /**
     * @param string $date_created
     */
    public function setDateCreated($date_created)
    {
        if($date_created instanceof Zend_Date) {
            $date_created = $date_created->toString('YYYY-MM-dd HH:mm:ss');
        }

        $this->_date_created = $date_created;
    }

    /**
     * @return string
     */
    public function getDateCreated()
    {
        return $this->_date_created;
    }

    protected function _insert() {
        $this->_date_created = Zend_Date::now()->toString('YYYY-MM-dd HH:mm:ss');
        parent::_insert();
    }
}