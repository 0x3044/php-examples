<?php

class App_Db_Row_TicketExplanatoryNote extends App_Db_Row_Abstract implements App_Finance_Ticket_Component_Attachment_AbstractAttachmentDbRowInterface
{
    /**
     * @var int
     */
    protected $_id;

    /**
     * @var int
     */
    protected $_ticket_id;

    /**
     * @var string
     */
    protected $_filename;

    /**
     * Возвращает список полей записи
     * @return array
     */
    public function getFields()
    {
        return array('id', 'ticket_id', 'filename');
    }

    /**
     * @param string $filename
     */
    public function setFilename($filename)
    {
        $this->_filename = $filename;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->_filename;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param int $ticket_id
     */
    public function setTicketId($ticket_id)
    {
        $this->_ticket_id = $ticket_id;
    }

    /**
     * @return int
     */
    public function getTicketId()
    {
        return $this->_ticket_id;
    }
}