<?php

class App_Db_Row_TicketModifiedFields extends App_Db_Row_Abstract
{
    /**
     * @var int
     */
    protected $_ticket_id;

    /**
     * @var int
     */
    protected $_claim_modified_event_id;

    /**
     * @var int
     */
    protected $_has_claim_modifications;

    /**
     * Возвращает список полей записи
     * @return array
     */
    public function getFields() {
        return array('ticket_id', 'claim_modified_event_id', 'has_claim_modifications');
    }

    /**
     * @param int $ticket_id
     */
    public function setTicketId($ticket_id) {
        $this->_ticket_id = $ticket_id;
    }

    /**
     * @return int
     */
    public function getTicketId() {
        return $this->_ticket_id;
    }

    /**
     * @param int $claim_modified_event_id
     */
    public function setClaimModifiedEventId($claim_modified_event_id) {
        $this->_claim_modified_event_id = $claim_modified_event_id;
    }

    /**
     * @return int
     */
    public function getClaimModifiedEventId() {
        return $this->_claim_modified_event_id;
    }

    /**
     * @param int $claim_has_modifications
     */
    public function setHasClaimModifications($claim_has_modifications)
    {
        $this->_has_claim_modifications = $claim_has_modifications;
    }

    /**
     * @return int
     */
    public function getHasClaimModifications()
    {
        return $this->_has_claim_modifications;
    }
}