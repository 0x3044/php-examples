<?php

class App_Db_Row_Ticket extends App_Db_Row_Abstract
{
    /**
     * @var int
     */
    protected $_id;

    /**
     * @var \DateTime
     */
    protected $_date_created;

    /**
     * @var \DateTime
     */
    protected $_date_to_process;

    /**
     * @var \DateTime
     */
    protected $_date_processed;

    /**
     * @var int
     */
    protected $_ticket_type_id;

    /**
     * @var int
     */
    protected $_author_user_id;

    /**
     * @var int
     */
    protected $_processed_user_id;

    /**
     * @var int
     */
    protected $_claim_id;

    /**
     * @var int
     */
    protected $_status;

    /**
     * @var bool
     */
    protected $_is_completed;

    /**
     * @var array
     */
    public $_json_data;

    /**
     * Возвращает список полей записи
     * @return array
     */
    public function getFields() {
        return array(
            'id', 'date_created', 'date_to_process', 'date_processed', 'ticket_type_id',
            'author_user_id', 'processed_user_id', 'claim_id', 'status', 'is_completed', 'json_data'
        );
    }

    /**
     * @param int $id
     * @throws InvalidArgumentException
     */
    public function setId($id) {
        if (!(filter_var($id, FILTER_VALIDATE_INT) || is_null($id)) || $id < 0) {
            throw new \InvalidArgumentException('Field "id" should have a int value, got '.gettype($id));
        }

        $this->_id = $id;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * @param string $dateToProcess
     * @throws Exception
     */
    public function setDateToProcess($dateToProcess) {
        if ($dateToProcess instanceof Zend_Date) {
            $dateToProcess = $dateToProcess->toString(App_Db::ZEND_DATETIME_FORMAT);
        }

        $this->_date_to_process = $dateToProcess;
    }

    /**
     * @return string
     */
    public function getDateToProcess() {
        return $this->_date_to_process;
    }

    /**
     * @param string $dateCreated
     */
    public function setDateCreated($dateCreated) {
        if ($dateCreated instanceof Zend_Date) {
            $dateCreated = $dateCreated->toString(App_Db::ZEND_DATETIME_FORMAT);
        }

        $this->_date_created = $dateCreated;
    }

    /**
     * @return string
     */
    public function getDateCreated() {
        return $this->_date_created;
    }

    /**
     * @param Zend_Date $date_processed
     */
    public function setDateProcessed($date_processed) {
        if ($date_processed instanceof Zend_Date) {
            $date_processed = $date_processed->toString(App_Db::ZEND_DATETIME_FORMAT);
        }

        $this->_date_processed = $date_processed;
    }

    /**
     * @return \DateTime
     */
    public function getDateProcessed() {
        return $this->_date_processed;
    }

    /**
     * @param int $type_id
     * @throws InvalidArgumentException
     */
    public function setTicketTypeId($type_id) {
        if (!(filter_var($type_id, FILTER_VALIDATE_INT))) {
            throw new \InvalidArgumentException('Field "type_id" should have a int value, got '.gettype($type_id));
        }

        $this->_ticket_type_id = $type_id;
    }

    /**
     * @return int
     */
    public function getTicketTypeId() {
        return $this->_ticket_type_id;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getTicketTypeName() {
        if (!($this->getTicketTypeId())) {
            throw new \Exception('ticketTypeId required');
        }

        /** @var $dbTicketTypeTable App_Db_TicketType */
        $dbTicketTypeTable = App_Db::get(DB_TICKET_TYPE);

        return $dbTicketTypeTable->getTicketTypeNameById($this->getTicketTypeId());
    }

    /**
     * @param int $author_user_id
     * @throws InvalidArgumentException
     */
    public function setAuthorUserId($author_user_id) {
        if (!(filter_var($author_user_id, FILTER_VALIDATE_INT))) {
            throw new \InvalidArgumentException('Field "_author_user_id" should have a int value, got '.gettype($author_user_id));
        }

        $this->_author_user_id = $author_user_id;
    }

    /**
     * @param int $claim_id
     * @throws InvalidArgumentException
     */
    public function setClaimId($claim_id) {
        $this->_claim_id = $claim_id;
    }

    /**
     * @return int
     */
    public function getClaimId() {
        return $this->_claim_id;
    }

    /**
     * @return int
     */
    public function getAuthorUserId() {
        return $this->_author_user_id;
    }

    /**
     * @param int $processed_user_id
     * @throws InvalidArgumentException
     */
    public function setProcessedUserId($processed_user_id) {
        if (is_null($processed_user_id)) {
            $this->_processed_user_id = NULL;

            return;
        }

        if (!(filter_var($processed_user_id, FILTER_VALIDATE_INT))) {
            throw new \InvalidArgumentException('Field "_processed_user_id" should have a int value, got '.gettype($processed_user_id));
        }

        $this->_processed_user_id = (int) $processed_user_id;
    }

    /**
     * @return int
     */
    public function getProcessedUserId() {
        return $this->_processed_user_id;
    }

    /**
     * @param int $status
     * @throws InvalidArgumentException
     */
    public function setStatus($status) {
        if (!(filter_var($status, FILTER_VALIDATE_INT))) {
            throw new \InvalidArgumentException('Field "Status" should have a int value, got '.gettype($status));
        }

        $this->_status = (int) $status;
    }

    /**
     * @return int
     */
    public function getStatus() {
        return $this->_status;
    }

    /**
     * @param boolean $is_completed
     * @throws InvalidArgumentException
     */
    public function setIsCompleted($is_completed) {
        if (!(is_numeric($is_completed) || is_bool($is_completed))) {
            throw new \InvalidArgumentException('Field "is_completed" should have a bool or int value, got '.gettype($is_completed));
        }

        $this->_is_completed = (bool) $is_completed;
    }

    /**
     * @return boolean
     */
    public function getIsCompleted() {
        return $this->_is_completed;
    }

    /**
     * @param array|string $jsonData
     * @throws InvalidArgumentException
     */
    public function setJsonData($jsonData) {
        if (is_array($jsonData)) {
            $jsonData = json_encode($jsonData);
        }

        if (!(is_string($jsonData) || is_null($jsonData))) {
            throw new \InvalidArgumentException('Field "json_data" should have a string, array or null value, got '.gettype($jsonData));
        }

        $this->_json_data = $jsonData;
    }

    /**
     * @return array
     */
    public function getJsonData() {
        return $this->_json_data;
    }

    public function validate() {
        $requiredField = array('date_created', 'date_to_process', 'ticket_type_id', '_author_user_id', 'status');

        if (!($this->getId())) {
            $this->setDateCreated(new Zend_Db_Expr('NOW()'));
        }

        foreach ($requiredField as $fieldName) {
            $fieldValue = $this->$fieldName;

            if (!($fieldValue)) {
                throw new \Exception("{$fieldName} required");
            }
        }

        $idFields = array('claim_id', 'ticket_type_id', '_author_user_id', '_processed_user_id', 'status');

        foreach ($idFields as $fieldName) {
            $fieldValue = $this->$fieldName;

            if ($fieldValue && !(is_numeric($fieldValue) && $fieldValue >= 0) && !(is_null($fieldValue))) {
                throw new \UnexpectedValueException("{$fieldName} should have a numeric value");
            }
        }

        if (!(is_string($this->_json_data) || is_null($this->_json_data))) {
            throw new \Exception('jsonData should have a string value');
        }

        foreach (array('_date_created', '_dateToProcess') as $fieldName) {
            $fieldValue = $this->$fieldName;

            if ($fieldValue instanceof Zend_Date) {
                $this->$fieldName = $fieldValue->toString(App_Db::ZEND_DATETIME_FORMAT);
            } else {
                if (!(is_string($fieldValue) || ($fieldValue instanceof Zend_Db_Expr))) {
                    throw new \Exception('dateCreated/dateToProcess should be string, got '.gettype($fieldValue));
                }
            }
        }

        $this->_is_completed = (bool) $this->_is_completed;

        if (is_null($this->getStatus())) {
            throw new \Exception('Status required');
        }

        return true;
    }

    protected function _insert() {
        $this->validate();

        parent::_insert();
    }

    protected function _update() {
        $this->validate();

        parent::_update();
    }
}