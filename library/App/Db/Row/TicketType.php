<?php
class App_Db_Row_TicketType extends App_Db_Row_Abstract
{
    /**
     * @var int
     */
    protected $_id;

    /**
     * @var string
     */
    protected $_name;

    /**
     * @var string
     */
    protected $_description;

    /**
     * @var string
     */
    protected $_recipient;

    /**
     * Возвращает список полей записи
     * @return array
     */
    public function getFields()
    {
        return array(
            'id', 'name', 'description', 'recipient'
        );
    }

    /**
     * @param int $id
     * @throws InvalidArgumentException
     */
    public function setId($id)
    {
        if (!(filter_var($id, FILTER_VALIDATE_INT)|| is_null($id)) || $id < 0) {
            throw new \InvalidArgumentException('Field "type_id" should have a int value, got ' . gettype($id));
        }

        $this->_id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->_description = $description;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * @param string $recipient
     */
    public function setRecipient($recipient) {
        $this->_recipient = $recipient;
    }

    /**
     * @return string
     */
    public function getRecipient() {
        return $this->_recipient;
    }

    public function save()
    {
        parent::save();

        $this->setId($this->_data['id']);
    }
}