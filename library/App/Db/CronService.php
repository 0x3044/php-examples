<?php
use App_Cron_RepositoryInterface as CronAdapter;

class App_Db_CronService extends App_Db_Abstract implements CronAdapter
{
    /**
     * {@inheritdoc}
     * @var string
     */
    protected $_name = DB_CRON_SERVICE;

    /**
     * {@inheritdoc}
     * @var string
     */
    protected $_primary = 'id';

    /**
     * Возвращает список всех CRON-сервисов
     * @return array
     */
    public function fetchListeners()
    {
        return $this->fetchAll();
    }

    /**
     * Обновляет дату/время последнего запуска для указанного cron-сервиса
     * @param string $listenerId
     */
    public function touchLastRun($listenerId)
    {
        $date = new Zend_Date();

        $this->update(array(
            'last_run' => $date->toString(App_Db::ZEND_DATETIME_FORMAT)
        ), array('listener = ?' => $listenerId));
    }

    /**
     * Возвращает дату/время последнего запуска для указанного сервиса либо FALSE в случае, если сервис ни разу не запускался
     * @param string $listenerId
     * @throws Exception
     * @return \Zend_Date|bool
     */
    public function getLastRun($listenerId)
    {
        if(!($row = $this->fetchRow($this->select()->where('listener = ?', $listenerId)))) {
            throw new \Exception(sprintf('CRON-сервис `%s` не описан в базе данных', $listenerId));
        }

        return strlen($row->last_run) ? new Zend_Date($row->last_run, App_Db::ZEND_DATETIME_FORMAT) : false;
    }
}