<?php
use App_Db_Row_TicketType as TicketType;

class App_Db_TicketType extends App_Db_Abstract
{
    /**
     * Name
     * @var string
     */
    protected $_name = DB_TICKET_TYPE;

    /**
     * Primary key
     * @var string
     */
    protected $_primary = 'id';

    /**
     * Row class
     * @var string
     */
    protected $_rowClass = 'App_Db_Row_TicketType';

    /**
     * Кэш типов тикетов
     * @var array
     */
    protected $_cachedTicketTypes;

    /**
     * Возвращает список всех доступных типов тикетов
     * @return array
     */
    public function &getTicketTypes() {
        if (is_null($this->_cachedTicketTypes)) {
            $ticketTypes = $this->fetchAll();

            if ($ticketTypes && count($ticketTypes)) {
                /** @var $ticketTypeRow App_Db_Row_TicketType */
                foreach ($ticketTypes as $ticketTypeRow) {
                    $this->_cachedTicketTypes[$ticketTypeRow->getId()] = $ticketTypeRow;
                }
            }
        }

        return $this->_cachedTicketTypes;
    }

    /**
     * Возвращает тип тикета по его Id
     * @param $typeId
     * @return TicketType
     * @throws Exception
     */
    public function getTicketTypeById($typeId) {
        $ticketTypes = $this->getTicketTypes();

        if(isset($ticketTypes[$typeId])) {
            return $ticketTypes[$typeId];
        }else{
            throw new \Exception("TicketType with id `{$typeId}` not found");
        }
    }

    /**
     * Возвращает тип тикета по его имени
     * @param $ticketTypeName
     * @return TicketType
     * @throws Exception
     */
    public function getTicketTypeByName($ticketTypeName) {
        $ticketTypes = $this->getTicketTypes();

        /** @var $ticketType TicketType */
        foreach($ticketTypes as $ticketType) {
            if($ticketType->getName() == $ticketTypeName) {
                return $ticketType;
            }
        }

        throw new \Exception("TicketType `{$ticketTypeName}` not found");
    }

    /**
     * Возвращает Id типа тикета по его имени
     * @param $ticketTypeName
     * @return int
     */
    public function getTicketTypeIdByName($ticketTypeName) {
        return $this->getTicketTypeByName($ticketTypeName)->getId();
    }

    /**
     * Возвращает true, если тип тикета с данным названием существует
     * @param $ticketTypeName
     * @return bool
     */
    public function hasTicketTypeName($ticketTypeName) {
        try {
            $this->getTicketTypeByName($ticketTypeName);
            return true;
        }catch(\Exception $e){
            return false;
        }
    }

    /**
     * Возвращает имя типа тикета по его Id
     * @param $ticketTypeId
     * @return string
     */
    public function getTicketTypeNameById($ticketTypeId) {
        return $this->getTicketTypeById($ticketTypeId)->getName();
    }

    /**
     * Возвращает описания типа тикета по его Id
     * @param $ticketTypeId
     * @return string
     */
    public function getTicketTypeDescription($ticketTypeId) {
        return $this->getTicketTypeById($ticketTypeId)->getDescription();
    }

    /**
     * Возврашает true, если тип тикета с таким Id существует
     * @param $ticketId
     * @return bool
     */
    public function hasTicketTypeId($ticketId) {
        try {
            $this->getTicketTypeById($ticketId);
            return true;
        }catch(\Exception $e){
            return false;
        }
    }

    /**
     * Очищает кэш типов тикетов
     */
    public function clearCache() {
        $this->_cachedTicketTypes = array();
    }

    /**
     * Возвращает права на ту или иную операцию в рамках данного типа тикета
     * @param string $ticketTypeName
     * @param string $subPath
     * @return Mixed
     */
    public function getAccess($ticketTypeName, $subPath) {
        $ticketType = $this->getTicketTypeByName($ticketTypeName);

        return App_Access::get("access", "finance>{$ticketType->getRecipient()}>{$ticketType->getName()}>{$subPath}");
    }
}