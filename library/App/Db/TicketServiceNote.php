<?php
use App_Db_Row_TicketServiceNote as Row;

class App_Db_TicketServiceNote extends App_Db_Abstract
{
    /**
     * Name
     * @var string
     */
    protected $_name = DB_TICKET_SERVICE_NOTE;

    /**
     * Primary key
     * @var string
     */
    protected $_primary = 'id';

    /**
     * Row class
     * @var string
     */
    protected $_rowClass = 'App_Db_Row_TicketServiceNote';

    /**
     * Создает/обновляет запись о служебной записке
     * @param $ticketId
     * @param $fileName
     */
    public function create($ticketId, $fileName)
    {
        App_Spl_TypeCheck::getInstance()->positiveNumeric($ticketId);

        $updateData = array(
            'ticket_id' => $ticketId,
            'filename' => $fileName
        );

        if($this->exists($ticketId)) {
            $this->update($updateData, array(
                'ticket_id = ?' => $ticketId
            ));
        }else{
            $this->insert($updateData);
        }
    }

    /**
     * Удаляет информацию о служебной записке
     * @param $ticketId
     */
    public function destroy($ticketId)
    {
        App_Spl_TypeCheck::getInstance()->positiveNumeric($ticketId);

        $this->delete(array('ticket_id = ?' => $ticketId));
    }

    /**
     * Возвращает true, если есть информация о служебной записке
     * @param $ticketId
     * @return bool
     */
    public function exists($ticketId)
    {
        return (bool) $this->getByTicketId($ticketId);
    }

    /**
     * Возвращает информацию о служебной записке
     * @param $ticketId
     * @return null|Row
     */
    public function getByTicketId($ticketId)
    {
        App_Spl_TypeCheck::getInstance()->positiveNumeric($ticketId);

        return $this->fetchRow($this->select()->where('ticket_id = ?', $ticketId));
    }
}