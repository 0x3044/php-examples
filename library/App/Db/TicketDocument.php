<?php
class App_Db_TicketDocument extends App_Db_Abstract
{
    /**
     * Name
     * @var string
     */
    protected $_name = DB_TICKET_DOCUMENT;

    /**
     * Primary key
     * @var string
     */
    protected $_primary = 'id';

    /**
     * Row class
     * @var string
     */
    protected $_rowClass = 'App_Db_Row_TicketDocument';
}