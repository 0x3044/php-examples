<?php
use App_Db_Row_Ticket as Row;

class App_Db_Ticket extends App_Db_Abstract
{
    /**
     * Name
     * @var string
     */
    protected $_name = DB_TICKET;

    /**
     * Primary key
     * @var string
     */
    protected $_primary = 'id';

    /**
     * Row class
     * @var string
     */
    protected $_rowClass = 'App_Db_Row_Ticket';

    /**
     * Return ticket by ticketId
     * @param $ticketId
     * @return null|App_Db_Row_Ticket
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function getTicketById($ticketId)
    {
        if(!(is_numeric($ticketId)) || $ticketId < 0) {
            throw new \InvalidArgumentException('ticketId should have numeric value, got '.gettype($ticketId));
        }

        $selectQuery = $this->select();
        $selectQuery->where('id='.(int) $ticketId);
        $result = $this->fetchRow($selectQuery);

        if(!($result)) {
            throw new \Exception("Ticket({$ticketId}) was not found");
        }

        return $result;
    }

    /**
     * Return ticket by ticket_type_id and claim_id
     * @param $ticketTypeId
     * @param $claimId
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getTicketByTypeAndClaimIds($ticketTypeId, $claimId)
    {
        $select = $this->select();
        $select->where('claim_id = ?', (int) $ticketTypeId);
        $select->where('ticket_type_id = ?', (int) $claimId);

        return $this->fetchRow($select);
    }

    /**
     * Return num of tickets by ticket type ids and status value
     * @param array $ticketTypeIds
     * @param int $status
     * @throws Exception
     * @return int
     */
    public function getCountTicketsByTypeIds(array $ticketTypeIds, $status)
    {
        if(!($ticketTypeIds = implode(',', $ticketTypeIds))) {
            throw new \InvalidArgumentException("Empty ticketTypeIds");
        }

        if(!(is_numeric($status) && $status > 0)) {
            throw new \InvalidArgumentException("Invalid status, expected int, got ".gettype($status));
        }

        $sqlQuery = <<<SQL
          SELECT
            COUNT(*) AS num
          FROM ticket
          WHERE status = {$status} AND ticket_type_id IN ({$ticketTypeIds})
SQL;

        if($result = Zend_Db_Table_Abstract::getDefaultAdapter()->query($sqlQuery)) {
            $result = $result->fetch(Zend_Db::FETCH_ASSOC);

            return $result['num'];
        } else {
            return 0;
        }
    }

    /**
     * Возвращает массив Id тикетов с данным статусом и данными типами
     * @param array $ticketTypeIds
     * @param $status
     * @return array
     * @throws InvalidArgumentException
     */
    public function getTicketIdsByTicketTypeIds(array $ticketTypeIds, $status)
    {
        if(!($ticketTypeIds = implode(',', $ticketTypeIds))) {
            throw new \InvalidArgumentException("Empty ticketTypeIds");
        }

        if(!(is_numeric($status) && $status > 0)) {
            throw new \InvalidArgumentException("Invalid status, expected int, got ".gettype($status));
        }

        $sqlQuery = <<<SQL
          SELECT DISTINCT
            id
          FROM ticket
          WHERE status = {$status} AND ticket_type_id IN ({$ticketTypeIds})
SQL;

        if($result = Zend_Db_Table_Abstract::getDefaultAdapter()->query($sqlQuery)) {
            return array_map('intval', $result->fetchAll(Zend_Db::FETCH_COLUMN));
        } else {
            return 0;
        }
    }

    /**
     * Return array of tickets by claimId
     * @param $claimId
     * @param string $orderExpression ORDER BY expression
     * @throws InvalidArgumentException
     * @return \Zend_Db_Table_Rowset_Abstract
     */
    public function getTicketsByClaimId($claimId, $orderExpression = NULL)
    {
        if(!(is_numeric($claimId) && $claimId > 0)) {
            throw new \InvalidArgumentException("Invalid claimId, got ".gettype($claimId));
        }

        $select = $this->select();
        $select->where('claim_id = ?', (int) $claimId);

        if($orderExpression) {
            $select->order($orderExpression);
        }

        return $this->fetchAll($select);
    }

    /**
     * Return array of tickets by claimId, icketTypeId
     * @param $claimId
     * @param $ticketTypeId
     * @param string $orderExpression ORDER BY expression
     * @throws InvalidArgumentException
     * @return \Zend_Db_Table_Rowset_Abstract
     */
    public function getTicketsByClaimIdAndTicketTypeId($claimId, $ticketTypeId, $orderExpression = NULL)
    {
        if(!(is_numeric($claimId) && $claimId > 0)) {
            throw new \InvalidArgumentException("Invalid claimId, got ".gettype($claimId));
        }

        if(!(is_numeric($ticketTypeId) && $ticketTypeId > 0)) {
            throw new \InvalidArgumentException("Invalid ticketTypeId, got ".gettype($ticketTypeId));
        }

        $select = $this->select();
        $select->where('claim_id = ?', (int) $claimId);
        $select->where('ticketTypeId = ?', (int) $ticketTypeId);

        if($orderExpression) {
            $select->order($orderExpression);
        }

        return $this->fetchAll($select);
    }

    /**
     * Remove all tickets by claimId
     * @param int $claimId
     * @throws InvalidArgumentException
     */
    public function removeTicketsByClaimId($claimId)
    {
        if(!(is_numeric($claimId) && $claimId > 0)) {
            throw new \InvalidArgumentException("Invalid claimId, got ".gettype($claimId));
        }

        $this->delete('claim_id = '.(int) $claimId);
    }

    /**
     * Возвращает тикеты, дата просрочки которых находится в +12/-12 часовом промежутке
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getDailyTickets()
    {
        $startDate = new Zend_Date();
        $startDate->sub('12:00:00', Zend_Date::TIMES);

        $endDate = new Zend_Date();
        $endDate->add('12:00:00', Zend_Date::TIMES);

        $select = $this->select();
        $select->where('date_to_process >= ?', $startDate->toString(App_Db::ZEND_DATETIME_FORMAT));
        $select->where('date_to_process <= ?', $endDate->toString(App_Db::ZEND_DATETIME_FORMAT));

        return $this->fetchAll($select);
    }

    /**
     * Возвращает последние запросы всех типов в саппорт по указанной заявке
     * @param $claimId
     * @return array
     * @throws InvalidArgumentException
     */
    public function getLatestTicketsByClaimId($claimId)
    {
        if(!(is_numeric($claimId) && $claimId > 0)) {
            throw new \InvalidArgumentException("Invalid claimId, got ".gettype($claimId));
        }

        $latestSelectQuery = $this->select()
            ->from(DB_TICKET, array(new Zend_Db_Expr('max(id) as id'), 'ticket_type_id'))
            ->where("claim_id = ?", $claimId)
            ->group('ticket_type_id');

        $latestIds = array();
        $latestIdsResult = $this->fetchAll($latestSelectQuery)->toArray();

        if(count($latestIdsResult) > 0) {
            foreach($latestIdsResult as $rowData) {
                $latestIds[] = (int) $rowData['id'];
            }
        } else {
            return array();
        }

        $select = $this->select()->where('id in ('.implode(',', $latestIds).')');

        return $this->fetchAll($select);
    }

    /**
     * Возвращает активные запросы по указанному типу запроса и Id заявки
     * @param $ticketTypeId
     * @param $claimId
     * @throws InvalidArgumentException
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getActiveTicketsByTicketTypeIdAndClaimId($ticketTypeId, $claimId)
    {
        if(!(is_numeric($claimId) && $claimId > 0)) {
            throw new \InvalidArgumentException("Invalid claimId, got ".gettype($claimId));
        }

        if(!(is_numeric($ticketTypeId) && $ticketTypeId > 0)) {
            throw new \InvalidArgumentException("Invalid ticketTypeId, got ".gettype($ticketTypeId));
        }

        $selectQuery = $this->select()
            ->where("status = ?", App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_ACTIVE)
            ->where("claim_id = ?", $claimId)
            ->where("ticket_type_id = ?", $ticketTypeId);

        return $this->fetchAll($selectQuery);
    }

    /**
     * Возвращает данные об активном запросе к начальнику склада либо NULL в случае его отсутствия
     * @param int $claimId
     * @return null|Row
     * @throws InvalidArgumentException
     */
    public function getActiveDepotTicket($claimId)
    {
        if(!(is_numeric($claimId) && $claimId > 0)) {
            throw new \InvalidArgumentException("Invalid claimId, got ".gettype($claimId));
        }

        /** @var $dbTicketType App_Db_TicketType */
        $dbTicketType = App_Db::get(DB_TICKET_TYPE);
        $ticketType = $dbTicketType->getTicketTypeByName('depot_verify');

        $selectQuery = $this->select()
            ->where('claim_id = ?', (int) $claimId)
            ->where('ticket_type_id = ?', $ticketType->getId())
            ->where('status = ?', App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_ACTIVE);

        return $this->fetchRow($selectQuery);
    }

    /**
     * Возвращает true, если существуют тикеты с указанным статусом указанных типов для данной заявки
     * @param int $claimId
     * @param int|array $status
     * @param int|array $ticketTypeIds
     * @param array $excludeIds
     * @throws InvalidArgumentException
     * @return bool
     */
    public function ticketsExists($claimId, $status, $ticketTypeIds, $excludeIds = array())
    {
        if(!(is_numeric($claimId) && $claimId > 0)) {
            throw new \InvalidArgumentException("Invalid claimId, got ".gettype($claimId));
        }

        if(!(is_array($status))) {
            $status = (array) $status;
        }

        if(!(count($status))) {
            throw new \InvalidArgumentException("No status available");
        }

        foreach($status as $cStatus) {
            if(!(is_numeric($cStatus) && $cStatus > 0)) {
                throw new \InvalidArgumentException("Invalid status, got ".gettype($cStatus));
            }
        }

        $selectQuery = Zend_Db_Table_Abstract::getDefaultAdapter()->select()
            ->from($this->_name, array("num" => "COUNT(*)"))
            ->where('claim_id = ?', (int) $claimId)
            ->where('status IN ('.implode(',', $status).')');

        if(is_array($ticketTypeIds)) {
            /** @var $ticketTypeDbTable App_Db_TicketType */
            $ticketTypeDbTable = App_Db::get(DB_TICKET_TYPE);
            $ttIds = array();

            foreach($ticketTypeIds as $ticketTypeId) {
                if(is_integer($ticketTypeId)) {
                    $ttIds[] = $ticketTypeId;
                } else if(is_string($ticketTypeId)) {
                    $ttIds[] = $ticketTypeDbTable->getTicketTypeIdByName($ticketTypeId);
                } else {
                    throw new \InvalidArgumentException("Invalid ticketTypeId, expected integer, got ".gettype($ticketTypeId));
                }
            }

            $ticketTypeIds = implode(',', $ttIds);
            $selectQuery->where("ticket_type_id IN ({$ticketTypeIds})");
        } else if(is_integer($ticketTypeIds)) {
            $selectQuery->where('ticket_type_id = ?', $ticketTypeIds);
        } else {
            throw new \InvalidArgumentException("Invalid ticketTypeIds, expected array|int, got ".gettype($ticketTypeIds));
        }

        if(is_array($excludeIds) && count($excludeIds) > 0) {
            $excludeIds = implode(',', $excludeIds);
            $selectQuery->where("id NOT IN ({$excludeIds})");
        }

        if(!($result = Zend_Db_Table_Abstract::getDefaultAdapter()->fetchRow($selectQuery))) {
            return false;
        } else {
            return ((int) $result->num) > 0;
        }
    }

    /**
     * Возвращает даты к обработке запросов по их Id
     * @param array $ticketIds
     * @return Zend_Date[]
     */
    public function getDateToProcess(array $ticketIds)
    {
        $dateToProcessIds = array();

        if(is_array($ticketIds) && count($ticketIds)) {
            $selectQuery = Zend_Db_Table_Abstract::getDefaultAdapter()->select()
                ->from($this->_name, array("id", "date_to_process"))
                ->where('id in ('.implode(',', $ticketIds).')');

            if(($result = Zend_Db_Table_Abstract::getDefaultAdapter()->fetchAssoc($selectQuery)) && count($result)) {
                foreach($result as $rowData) {
                    if(strlen($rowData['date_to_process'])) {
                        $dateToProcessIds[(int) $rowData['id']] = new Zend_Date($rowData['date_to_process'], App_Db::ZEND_DATETIME_FORMAT);
                    } else {
                        $dateToProcessIds[(int) $rowData['id']] = NULL;
                    }
                }
            }
        }

        return $dateToProcessIds;
    }

    /**
     * Возвращает активные запросы по указанной заявке
     * @param int $claimId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getActiveTicketsByClaimId($claimId)
    {
        App_Spl_TypeCheck::getInstance()->positiveNumeric($claimId);

        $status = App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_ACTIVE;

        $select = $this->select()
            ->where("status IN ({$status})")
            ->where("claim_id = ?", $claimId)
        ;

        return $this->fetchAll($select);
    }
    
    
     /**
     * Есть ли просроченные выполненные тикеты по заявкам у пользователя
     * @param int $managerId
     * @return bool
     * @throws InvalidArgumentException
     */
    public function hasOverdueClaimTickets($managerId)
    {
        if(!(is_int($managerId))) {
            throw new \InvalidArgumentException("Invalid managerId, expected int, got ".gettype($managerId));
        }
        $managers = App_Access::get('managers', 'analitics>claimsdocs');
        if (!in_array($managerId, $managers['m'])){
            $managers['m'][] = $managerId;
        }
        $sqlQuery = <<<SQL
        SELECT * FROM (
            SELECT
                FLOOR((UNIX_TIMESTAMP() - UNIX_TIMESTAMP(t.date_processed))/(3600*24)) AS overdue 
            FROM `ticket` AS `t` 
            INNER JOIN `claims` AS `c`
                ON `t`.`claim_id` = `c`.`id`
            WHERE `c`.`manager_id` IN (%s) AND t.status = '2' AND c.claim_status NOT IN (0, 13) AND c.claimType = '1'
        ) AS result WHERE overdue >= 3
SQL;
        $sqlQuery = sprintf($sqlQuery, implode(",", $managers['m']));
        
        $result = Zend_Db_Table_Abstract::getDefaultAdapter()->fetchAll($sqlQuery);
        if(count($result) > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    
    
    /*
     * отослать E-mail админам, если есть просроченные тикеты по заявкам
     */
    public function sendNotification()
    {
        
        $sqlQuery = <<<SQL
        SELECT 
                `t`.*, 
                DATE_FORMAT(FROM_UNIXTIME(`t`.`date`), '%d.%m.%Y') AS `date`,  
                DATE_FORMAT(`t`.`date_processed`, '%d.%m.%Y') AS `date_processed`  
        FROM (
            SELECT
                c.full_id,
                clients.s_title AS `client`,
                o.title AS depot,
                u.name AS `manager`,
                t.date_processed,
                c.date,
                FLOOR((UNIX_TIMESTAMP() - UNIX_TIMESTAMP(t.date_processed))/(3600*24)) AS overdue, 
                st.status_title AS `claim_status_title`
            FROM `ticket` AS `t` 
            INNER JOIN `claims` AS `c`
                ON `t`.`claim_id` = `c`.`id`
            LEFT JOIN `status_type` AS `st`
                ON `c`.`claim_status` = `st`.`id`
            INNER JOIN `clients` 
                ON `c`.`client_id` = `clients`.`id`
            INNER JOIN `users` AS `u`
                ON c.manager_id = u.id
            INNER JOIN `orgstructure` AS `o`
                ON c.depot_id = o.id
            WHERE t.status = '2' AND c.claim_status NOT IN (0, 13) AND c.claimType = '1'
        ) AS `t` WHERE `overdue` >= 3
SQL;
        
        $result = App_Db::get()->query($sqlQuery)->fetchAll();
        if (count($result) > 0) {
            try {
                App_Sendmail_Abstract::sendMail(20, array('result' => $result));
            } catch(Exception $e) {
                echo '<b>Exception:</b> '. $e->getMessage() .'<br/>';
            }
        }
    }
    /*
     * отослать E-mail менеджерам, если есть просроченные тикеты по заявкам
     */
    public function sendManagerNotification()
    {
        
        $sqlQuery = <<<SQL
        SELECT 
                `t`.*, 
                DATE_FORMAT(FROM_UNIXTIME(`t`.`date`), '%d.%m.%Y') AS `date`,  
                DATE_FORMAT(`t`.`date_processed`, '%d.%m.%Y') AS `date_processed`  
        FROM (
            SELECT
                c.full_id,
                clients.s_title AS `client`,
                o.title AS depot,
                u.name AS `manager`,
                u.id AS `manager_id`,
                t.date_processed,
                c.date,
                FLOOR((UNIX_TIMESTAMP() - UNIX_TIMESTAMP(t.date_processed))/(3600*24)) AS overdue, 
                st.status_title AS `claim_status_title`,
                ue.email
            FROM `ticket` AS `t` 
            INNER JOIN `claims` AS `c`
                ON `t`.`claim_id` = `c`.`id`
            LEFT JOIN `status_type` AS `st`
                ON `c`.`claim_status` = `st`.`id`
            INNER JOIN `clients` 
                ON `c`.`client_id` = `clients`.`id`
            INNER JOIN `users` AS `u`
                ON c.manager_id = u.id
            INNER JOIN `orgstructure` AS `o`
                ON c.depot_id = o.id
            INNER JOIN `user_email` AS `ue`
                ON `ue`.`user_id` = c.manager_id
            WHERE t.status = '2' AND c.claim_status NOT IN (0, 13) AND c.claimType = '1' GROUP BY t.id
        ) AS `t` WHERE `overdue` >= 3
SQL;
        
        $result = App_Db::get()->query($sqlQuery)->fetchAll();
        
        if (count($result) > 0) {
            $maillistArray = array();
            foreach ($result as $item) {
                if ($item->email) {
                    $maillistArray[$item->email][] = $item;
                }
            }
            $subject = 'Необходимо проверить заявки на корректность выставленных документов';
            if (count($maillistArray) > 0) {                
                foreach ($maillistArray as $email => $rows) {
                    $mailText = 'По данным заявкам возможно есть выставленные документы бухгалтерией, которые необходимо удалить:<br/>';
                    // Сформируем тело письма
                    try {
                        /* @var $view Zend_View */
                        $view = Zend_Layout::getMvcInstance()->getView();
                        $view->addScriptPath(realpath(APPLICATION_PATH.'/../library/App/Sendmail/views'));
                        $mailText .= $view->partial("claimsDocsTable.phtml", array('result' => $rows));
                    } catch (Exception $e) {
                        Zend_Debug::dump($e->getMessage(), 'RenderPartialError');
                    }
                    //sending
                    try {
                        App_Sendmail_Abstract::sendZendMail(array(
                            "subject" => $subject,
                            "from" => "info@amd-co.ru",
                            "to" => $email,
                            "content" => $mailText
                        ));
                    } catch(Exception $e) {
                        echo '<b>Exception:</b> '. $e->getMessage() .'<br/>';
                    }
                }
            }
        }
    }
}