<?php

class App_Db_TicketPayment extends App_Db_Abstract
{
    /**
     * {@inheritdoc}
     */
    protected $_primary = 'ticket_id';

    /**
     * {@inheritdoc}
     */
    protected $_name = 'ticket_payment';

    /**
     * Возвращает данные об оплаты по Id запроса
     * @param $ticketId
     * @return Zend_Db_Table_Row
     */
    public function getByTicketId($ticketId)
    {
        return $this->find($ticketId)->getRow(0);
    }
    
    
    /**
     * Возвращает массив "похожих" расшифровок в зависимости от переданных параметров.
     * 
     * @param type $summ            Сумма запроса на оплату
     * @param type $paymentTypeId   Идентификатор типа запроса на оплату
     * @param type $clientId        Идентификатор клиента
     * @param type $date            Дата
     * 
     * @return array
     */
    public function getBySimilarDetails($summ = false, $paymentTypeId = false, $clientId = false, $date = false) 
    {
        $result = array();
        
        $ticketType = App_Finance_Ticket_Type_Payment_Form_DataSource::getInstance()->getPaymentTypes()->getPaymentTypeById($paymentTypeId);
        
        if ($ticketType) {
            // Если такой тип нашелся, выполняем поиск, иначе - будет исключение
            $select = $this->getAdapter()->select()
                ->from(array('tp' => $this->_name), array())
                ->joinLeft(array('t' => DB_TICKET), 'tp.ticket_id = t.id', array('id'))
                ->where('tp.payment_type = ?', $paymentTypeId);
            // Только подтвержденные тикеты
            $select->where('t.status = ?', App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_ARCHIVED);
            // Исключаем запросы "за отходы"
            $select->where('tp.is_waste IS NULL OR tp.is_waste = 0');
            
            if ($summ) {
                // Сумму инвертируем
                $summ = -$summ;
                $select->where('tp.sum = ?', $summ);
            }
            if ($clientId) {
                $select->where('tp.client_id = ?', $clientId);
            }
            if ($date) {
                // Дата должна лежать в пределах трёх дней
                $date = date_create($date);
                $select->where('DATE_FORMAT(t.date_created, "%Y-%m-%d") <= ?', $date->format('Y-m-d'));
                $date->sub(new DateInterval('P2D'));
                $select->where('DATE_FORMAT(t.date_created, "%Y-%m-%d") >= ?', $date->format('Y-m-d'));
            }

            $ticketIds = $select->query()->fetchAll(Zend_Db::FETCH_COLUMN);

            if (!empty($ticketIds)) {
                // Если нашлись подходящие запросы на оплату, возьмём их расшифровки
                foreach ($ticketIds as $ticketId) {
                    if (
                        ($ticket = App_Finance_Ticket_Factory::getInstance()->createFromTicketId($ticketId))
                        && ($ticketPaymentForm = new App_Finance_Ticket_Type_Payment_Form_Data_Form($ticket))
                        && $ticketPaymentForm->detailsExists()
                    ) {
                        $ticketPaymentFormArray = $ticketPaymentForm->toArray();
                        $result[] = $ticketPaymentFormArray['details'];
                    }
                }
            }
        }
        
        return $result;
    }

    /**
     * Возвращает платежные данные по запросу на оплату
     * @param int $ticketId
     * @return array|bool
     */
    public function getTicketPayment($ticketId)
    {
        $sqlQueryPayment = <<<SQL
          SELECT
            ticket_payment.*,
            clients.s_title as client_name
          FROM ticket_payment
          LEFT JOIN clients ON clients.id = ticket_payment.client_id
          WHERE ticket_payment.ticket_id = :ticketId
          ORDER BY id ASC
SQL;

        return App_Db::get()->query($sqlQueryPayment, array('ticketId' => $ticketId))->fetch(Zend_Db::FETCH_ASSOC);
    }

    /**
     * Возврашает детализацию по платежным данным для запроса на оплату
     * @param $ticketId
     * @return array|bool
     */
    public function getTicketPaymentDetails($ticketId)
    {
        $sqlQueryDetails = <<<SQL
          SELECT
            ticket_payment_details.*,
            clients.s_title as client_name
          FROM ticket_payment_details
          LEFT JOIN clients ON clients.id = ticket_payment_details.client_id
          WHERE ticket_id = :ticketId
          ORDER BY id ASC
SQL;

        return App_Db::get()->query($sqlQueryDetails, array('ticketId' => $ticketId))->fetchAll(Zend_Db::FETCH_ASSOC);
    }
}
