<?php
class App_Db_TicketPaymentDetails extends App_Db_Abstract
{
    /**
     * {@inheritdoc}
     */
    protected $_primary = 'id';

    /**
     * {@inheritdoc}
     */
    protected $_name = 'ticket_payment_details';

    /**
     * Удаляет все записи по Id запроса
     * @param $ticketId
     */
    public function destroyByTicketId($ticketId) {
        $this->delete('ticket_id = '.(int) $ticketId);
    }
}
