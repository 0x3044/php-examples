<?php
class App_Zoho_Source_PublicUrl implements App_Zoho_Source_SourceInterface
{
    /**
     * Общедоступный URL к файлу
     * @var string
     */
    protected $_url;

    /**
     * Имя файла
     * @var string
     */
    protected $_filename;

    /**
     * Открытие файла в Zoho-редакторе через общедоступный URL
     * @param string $url
     * @param string $filename Указать специальное имя файла
     * @throws Exception
     */
    public function __construct($url, $filename = null)
    {
        if(!(strlen(Zend_Uri::check($url)))) {
            throw new \Exception(sprintf('Не указан URL либо неверный формат: %s', var_export($url, true)));
        }

        if(is_null($filename)) {
            $filename = basename($url);
        }

        if(!(strlen($filename = trim($filename)))) {
            throw new \Exception('Не указано имя файла');
        }

        $this->_url = $url;
        $this->_filename = $filename;
    }

    /**
     * Возвращает переданный конструктору URL
     * @return string
     */
    public function getUrl()
    {
        return $this->_url;
    }

    /**
     * {inheritddoc}
     * @return string
     */
    public function getFilename()
    {
        return $this->_filename;
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getFormEncType()
    {
        return 'application/x-www-form-urlencoded';
    }

    /**
     * {@inheritdoc}
     * @return mixed
     */
    public function getAdditionalOptions()
    {
        return array(
            'url' => $this->getUrl(),
        );
    }
}