<?php
interface App_Zoho_Source_SourceInterface
{
    /**
     * Возвращает enctype формы
     * @return string
     */
    public function getFormEncType();

    /**
     * Возвращает имя файла
     * @return string
     */
    public function getFilename();

    /**
     * Возвращает $_POST-опции, которые мерджатся с общими в POST-запросе на открытие файла
     * @return mixed
     */
    public function getAdditionalOptions();
}