<?php
class App_Zoho_Source_FileUpload implements App_Zoho_Source_SourceInterface
{
    /**
     * Путь к файлу
     * @var string
     */
    protected $_filename;

    /**
     * Указанный пользователем специальное имя файла
     * @var string
     */
    protected $_customFileName;

    /**
     * Открытие файла в Zoho-редакторе через загрузку локального файла
     * @param $filename
     * @throws Exception
     */
    public function __construct($filename)
    {
        if(!file_exists($filename) || is_dir($filename)) {
            throw new \Exception(sprintf('Отсутствует файл либо указана директория: %s', var_export($filename, true)));
        }

        $this->_filename = $filename;
    }

    /**
     * {inheritdoc}
     * @return string
     */
    public function getFilename()
    {
        return strlen($this->_customFileName) ? $this->_customFileName : end(explode(DIRECTORY_SEPARATOR, $this->_filename));
    }

    /**
     * Возвращает специально указанное имя файла
     * @return string
     */
    public function getCustomFileName()
    {
        return $this->_customFileName;
    }

    /**
     * Указать имя файла для Zoho-сервиса7
     * @param string $customFileName
     */
    public function setCustomFileName($customFileName)
    {
        $this->_customFileName = $customFileName;
    }

    /**
     * {inheritdoc}
     * @return string
     */
    public function getFormEncType()
    {
        return 'multipart/form-data';
    }

    /**
     * {inheritdoc}
     * @return mixed
     */
    public function getAdditionalOptions()
    {
        if(class_exists('CURLFile')) {
            $curlFile = new CURLFile($this->_filename);
        }else{
            $curlFile = '@'.realpath($this->_filename);
        }

        return array(
            'content' => $curlFile
        );
    }
}