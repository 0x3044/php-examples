<?php

class App_Zoho_FileDetector
{
    /**
     * Возвращает true, если файл может быть открыт Zoho-сервисом
     * @param $fileName
     * @return bool
     */
    public static function isReadable($fileName)
    {
        return in_array(pathinfo($fileName, PATHINFO_EXTENSION), array_merge(
                App_Zoho_Configuration::$docExtensions,
                App_Zoho_Configuration::$excelExtensions,
                App_Zoho_Configuration::$pptExtensions)
        );
    }
}