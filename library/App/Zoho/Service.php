<?php
use App_Zoho_Configuration as ZohoConfiguration;
use App_Zoho_ServiceException as ServiceException;

/**
 * Class App_Zoho_Service
 * Шлюз для открытия файлов в Zoho Reader
 * Опции передаются идентичные, описанные в документации:
 * @see https://apihelp.wiki.zoho.com/Open-Document.html
 */
class App_Zoho_Service
{
    /**
     * Возвращает API-key к Zoho-сервису из конфигурации
     * @return string
     */
    public static function getApiKey()
    {
        return App_Config::get()->zoho_api->remote->apiKey;
    }

    /**
     * Делает запрос Zoho-сервису на открытие файла и возвращает URL, если попытка открытия прошла успешно
     * @param App_Zoho_Configuration $configuration
     * @throws Exception
     * @return string
     */
    public function open(ZohoConfiguration $configuration)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $configuration->getServiceUrl());
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $configuration->exportToPost());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = $this->_parseResponse(curl_exec($curl));
        curl_close($curl);

        if(!(isset($response['RESULT']))) {
            throw new \Exception('Получен неизвестный ответ от Zoho-сервиса');
        }else{
            if($response['RESULT'] && isset($response['URL'])) {
                return $response['URL'];
            }else{
                throw new ServiceException(sprintf('Получена ошибка от Zoho-сервиса: %s', isset($response['WARNING']) ? $response['WARNING'] : '[неизвестная ошибка]'));
            }
        }
    }

    /**
     * Парсит ответ от Zoho-сервиса
     * @param $content
     * @return array
     */
    protected function _parseResponse($content)
    {
        $response = array();

        foreach(explode("\n", $content) as $line) {
            list($name, $value) = explode('=', $line, 2);

            if(strlen($name)) {
                if($value === "NULL") $value = null;
                if($value === "TRUE") $value = true;
                if($value === "FALSE") $value = false;

                $response[$name] = $value;
            }
        }

        return $response;
    }
}