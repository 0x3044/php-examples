<?php
use App_Zoho_Source_SourceInterface as SourceInterface;

/**
 * Конфигурация запроса к API-сервису Zoho
 * @see https://apihelp.wiki.zoho.com/Open-Document.html
 */
class App_Zoho_Configuration
{
    public static $excelExtensions = array('xls', 'xlsx', 'ods', 'sxc', 'csv', 'tsv');
    public static $docExtensions = array('doc', 'docx', 'rtf', 'odt', 'htm', 'html', 'txt');
    public static $pptExtensions = array('ppt', 'pptx', 'pps', 'ppsx', 'odp', 'sxi');

    /**
     * Уникальный идентификатор
     * @var string
     */
    protected $_id;

    /**
     * API-ключ от Zoho-сервиса
     * @var string
     */
    protected $_apiKey;

    /**
     * Сервис
     * @var string
     */
    protected $_service;

    /**
     * Источник файла
     * @var SourceInterface
     */
    protected $_source;

    /**
     * Режим "ответа" от Zoho_Service
     * Опция mode
     * @var string
     */
    protected $_mode = 'view';

    /**
     * URL сохранения изменений в файле
     * Опция saveurl
     * @var string
     */
    protected $_saveUrl = '';

    /**
     * Формат файла, который вернет Zoho при операции сохранения
     * Опция format
     * @var string
     */
    protected $_format;

    /**
     * Опция lang
     * @var string
     */
    protected $_lang = 'ru';

    public function __construct($id, $apiKey, SourceInterface $source, $service = 'auto', $format = 'auto')
    {
        $this->_id = $id;
        $this->_apiKey = $apiKey;
        $this->_format = $format;
        $this->_source = $source;
        $this->_service = $service;
    }

    /**
     * Экспортирует конфигурацию для отправки в Zoho API
     * @return array
     */
    public function exportToPost()
    {
        $source = $this->getSource();

        $defaultOptions = array(
            'output' => 'url'
        );

        $sourceOptions = array_merge(array(
            'filename' => $source->getFilename()
        ), $source->getAdditionalOptions());

        $options = array_merge($defaultOptions, array(
            'apikey' => $this->getApiKey(),
            'output' => 'url',
            'mode' => $this->getMode(),
            'id' => $this->getId(),
            'format' => $this->getFormat(),
        ), $sourceOptions);

        if(strlen($this->getLang())) {
            $options['lang'] = $this->getLang();
        }

        if(strlen($this->getSaveUrl())) {
            $options['saveurl'] = $this->getSaveUrl();
        }

        return $options;
    }

    /**
     * Открыть редактор в режиме "только для чтения"
     */
    public function setReadOnly()
    {
        $this->setMode('view');
    }

    /**
     * Открыть редактор в режиме "чтение-запись"
     */
    public function setWritable()
    {
        $this->setMode('normaledit');
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->_apiKey;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        if($this->_format == 'auto') {
            return pathinfo($this->getSource()->getFilename(), PATHINFO_EXTENSION);
        }else{
            return $this->_format;
        }
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @return string
     */
    public function getLang()
    {
        return $this->_lang;
    }

    /**
     * @param string $lang
     */
    public function setLang($lang)
    {
        $this->_lang = $lang;
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->_mode;
    }

    /**
     * @param string $mode
     */
    public function setMode($mode)
    {
        $this->_mode = $mode;
    }

    /**
     * @return string
     */
    public function getSaveUrl()
    {
        return $this->_saveUrl;
    }

    /**
     * @param string $saveUrl
     */
    public function setSaveUrl($saveUrl)
    {
        $this->_saveUrl = $saveUrl;
    }

    /**
     * Возвращает название сервиса
     * @throws Exception
     * @return string
     */
    public function getService()
    {
        if($this->_service == 'auto') {
            $extension = pathinfo($this->getSource()->getFilename(), PATHINFO_EXTENSION);

            if(in_array($extension, self::$excelExtensions)) {
                return 'sheet';
            }else if(in_array($extension, self::$docExtensions)) {
                return 'writer';
            }else if(in_array($extension, self::$pptExtensions)) {
                return 'show';
            }else{
                throw new \Exception('Неизвестный тип файла');
            }
        }else{
            return $this->_service;
        }
    }

    /**
     * Возвращает URL сервиса
     * @return string
     * @throws Exception
     */
    public function getServiceUrl()
    {
        switch(strtolower($this->getService())) {
            default:
                throw new \Exception(sprintf('Неизвестный сервис %s', var_export($this->_service, true)));

            case 'writer':
                return 'https://exportwriter.zoho.com/remotedoc.im';

            case 'sheet':
                return 'https://sheet.zoho.com/remotedoc.im';

            case 'show':
                return 'https://show.zoho.com/remotedoc.im';
        }
    }

    /**
     * @return \App_Zoho_Source_SourceInterface
     */
    public function getSource()
    {
        return $this->_source;
    }
}