<?php
use App_Form_AjaxForm_Form_AbstractForm as AbstractForm;

class App_Form_AjaxForm_Form_CallbackForm extends App_Form_AjaxForm_Form_AbstractForm
{
    /**
     * Коллбэк
     * @var Closure
     */
    protected $_callback;

    /**
     * Обработчик формы, коллбэковая версия
     * Коллбэк должен вернуть результат выполнения операции
     * @param callable $callback callback(App_Form_AjaxForm_Form_CallbackForm $form)
     * @param array $options
     */
    public function __construct(Closure $callback, array $options = array())
    {
        $this->_callback = $callback;
        $this->_options = array_merge(array(
            'allowedExceptions' => array(),
            'ignoreDeveloperFlag' => false
        ), $options);
        $this->_options['allowedExceptions'][] = 'App_Form_AjaxForm_Form_FormException';
    }

    /**
     * Возвращает переданный конструктору коллбэк
     * @return callable
     */
    protected function _getCallback()
    {
        return $this->_callback;
    }

    /**
     * {@inheritdoc}
     * @return mixed
     */
    protected function _performProcess()
    {
        $callback = $this->_getCallback();

        return $callback($this);
    }
}