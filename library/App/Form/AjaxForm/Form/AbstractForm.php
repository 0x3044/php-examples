<?php
use App_Form_AjaxForm_Form_FormInterface as FormInterface;

abstract class App_Form_AjaxForm_Form_AbstractForm implements FormInterface
{
    /**
     * Дополнительные опции
     * @var array
     */
    protected $_options = array();

    /**
     * Результат обработки формы
     * @var array
     */
    protected $_processingResult = array();

    /**
     * Обработчик формы
     * Коллбэк должен вернуть результат выполнения операции
     * @param array $options
     */
    public function __construct(array $options = array())
    {
        $this->_options = array_merge(array(
            'allowedExceptions' => array(),
            'ignoreDeveloperFlag' => false
        ), $options);
        $this->_options['allowedExceptions'][] = 'App_Form_AjaxForm_Form_FormException';
    }

    /**
     * Возвращает конфигурацию формы
     * @return array
     */
    protected function &_getOptions()
    {
        return $this->_options;
    }

    /**
     * Возвращает результат обработки формы
     * @return array
     */
    protected function _getProcessingResult()
    {
        return $this->_processingResult;
    }

    /**
     * {@inheritdoc}
     * @return bool
     */
    public function process()
    {
        $processingResult = array();
        $options = $this->_getOptions();

        try {
            $result = $this->_performProcess();

            $processingResult = array_merge(array(
                'success' => true
            ), is_array($result) ? $result : array());
        }catch(\Exception $e){
            $isDeveloper = ((int) getenv('DEVELOPMENT')) && !($options['ignoreDeveloperFlag']);

            if($isDeveloper || in_array(get_class($e), $options['allowedExceptions'])) {
                $processingResult = array(
                    'success' => false,
                    'error' => $e->getMessage()
                );

                if($isDeveloper) {
                    $processingResult['trace'] = $e->getTrace();
                }
            }else{
                throw $e;
            }
        }

        $this->_processingResult = $processingResult;
        return $this->_processingResult['success'];
    }

    abstract protected function _performProcess();

    /**
     * Триггер ошибки
     * @param $message
     * @throws App_Form_AjaxForm_Form_FormException
     */
    public function error($message)
    {
        throw new App_Form_AjaxForm_Form_FormException($message);
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function getResultAsArray()
    {
        return $this->_getProcessingResult();
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getResultAsJSONString()
    {
        return json_encode($this->_getProcessingResult());
    }

    /**
     * {@inheritdoc}
     */
    public function sendJSONResponse()
    {
        header('Content-Type: application/json');
        die($this->getResultAsJSONString());
    }
}