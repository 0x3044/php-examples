<?php
interface App_Form_AjaxForm_Form_FormInterface
{
    /**
     * Обработка отправленной формы.
     * Возвращает результат обработки.
     * @return bool
     */
    public function process();

    /**
     * Возвращает результат обработки формы в виде массива
     * @return array
     */
    public function getResultAsArray();

    /**
     * Возвращает результат обработки формы в виде JSON-строки
     * @return string
     */
    public function getResultAsJSONString();

    /**
     * Отправляет браузеру JSON-ответ с результатами обработки данной формы
     */
    public function sendJSONResponse();
}

class App_Form_AjaxForm_Form_FormException extends \Exception {};