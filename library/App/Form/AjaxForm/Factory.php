<?php

/**
 * Фабрика обработчиков форм
 */
class App_Form_AjaxForm_Factory
{
    const TYPE_CALLABLE = 'callable';

    public static function create($type, array $options = array())
    {
        // Поддержка специального формата вызова метода: (callback, options)
        if(is_callable($type)) {
            $options['callback'] = $type;
            $type = self::TYPE_CALLABLE;
        }

        switch($type) {
            default:
                throw new \Exception(sprintf("Неизвестный тип ajax-формы `%s`", var_export($type, true)));

            case self::TYPE_CALLABLE:
                return new App_Form_AjaxForm_Form_CallbackForm($options['callback'], $options);
        }
    }
}