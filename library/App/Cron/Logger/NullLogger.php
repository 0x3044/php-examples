<?php
use App_Cron_LoggerInterface as LoggerInterface;

class App_Cron_Logger_NullLogger implements LoggerInterface
{
    /**
     * Логгер для CRON-сервиса
     * @throws Exception
     */
    public function __construct()
    {
    }

    /**
     * Записать в лог информационное сообщение
     * @param string $message
     */
    public function info($message)
    {
    }

    /**
     * Записть в лог сообщение об ошибке
     * @param string $message
     */
    public function error($message)
    {
    }

    /**
     * Записать в лог исключение
     * @param Exception $e
     */
    public function exception(\Exception $e)
    {
    }

    /**
     * Закрыть(сохранить) лог-файл
     */
    public function close()
    {
    }
}