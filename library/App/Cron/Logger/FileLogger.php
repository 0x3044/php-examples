<?php
use App_Cron_LoggerInterface as LoggerInterface;

class App_Cron_Logger_FileLogger implements LoggerInterface
{
    /**
     * Лог-файл
     * @const string
     */
    const LOG_FILE = '/var/log/cron-php-service.log';

    /**
     * Лог-файл
     * @var resource
     */
    protected $_fileResource;

    /**
     * Выводить в stdout сообщения
     * @var bool
     */
    protected $_output = false;

    /**
     * Логгер для CRON-сервиса
     * @throws Exception
     */
    public function __construct()
    {
        $this->_fileResource = fopen(self::LOG_FILE, 'a');

        if(!($this->_fileResource)) {
            throw new \Exception(sprintf('Не удалось открыть лог-файл `%s`', self::LOG_FILE));
        }
    }

    /**
     * Включить вывод сообщений лога в stdout
     */
    public function enableOutput()
    {
        $this->_output = true;
    }

    /**
     * Отключить вывод сообщений лога в stdout
     */
    public function disableOutput()
    {
        $this->_output = false;
    }

    /**
     * Записать в файл сообщение
     * @param string $message
     */
    protected function _write($message)
    {
        fwrite($this->_fileResource, $message);

        if($this->_output) {
            echo $message;
        }
    }

    /**
     * Записать в лог информационное сообщение
     * @param string $message
     */
    public function info($message)
    {
        $this->_write(sprintf("[%s] [INFO] %s\n", $this->_getTimestamp(), $message));
    }

    /**
     * Записть в лог сообщение об ошибке
     * @param string $message
     */
    public function error($message)
    {
        sprintf("[%s] [ERROR] %s\n", $this->_getTimestamp(), $message);
    }

    /**
     * Записать в лог исключение
     * @param Exception $e
     */
    public function exception(\Exception $e)
    {
        $this->_write(sprintf("[%s] [EXCEPTION] %s\n", $this->_getTimestamp(), $e->getMessage()));

        foreach(explode("\n", $e->getTraceAsString()) as $trace) {
            $this->_write(sprintf("\t%s\n", $trace));
        }
    }

    /**
     * Закрыть(сохранить) лог-файл
     */
    public function close()
    {
        fclose($this->_fileResource);
    }

    /**
     * Возвращает текущий timestamp
     * @return bool|string
     */
    protected function _getTimestamp()
    {
        return date('d.m.Y H:i:s');
    }
}