<?php
use App_Cron_CronService as CronService;
use App_EventManager_EventManagerFactory as EventManagerFactory;

class App_Cron_CronServiceFactory
{
    /**
     * Возвращает CronService
     * @param array $options
     * @return \App_Cron_CronService
     */
    public static function create(array $options = array())
    {
        $options = array_merge(array(
            'verbose' => false,
            'logger' => null,
            'namespaces' => array(
                'App_Cron_Listener',
                'App_Penalty_Cron'
            )
        ), $options);

        /** @var App_Db_CronService $dbAdapter */
        $dbAdapter = App_Db::get(DB_CRON_SERVICE);
        $cronEventManager = EventManagerFactory::create();

        if($options['logger'] == 'file') {
            $logger = new App_Cron_Logger_FileLogger();
        }else{
            $logger = new App_Cron_Logger_NullLogger();
        }

        if($options['verbose'] && method_exists($logger, 'enableOutput')) {
            $logger->enableOutput();
        }

        return new CronService($cronEventManager, $dbAdapter, $logger, $options['namespaces']);
    }
}