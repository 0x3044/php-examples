<?php
interface App_Cron_RepositoryInterface
{
    /**
     * Возвращает список всех CRON-сервисов
     * @return array
     */
    public function fetchListeners();

    /**
     * Обновляет дату/время последнего запуска для указанного cron-сервиса
     * @param string $listenerId
     */
    public function touchLastRun($listenerId);

    /**
     * Возвращает дату/время последнего запуска для указанного сервиса либо FALSE в случае, если сервис ни разу не запускался
     * @param string $listenerId
     * @return \Zend_Date|bool
     */
    public function getLastRun($listenerId);
}