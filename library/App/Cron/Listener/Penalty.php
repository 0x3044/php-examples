<?php
use App_EventManager_Component_Listener_BasicListener as Listener;
use App_Penalty_Service_Factory as PenaltyServiceFactory;

class App_Cron_Listener_Penalty extends Listener
{
    /**
     * Возвращает ID обработчика событий
     * @return string
     */
    public function getListenerId()
    {
        return 'penalty';
    }

    /**
     * Событие вызывается при запуске CRON-сервиса
     */
    public function onCron()
    {
        /** @var App_Db_PenaltyService $repository */
        $repository = App_Db::get(DB_PENALTY_SERVICE);
        $factory = new PenaltyServiceFactory($repository);

        $penaltyService = $factory->create();
        $penaltyService->cron();
    }
}
