<?php
use App_EventManager_Component_Listener_BasicListener as Listener;

class App_Cron_Listener_Test extends Listener
{
    /**
     * {inheritdoc}
     * @return string
     */
    public function getListenerId()
    {
        return 'test';
    }

    /**
     * Событие вызывается при запуске CRON-сервиса
     */
    public function onCron(array $options)
    {
        /** @var App_Cron_Logger_FileLogger $logger */
        $logger = $options['logger'];
        $logger->info('Hello logger');
    }
}