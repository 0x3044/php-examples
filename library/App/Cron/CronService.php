<?php
use App_EventManager_EventManager as EventManager;
use App_Cron_RepositoryInterface as Repository;
use App_Cron_LoggerInterface as Logger;

class App_Cron_CronService
{
    /**
     * Менеджер событий
     * @var EventManager
     */
    protected $_eventManager;

    /**
     * Адаптер для event_manager_cron
     * @var Repository
     */
    protected $_dbEventManagerCron;

    /**
     * Логгер
     * @var Logger
     */
    protected $_logger;

    /**
     * Набор неймспейсов, в которых требуется искать CRON-наблюдателей
     * @var string[]
     */
    protected $_namespaces;

    /**
     * Данные о наблюдателях, хранящиеся в базе данных
     * @var array
     */
    protected $_listenerData = array();

    /**
     * CRON-сервис
     * Автоматически загружает список наблюдателей из переданного адаптера
     * @param App_EventManager_EventManager $eventManager
     * @param Repository $repository
     * @param Logger $logger
     * @param string[] $namespaces
     * @throws Exception
     */
    public final function __construct(EventManager $eventManager, Repository $repository, Logger $logger, array $namespaces)
    {
        $this->_eventManager = $eventManager;
        $this->_dbEventManagerCron = $repository;
        $this->_logger = $logger;
        $this->_namespaces = $namespaces;

        $this->_loadListeners();
    }

    /**
     * Загружает список существующих наблюдателей из базы данных
     * @throws Exception
     */
    protected function _loadListeners()
    {
        $eventManager = $this->_getEventManager();
        $dbTable = $this->_getDbEventManagerCron();
        $filter = new Zend_Filter_Word_SeparatorToCamelCase('_');

        foreach($dbTable->fetchListeners() as $eventRow) {
            $className = false;

            foreach($this->_namespaces as $namespace) {
                $className = $namespace.'_'.ucfirst($filter->filter($eventRow->listener));

                if(class_exists($className)) {
                    break;
                }
            }

            if(!($className && class_exists($className))) {
                throw new \Exception(sprintf('Не найден наблюдатель для сервиса `%s`', $eventRow->listener));
            }

            /** @var App_EventManager_Component_Listener_BasicListener $listener */
            if($eventRow->enabled) {
                $listener = new $className();
                $eventManager->getListeners()->register($listener);
            }

            $this->_listenerData[$eventRow->id] = $eventRow;
        }
    }

    /**
     * Возвращает переданный конструктору CRON-сервис
     * @return App_EventManager_EventManager
     */
    protected final function _getEventManager()
    {
        return $this->_eventManager;
    }

    /**
     * Возврашает переданный конструктору адаптер для event_manager_cron
     * @return Repository
     */
    protected final function _getDbEventManagerCron()
    {
        return $this->_dbEventManagerCron;
    }

    /**
     * Возвращает переданный конструктору логгер
     * @return App_Cron_Logger_FileLogger
     */
    protected final function _getLogger()
    {
        return $this->_logger;
    }

    /**
     * Запуск CRON-сервисов
     *
     * CRON-сервис запускается в следующих случаях:
     *  - Отсутствует время последнего запуска сервиса
     *  - Для сервиса заполнено значение run_period и с момента последнего запуска прошло указанное количество секунд
     *  - Для сервиса заполнено значение run_date и текущее время больше или равно указанного значения
     */
    public function runCronServices()
    {
        $eventManager = $this->_getEventManager();
        $dbTable = $this->_getDbEventManagerCron();
        $logger = $this->_getLogger();

        foreach($this->_listenerData as $listenerRow) {
            $listenerId = $listenerRow->listener;
            $listenerEnabled = $listenerRow->enabled;

            // Первая проверка: CRON-сервис включен
            if($listenerEnabled) {
                $listenerEnabled = false;
                $listenerLastRun = $dbTable->getLastRun($listenerId);
                $listenerRunPeriod = (int) $listenerRow->run_period;
                $listenerRunDate = strlen($listenerRow->run_date) ? new Zend_Date((string) $listenerRow->run_date) : false;

                if($listenerLastRun instanceof Zend_Date) {
                    $listenerLastRun = (int) $listenerLastRun->toValue();
                }

                if($listenerRunDate && $listenerRow->period > 0) {
                    throw new \Exception(sprintf('Неправильная конфигурация для CRON-сервиса `%s`: нельзя задавать одновременно и дату, и период запуска', $listenerId));
                }else if($listenerRunPeriod > 0){
                    $secondsPassedFromLastRun = time() - $listenerLastRun;

                    // Запуск CRON-сервиса, если с момента последнего запуска прошло указанное в run_period или более количество секунд
                    if($secondsPassedFromLastRun >= $listenerRunPeriod) {
                        $listenerEnabled = true;
                    }
                }else if($listenerRunDate){
                    // Запуск CRON-сервиса, если текущая дата равна или более указанного в run_date значении
                    if(time() >= $listenerRunDate) {
                        $listenerEnabled = true;
                    }
                }

                if($listenerEnabled) {
                    try {
                        $startTime = microtime(true);

                        $logger->info(sprintf('Execute cron service `%s`', $listenerId));
                        $eventManager->fire($listenerId, 'cron', (array) $listenerRow + array('logger' => $logger));
                        $dbTable->touchLastRun($listenerId);

                        $logger->info(sprintf('Successful execution cron service `%s`, execution time: %ss', $listenerId, round(microtime(true) - $startTime, 3)));
                    }catch(\Exception $e){
                        $logger->exception($e);

                        echo $e->getTraceAsString();
                    }
                }
            }
        }

        $logger->close();
    }
}