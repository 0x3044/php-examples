<?php
class App_Filemanager_Preview_Converter_PdfConverter
{
    /**
     * Возвращает первую страницу PDF-файла в виде PNG-картинки
     * @param $filePath
     * @return imagick
     * @throws Exception
     */
    public function convert($filePath)
    {
        if(!file_exists($filePath)) {
            throw new \Exception('Файл `%s` отсутствует на диске', $filePath);
        }

        $im = new imagick($filePath.'[0]');
        $im->setImageFormat('png');

        return $im;
    }
}