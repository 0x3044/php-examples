<?php
class App_Filemanager_Preview_LinkFactory
{
    /**
     * Создает js-параметры для ссылки превью файла
     * @param string $path Путь к файлу на сервере
     * @param string $publicLink Публичная ссылка к файлу
     * @param string $vendorLink Ссылка на открытие файла сторонним сервисом
     * @param bool $downloadLink (опционально) Ссылка на скачивание файла
     * @param bool $title (опционально) Заголовок
     * @return array
     * @throws Exception
     */
    public static function createLink($path, $publicLink, $vendorLink, $downloadLink = false, $title = false)
    {
        if(!(file_exists($path))) {
            throw new \Exception(sprintf('Файл `%s` отсутствует', $path));
        }

        $fileHelper = new App_Filemanager_Preview_FileTypeDetector($path);

        return array(
            'type' => $fileHelper->getBrowsableType(),
            'fileName' => basename($path),
            'publicLink' => $publicLink,
            'vendorLink' => $vendorLink,
            'downloadLink' => $downloadLink ? $downloadLink : false,
            'title' => $title
        );
    }
}