<?php
class App_Filemanager_Preview_FileTypeDetector
{
    protected $_textTypes = array('text/plain');
    protected $_imageTypes = array('image/png', 'image/jpeg', 'image/gif', 'image/bmp');
    protected $_pdfTypes = array('application/pdf');
    protected $_toImageConvertable = array('image/tiff');

    /**
     * @var App_Finance_Ticket_Component_Document
     */
    protected $_path;

    public function __construct($path) {
        $this->_path = $path;
    }

    /**
     * Возвращает переданный конструктору документ
     * @return \App_Finance_Ticket_Component_Document
     */
    public function getPath()
    {
        return $this->_path;
    }

    /**
     * Возвращает "тип" файла
     * @return bool|string
     */
    public function getBrowsableType()
    {
        if($this->isText()) {
            return 'text';
        }else if($this->isImage()) {
            return 'image';
        }else if($this->isPdf() || $this->isToImageConvertable()){
            return 'image-vendor';
        }else if($this->isZoho()) {
            return 'vendor';
        }else{
            return false;
        }
    }


    /**
     * Возвращает true, если финансовый саппорт имеет возможность "просмотра" этого файла
     * @return bool
     */
    public function isBrowsable()
    {
        return $this->isText() || $this->isImage() || $this->isZoho() || $this->isPdf() || $this->isToImageConvertable();
    }

    /**
     * Возврашает true, если файл относится к plain/text-типу
     * @return bool
     */
    public function isText()
    {
        if(!(file_exists($this->getPath()))) {
            return false;
        }

        return in_array(mime_content_type($this->getPath()), $this->_textTypes);
    }

    /**
     * Возвращает true, если файл является картинкой
     * @return bool
     */
    public function isImage()
    {
        if(!(file_exists($this->getPath()))) {
            return false;
        }

        return in_array(mime_content_type($this->getPath()), $this->_imageTypes);
    }

    /**
     * Возврашает true, если файл может быть открыт с помощью сервиса Zoho
     * @return bool
     */
    public function isZoho()
    {
        return App_Zoho_FileDetector::isReadable($this->getPath());
    }

    /**
     * Возвращает true, если файл является PDF-документом
     * @return bool
     */
    public function isPdf()
    {
        if(!(file_exists($this->getPath()))) {
            return false;
        }

        return in_array(mime_content_type($this->getPath()), $this->_pdfTypes);
    }

    /**
     * Возвращает true, если файл является изображением, которые можно получить с помощью конвертера
     * @return bool
     */
    public function isToImageConvertable()
    {
        if(!(file_exists($this->getPath()))) {
            return false;
        }

        return in_array(mime_content_type($this->getPath()), $this->_toImageConvertable);
    }
}