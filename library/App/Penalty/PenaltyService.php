<?php
use App_Penalty_Service_RepositoryInterface as PenaltyRepositoryInterface;
use App_Penalty_Watcher_Factory as WatcherFactory;
use App_Penalty_Scheme_Factory as SchemeFactory;
use App_Penalty_System_Factory as SystemFactory;

class App_Penalty_PenaltyService
{

    /**
     * Репозиторий
     * @var PenaltyRepositoryInterface
     */
    protected $_penaltyRepository;

    /**
     * Опции сервиса (из файла Service/options.php)
     * @var array
     */
    protected $_options;

    /**
     * Фабрика watcher'ов
     * @var WatcherFactory
     */
    protected $_watcherFactory;

    /**
     * Фабрика scheme'ов
     * @var SchemeFactory
     */
    protected $_schemeFactory;

    /**
     * Фабрика систем автоматического штрафования
     * @var SystemFactory
     */
    protected $_systemFactory;

    /**
     * Penalty Service
     * @param App_Penalty_Service_RepositoryInterface $repository
     */
    public function __construct(PenaltyRepositoryInterface $repository)
    {
        $this->_penaltyRepository = $repository;
        $this->_options = require_once __DIR__.'/Service/options.php';
        $this->_watcherFactory = new WatcherFactory($this);
        $this->_schemeFactory = new SchemeFactory($this);
        $this->_systemFactory = new SystemFactory($this);
    }

    /**
     * Возвращает переданный конструктору PenaltyRepository
     * @return App_Penalty_Service_RepositoryInterface
     */
    public function getPenaltyRepository()
    {
        return $this->_penaltyRepository;
    }

    /**
     * @return App_Penalty_Scheme_Factory
     */
    public function getSchemeFactory()
    {
        return $this->_schemeFactory;
    }

    /**
     * @return App_Penalty_Watcher_Factory
     */
    public function getWatcherFactory()
    {
        return $this->_watcherFactory;
    }

    /**
     * @return App_Penalty_System_Factory
     */
    public function getSystemFactory()
    {
        return $this->_systemFactory;
    }

    /**
     * Возвращает опции PenaltyService
     * @return array
     */
    public function getOptions()
    {
        return $this->_options;
    }

    /**
     * Возвращает данные о указанной схеме штрафования
     * @param string $schemeName
     * @return array
     */
    public function getSchemeData($schemeName)
    {
        if(!(isset($this->_options['schemes'][$schemeName]))) {
            throw new \OutOfBoundsException(sprintf('Схема `%s` не найдена', $schemeName));
        }

        return $this->_options['schemes'][$schemeName];
    }

    /**
     * Возвращение ланных наблюдателя
     * @param string $watcherName
     * @return array
     */
    public function getWatcherData($watcherName)
    {
        if(!(isset($this->_options['watchers'][$watcherName]))) {
            throw new \OutOfBoundsException(sprintf('Наблюдатель `%s` не найден', $watcherName));
        }

        return $this->_options['watchers'][$watcherName];
    }
}