<?php
use App_Penalty_Service_RepositoryInterface as PenaltyRepositoryInterface;

class App_Penalty_Form
{
    /**
     * Id системы автоматического штрафования
     * @var int
     */
    protected $_id;

    /**
     * Название системы автоматического штрафования
     * @var string
     */
    protected $_name;

    /**
     * Наблюдатель для указанной системы штрафования
     * @var string
     */
    protected $_watcher;

    /**
     * Схема штрафования
     * @var string
     */
    protected $_scheme;

    /**
     * Период проверки
     * @var int
     */
    protected $_period;

    /**
     * Сумма штрафа
     * @var int
     */
    protected $_sum;

    /**
     * Причина штрафа, текст рассылки
     * @var string
     */
    protected $_reason;

    /**
     * Флаг "система штрафования включена"
     * @var bool
     */
    protected $_enabled;

    /**
     * Дополнительные опции системы штрафования
     * @var array
     */
    protected $_options = array();

    /**
     * Форма для создания/редактирования систем штрафования
     * @param int $id
     */
    public function __construct($id = NULL)
    {
        if(!(is_null($id))) {
            App_Spl_TypeCheck::getInstance()->id($id);
        }

        $this->_setId($id);
    }

    /**
     * Загрузка данных формы из базы данных
     * @param int $id
     * @param App_Penalty_PenaltyService $service
     */
    public function loadFromDb($id, App_Penalty_PenaltyService $service)
    {
        $reader = new App_Penalty_Form_Reader_Db();
        $reader->read($id, $this, $service);
    }

    /**
     * Загрузка данных формы из массива
     * @param array $formData
     */
    public function loadFromPost(array $formData)
    {
        $reader = new App_Penalty_Form_Reader_Post();
        $reader->read($this, $formData);
    }

    /**
     * Заполнить данные формы из параметров системы
     * @param App_Penalty_System_Params $params
     */
    public function apply(App_Penalty_System_Params $params)
    {
        $this->setEnabled($params->isEnabled());
        $this->setPeriod($params->getPeriod());
        $this->setName($params->getName());
        $this->setReason($params->getReason());
        $this->setScheme($params->getSchemeName());
        $this->setWatcher($params->getWatcherName());
        $this->setOptions($params->getOptions());
        $this->setSum($params->getSum());
    }

    /**
     * Сохранение данных формы в базу данных
     * @param App_Penalty_PenaltyService $service
     */
    public function save(App_Penalty_PenaltyService $service)
    {
        $writer = new App_Penalty_Form_Writer_Db();
        $writer->write($this, $service);
    }

    /**
     * Возвращает данные формы в виде массива
     * @return array
     */
    public function toArray()
    {
        return array(
            'id' => $this->getId(),
            'name' => $this->getName(),
            'watcher' => $this->getWatcher(),
            'scheme' => $this->getScheme(),
            'period' => $this->getPeriod(),
            'sum' => $this->getSum(),
            'enabled' => $this->isEnabled(),
            'options' => $this->getOptions(),
            'reason' => $this->getReason()
        );
    }

    /**
     * Возвращает данные формы для вставки их в запись БД
     * @return array
     */
    public function toDbRow()
    {
        return $this->toArray();
    }

    /**
     * Возврашает Id
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Возвращает true, если указан Id системы автоматической системы штрафования
     * @return bool
     */
    public function hasId()
    {
        return $this->_id > 0;
    }

    /**
     * Установить Id
     * @param int $id
     */
    protected function _setId($id)
    {
        if($id > 0) {
            App_Spl_TypeCheck::getInstance()->id($id);
        }

        $this->_id = (int) $id;
    }

    /**
     * Возвращает название(заголовок) системы автоматического штрафования
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Устанавливает название(заголово) системы автоматического штрафования
     * @param string $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * Возвращает период (в часах) проверки условий
     * @return int
     */
    public function getPeriod()
    {
        return (int) $this->_period;
    }

    /**
     * Задать период (в часах) проверки условий
     * @param int $period
     */
    public function setPeriod($period)
    {
        $this->_period = $period;
    }

    /**
     * Возвращает текст e-mail'а, причину для рассылки
     * @return string
     */
    public function getReason()
    {
        return $this->_reason;
    }

    /**
     * Устанавливает текст e-mail'а, причину для рассылки
     * @param string $reason
     */
    public function setReason($reason)
    {
        $this->_reason = $reason;
    }

    /**
     * Возврашает схему штрафования
     * @return string
     */
    public function getScheme()
    {
        return $this->_scheme;
    }

    /**
     * Задать схему штрафования
     * @param string $scheme
     */
    public function setScheme($scheme)
    {
        $this->_scheme = $scheme;
    }

    /**
     * Возвращает сумму штрафа
     * @return int
     */
    public function getSum()
    {
        return $this->_sum;
    }

    /**
     * Установить сумму штрафа
     * @param int $sum
     */
    public function setSum($sum)
    {
        if(($sum = round($sum, 2)) < 0) {
            throw new \InvalidArgumentException(sprintf('Задана неправильная сумма штрафа', var_export($sum, true)));
        }

        $this->_sum = $sum;
    }

    /**
     * Возвращает наблюдателя(watcher)
     * @return string
     */
    public function getWatcher()
    {
        return $this->_watcher;
    }

    /**
     * Задать наблюдателя(watcher)
     * @param string $watcher
     */
    public function setWatcher($watcher)
    {
        $this->_watcher = $watcher;
    }

    /**
     * Возвращает дополнительные опции
     * @return array
     */
    public function getOptions()
    {
        return $this->_options;
    }

    /**
     * Установить дополнительные опции
     * @param array $options
     */
    public function setOptions(array $options)
    {
        $this->_options = $options;
    }

    /**
     * Возвращает true, если система автоматического штрафования включена
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->_enabled;
    }

    /**
     * Установить флаг "включено?"
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->_enabled = $enabled;
    }
}