<?php
use App_Penalty_PenaltyService as PenaltyService;
use App_Penalty_Service_RepositoryInterface as PenaltyRepositoryInterface;

class App_Penalty_Service_Factory
{
    /**
     * @var PenaltyRepositoryInterface
     */
    protected $_repository;

    /**
     * PenaltyService с репозиторием в базе данных
     * @var PenaltyService
     */
    protected static $_instance;

    public function __construct(PenaltyRepositoryInterface $repository)
    {
        $this->_repository = $repository;
    }

    /**
     * Возврашает PenaltyService с репозиторием в базе данных
     * @return PenaltyService
     */
    public static function getDbInstance()
    {
        if(!(self::$_instance)) {
            /** @var App_Db_PenaltyService $db */
            $db = App_Db::get(DB_PENALTY_SERVICE);
            $factory = new self($db);
            self::$_instance = $factory->create();
        }

        return self::$_instance;
    }

    /**
     * @return App_Penalty_Service_RepositoryInterface
     */
    protected final function getPenaltyRepository()
    {
        return $this->_repository;
    }

    /**
     * Возвращает PenaltyService с загруженного из репозитория списка стратегий
     * @param array $options
     * @return App_Penalty_PenaltyService
     */
    public function create(array $options = array())
    {
        $options = array_merge(array(), $options);

        $penaltyService = new PenaltyService($this->getPenaltyRepository());

        return $penaltyService;
    }
}