<?php
return array(
    /**
     * Список доступных наблюдателей(watcher'ов), следящих за необходимостью выписать кому-либо штраф
     */
    'watchers' => array(
        'null' => array(
            'name' => 'null',
            'className' => 'App_Penalty_Watcher_Null_Strategy',
            'title' => 'Отсутствие стратегии',
            'description' => 'Отсутствует стратегия наблюдения за необходимостью выписывания штрафов',
            'subForm' => false,
            'visible' => false
        ),
        'test' => array(
            'name' => 'test',
            'className' => 'App_Penalty_Watcher_Test_Strategy',
            'title' => 'Тестовая стратегия',
            'description' => 'Данная стратегия выписывает штраф раз в определенный период',
            'subForm' => false,
            'visible' => true
        ),
        'base_price_request' => array(
            'name' => 'base_price_request',
            'className' => 'App_Penalty_Watcher_BasePriceRequest_Strategy',
            'title' => 'Запрос базовой цены',
            'description' => 'Штрафование при неуказании базовой цены',
            'subForm' => false,
            'visible' => true
        )
    ),

    /**
     * Список доступных стратегий выписывания штрафов
     */
    'schemes' => array(
        'once' => array(
            'name' => 'once',
            'className' => 'App_Penalty_Scheme_Once_Strategy',
            'title' => 'Одноразовое штрафование',
            'description' => 'При выполнении условий штрафования пользователям в указанном контексте будет назначен один и только один штраф',
            'subForm' => true,
            'visible' => true
        ),
        'constant' => array(
            'name' => 'constant',
            'className' => 'App_Penalty_Scheme_Constant_Strategy',
            'title' => 'Многоразовое штрафование',
            'description' => 'При выполнении условий штрафования пользователям в указанном контексте будет назначаться штраф раз в указанный период',
            'subForm' => true,
            'visible' => true
        ),
        'progressive' => array(
            'name' => 'progressive',
            'className' => 'App_Penalty_Scheme_Progressive_Strategy',
            'title' => 'Прогрессивное многоразовое штрафование',
            'description' => 'При выполнении условий штрафования пользователям в указанном контексте будет назначаться нарастающий штраф раз в указанный период',
            'subForm' => true,
            'visible' => true
        ),
        'null' => array(
            'name' => 'null',
            'className' => 'App_Penalty_Scheme_Null_Strategy',
            'title' => 'Нулевой штраф',
            'description' => 'При выполнении условий штрафования пользователям раз в указанный период будет назначаться штраф в размере 0(ноль) рублей.',
            'subForm' => true,
            'visible' => true
        )
    )
);