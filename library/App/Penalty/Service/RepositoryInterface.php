<?php

/**
 * Репозиторий для системы автоматического штрафования
 */
interface App_Penalty_Service_RepositoryInterface
{
    /**
     * Возвращает все существующие системы автоматического штрафования
     * @return stdClass[]
     */
    public function fetchPenaltySystems();

    /**
     * Возвращает параметры автоматической системы штрафования
     * @param int $id
     * @return array
     */
    public function getPenaltyParams($id);

    /**
     * Регистрирует новую систему автоматического штрафование
     * @param array $params
     */
    public function registerPenaltySystem(array $params);

    /**
     * Обновляет параметры уже существующей системы автоматического штрафования
     * @param int $id
     * @param array $params
     */
    public function updatePenaltySystem($id, $params);

    /**
     * Удаляет существующую систему автоматического штрафования
     * @param int $id
     */
    public function unregisterPenaltySystemById($id);
}