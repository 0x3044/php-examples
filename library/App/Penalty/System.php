<?php
use App_Penalty_PenaltyService as PenaltyService;
use App_Penalty_System_SystemInterface as SystemInterface;
use App_Penalty_System_Params as SystemParams;
use App_EventManager_EventManager as EventManager;
use App_Penalty_Watcher_WatcherInterface as WatcherInterface;
use App_Penalty_Scheme_SchemeInterface as SchemeInterface;

class App_Penalty_System implements SystemInterface
{
    /**
     * Переданные конструктору параметры системы автоматического штрафования
     * @var App_Penalty_System_Params
     */
    protected $_systemParams;

    /**
     * Переданный конструктору сервис
     * @var PenaltyService
     */
    protected $_penaltyService;

    /**
     * EventManager
     * @var EventManager
     */
    protected $_eventManager;

    /**
     * Наблюдатель
     * @var WatcherInterface
     */
    protected $_watcher;

    /**
     * Схема выписывания штрафов
     * @var SchemeInterface
     */
    protected $_scheme;

    /**
     * {@inheritdoc}
     * @param App_Penalty_System_Params $systemParams
     * @param App_Penalty_PenaltyService $penaltyService
     */
    public function __construct(SystemParams $systemParams, PenaltyService $penaltyService)
    {
        $this->_systemParams = $systemParams;
        $this->_penaltyService = $penaltyService;
        $this->_eventManager = App_EventManager_EventManagerFactory::create();

        if($systemParams->hasId()) {
            $this->_scheme = $penaltyService->getSchemeFactory()->create($systemParams->getSchemeName(), $this);
            $this->_watcher = $penaltyService->getWatcherFactory()->create($systemParams->getWatcherName(), $this);
        } else {
            $this->_scheme = $penaltyService->getSchemeFactory()->create('null', $this);
            $this->_watcher = $penaltyService->getWatcherFactory()->create('null', $this);
        }
    }

    /**
     * {@inheritdoc}
     * @return SystemParams
     */
    public function getSystemParams()
    {
        return $this->_systemParams;
    }

    /**
     * {@inheritdoc}
     * @param SystemParams $params
     */
    public function replaceSystemParams(SystemParams $params)
    {
        $this->_systemParams = $params;
    }

    /**
     * {@inheritdoc}
     * @return \App_Penalty_PenaltyService
     */
    public function getPenaltyService()
    {
        return $this->_penaltyService;
    }

    /**
     * {@inheritdoc}
     * @return EventManager
     */
    public function getEventManager()
    {
        return $this->_eventManager;
    }

    /**
     * {@inheritdoc}
     * @return \App_Penalty_Scheme_SchemeInterface
     */
    public function getScheme()
    {
        return $this->_scheme;
    }

    /**
     * {@inheritdoc}
     * @return \App_Penalty_Watcher_WatcherInterface
     */
    public function getWatcher()
    {
        return $this->_watcher;
    }

    /**
     * {@inheritdoc}
     */
    public function register()
    {
        $this->getEventManager()->fire('*', 'register', $this, array());
    }

    /**
     * {@inheritdoc}
     */
    public function update()
    {
        $this->getEventManager()->fire('*', 'update', $this, array());
    }

    /**
     * {@inheritdoc}
     */
    public function unregister()
    {
        $this->getEventManager()->fire('*', 'unregister', $this, array());
    }

    /**
     * {@inheritdoc}
     */
    public function validate()
    {
        // TODO: Implement validate() method.
    }

    /**
     * {@inheritdoc}
     */
    public function cron()
    {
        // TODO: Implement cron() method.
    }
}