<?php
use App_Penalty_System_SystemInterface as SystemInterface;

interface App_Penalty_Watcher_WatcherInterface
{
    public function __construct(SystemInterface $penaltySystem);

    public function getSystem();

    public function getName();

    public function validate();
}