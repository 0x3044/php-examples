<?php
use App_Penalty_Watcher_AbstractStrategy_Strategy as AbstractStrategy;

class App_Penalty_Watcher_BasePriceRequest_Strategy extends AbstractStrategy
{
    /**
     * {@inheritdoc}
     * @return string
     */
    public function getName()
    {
        return 'base_price_request';
    }

    /**
     * {@inheritdoc}
     */
    public function validate()
    {
    }
}