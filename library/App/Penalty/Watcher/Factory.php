<?php
use App_Penalty_PenaltyService as PenaltyService;
use App_Penalty_Watcher_WatcherInterface as WatcherInterface;
use App_Penalty_System_SystemInterface as SystemInterface;

class App_Penalty_Watcher_Factory
{

    /**
     * Переданный конструктору PenaltyService
     * @var PenaltyService
     */
    protected $_penaltyService;

    /**
     * Фабрика наблюдателей
     * @param $penaltyService
     */
    public function __construct($penaltyService)
    {
        $this->_penaltyService = $penaltyService;
    }

    /**
     * Возвращает переданный конструктору PenaltyService
     * @return App_Penalty_PenaltyService
     */
    protected function _getPenaltyService()
    {
        return $this->_penaltyService;
    }

    /**
     * Возвращает Watcher
     * @param $watcherName
     * @param App_Penalty_System_SystemInterface $penaltySystem
     * @return WatcherInterface
     */
    public function create($watcherName, SystemInterface $penaltySystem)
    {
        $options = $this->_getPenaltyService()->getWatcherData($watcherName);

        return new $options['className']($penaltySystem);
    }
}