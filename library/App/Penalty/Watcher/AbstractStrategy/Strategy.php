<?php
use App_Penalty_Watcher_WatcherInterface as WatcherInterface;
use App_Penalty_System_SystemInterface as SystemInterface;

abstract class App_Penalty_Watcher_AbstractStrategy_Strategy implements WatcherInterface
{
    protected $_penaltySystem;

    public function __construct(SystemInterface $penaltySystem)
    {
        $this->_penaltySystem = $penaltySystem;
    }

    public function getSystem()
    {
        return $this->_penaltySystem;
    }
}