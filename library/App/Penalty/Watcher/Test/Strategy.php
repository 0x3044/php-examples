<?php
use App_Penalty_Watcher_AbstractStrategy_Strategy as AbstractStrategy;

class App_Penalty_Watcher_Test_Strategy extends AbstractStrategy
{
    /**
     * {@inheritdoc}
     * @return string
     */
    public function getName()
    {
        return 'test';
    }

    /**
     * Выписывает штраф самому косячному в компании сотруднику
     * {@inheritdoc}
     */
    public function validate()
    {
        $this->createPenalty(214);
    }
}