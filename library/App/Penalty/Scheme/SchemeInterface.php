<?php
use App_Penalty_System_SystemInterface as SystemInterface;

interface App_Penalty_Scheme_SchemeInterface
{
    /**
     * Стратегия выписывания штрафов
     * Данный слой определяет, как именно будут выписываться штрафы - например, одноразово, многоразово, прогрессивно, etc
     * Параметр $sContext определяет контекст, в котором будут выписывать штрафы (например, если нужно выписывать
     *  штрафы пользователю относительно заявки TASK12345, то параметр $sContext будет иметь значение "claim_12345".
     * @param SystemInterface $penaltySystem
     */
    public function __construct(SystemInterface $penaltySystem);

    /**
     * Возвращает переданную конструктору систему автоматического штрафования
     * @return SystemInterface
     */
    public function getSystem();

    /**
     * Возвращает название стратегии
     * @return string
     */
    public function getName();

    /**
     * Создать штраф для указанных получателя
     * @param string $sContext
     * @param mixed $recipients
     */
    public function createPenalty($sContext, $recipients);

    /**
     * Возвращает true, если получатели имеют штраф
     * @param string $sContext
     * @param $recipients
     * @return bool
     */
    public function hasPenalty($sContext, $recipients);

    /**
     * Возвращает количество выписанных штрафов указанным получателям относительно переданного конструктору контекста
     * @param string $sContext
     * @param $recipients
     * @return int
     */
    public function numPenalties($sContext, $recipients);

    /**
     * Удаляет штраф с указанных получателей
     * @param string $sContext
     * @param mixed $recipients
     */
    public function destroyPenalty($sContext, $recipients);
}