<?php
use App_Penalty_Scheme_Progressive_Strategy as ProgressivePenaltyStrategy;

/**
 * Многоразовая, прогрессивная стратегия выписывания штрафов
 * Выписывает пользователю штраф до тех пор, пока выполняется условие штрафования
 * повторного выписывания пользователю штрафа в указанном контексте
 */
class App_Penalty_Scheme_Constant_Strategy extends ProgressivePenaltyStrategy
{
    /**
     * {@inheritdoc}
     * @return string
     */
    public function getName()
    {
        return 'constant';
    }
}