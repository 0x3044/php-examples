<?php
use App_Penalty_PenaltyService as PenaltyService;
use App_Penalty_Scheme_SchemeInterface as SchemeInterface;
use App_Penalty_System_SystemInterface as SystemInterface;

class App_Penalty_Scheme_Factory
{
    /**
     * Переданный конструктору PenaltyService
     * @var PenaltyService
     */
    protected $_penaltyService;

    /**
     * Фабрика наблюдателей
     * @param $penaltyService
     */
    public function __construct($penaltyService)
    {
        $this->_penaltyService = $penaltyService;
    }

    /**
     * Возвращает переданный конструктору PenaltyService
     * @return App_Penalty_PenaltyService
     */
    protected function _getPenaltyService()
    {
        return $this->_penaltyService;
    }

    /**
     * Возвращает Scheme
     * @param string $schemeName
     * @param SystemInterface $penaltySystem
     * @return SchemeInterface
     */
    public function create($schemeName, SystemInterface $penaltySystem)
    {
        $options = $this->_getPenaltyService()->getSchemeData($schemeName);

        return new $options['className']($penaltySystem);
    }
}