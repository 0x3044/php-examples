<?php
use App_Penalty_Scheme_AbstractStrategy_Strategy as AbstractPenaltyStrategy;

/**
 * NullStrategy, тестовая затычка для штрафов
 * Используется при тестировании и разработке
 */
class App_Penalty_Scheme_Null_Strategy extends AbstractPenaltyStrategy
{
    /**
     * Возвращает название стратегии
     * @return string
     */
    public function getName()
    {
        return 'null';
    }
}