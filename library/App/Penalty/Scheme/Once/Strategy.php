<?php
use App_Penalty_Scheme_AbstractStrategy_Strategy as AbstractPenaltyStrategy;

/**
 * Единовременная, одноразовая стратегия выписывания штрафов
 * Выписывает пользователю один и только один штраф в указанном контексте, и ничего не будет делать при попытке
 * повторного выписывания пользователю штрафа в указанном контексте
 */
class App_Penalty_Scheme_Once_Strategy extends AbstractPenaltyStrategy
{
    /**
     * {@inheritdoc}
     * @return string
     */
    public function getName()
    {
        return 'once';
    }

    /**
     * Проверяет входное значение на допустимость в качестве использования как получателя штрафа в данной стратегии.
     * Выбрасывает исключение \InvalidArgumentException в случае ошибки
     * @param $input
     */
    protected final function _validateRecipients(&$input)
    {
        if(!(is_numeric($input)) || $input <= 0) {
            throw new \InvalidArgumentException(sprintf('Переданной значение `%s` не является валидным для данной стратегии выписывания штрафов', var_export($input, true)));
        }

        $input = (int) $input;
    }
}