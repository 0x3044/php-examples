<?php
use App_Penalty_System_SystemInterface as SystemInterface;
use App_Penalty_Scheme_SchemeInterface as SchemeInterface;

abstract class App_Penalty_Scheme_AbstractStrategy_Strategy implements SchemeInterface
{
    /**
     * Переданная конструктору система автоматического штрафования
     * @var SystemInterface
     */
    protected $_system;

    /**
     * {@inheritdoc}
     * @param \App_Penalty_System_SystemInterface $penaltyWatcher
     */
    public function __construct(SystemInterface $penaltySystem)
    {
        $this->_system = $penaltySystem;
    }

    /**
     * {@inheritdoc}
     * @return SystemInterface
     */
    public function getSystem()
    {
        return $this->_system;
    }

    /**
     * {@inheritdoc}
     * @param string $sContext
     * @param mixed $recipients
     */
    public function createPenalty($sContext, $recipients)
    {
        // TODO: Implement createPenalty() method.
    }

    /**
     * {@inheritdoc}
     * @param string $sContext
     * @param $recipients
     * @return bool
     */
    public function hasPenalty($sContext, $recipients)
    {
        // TODO: Implement hasPenalty() method.
    }

    /**
     * {@inheritdoc}
     * @param string $sContext
     * @param $recipients
     * @return int
     */
    public function numPenalties($sContext, $recipients)
    {
        // TODO: Implement numPenalties() method.
    }

    /**
     * {@inheritdoc}
     * @param string $sContext
     * @param mixed $recipients
     */
    public function destroyPenalty($sContext, $recipients)
    {
        // TODO: Implement destroyPenalty() method.
    }

}