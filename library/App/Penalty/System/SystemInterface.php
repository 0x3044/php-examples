<?php
use App_Penalty_PenaltyService as PenaltyService;
use App_EventManager_EventManager as EventManagerInterface;
use App_Penalty_Watcher_WatcherInterface as WatcherInterface;
use App_Penalty_Scheme_SchemeInterface as SchemeInterface;
use App_Penalty_System_Params as SystemParams;

interface App_Penalty_System_SystemInterface
{
    /**
     * Система штрафования
     * Аггрегирует в себя WatcherInterface, SchemeInterface и EventManagerInterface для реализации системы
     * автоматического штрафования пользователей.
     * @param App_Penalty_System_Params $systemParams
     * @param App_Penalty_PenaltyService $penaltyService
     */
    public function __construct(SystemParams $systemParams, PenaltyService $penaltyService);

    /**
     * Возвращает переданные конструктору параметры, опции системы
     * @return SystemParams
     */
    public function getSystemParams();

    /**
     * Замена параметров системы на другие
     * @param SystemParams $params
     */
    public function replaceSystemParams(SystemParams $params);

    /**
     * Возвращает переданный конструктору PenaltyService
     * @return PenaltyService
     */
    public function getPenaltyService();

    /**
     * Возврашает EventManager данной системы штрафования
     * @return EventManagerInterface
     */
    public function getEventManager();

    /**
     * Возвращает схему выписывания штрафов
     * В случае, если в качестве $id для SystemParams передано значение NULL метод вернет NullStrategy
     * @return SchemeInterface
     */
    public function getScheme();

    /**
     * Возврашает наблюдатель системы штрафования
     * В случае, если в качестве $id для SystemParams передано значение NULL метод вернет NullStrategy
     * @return WatcherInterface
     */
    public function getWatcher();

    /**
     * Регистрация системы автоматического штрафования
     */
    public function register();

    /**
     * Обновляет параметры системы автоматического штрафования
     */
    public function update();

    /**
     * Удаление системы автоматического штрафования из базы данных
     */
    public function unregister();

    /**
     * Запустить проверку на необходимость выписывания новых штрафов
     * Если штрафы будут найдены, то данный метод автоматически их выпишет
     */
    public function validate();

    /**
     * Метод, вызываемый CRON-сервисом. Делегирует вызов метода validate()
     */
    public function cron();
}