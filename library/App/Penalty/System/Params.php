<?php

class App_Penalty_System_Params
{
    /**
     * Id системы
     * @var int|null
     */
    protected $_id;

    /**
     * Опции, параметры системы автоматического штрафования
     * @var object
     */
    protected $_row;

    /**
     * Параметры, опции системы автоматического штрафования
     * Параметр $id должен быть либо числовым значением (в этом случае объект создается из данных с БД), либо NULL
     * для новой система автоматического штрафования
     * @param int|null $id
     * @param stdClass $row
     */
    public function __construct($id, $row)
    {
        if($id == 0) {
            $id = null;
        }

        if(!(is_null($id))) {
            App_Spl_TypeCheck::getInstance()->id($id);
        }

        $this->_id = $id;
        $this->_row = (object) $row;
    }

    /**
     * Возвращает Id стратегии автоматического штрафования
     * @return int
     */
    public function getId()
    {
        return (int) $this->_row->id;
    }

    /**
     * Возвращает true, если запись имеет Id
     * @return bool
     */
    public function hasId()
    {
        return !(is_null($this->_id));
    }

    /**
     * Возвращает название
     * @return string
     */
    public function getName()
    {
        return (string) $this->_row->name;
    }

    /**
     * Возвращает название наблюдателя автоматического штрафования
     * @return string
     */
    public function getWatcherName()
    {
        return (string) $this->_row->watcher;
    }

    /**
     * Возвращает название схемы выписывания штрафа
     * @return string
     */
    public function getSchemeName()
    {
        return (string) $this->_row->scheme;
    }

    /**
     * Возвращает сумму штрафа
     * @return float
     */
    public function getSum()
    {
        return round((float) $this->_row->sum, 2);
    }

    /**
     * Возвращает текст для рассылки пользователям, попавшим под действие данной стратегии автоматического штрафования
     * @return string
     */
    public function getReason()
    {
        return (string) $this->_row->reason;
    }

    public function getPeriod()
    {
        return (int) $this->_row->period;
    }

    /**
     * Возвращает true, если включена данная стратегия автоматического штрафования
     * @return bool
     */
    public function isEnabled()
    {
        return (bool) (int) $this->_row->enabled;
    }

    /**
     * Возвращает дополнительные опции
     * @return array
     */
    public function getOptions()
    {
        return (array) $this->_row->options;
    }

    /**
     * Возвращает параметры системы в виде массива
     * @return array
     */
    public function toArray()
    {
        return array(
            'id' => $this->getId(),
            'name' => $this->getName(),
            'watcher' => $this->getWatcherName(),
            'scheme' => $this->getSchemeName(),
            'period' => $this->getPeriod(),
            'sum' => $this->getSum(),
            'enabled' => $this->isEnabled(),
            'options' => $this->getOptions(),
            'reason' => $this->getReason()
        );
    }

    /**
     * Возвращает параметры системы для вставки/сохранения в БД
     * @return array
     */
    public function toDbRow()
    {
        return $this->toArray();
    }
}