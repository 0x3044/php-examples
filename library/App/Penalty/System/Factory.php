<?php
use App_Penalty_PenaltyService as PenaltyService;
use App_Penalty_System_SystemInterface as SystemInterface;
use App_Penalty_System as PenaltySystem;

class App_Penalty_System_Factory
{

    /**
     * Переданный конструктору PenaltyService
     * @var PenaltyService
     */
    protected $_penaltyService;

    /**
     * Фабрика систем автоматического штрафования
     * @param App_Penalty_PenaltyService $penaltyService
     */
    public function __construct(PenaltyService $penaltyService)
    {
        $this->_penaltyService = $penaltyService;
    }

    /**
     * Возвращает переданный конструктору PenaltyService
     * @return App_Penalty_PenaltyService
     */
    protected function _getPenaltyService()
    {
        return $this->_penaltyService;
    }

    /**
     * Создает и возврашает новую систему автоматического штрафования для записи ее в базу данных
     * @param App_Penalty_System_Params $penaltySystemParams
     * @return SystemInterface
     */
    public function createNewSystemInstance(App_Penalty_System_Params $penaltySystemParams)
    {
        $instance = new PenaltySystem($penaltySystemParams, $this->_getPenaltyService());

        return $this->_decorateInstance($instance);
    }

    /**
     * Создает и возвращает систему автоматического штрафования на основе параметров из базы данных
     * @param int $id
     * @return SystemInterface
     */
    public function createSystemFromDb($id)
    {
        $penaltySystemParams = new App_Penalty_System_Params($id, $this->_getPenaltyService()->getPenaltyRepository()->getPenaltyParams($id));
        $instance = new PenaltySystem($penaltySystemParams, $this->_getPenaltyService());

        return $this->_decorateInstance($instance);
    }

    /**
     * Декорирует, добавляет стандартное поведение для PenaltySystem
     * @param App_Penalty_System_SystemInterface $penaltySystem
     * @return \App_Penalty_System_SystemInterface
     */
    private function _decorateInstance(SystemInterface $penaltySystem)
    {
        $listener = new App_EventManager_Component_Listener_CallbackListener('penalty-default');
        $listener->add('register', /**
             * @param string $eventId
             * @param array $options
             * @param SystemInterface $source
             */
            function($eventId, $options, $source){
                if(!($source instanceof SystemInterface)) {
                    throw new \InvalidArgumentException('Параметр $source должен быть объектом App_Penalty_SystemInterface');
                }

                $repository = $source->getPenaltyService()->getPenaltyRepository();
                $repository->registerPenaltySystem($source->getSystemParams()->toDbRow());
            }
        );

        $listener->add('register', /**
             * @param string $eventId
             * @param array $options
             * @param SystemInterface $source
             */
            function($eventId, $options, $source){
                if(!($source instanceof SystemInterface)) {
                    throw new \InvalidArgumentException('Параметр $source должен быть объектом App_Penalty_SystemInterface');
                }

                // cron service registration process
            }
        );

        $listener->add('unregister', /**
             * @param string $eventId
             * @param array $options
             * @param SystemInterface $source
             */
            function($eventId, $options, $source){
                if(!($source instanceof SystemInterface)) {
                    throw new \InvalidArgumentException('Параметр $source должен быть объектом App_Penalty_SystemInterface');
                }

                if(!($source->getSystemParams()->hasId())) {
                    throw new \Exception('Данная система автоматического штрафования еще не зарегестрирована в системе');
                }

                $repository = $source->getPenaltyService()->getPenaltyRepository();
                $repository->unregisterPenaltySystemById($source->getSystemParams()->getId());
            }
        );

        $listener->add('unregister', /**
             * @param string $eventId
             * @param array $options
             * @param SystemInterface $source
             */
            function($eventId, $options, $source){
                if(!($source instanceof SystemInterface)) {
                    throw new \InvalidArgumentException('Параметр $source должен быть объектом App_Penalty_SystemInterface');
                }

                // cron service unregistration process
            }
        );

        $listener->add('update', /**
             * @param string $eventId
             * @param array $options
             * @param SystemInterface $source
             */
            function($eventId, $options, $source){
                if(!($source instanceof SystemInterface)) {
                    throw new \InvalidArgumentException('Параметр $source должен быть объектом App_Penalty_SystemInterface');
                }

                if(!($source->getSystemParams()->hasId())) {
                    throw new \Exception('Данная система автоматического штрафования еще не зарегестрирована в системе');
                }

                $repository = $source->getPenaltyService()->getPenaltyRepository();
                $repository->updatePenaltySystem($source->getSystemParams()->getId(), $source->getSystemParams()->toDbRow());
            }
        );

        $penaltySystem->getEventManager()->getListeners()->register($listener);
        return $penaltySystem;
    }
}