<?php

class App_Penalty_Form_Writer_Db
{
    public function write(App_Penalty_Form $form, App_Penalty_PenaltyService $penaltyService)
    {
        try {
            App_Db::get()->beginTransaction();

            if($form->hasId()) {
                $systemParams = new App_Penalty_System_Params($form->getId(), (object) $form->toDbRow());

                $system = $penaltyService->getSystemFactory()->createSystemFromDb($form->getId());
                $system->replaceSystemParams($systemParams);
                $system->update();
            }else{
                $newPenaltySystemParams = new App_Penalty_System_Params($form->getId(), (object) $form->toDbRow());
                $newPenaltySystemInstance = $penaltyService->getSystemFactory()->createNewSystemInstance($newPenaltySystemParams);
                $newPenaltySystemInstance->register();
            }

            App_Db::get()->commit();
        }catch(\Exception $e){
            App_Db::get()->rollBack();
            throw $e;
        }
    }
}