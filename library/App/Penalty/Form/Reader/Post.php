<?php

class App_Penalty_Form_Reader_Post
{
    /**
     * Читает и заполняет форму из $_POST-данных
     * @param App_Penalty_Form $form
     * @param array $formData
     */
    public function read(App_Penalty_Form $form, array $formData)
    {
        foreach(array('name', 'watcher', 'scheme', 'period', 'enabled', 'reason', 'sum', 'options') as $required) {
            if(!(isset($formData[$required]))) {
                throw new \InvalidArgumentException(sprintf('Отсутствует POST[%s]', $required));
            }
        }

        $form->setName($formData['name']);
        $form->setWatcher($formData['watcher']);
        $form->setScheme($formData['scheme']);
        $form->setEnabled((bool) (int) $formData['enabled']);
        $form->setPeriod((int) $formData['period']);
        $form->setReason($formData['reason']);
        $form->setSum($formData['sum']);
        $form->setOptions(array_merge($form->getOptions(), $formData['options']));
    }
}