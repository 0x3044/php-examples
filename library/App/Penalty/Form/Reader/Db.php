<?php

class App_Penalty_Form_Reader_Db
{
    public function read($id, App_Penalty_Form $form, App_Penalty_PenaltyService $penaltyService)
    {
        $penaltySystem = $penaltyService->getSystemFactory()->createSystemFromDb($id);
        $penaltyParams = $penaltySystem->getSystemParams();

        $form->apply($penaltyParams);
    }
}