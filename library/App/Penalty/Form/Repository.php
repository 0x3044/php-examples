<?php

class App_Penalty_Form_Repository
{

    /**
     * Список доступных стратегий выписывания штрафов
     * @var array
     */
    protected $_schemes = array();

    /**
     * Список доступных наблюдателей для автоматического штрафования
     * @var array
     */
    protected $_watchers = array();

    /**
     * Репозиторий данных для формы создания/редактирования записей автоматического штрафования
     * @param array $options
     */
    public function __construct(array $options)
    {
        // Валидация входных параметров
        foreach(array('schemes', 'watchers') as $requiredKey) {
            if(!(isset($options[$requiredKey]) && is_array($options[$requiredKey]))) {
                throw new \InvalidArgumentException(sprintf('Отсутствует опция `%s`', $requiredKey));
            }

            foreach($options[$requiredKey] as $item) {
                foreach(array('name', 'className', 'title', 'description') as $subRequiredKey) {
                    if(!(isset($item[$subRequiredKey]))) {
                        throw new \InvalidArgumentException(sprintf('Отсутствует опция `%s`', $subRequiredKey));
                    }
                }
            }
        }

        // Сохранение опций в репозиторий
        $this->_schemes = $options['schemes'];
        $this->_watchers = $options['watchers'];
    }

    /**
     * Возвращает список доступных схем для автоматического штрафования
     * @return array
     */
    public function getSchemes()
    {
        return $this->_schemes;
    }

    /**
     * Возвращает список доступных наблюдателей для автоматического штрафования
     * @return array
     */
    public function getWatchers()
    {
        return $this->_watchers;
    }
}