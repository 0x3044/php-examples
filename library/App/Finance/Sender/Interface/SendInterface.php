<?php

interface App_Finance_Sender_Interface_SendInterface
{
    /**
     * Возвращает Id заявки, к которой будут прикрепляться тикеты
     * @return int
     */
    public function getClaimId();

    /**
     * Установить Id заявки, к которой будут прикрепляться тикеты
     * @param int $claimId
     */
    public function setClaimId($claimId);

    /**
     * Заполняет форму/маршрут данными
     * @param array $data
     */
    public function createFromPost(array &$data);

    /**
     * Заполняет форму/маршрут данными, исходя из существующех для указанной заявки тикетов
     */
    public function createFromClaimId();

    /**
     * Отправляет отмеченные маршруты
     */
    public function send();

    /**
     * Конвертация маршрута/формы в массив
     * @return array
     */
    public function toArray();
}