<?php

interface App_Finance_Sender_Interface_CheckInterface
{
    /**
     * Отмечает маршрут к отправке
     */
    public function check();

    /**
     * Снимает маршрут с отправки
     */
    public function unCheck();

    /**
     * Возвращает true, если маршрут отмечен к отправке
     * @return bool
     */
    public function isChecked();
}