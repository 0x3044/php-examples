<?php
use App_Finance_Sender_Interface_CheckInterface as CheckInterface;

interface App_Finance_Sender_Interface_ParamsInterface extends CheckInterface
{
    /**
     * Устанавить максимальную дату обработки(просрочку) тикета
     * @param mixed $date_to_process
     */
    public function setDateToProcess($date_to_process);

    /**
     * Возвращает максимальную дату обработки(просрочку) тикета
     * @return mixed
     */
    public function getDateToProcess();

    /**
     * Конвертирует в массив
     * @return array
     */
    public function toArray();
}