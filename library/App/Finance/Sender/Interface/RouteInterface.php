<?php
use App_Finance_Sender_Interface_CheckInterface as CheckInterface;
use App_Finance_Sender_Interface_SendInterface as SendInterface;

interface App_Finance_Sender_Interface_RouteInterface extends SendInterface
{
    /**
     * Создает маршрут. Указание уникального Id обязательно
     * @param string $uniqueId
     */
    public function __construct($uniqueId);

    /**
     * @return \App_Finance_Sender_Form_Route_Element_Message
     */
    public function getMessageHandler();

    /**
     * @return \App_Finance_Sender_Form_Route_Element_Params
     */
    public function getParamsHandler();

    /**
     * @return \App_Finance_Sender_Form_Route_Element_Attachment_ServiceNote
     */
    public function getServiceNoteHandler();

    /**
     * @return \App_Finance_Sender_Form_Route_Element_State
     */
    public function getStateHandler();

    /**
     * Возвращает уникальный для маршрута Id
     * @return string
     */
    public function getUniqueId();

    /**
     * Возвращает ID типа тикета, который данный маршрут создает
     * @return int
     */
    public function getTicketTypeId();

    /**
     * Устанавливает ID типа тикета, который данный маршрут создает
     * @param int $ticketTypeId
     */
    public function setTicketTypeId($ticketTypeId);
}