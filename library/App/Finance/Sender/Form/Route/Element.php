<?php
use App_Finance_Sender_Interface_RouteInterface as RouteInterface;
use App_Finance_Sender_Form_Route_Element_Params as Params;
use App_Spl_Exception_TypeException as TypeException;
use App_Finance_Ticket_Factory as TicketFactory;
use App_Finance_Sender_Form_Route_Element_Params as ParamsHandler;
use App_Finance_Sender_Form_Route_Element_Message as MessageHandler;
use App_Finance_Sender_Form_Route_Element_State as StateHandler;

class App_Finance_Sender_Form_Route_Element implements RouteInterface
{
    /**
     * Тип запроса
     * @var int
     */
    protected $_ticketTypeId;

    /**
     * ID маршрута
     * @var string
     */
    protected $_uniqueId;

    /**
     * Id заявки
     * @var int
     */
    protected $_claimId;

    /**
     * Флаг "включить возможность выбора даты
     * @var bool
     */
    protected $_dateClientDeliveryFactEnabled = false;

    /**
     * Параметра маршрута
     * @var Params
     */
    protected $_paramsHandler;

    /**
     * Сообщение
     * @var MessageHandler
     */
    protected $_messageHandler;

    /**
     * Статус
     * @var StateHandler
     */
    protected $_stateHandler;

    /**
     * Служебная записка
     * @var App_Finance_Sender_Form_Route_Element_Attachment_ServiceNote
     */
    protected $_serviceNoteHandler;

    /**
     * Пояснительная записка
     * @var App_Finance_Sender_Form_Route_Element_Attachment_ExplanatoryNote
     */
    protected $_explanatoryNoteHandler;

    /**
     * @construct
     * @param string $uniqueId
     */
    public function __construct($uniqueId)
    {
        $this->_uniqueId = $uniqueId;

        $this->_paramsHandler = new ParamsHandler();
        $this->_messageHandler = new App_Finance_Sender_Form_Route_Element_Message();
        $this->_stateHandler = new App_Finance_Sender_Form_Route_Element_State();
        $this->_serviceNoteHandler = new App_Finance_Sender_Form_Route_Element_Attachment_ServiceNote();
        $this->_explanatoryNoteHandler = new App_Finance_Sender_Form_Route_Element_Attachment_ExplanatoryNote();
    }

    /**
     * Заполняет маршрут данными
     * @param array $data
     * @throws Exception
     */
    public function createFromPost(array &$data)
    {
        foreach(array("isChecked", "dateToProcess") as $requiredParams) {
            if(!(array_key_exists($requiredParams, $data))) {
                throw new \Exception("No `{$requiredParams}` param found");
            }
        }

        $isChecked = $data["isChecked"];
        $dateToProcess = $data["dateToProcess"];
        $serviceNoteFileParamName = "service_note";
        $explanatoryNoteFileParamName = "explanatory_note";

        $isChecked ? $this->getParamsHandler()->check() : $this->getParamsHandler()->unCheck();
        $this->getParamsHandler()->setDateToProcess($dateToProcess);
        $this->getParamsHandler()->setServiceNoteFileParamName($serviceNoteFileParamName);
        $this->getParamsHandler()->setExplanatoryNoteFileParamName($explanatoryNoteFileParamName);
    }

    /**
     * Отправляет отмеченные маршруты
     */
    public function send()
    {
        $params = $this->getParamsHandler();
        $toSend = $this->getStateHandler()->isActive() || $this->getStateHandler()->isRequired();

        if($this->getStateHandler()->isRequired() && !($this->getParamsHandler()->isChecked())) {
            throw new \Exception("Route width ticketTypeId:{$this->getTicketTypeId()} is required");
        }

        if($toSend && $params->isChecked()) {
            $ticket = TicketFactory::getInstance()->createFromTicketTypeId($this->getTicketTypeId());
            $ticket->setClaimId($this->getClaimId());
            $ticket->setDateToProcessFromMixed($params->getDateToProcess());

            if($this->getServiceNoteHandler()->isEnabled()) {
                /** @var $observer App_Finance_Ticket_Component_Observer_Type_ServiceNote_Observer */
                $observer = $ticket->getObserversHandler()->get('ServiceNote');
                $observer->setUploadServiceNote($params->getServiceNoteFileParamName());
            }

            if($this->getExplanatoryNoteHandler()->isEnabled()) {
                /** @var $observer App_Finance_Ticket_Component_Observer_Type_ExplanatoryNote_Observer */
                $observer = $ticket->getObserversHandler()->get('ExplanatoryNote');
                $observer->setUploadExplanationNote($params->getExplanatoryNoteFileParam());
            }

            $ticket->save();
        }
    }

    /**
     * Конвертация маршрута/формы в массив
     * @return array
     */
    public function toArray()
    {
        /** @var $ticketTypeDbTable App_Db_TicketType */
        $ticketTypeDbTable = App_Db::get(DB_TICKET_TYPE);
        $ticketTypeId = $this->getTicketTypeId();

        return array(
            'ticketType' => array(
                'id' => (int) $ticketTypeId,
                'name' => $ticketTypeDbTable->getTicketTypeNameById($ticketTypeId),
                'description' => $ticketTypeDbTable->getTicketTypeDescription($ticketTypeId)
            ),
            'claimId' => $this->getClaimId(),
            'dateClientDeliveryFactEnabled' => (int) $this->getDateClientDeliveryFactEnabled(),
            'status' => $this->getStateHandler()->getStatus(),
            'uniqueId' => $this->getUniqueId(),
            'params' => $this->getParamsHandler()->toArray(),
            'message' => $this->getMessageHandler()->getMessage(),
            'messageLevel' => $this->getMessageHandler()->getMessageLevel(),
            'annotation' => $this->getMessageHandler()->getAnnotation(),
            'serviceNote' => $this->getServiceNoteHandler()->toArray(),
            'explanatoryNote' => $this->getExplanatoryNoteHandler()->toArray()
        );
    }

    /**
     * Возврашает уникальный Id маршрута
     * @return string
     */
    public function getUniqueId()
    {
        return $this->_uniqueId;
    }

    /**
     * @param int $ticket_type_id
     */
    public function setTicketTypeId($ticket_type_id)
    {
        $this->_ticketTypeId = $ticket_type_id;
    }

    /**
     * @return int
     */
    public function getTicketTypeId()
    {
        return $this->_ticketTypeId;
    }

    /**
     * @return \App_Finance_Sender_Form_Route_Element_Message
     */
    public function getMessageHandler()
    {
        return $this->_messageHandler;
    }

    /**
     * @return \App_Finance_Sender_Form_Route_Element_Params
     */
    public function getParamsHandler()
    {
        return $this->_paramsHandler;
    }

    /**
     * @return \App_Finance_Sender_Form_Route_Element_Attachment_ServiceNote
     */
    public function getServiceNoteHandler()
    {
        return $this->_serviceNoteHandler;
    }

    /**
     * @return App_Finance_Sender_Form_Route_Element_Attachment_ExplanatoryNote
     */
    public function getExplanatoryNoteHandler()
    {
        return $this->_explanatoryNoteHandler;
    }

    /**
     * @return \App_Finance_Sender_Form_Route_Element_State
     */
    public function getStateHandler()
    {
        return $this->_stateHandler;
    }

    /**
     * Возвращает Id заявки, к которой будут прикрепляться тикеты
     * @return int
     */
    public function getClaimId()
    {
        return $this->_claimId;
    }

    /**
     * Установить Id заявки, к которой будут прикрепляться тикеты
     * @param int $claimId
     * @throws App_Spl_Exception_TypeException
     */
    public function setClaimId($claimId)
    {
        if(!(is_numeric($claimId) && $claimId > 0)) {
            throw new TypeException("claimId", "int", $claimId);
        }

        $this->_claimId = (int) $claimId;
    }

    /**
     * Возвращает тип заявки
     */
    public function getClaimType()
    {
        $claimData = App_Claim_Factory::getInstance()->getClaimData($this->getClaimId());

        return App_Claim_Factory::getInstance()->getClaimTypeIdToTranslatedString($claimData->typeId);
    }

    /**
     * Включить/отключить возможность выбора даты "По факту приезда клиента"
     * @param boolean $dateClientDeliveryFactEnabled
     */
    public function setDateClientDeliveryFactEnabled($dateClientDeliveryFactEnabled)
    {
        $this->_dateClientDeliveryFactEnabled = $dateClientDeliveryFactEnabled;
    }

    /**
     * Возвращает true, если включена возможность выбора даты "По факту приезда клиента"
     * @return boolean
     */
    public function getDateClientDeliveryFactEnabled()
    {
        return $this->_dateClientDeliveryFactEnabled;
    }

    /**
     * Заполняет форму/маршрут данными, исходя из существующех для указанной заявки тикетов
     * @throws Exception
     */
    public function createFromClaimId()
    {
        throw new \Exception("Method not available");
    }
}