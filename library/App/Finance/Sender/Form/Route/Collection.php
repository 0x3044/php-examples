<?php
use App_Finance_Sender_Interface_CheckInterface as CheckInterface;
use App_Finance_Sender_Interface_SendInterface as SendInterface;
use App_Spl_Collection_Abstract as SplCollection;
use App_Finance_Sender_Form_Route_Element as Route;
use App_Spl_Exception_TypeException as TypeException;
use App_Finance_Recipient_RecipientInterface as Recipient;

class App_Finance_Sender_Form_Route_Collection extends SplCollection implements CheckInterface, SendInterface
{
    /**
     * Id заявки, к которой прикрепляются тикеты
     * @var int
     */
    protected $_claimId;

    /**
     * Получатель, с которым ассоциируется данный список маршрутов
     * @var Recipient
     */
    protected $_recipient;

    /**
     * При создании коллекции в обязательном порядке устанавливается Id заявки
     * @param int $claimId
     * @param App_Finance_Recipient_RecipientInterface $recipient
     */
    public function __construct($claimId, Recipient $recipient)
    {
        $this->setRecipient($recipient);
        $this->setClaimId($claimId);
    }

    /**
     * Добавляет маршрут в коллекцию
     * @param Route $item
     * @return Route
     */
    public function add($item)
    {
        $item->setClaimId($this->getClaimId());

        return parent::add($item);
    }

    /**
     * Returns true if item is allowed for this collection
     * @param $item
     * @return bool
     */
    public function isItemAllowed($item)
    {
        return $item instanceof Route;
    }

    /**
     * Отмечает маршрут к отправке
     */
    public function check()
    {
        /** @var $route Route */
        if($this->size()) {
            foreach($this->getItems() as $route) {
                $route->getParamsHandler()->check();
            }
        }
    }

    /**
     * Снимает маршрут с отправки
     */
    public function unCheck()
    {
        /** @var $route Route */
        if($this->size()) {
            foreach($this->getItems() as $route) {
                $route->getParamsHandler()->unCheck();
            }
        }
    }

    /**
     * Возвращает true, если маршрут отмечен к отправке
     * @return bool
     */
    public function isChecked()
    {
        if($this->size()) {
            /** @var $route Route */
            foreach($this->getItems() as $route) {
                if($route->getParamsHandler()->isChecked()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Возвращает Id заявки, к которой будут прикрепляться тикеты
     * @return int
     */
    public function getClaimId()
    {
        return $this->_claimId;
    }

    /**
     * Установить Id заявки, к которой будут прикрепляться тикеты
     * @param int $claimId
     * @throws App_Spl_Exception_TypeException
     */
    public function setClaimId($claimId)
    {
        if(!(is_numeric($claimId) && $claimId > 0)) {
            throw new TypeException("claimId", "int", $claimId);
        }

        $this->_claimId = $claimId;

        if($this->size()) {
            /** @var $item Route */
            foreach($this->getItems() as $item) {
                $item->setClaimId($claimId);
            }
        }
    }

    /**
     * @param \App_Finance_Recipient_RecipientInterface $recipient
     */
    public function setRecipient($recipient)
    {
        $this->_recipient = $recipient;
    }

    /**
     * @return \App_Finance_Recipient_RecipientInterface
     */
    public function getRecipient()
    {
        return $this->_recipient;
    }

    /**
     * Заполняет форму/маршрут данными
     * @param array $data
     * @throws Exception
     */
    public function createFromPost(array &$data)
    {
        if(!(array_key_exists("routes", $data) && is_array($data["routes"]))) {
            throw new \Exception("No routes available");
        }

        foreach($data["routes"] as $routeData) {
            if(!(isset($routeData['ticketTypeId']))) {
                throw new \InvalidArgumentException("Required param `ticketTypeId` not found");
            }

            foreach(array('isChecked', 'ticketTypeId', 'dateToProcess') as $requiredParams) {
                if(!isset($routeData[$requiredParams])) {
                    throw new \InvalidArgumentException("Required param `{$requiredParams}` not found");
                }
            }

            $ticketTypeId = $routeData['ticketTypeId'];
            $route = $this->getRouteByTicketTypeId($ticketTypeId);

            if($route->getStateHandler()->isActive() || $route->getStateHandler()->isRequired()) {
                $route->createFromPost($routeData);
            }
        }
    }

    /**
     * Заполняет форму/маршрут данными, исходя из существующех для указанной заявки тикетов
     */
    public function createFromClaimId()
    {
        if($this->size()) {
            /** @var $ticketDbTable App_Db_Ticket */
            $ticketDbTable = App_Db::get(DB_TICKET);
            $tickets = $ticketDbTable->getTicketsByClaimId($this->getClaimId());

            /** @var $ticketRow App_Db_Row_Ticket */
            foreach($tickets as $ticketRow) {
                try {
                    $route = $this->getRouteByTicketTypeId($ticketRow->getTicketTypeId());
                }
                catch(\Exception $e) {
                    $route = false;
                }

                if($route) {
                    /** @var $params App_Finance_Sender_Form_Route_Element_Params */
                    $params = $route->getParamsHandler();
                    $params->check();
                    $params->setDateToProcess(new Zend_Date($ticketRow->getDateToProcess(), App_Db::ZEND_DATETIME_FORMAT));
                }
            }
        }
    }

    /**
     * Возврашает маршрут с указанным типом тикета
     * @param $ticketTypeId
     * @return App_Finance_Sender_Form_Route_Element
     * @throws Exception
     */
    public function getRouteByTicketTypeId($ticketTypeId)
    {
        /** @var $route Route */
        foreach($this->getItems() as $route) {
            if($route->getTicketTypeId() === $ticketTypeId) {
                return $route;
            }
        }

        throw new \Exception("Route with ticketTypeId:{$ticketTypeId} not found");
    }

    /**
     * Отправляет отмеченные маршруты
     */
    public function send()
    {
        if($this->size()) {
            /** @var $route Route */
            foreach($this->getItems() as $route) {
                $route->send();
            }
        }
    }

    /**
     * Возвращает коллекцию маршрутов в виде массива
     * @return array
     */
    public function toArray()
    {
        $result = array();

        /** @var $routes Route */
        foreach($this->getItems() as $routes) {
            $result[] = $routes->toArray();
        }

        return array(
            "claimId" => $this->getClaimId(),
            "recipient" => $this->getRecipient()->toArray(),
            "routes" => $result
        );
    }
}