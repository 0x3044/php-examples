<?php
use App_Finance_Sender_Interface_ParamsInterface as ParamsInterface;

class App_Finance_Sender_Form_Route_Element_Params implements ParamsInterface
{
    /**
     * Флаг "марщрут отмечен к отправке"
     * @var bool
     */
    protected $_isChecked;

    /**
     * Желаемая дата обработки запроса
     * @var mixed
     */
    protected $_dateToProcess;

    /**
     * Имя input[file] со служебной запиской
     * @var string
     */
    protected $_serviceNoteFileParamName;

    /**
     * Имя input[file] с пояснительной запиской
     * @var string
     */
    protected $_explanatoryNoteFileParamName;

    /**
     * {@inheritdoc}
     */
    public function check()
    {
        $this->_isChecked = true;
    }

    /**
     * {@inheritdoc}
     */
    public function unCheck()
    {
        $this->_isChecked = false;
    }

    /**
     * Возвращает true, если маршрут отмечен к отправке
     * @return bool
     */
    public function isChecked()
    {
        return (bool) $this->_isChecked;
    }

    /**
     * Установить желаемую дату обработки запроса
     * @param mixed $dateToProcess
     */
    public function setDateToProcess($dateToProcess)
    {
        $this->_dateToProcess = $dateToProcess;
    }

    /**
     * Возвращает желаему дату обработки запроса
     * @return \Zend_Date
     */
    public function getDateToProcess()
    {
        return $this->_dateToProcess;
    }

    /**
     * Установить название input[file] с файлом служебной записки
     * @param string $serviceNoteFileParamName
     */
    public function setServiceNoteFileParamName($serviceNoteFileParamName)
    {
        $this->_serviceNoteFileParamName = $serviceNoteFileParamName;
    }

    /**
     * Возврашает название input[file] с файлом служебной записки
     * @return string
     */
    public function getServiceNoteFileParamName()
    {
        return $this->_serviceNoteFileParamName;
    }

    /**
     * Установить название input[file] с файлом пояснительной записки
     * @param $explanatoryNoteFileParamName
     */
    public function setExplanatoryNoteFileParamName($explanatoryNoteFileParamName)
    {
        $this->_explanatoryNoteFileParamName = $explanatoryNoteFileParamName;
    }

    /**
     * Возвращает название input[file] с файлом пояснительной записки
     * @return string
     */
    public function getExplanatoryNoteFileParam()
    {
        return $this->_explanatoryNoteFileParamName;
    }

    /**
     * Конвертирует в массив
     * @return array
     */
    public function toArray()
    {
        if($this->getDateToProcess() instanceof Zend_Date) {
            $dateToProcess = $this->getDateToProcess()->toString();
            $dateToProcessDetails = $this->getDateToProcess()->toArray();
        } else {
            $dateToProcess = NULL;
            $dateToProcessDetails = NULL;
        }

        return array(
            'isChecked' => $this->isChecked(),
            'dateToProcess' => $dateToProcess,
            'dateToProcessDetails' => $dateToProcessDetails,
            'serviceNoteFileParamName' => $this->getServiceNoteFileParamName(),
            'explanatoryNoteFileParamName' => $this->getExplanatoryNoteFileParam()
        );
    }
}