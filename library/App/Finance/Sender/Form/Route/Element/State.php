<?php
class App_Finance_Sender_Form_Route_Element_State
{
    const STATUS_ACTIVE = 'active';
    const STATUS_REQUIRED = 'required';
    const STATUS_DISABLED = 'disabled';
    const STATUS_SENT = 'sent';
    const STATUS_COMPLETED = 'completed';
    const STATUS_HIDDEN = 'hidden';

    /**
     * Статус
     * @var string
     */
    protected $_status;

    /**
     * Имеется принципиальная возможность отправить запрос по данному маршруту
     * @var bool
     */
    protected $_isAvailable = true;

    /**
     * Отметить маршрут как возможный к отправке
     * @param boolean $isAvailable
     */
    public function setIsAvailable($isAvailable)
    {
        $this->_isAvailable = $isAvailable;
    }

    /**
     * Возвращает ма
     * @return boolean
     */
    public function getIsAvailable()
    {
        return $this->_isAvailable;
    }

    /**
     * Устанавливает статус (из строкового значения)
     * @param string $status
     * @throws Exception
     */
    public function setStatus($status)
    {
        if(!(in_array($status, array(self::STATUS_ACTIVE, self::STATUS_REQUIRED, self::STATUS_SENT, self::STATUS_DISABLED, self::STATUS_COMPLETED)))) {
            throw new \Exception("Unknown status `{$status}`");
        }
        $this->_status = $status;
    }

    /**
     * Возвращает строковое значение статуса
     * @return string
     */
    public function getStatus()
    {
        return $this->_status;
    }

    /**
     * Возврашает true, если маршрут активен
     * @return mixed
     */
    public function isActive()
    {
        return $this->_status == self::STATUS_ACTIVE;
    }

    /**
     * Возвращает true, если маршрут отмечен обязательным к отправке
     * @return mixed
     */
    public function isRequired()
    {
        return $this->_status == self::STATUS_REQUIRED;
    }

    /**
     * Возврашает true, если маршрут отмечен уже отправленным
     * @return mixed
     */
    public function isSent()
    {
        return $this->_status == self::STATUS_SENT;
    }

    /**
     * Возврашает true, если маршрут заблокирован
     * @return mixed
     */
    public function isDisabled()
    {
        return $this->_status == self::STATUS_DISABLED;
    }

    /**
     * Возвращает true, если маршрут отмечен как скрытый
     */
    public function isHidden()
    {
        return $this->_status == self::STATUS_HIDDEN;
    }

    /**
     * Возвращает true, если по данному маршруту уже отправлен и обработан запрос, и новые запросы в данное
     * направление больше создавать нелья
     * @return bool
     */
    public function isCompleted()
    {
        return $this->_status == self::STATUS_COMPLETED;
    }

    /**
     * Отметить маршрут активным
     */
    public function setActive()
    {
        $this->_status = self::STATUS_ACTIVE;
    }

    /**
     * Отметить маршрут как уже отправленный
     */
    public function setSent()
    {
        $this->_status = self::STATUS_SENT;
    }

    /**
     * Отметить маршрут обязательным к отправке
     */
    public function setRequired()
    {
        $this->_status = self::STATUS_REQUIRED;
    }

    /**
     * Заблокировать марщрут для отправки
     */
    public function setDisabled()
    {
        $this->_status = self::STATUS_DISABLED;
    }

    /**
     * Отметить маршрут как выполненный
     */
    public function setCompleted()
    {
        $this->_status = self::STATUS_COMPLETED;
    }

    /**
     * Отметить марщрут как скрытый
     */
    public function setHidden()
    {
        $this->_status = self::STATUS_HIDDEN;
    }
}