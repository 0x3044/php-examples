<?php
class App_Finance_Sender_Form_Route_Element_Message
{
    const MESSAGE_INFO = 'info';
    const MESSAGE_ERROR = 'error';

    /**
     * Тип сообщения
     * @var string
     */
    protected $_messageLevel = self::MESSAGE_ERROR;

    /**
     * Сообщение
     * @var string
     */
    protected $_message;

    /**
     * Аннотация (для названия марщрута)
     * @var string
     */
    protected $_annotation;

    /**
     * Сообщение об ошибке
     * @param string $message
     */
    public function errorMessage($message = null) {
        $this->_message = $message;
        $this->_messageLevel = self::MESSAGE_ERROR;
    }

    /**
     * Информационное сообщегие
     * @param string $message
     */
    public function infoMessage($message = null) {
        $this->_message = $message;
        $this->_messageLevel = self::MESSAGE_INFO;
    }

    /**
     * Возвращает сообщение
     * @return string
     */
    public function getMessage()
    {
        return $this->_message;
    }

    /**
     * Возвращает тип сообщения
     * @return int
     */
    public function getMessageLevel()
    {
        return $this->_messageLevel;
    }

    /**
     * Задать аннотацию (для заголовка)
     * @param string $annotation
     */
    public function setAnnotation($annotation)
    {
        $this->_annotation = $annotation;
    }

    /**
     * Возвращает аннотацию (для заголовка)
     * @return string
     */
    public function getAnnotation()
    {
        return $this->_annotation;
    }
}