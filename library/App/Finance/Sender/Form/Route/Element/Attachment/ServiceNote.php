<?php
use App_Finance_Ticket_Factory as TicketFactory;
use App_Finance_Ticket_Component_Attachment_ServiceNote_Document as TicketServiceNote;
use App_Finance_Sender_Form_Route_Element__Attachment_AbstractAttachment as AbstractAttachment;

class App_Finance_Sender_Form_Route_Element_Attachment_ServiceNote extends AbstractAttachment
{
    /**
     * {@inheritdoc}
     * @return \App_Finance_Ticket_Component_Attachment_AbstractAttachment
     */
    protected function _getAttachment()
    {
        return new TicketServiceNote(TicketFactory::getInstance()->createFromTicketId($this->getTicketId()));
    }
}