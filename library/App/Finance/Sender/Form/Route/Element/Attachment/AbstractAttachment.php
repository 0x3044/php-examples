<?php
use App_Finance_Ticket_Component_Attachment_AbstractAttachment as Attachment;

abstract class App_Finance_Sender_Form_Route_Element__Attachment_AbstractAttachment
{
    /**
     * Аттачмент
     * @var Attachment
     */
    protected $_attachment;

    /**
     * Флаг "запрашивать аттачмент?"
     * @var bool
     */
    protected $_attachmentEnabled = false;

    /**
     * ID запроса с аттачментом
     * @var null|int
     */
    protected $_ticketId = NULL;

    /**
     * Показать форму прикрепления файла
     */
    public function enable()
    {
        $this->_attachmentEnabled = true;
    }

    /**
     * Скрыть форму прикрепления файла
     */
    public function disable()
    {
        $this->_attachmentEnabled = false;
    }

    /**
     * Возвращает true, если форма прикрепления файла будет показываться пользователю
     * @return bool
     */
    public function isEnabled()
    {
        return $this->_attachmentEnabled;
    }

    /**
     * Возвращает ID запроса со служебной запиской
     * @return int|null
     */
    public function getTicketId()
    {
        return $this->_ticketId;
    }

    /**
     * Указать ID запроса с аттачментом
     * @param $ticketId
     */
    public function setTicketId($ticketId)
    {
        $this->_ticketId = $ticketId;
    }

    /**
     * Возвращает информацию об аттачменте
     * @return array
     */
    public function toArray()
    {
        if($this->isEnabled()) {
            if($this->getTicketId()) {
                $attachment = $this->_getAttachment();

                if($attachment->isUploaded()) {
                    $attachment = array(
                        'enabled' => true,
                        'file' => $attachment->getFileName(),
                        'url' => $attachment->getLink()
                    );

                    return $attachment;
                } else {
                    $attachment = array(
                        'enabled' => true,
                        'file' => NULL,
                        'url' => NULL
                    );

                    return $attachment;
                }
            } else {
                $attachment = array(
                    'enabled' => true,
                    'file' => NULL,
                    'url' => NULL
                );

                return $attachment;
            }
        } else {
            $attachment = array(
                'enabled' => false,
                'file' => NULL,
                'url' => NULL
            );

            return $attachment;
        }
    }

    /**
     * Возвращает хелпер по работе с аттачментом
     * @return Attachment
     */
    abstract protected function _getAttachment();
}