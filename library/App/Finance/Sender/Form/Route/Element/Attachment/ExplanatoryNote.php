<?php
use App_Finance_Ticket_Factory as TicketFactory;
use App_Finance_Ticket_Component_Attachment_ExplanatoryNote_Document as TicketExplanationNote;
use App_Finance_Sender_Form_Route_Element__Attachment_AbstractAttachment as AbstractAttachment;

class App_Finance_Sender_Form_Route_Element_Attachment_ExplanatoryNote extends AbstractAttachment
{
    /**
     * {@inheritdoc}
     * @return \App_Finance_Ticket_Component_Attachment_AbstractAttachment
     */
    protected function _getAttachment()
    {
        return new TicketExplanationNote(TicketFactory::getInstance()->createFromTicketId($this->getTicketId()));
    }
}