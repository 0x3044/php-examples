<?php
use App_Finance_Sender_Interface_SendInterface as SendInterface;
use App_Spl_Collection_Abstract as SplCollection;
use App_Finance_Recipient_RecipientInterface as Recipient;
use App_Finance_Recipient_Factory as RecipientFactory;
use App_Finance_Sender_Form_Route_Collection as Routes;
use App_Spl_Exception_TypeException as TypeException;

class App_Finance_Sender_Form_Routes extends SplCollection implements SendInterface
{
    /**
     * Id заявки
     * @var int
     */
    protected $_claimId;

    /**
     * При создании коллекции в обязательном порядке устанавливается Id заявки
     * @param int $claimId
     */
    public function __construct($claimId)
    {
        $this->setClaimId($claimId);
    }

    /**
     * Добавить маршрут/список маршрутов
     * @param SendInterface $item
     * @return SendInterface
     */
    public function add($item)
    {
        $item->setClaimId($this->getClaimId());

        return parent::add($item);
    }

    /**
     * Returns true if item is allowed for this collection
     * @param $item
     * @return bool
     */
    public function isItemAllowed($item)
    {
        return $item instanceof SendInterface;
    }

    /**
     * Возвращает Id заявки, к которой будут прикрепляться тикеты
     * @return int
     */
    public function getClaimId()
    {
        return $this->_claimId;
    }

    /**
     * Установить Id заявки, к которой будут прикрепляться тикеты
     * @param int $claimId
     * @throws App_Spl_Exception_TypeException
     */
    public function setClaimId($claimId)
    {
        if(!(is_numeric($claimId) && $claimId > 0)) {
            throw new TypeException("claimId", "int", $claimId);
        }

        $this->_claimId = $claimId;

        if($this->size()) {
            /** @var $item SendInterface */
            foreach($this->getItems() as $item) {
                $item->setClaimId($claimId);
            }
        }
    }

    /**
     * Заполняет форму/маршрут данными
     * @param array $data
     * @throws Exception
     */
    public function createFromPost(array &$data)
    {
        if(!(array_key_exists("routes", $data) && is_array($data["routes"]))) {
            throw new \Exception("No routes available");
        }

        if(isset($data['claimId'])) {
            $this->setClaimId($data['claimId']);
        }

        foreach($data["routes"] as $recipientName => $routesData) {
            $this->getRoutesByRecipient(RecipientFactory::getInstance()->createFromRecipientName($recipientName))->createFromPost($routesData);
        }
    }

    /**
     * Заполняет форму/маршрут данными, исходя из существующех для указанной заявки тикетов
     */
    public function createFromClaimId()
    {
        if($this->size()) {
            /** @var $routes Routes */
            foreach($this->getItems() as $routes) {
                $routes->createFromClaimId();
            }
        }
    }

    /**
     * Отправляет отмеченные маршруты
     */
    public function send()
    {
        if($this->size()) {
            /** @var $route SendInterface */
            foreach($this->getItems() as $route) {
                $route->send();
            }
        }
    }

    /**
     * Конвертирует списко маршрутов в массив
     * @return array
     */
    public function toArray()
    {
        $result = array();

        /** @var $routes Routes */
        foreach($this->getItems() as $routes) {
            $result[] = $routes->toArray();
        }

        return array(
            "claimId" => $this->getClaimId(),
            "routes" => $result
        );
    }

    /**
     * Возвращает список маршрутов по их получателю
     * @param App_Finance_Recipient_RecipientInterface $recipient
     * @return App_Finance_Sender_Form_Route_Collection
     * @throws OutOfBoundsException
     */
    public function getRoutesByRecipient(Recipient $recipient)
    {
        /** @var $routes Routes */
        foreach($this->getItems() as $routes) {
            if($routes->getRecipient() == $recipient) {
                return $routes;
            }
        }

        throw new \OutOfBoundsException("Recipient `{$recipient->getName()}` not found");
    }

    /**
     * Возвращает количество доступных к отправке маршрутов
     * @return int
     */
    public function getNumAvailableRoutes()
    {
        $num = 0;

        /** @var $routes Routes */
        foreach($this->getItems() as $routes) {
            /** @var $route App_Finance_Sender_Form_Route_Element */
            foreach($routes->getItems() as $route) {
                if($route->getStateHandler()->isActive() || $route->getStateHandler()->isRequired()) {
                    $num++;
                }
            }
        }

        return $num;
    }

    /**
     * Возвращает максимальное количество маршрутов к отправке
     * @return int
     */
    public function getNumMaxAvailableRoutes()
    {
        $num = 0;

        /** @var $routes Routes */
        foreach($this->getItems() as $routes) {
            /** @var $route App_Finance_Sender_Form_Route_Element */
            foreach($routes->getItems() as $route) {
                if($route->getStateHandler()->getIsAvailable()) {
                    $num++;
                }
            }
        }

        return $num;
    }

    /**
     * Возвращает количество маршрутов, запросы к которым уже отправлены
     * @return int
     */
    public function getNumSentRoutes()
    {
        $num = 0;

        /** @var $routes Routes */
        foreach($this->getItems() as $routes) {
            /** @var $route App_Finance_Sender_Form_Route_Element */
            foreach($routes->getItems() as $route) {
                if($route->getStateHandler()->isSent() || $route->getStateHandler()->isCompleted()) {
                    $num++;
                }
            }
        }

        return $num;
    }
}