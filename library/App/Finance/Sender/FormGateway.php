<?php
use Finance_Model_Sender_Form as SenderForm;
use App_Finance_Ticket_Component_Validation_Type_ClientId_Exception as ClientNotFoundException;

class App_Finance_Sender_FormGateway
{
    /**
     * Формы отправки запросов
     * Кэшируются
     * @var SenderForm[]
     */
    protected $_senderForms = array();

    /**
     * Возвращает форму отправки запросов
     * @param int $claimId
     * @return Finance_Model_Sender_Form
     */
    public function getSenderForm($claimId)
    {
        App_Spl_TypeCheck::getInstance()->positiveNumeric($claimId);

        if(!(isset($this->_senderForms[$claimId]))) {
            $this->_senderForms[$claimId] = new SenderForm($claimId);
            $this->_senderForms[$claimId]->createFromClaimId();
        }

        return $this->_senderForms[$claimId];
    }

    /**
     * Возвращает список маршрутов в json-формате
     */
    public function getRoutes($claimId)
    {
        try {
            /** @var $claimDbTable App_Db_Claims */
            $claimDbTable = App_Db::get(DB_CLAIMS);
            $claim = $claimDbTable->getById($claimId);

            if(!($claim && $claim->client_id > 0)) {
                throw new ClientNotFoundException("Запрещено создавать запросы в заявках, в которых не указан клиент", 9);
            }

            $numAvailableRoutes = 0;
            $senderForm = $this->getSenderForm($claimId);

            /** @var $routes App_Finance_Sender_Form_Route_Collection */
            foreach($senderForm->getRoutes() as $routes) {
                /** @var $route App_Finance_Sender_Form_Route_Element */
                foreach($routes->getItems() as $route) {
                    if($route->getStateHandler()->isActive() || $route->getStateHandler()->isRequired()) {
                        $numAvailableRoutes++;
                    }
                }
            }

            $jsonData = array(
                "success" => true,
                "routes" => $senderForm->toArray(),
                "requireShow" => $senderForm->isEnabledRequireShow(),
                "available" => $numAvailableRoutes
            );
        }
        catch(\Exception $e) {
            $jsonData = array(
                'success' => false,
                'error' => $e->getMessage(),
                'type' => get_class($e),
                'trace' => $e->getTrace()
            );
        }

        return $jsonData;
    }

    /**
     * Отправка запросов в финансовый саппорт
     * @param $claimId
     * @param array $formData
     * @return array
     */
    public function sendRoutes($claimId, array $formData)
    {
        $jsonData = array();

        try {
            $senderForm = $this->getSenderForm($claimId);
            $senderForm->createFromPost($formData);
            $senderForm->send();

            $jsonData['success'] = true;
        }
        catch(\Exception $e) {
            $jsonData = array(
                'success' => false,
                'error' => $e->getMessage(),
                'type' => get_class($e)
            );
        }

        return $jsonData;
    }
}