<?php
use App_Finance_Sender_Interface_SendInterface as SendInterface;
use App_Finance_Sender_Form_Routes as Routes;

abstract class App_Finance_Sender_Form implements SendInterface
{
    /**
     * Список маршрутов
     * @var Routes
     */
    protected $_routes;

    /**
     * Id заявки
     * @var int
     */
    protected $_claimId;

    /**
     * Принудительный показ формы
     * @var bool
     */
    protected $_requireShow = false;

    /**
     * Создает форму, указание Id заявки обязательно
     * @param $claimId
     */
    public function __construct($claimId)
    {
        $this->setClaimId($claimId);
    }

    /**
     * Конфигурация списка маршрутов
     * @param App_Finance_Sender_Form_Routes $routes
     */
    abstract protected function _setUpRoutes(Routes $routes);

    /**
     * Возвращает URL для получения данных о марщрутах в json-формате
     * @return string
     */
    abstract public function getDataUrl();

    /**
     * Возвращает URL для отправки тикетов по отмеченным маршрутам
     * @return string
     */
    abstract public function getPostUrl();

    /**
     * Возвращает список маршрутов
     * @return \App_Finance_Sender_Form_Routes
     */
    public function getRoutes()
    {
        return $this->_routes;
    }

    /**
     * Возвращает Id заявки, к которой будут прикрепляться тикеты
     * @return int
     */
    public function getClaimId()
    {
        return $this->_claimId;
    }

    /**
     * Установить Id заявки, к которой будут прикрепляться тикеты
     * @param int $claimId
     */
    public function setClaimId($claimId)
    {
        $this->_claimId = $claimId;

        if($claimId) {
            $this->_routes = new Routes((int) $claimId);
            $this->_setUpRoutes($this->getRoutes());

            $this->getRoutes()->setClaimId($claimId);
        }
    }

    /**
     * Возвращает true, если форма имеет ID заявки
     * @return bool
     */
    public function hasClaimId()
    {
        return $this->getClaimId() > 0;
    }

    /**
     * Заполняет форму/маршрут данными
     * @param array $data
     * @throws Exception
     */
    public function createFromPost(array &$data)
    {
        if(!($this->hasClaimId())) {
            throw new \Exception("Отсутствует ID заявки");
        }

        if(isset($data['claimId'])) {
            $this->setClaimId($data['claimId']);
        }

        $this->getRoutes()->createFromPost($data);
    }

    /**
     * Отправляет отмеченные маршруты
     */
    public function send()
    {
        if(!($this->hasClaimId())) {
            throw new \Exception("Отсутствует ID заявки");
        }

        $this->getRoutes()->send();
    }

    /**
     * Включить флаг "принудительно показывать форму"
     */
    public function enableRequireShow()
    {
        $this->_requireShow = true;
    }

    /**
     * Отключить флаг "принудительно показывать форму"
     */
    public function disableRequireShow()
    {
        $this->_requireShow = false;
    }

    /**
     * Возвращает true, если установлен флаг "принудительно показывать форму"
     * @return bool
     */
    public function isEnabledRequireShow()
    {
        return $this->_requireShow;
    }

    /**
     * Конвертация маршрута/формы в массив
     * @return array
     */
    public function toArray()
    {
        return array_merge(array(
            'claimId' => $this->getClaimId(),
            'requireShow' => $this->isEnabledRequireShow()
        ), $this->getRoutes()->toArray());
    }
}