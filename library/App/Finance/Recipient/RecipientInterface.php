<?php
use App_Finance_Recipient_NewTicketsWatcher as SessionHandler;

interface App_Finance_Recipient_RecipientInterface
{
    /**
     * Возвращает название данного почтового ящика
     * @return string
     */
    public function getName();

    /**
     * Возвращает русифицированное название данного почтового ящика
     * @return string
     */
    public function getDescription();

    /**
     * Возвращает названия типов тикетов, которые данный получатель обслуживает
     * @return string[]
     */
    public function getTicketTypes();

    /**
     * Возвращает массив Id типов тикетов, который данный получатель обслуживает
     * @return array
     */
    public function getTicketTypeIds();

    /**
     * Возвращает сессию
     * @return SessionHandler
     */
    public function getSessionHandler();

    /**
     * Возвращает информацию о получателе в виде массива
     * @return array
     */
    public function toArray();

    /**
     * Возвращает TRUE в случаи, если текущий пользователь имеет права на доступ в ящик данного получателя.
     * @return bool
     */
    public function hasAuthorization();

    /**
     * Возвращает относительный URL главной страницы получателя
     * @return string
     */
    public function getUrl();

    /**
     * Возвращает TRUE в случаи, если в ящике получателя есть новые, необработанные тикеты
     * @return bool
     */
    public function hasNewTickets();

    /**
     * Возвращает количество необработанных тикетов
     * @return int
     */
    public function numNewTickets();

    /**
     * Возвращает id новых записей
     * @return int[]
     */
    public function getNewTicketIds();
}