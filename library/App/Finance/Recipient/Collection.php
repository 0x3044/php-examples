<?php
use App_Spl_Collection_Abstract as SplCollection;
use App_Finance_Recipient_Factory as RecipientFactory;
use App_Finance_Recipient_RecipientInterface as Recipient;

class App_Finance_Recipient_Collection extends SplCollection
{
    /**
     * Returnss true if item is allowed for this collection
     * @param $item
     * @return bool
     */
    function isItemAllowed($item)
    {
        return $item instanceof Recipient;
    }

    /**
     * Добавляет получателя по его названию
     * @param string $recipientName
     */
    public function addRecipientByName($recipientName)
    {
        $this->add(RecipientFactory::getInstance()->createFromRecipientName($recipientName));
    }

    /**
     * Возвращает получателя по его названию
     * @param string $recipientName
     * @return App_Finance_Recipient_RecipientInterface
     * @throws Exception
     */
    public function getRecipientByName($recipientName)
    {
        /** @var $nRecipient Recipient */
        foreach($this->_items as $key => $nRecipient) {
            if($nRecipient->getName() == $recipientName) {
                return $nRecipient;
            }
        }

        throw new \Exception("Recipient \"{$recipientName}\" not found");
    }
}