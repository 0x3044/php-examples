<?php
/**
 * Список ТИП_ПОЛУЧАТЕЛЯ(строка) => КЛАСС_ПОЛУЧАТЕЛЯ для фабрики App_Finance_Recipient_Factory
 */
return array(
    'accounting' => 'App_Finance_Recipient_Type_Accounting_Recipient',
    'dispatcher' => 'App_Finance_Recipient_Type_Dispatcher_Recipient',
    'depot' => 'App_Finance_Recipient_Type_Depot_Recipient',
    'payment' => 'App_Finance_Recipient_Type_Payment_Recipient'
);