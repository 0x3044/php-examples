<?php
use App_Finance_Recipient_Type_Abstract_Recipient as AbstractRecipient;

class App_Finance_Recipient_Type_Accounting_Recipient extends AbstractRecipient
{
    /**
     * Возвращает название данного почтового ящика
     * @return string
     */
    public function getName()
    {
        return 'accounting';
    }

    /**
     * Возвращает русифицированное название данного почтового ящика
     * @return string
     */
    public function getDescription()
    {
        return 'Бухгалтерия';
    }

    /**
     * Возвращает названия типов тикетов, которые данный получатель обслуживает
     * @return string[]
     */
    public function getTicketTypes()
    {
        return array('accounting_waybill');
    }

    /**
     * Возвращает TRUE в случаи, если текущий пользователь имеет права на доступ в ящик данного получателя.
     * @return bool
     */
    public function hasAuthorization()
    {
        return App_Access::get('access', 'finance>accounting>access');
    }
}