<?php
use App_Finance_Recipient_Type_Abstract_Recipient as AbstractRecipient;

class App_Finance_Recipient_Type_Payment_Recipient extends AbstractRecipient
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'payment';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Запрос на оплату';
    }

    /**
     * {@inheritdoc}
     */
    public function getTicketTypes()
    {
        return array('payment_request');
    }

    /**
     * {@inheritdoc}
     */
    public function hasAuthorization()
    {
        return App_Access::get('access', 'finance>payment>access');
    }

    /**
     * Возвращает количество запросов c указанным статусом в саппорт
     * @param $status
     * @return int
     */
    public function getNumTickets($status)
    {
        $ticketTypes = implode(',', $this->getTicketTypeIds());
        App_Spl_TypeCheck::getInstance()->positiveNumeric($status);

        /** @var $dbTicket App_Db_Ticket */
        $aclPrefix = 'finance>payment>payment_request';
        $clientIds = false;

        if((App_Access::get('access', $aclPrefix.'>clients>all') == App_Access_Field_Type1::ACCESS_ALLOWED) ||
            (App_Access::get('access', $aclPrefix.'>clients>allusers') == App_Access_Field_Type1::ACCESS_ALLOWED)) {
            $sqlQuery = <<<SQL
              SELECT count(*) as num
              FROM ticket
              WHERE status = {$status} AND ticket_type_id IN ($ticketTypes)
SQL;
        }else{
            $clientIds = array();
            $clientAccess = App_Helper_Client::get(array(
                'key' => $aclPrefix
            ));

            foreach($clientAccess as $client) {
                $clientIds[] = $client->clientId;
            }

            $clientIds = implode(',', $clientIds);

            $sqlQuery = <<<SQL
              SELECT count(*) as num
              FROM ticket AS t
              LEFT JOIN ticket_payment AS tp ON tp.ticket_id = t.id
              WHERE client_id IN ({$clientIds}) AND status = {$status} AND ticket_type_id IN ($ticketTypes)
SQL;
        }

        $result = Zend_Db_Table_Abstract::getDefaultAdapter()->query($sqlQuery)->fetch(Zend_Db::FETCH_ASSOC);

        return $result['num'];
    }
}