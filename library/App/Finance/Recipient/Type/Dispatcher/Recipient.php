<?php
use App_Finance_Recipient_Type_Abstract_Recipient as AbstractRecipient;

class App_Finance_Recipient_Type_Dispatcher_Recipient extends AbstractRecipient
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'dispatcher';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Диспетчер';
    }

    /**
     * {@inheritdoc}
     */
    public function getTicketTypes()
    {
        return array('dispatcher_verify');
    }

    /**
     * {@inheritdoc}
     */
    public function hasAuthorization()
    {
        return App_Access::get('access', 'finance>dispatcher>access');
    }

    /**
     * Возвращает SQL-запрос подсчета новых записей в саппорте
     * @param $ticketTypeIds
     * @return string
     */
    protected function _numNewTickets_getSqlQuery($ticketTypeIds)
    {
        $dispatcherIds = $this->getAvailableDispatcherIds();

        if(!(count($dispatcherIds))) {
            $dispatcherIds = array(-1); // "Фейковый" запрос
        } else {
            $dispatcherIds = array_keys($dispatcherIds);
        }

        $status = App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_ACTIVE;
        $dispatcherIds = implode(',', $dispatcherIds);

        $sqlQuery = <<<SQL
            SELECT ticket.`id`
            FROM ticket
            LEFT JOIN claims ON claims.id=ticket.`claim_id`
            WHERE ticket.`status`={$status}
              AND ticket.`ticket_type_id` IN({$ticketTypeIds})
              AND claims.`dispetcher` IN({$dispatcherIds})
SQL;

        return $sqlQuery;
    }

    /**
     * Возвращает хэщ [id => name] диспетчеров, к запросам которых текущий пользователь имеет право просмотра/редактирования
     */
    public function getAvailableDispatcherIds()
    {
        $hasAccessToAllDispatchers = App_Access::get('access', 'finance>dispatcher>managers>all');
        $hasAccessToDispatcherIds = App_Access::get('managers', 'finance>dispatcher');

        if($hasAccessToAllDispatchers) {
            return App_Helper_Driver::getAllDispetchers();
        } else {
            $allDispatchers = App_Helper_Driver::getAllDispetchers();

            if(!(is_array($hasAccessToDispatcherIds) && isset($hasAccessToDispatcherIds['m']) && is_array($hasAccessToDispatcherIds['m']))) {
                return array();
            }

            $hasAccessToDispatcherIds = $hasAccessToDispatcherIds['m'];

            if(count($hasAccessToDispatcherIds)) {
                foreach(array_keys($allDispatchers) as $dispatcherId) {
                    if(!(in_array($dispatcherId, $hasAccessToDispatcherIds))) {
                        unset($allDispatchers[$dispatcherId]);
                    }
                }

                return $allDispatchers;
            } else {
                return array();
            }
        }
    }
}