<?php
use App_Finance_Recipient_Type_Abstract_Recipient as AbstractRecipient;

class App_Finance_Recipient_Type_Depot_Recipient extends AbstractRecipient
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'depot';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Начальник склада';
    }

    /**
     * {@inheritdoc}
     */
    public function getTicketTypes()
    {
        return array('depot_verify');
    }

    /**
     * {@inheritdoc}
     */
    public function hasAuthorization()
    {
        return App_Access::get('access', 'finance>depot>access');
    }
}