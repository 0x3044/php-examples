<?php
use App_Finance_Recipient_RecipientInterface as RecipientInterface;
use App_Finance_Recipient_NewTicketsWatcher as SessionHandler;

abstract class App_Finance_Recipient_Type_Abstract_Recipient implements RecipientInterface
{
    /**
     * @var SessionHandler
     */
    protected $_sessionHandler;

    /**
     * Возвращает название данного почтового ящика
     * @return string
     */
    abstract public function getName();

    /**
     * Возвращает русифицированное название данного почтового ящика
     * @return string
     */
    abstract public function getDescription();

    /**
     * Возвращает названия типов тикетов, которые данный получатель обслуживает
     * @return string[]
     */
    abstract public function getTicketTypes();

    /**
     * Возвращает сессию
     * @return SessionHandler
     */
    public final function getSessionHandler()
    {
        if(!($this->_sessionHandler instanceof SessionHandler)) {
            $this->_sessionHandler = new SessionHandler($this);
        }

        return $this->_sessionHandler;
    }

    /**
     * Возвращает массив Id типов тикетов, который данный получатель обслуживает
     * @return int[]
     */
    public final function getTicketTypeIds()
    {
        /** @var $ticketTypeDbTable App_Db_TicketType */
        $ticketTypeDbTable = App_Db::get(DB_TICKET_TYPE);
        $ticketTypes = $this->getTicketTypes();
        $ticketTypeIds = array();

        if(is_array($ticketTypes) && count($ticketTypes)) {
            foreach($ticketTypes as $ticketTypeName) {
                $ticketTypeIds[] = $ticketTypeDbTable->getTicketTypeIdByName($ticketTypeName);
            }
        }

        return $ticketTypeIds;
    }

    /**
     * Возвращает информацию о получателе в виде массива
     * @return array
     */
    public final function toArray()
    {
        return array(
            'name' => $this->getName(),
            'description' => $this->getDescription()
        );
    }

    /**
     * Возвращает TRUE в случаи, если текущий пользователь имеет права на доступ в ящик данного получателя.
     * @return bool
     */
    abstract public function hasAuthorization();

    /**
     * Возвращает TRUE в случаи, если в ящике получателя есть новые, необработанные тикеты
     * @return bool
     */
    public final function hasNewTickets()
    {
        return (bool) $this->numNewTickets();
    }

    /**
     * Возвращает количество необработанных тикетов
     * @throws Exception
     * @return int
     */
    public final function numNewTickets()
    {
        return count($this->getNewTicketIds());
    }

    /**
     * Возвращает id новых записей
     * @throws Exception
     * @return int[]
     */
    public function getNewTicketIds()
    {
        /** @var $ticketTypeDbTable App_Db_TicketType */
        $ticketTypeDbTable = App_Db::get(DB_TICKET_TYPE);
        $ticketTypeIds = array();
        $ticketTypes = $this->getTicketTypes();

        if(is_array($ticketTypes) && count($ticketTypes) > 0) {
            foreach($this->getTicketTypes() as $ticketTypeName) {
                $ticketTypeIds[] = $ticketTypeDbTable->getTicketTypeIdByName($ticketTypeName);
            }
        }

        if(!(is_array($ticketTypeIds) && count($ticketTypeIds) > 0)) {
            throw new \Exception("No ticketTypes available for recipient `{$this->getName()}`");
        } else {
            $ticketTypeIds = implode(',', $ticketTypeIds);
        }

        $sqlQuery = $this->_numNewTickets_getSqlQuery($ticketTypeIds);

        /** @var $db Zend_Db_Adapter_Abstract */
        $db = App_Db_Abstract::getDefaultAdapter();
        $result = $db->query($sqlQuery)->fetchAll(Zend_Db::FETCH_COLUMN);
        $result = array_map('intval', $result);

        return $result;
    }

    /**
     * Возвращает SQL-запрос получения ID новых тикетов
     * @param $ticketTypeIds
     * @return string
     */
    protected function _numNewTickets_getSqlQuery($ticketTypeIds)
    {
        return <<<SQL
            SELECT `id`
            FROM ticket
            WHERE `status`=1 AND `ticket_type_id` IN({$ticketTypeIds})
SQL;
    }

    /**
     * Возвращает относительный URL главной страницы получателя
     * @return string
     */
    public final function getUrl()
    {
        return '/finance/list/'.$this->getName().'/';
    }
}