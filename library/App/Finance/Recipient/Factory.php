<?php
use App_Finance_Recipient_Type_Accounting_Recipient as AccountingRecipient;
use App_Finance_Recipient_Type_Dispatcher_Recipient as DispatcherRecipient;
use App_Finance_Recipient_Type_Depot_Recipient as DepotRecipient;
use App_Finance_Recipient_Type_Payment_Recipient as PaymentRecipient;

class App_Finance_Recipient_Factory
{
    /**
     * @var App_Finance_Recipient_Factory
     */
    protected static $_instance;

    /**
     * Список доступных к созданию получателей
     * @var array
     */
    protected $_recipientNames = array();

    /**
     * Фабрика получателей (синглтон)
     * Создает получателей по их названию
     */
    public function __construct()
    {
        $this->_loadRecipientNames();
    }

    /**
     * Синглтон
     * @return App_Finance_Recipient_Factory
     */
    public static function getInstance()
    {
        if(is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * Загружает данные о существующих получателях из файла "recipient_types.php"
     * @throws Exception
     */
    protected function _loadRecipientNames()
    {
        $fileName = __DIR__.'/Factory/recipient_types.php';

        if(!(is_readable($fileName))) {
            throw new \Exception('Cannot open factory configuration file');
        }

        /** @var $recipientTypesConfig array */
        $recipientTypesConfig = include $fileName;

        if(!(is_array($recipientTypesConfig))) {
            throw new \Exception('Invalid factory configuration file');
        }

        if(count($recipientTypesConfig)) {
            foreach($recipientTypesConfig as $recipientType => $recipientClassName) {
                $this->addRecipient($recipientType, $recipientClassName);
            }
        }
    }

    /**
     * Добавляет информацию о создании получателя
     * @param string $recipientType
     * @param string $recipientClassName
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function addRecipient($recipientType, $recipientClassName)
    {
        if(!(is_string($recipientType)) || !(is_string($recipientClassName))) {
            throw new \InvalidArgumentException('recipientType/recipientClassName should have a string type, got '.gettype($recipientType));
        }

        if(!(class_exists($recipientClassName) && (is_subclass_of($recipientClassName, 'App_Finance_Recipient_RecipientInterface')))) {
            throw new \Exception("Class {$recipientClassName} not exists or is not child of App_Finance_Recipient_Type_Abstract_Recipient class");
        }

        $this->_recipientNames[$recipientType] = $recipientClassName;
    }

    /**
     * Возвращает AccountingRecipient
     * @return AccountingRecipient
     */
    public function getAccountingRecipient()
    {
        return $this->createFromRecipientName('accounting');
    }

    /**
     * Возвращает DispatcherRecipient
     * @return DispatcherRecipient
     */
    public function getDispatcherRecipient()
    {
        return $this->createFromRecipientName('dispatcher');
    }

    /**
     * Возвращает DepotRecipient
     * @return DepotRecipient
     */
    public function getDepotRecipient()
    {
        return $this->createFromRecipientName('depot');
    }

    /**
     * Возвращает PaymentRecipient
     * @return PaymentRecipient
     */
    public function getPaymentRecipient()
    {
        return $this->createFromRecipientName('payment');
    }

    /**
     * Возвращает получателя по его названию
     * @param $recipientName
     * @return App_Finance_Recipient_Type_Abstract_Recipient
     * @throws Exception
     */
    public function createFromRecipientName($recipientName)
    {
        if(!($this->hasRecipient($recipientName))) {
            throw new \Exception("RecipientType {$recipientName} not exists");
        }

        $recipientClassName = $this->getRecipientClassNameByRecipientName($recipientName);

        if(!(class_exists($recipientClassName) && (is_subclass_of($recipientClassName, 'App_Finance_Recipient_RecipientInterface')))) {
            throw new \Exception("Class \"{$recipientClassName}\" not found or not instance of App_Finance_Recipient_RecipientInterface");
        }

        return new $recipientClassName;
    }

    /**
     * Возвращает true, если фабрика знает, как создать указанного получателя
     * @param $recipientType
     * @return bool
     * @throws InvalidArgumentException
     */
    public function hasRecipient($recipientType)
    {
        if(!(is_string($recipientType))) {
            throw new \InvalidArgumentException('recipientType should have a string type, got '.gettype($recipientType));
        }

        return isset($this->_recipientNames[$recipientType]);
    }

    /**
     * Возвращает имя класса по названию получателя
     * @param string $recipientName
     * @return string
     * @throws Exception
     */
    public function getRecipientClassNameByRecipientName($recipientName)
    {
        if(!($this->hasRecipient($recipientName))) {
            throw new \Exception("RecipientType {$recipientName} not exists");
        }

        return $this->_recipientNames[$recipientName];
    }

    /**
     * Удаляет информацию о возможности создания указанного получателя
     * @param string $recipientType
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function removeRecipient($recipientType)
    {
        if(!(is_string($recipientType))) {
            throw new \InvalidArgumentException('recipientType should have a string type, got '.gettype($recipientType));
        }

        if(!($this->hasRecipient($recipientType))) {
            throw new \Exception("RecipientType {$recipientType} not exists");
        }

        unset($this->_recipientNames[$recipientType]);
    }

    /**
     * Возвращает список доступных получателей в формату "название" => "класс"
     * @return array
     */
    public function getRecipients()
    {
        return array_keys($this->_recipientNames);
    }

    /**
     * @return App_Db_TicketType
     */
    protected function  _getTicketTypeTable()
    {
        return App_Db::get(DB_TICKET_TYPE);
    }
}