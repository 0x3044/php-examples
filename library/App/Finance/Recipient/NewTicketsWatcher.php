<?php
use App_Finance_Recipient_RecipientInterface as Recipient;

/**
 * Счетчик запросов, сохраняющий список Id запросов, о которых уже известно текущему пользователю
 * Данный класс используется для вывода уведомления о новых запросов в саппорт
 */
class App_Finance_Recipient_NewTicketsWatcher
{
    /**
     * @const int
     */
    const EXCEED_PERIOD = 15;

    /**
     * Получатель
     * @var Recipient
     */
    protected $_recipient;

    /**
     * "Зафиксированные" (известные текущему пользователю) Id запросов
     * @var int[]
     */
    protected $_fixedTicketIds;

    /**
     * Актуальные Id запросов
     * @var int[]
     */
    protected $_actualTicketIds;

    /**
     * Желаемая дата обработки
     * @var Zend_Date[]
     */
    protected $_dateToProcess;

    /**
     * Сохраняет в сессии список новых тикетов(в формате "было" - "стало"
     * @param App_Finance_Recipient_RecipientInterface $recipient
     */
    public function __construct(Recipient $recipient)
    {
        $this->_recipient = $recipient;
        $this->_fetchTicketIds();
    }

    /**
     * Получение Id новых тикетов
     */
    public function _fetchTicketIds()
    {
        $sessionData = $this->_getSessionData();

        $fixedTicketIds = & $this->_fixedTicketIds;
        $actualTicketIds = & $this->_actualTicketIds;

        /** @var $dbTable App_Db_Ticket */
        $dbTable = App_Db::get(DB_TICKET);
        $recipient = $this->getRecipient();
        $ticketIds = $recipient->getNewTicketIds();

        $fixedTicketIds = $sessionData;
        $this->_actualTicketIds = $ticketIds ? $ticketIds : array();

        if(!(is_array($sessionData))) {
            $fixedTicketIds = array();
        }

        // Удаляем Id тикетов, которые есть по каким-то причинам сохранились в сессии и уже стали неактуальными
        if(is_array($fixedTicketIds) && count($fixedTicketIds)) {
            foreach($fixedTicketIds as $key => $value) {
                if(array_search($value, $actualTicketIds) === false) {
                    unset($fixedTicketIds[$key]);
                }
            }
        }

        if(!(is_array($fixedTicketIds))) {
            $fixedTicketIds = array();
        }
        if(!(is_array($actualTicketIds))) {
            $actualTicketIds = array();
        }

        sort($fixedTicketIds = array_map('intval', $fixedTicketIds));
        sort($actualTicketIds = array_map('intval', $actualTicketIds));

        $this->_dateToProcess = $dbTable->getDateToProcess(array_merge($fixedTicketIds, $actualTicketIds));
    }

    /**
     * Обновляет, фиксирует Id тикетов
     */
    public function updateSessionData()
    {
        $_SESSION['financeRecipientSession'][$this->getRecipient()->getName()] = $this->_actualTicketIds;
    }

    /**
     * Возвращает true, если пользователю включены мигания
     * @return bool
     */
    public function hasBlinkingAccess()
    {
        return App_Access::get('access', "finance>{$this->getRecipient()->getName()}>blink");
    }

    /**
     * Возвращает true, если с момента фиксации появились новые тикеты
     * @return bool
     */
    public function hasNewTickets()
    {
        return (bool) $this->getNewTicketIds();
    }

    /**
     * Возвращает число новых тикетов с момента фиксации
     * @return int
     */
    public function numNewTickets()
    {
        return count($this->getNewTicketIds());
    }

    /**
     * Возвращает Id новых тикетов с момента фиксации
     * @return array
     */
    public function getNewTicketIds()
    {
        return array_diff($this->_actualTicketIds, $this->_fixedTicketIds);
    }

    /**
     * Возврашает переданный конструктору получателя
     * @return \App_Finance_Recipient_RecipientInterface
     */
    public function getRecipient()
    {
        return $this->_recipient;
    }

    /**
     * Возврашает Id всех новых тикетов
     * @return int[]
     */
    public function getActualTicketIds()
    {
        return $this->_actualTicketIds;
    }

    /**
     * Возвращает зафиксированные Id активных тикетов
     * @return int[]
     */
    public function getFixedTicketIds()
    {
        return $this->_fixedTicketIds;
    }

    /**
     * Возврашает количество просмотренных запросов
     * @return array
     */
    public function getExceedFixedTicketIds()
    {
        return $this->getExceedTickets($this->_fixedTicketIds);
    }

    /**
     * Возвращает актупальное количество запросов
     * @return array
     */
    public function getExceedActualTicketIds()
    {
        return $this->getExceedTickets($this->_actualTicketIds);
    }

    /**
     * @param $ticketIds
     * @return array
     */
    public function getExceedTickets($ticketIds)
    {
        $exceedIds = array();
        $currentTimestamp = time();

        if(is_array($ticketIds) && count($ticketIds)) {
            foreach($ticketIds as $ticketId) {
                if($dateToProcess = $this->getDateToProcess($ticketId)) {
                    if(($dateToProcess->toValue() - $currentTimestamp) / 60 < (self::EXCEED_PERIOD)) {
                        $exceedIds[] = $ticketId;
                    }
                }
            }
        }

        return $exceedIds;
    }

    /**
     * Возвращает дату к обработке тикета по его Id
     * @param $ticketId
     * @return null|Zend_Date
     */
    public function getDateToProcess($ticketId)
    {
        if(isset($this->_dateToProcess[$ticketId]) && ($this->_dateToProcess[$ticketId] instanceof Zend_Date)) {
            return clone $this->_dateToProcess[$ticketId];
        } else {
            return NULL;
        }
    }

    /**
     * Возвращает сессию
     * @return int[]|false
     */
    protected function &_getSessionData()
    {
        $recipient = $this->getRecipient();

        if(!(isset($_SESSION['financeRecipientSession']))) {
            $_SESSION['financeRecipientSession'] = array();
        }

        if(!(isset($_SESSION['financeRecipientSession'][$recipient->getName()]))) {
            $_SESSION['financeRecipientSession'][$recipient->getName()] = false;
        }

        return $_SESSION['financeRecipientSession'][$recipient->getName()];
    }
}