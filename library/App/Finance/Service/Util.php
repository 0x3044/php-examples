<?php
use App_Finance_Ticket_Factory as TicketFactory;
use App_Finance_Recipient_Factory as RecipientFactory;

class App_Finance_Service_Util
{
    /**
     * Возвращает последние тикеты по указанной заявке в формате "Получатель => Информация-о-Получателе, Тикеты"
     * @param int $claimId
     * @throws Exception
     * @return array
     */
    public function getLatestTickets($claimId)
    {
        /** @var $dbTable App_Db_Ticket */
        $dbTable = App_Db::get(DB_TICKET);
        $latestTickets = $dbTable->getLatestTicketsByClaimId($claimId);

        if(count($latestTickets) > 0) {
            $ticketFactory = TicketFactory::getInstance();
            $recipientFactory = RecipientFactory::getInstance();
            $recipients = array();

            foreach($recipientFactory->getRecipients() as $recipientName) {
                $recipients[$recipientName] = array(
                    "recipient" => $recipientFactory->createFromRecipientName($recipientName)->toArray(),
                    "tickets" => array()
                );
            }

            foreach($latestTickets as $ticketRowData) {
                $ticket = $ticketFactory->createFromZendRow($ticketRowData);
                $recipient = $ticket->getMainRecipient();

                if(!(array_key_exists($recipient, $recipients))) {
                    throw new \Exception(sprintf("Recipient `%s` not found", $recipient));
                }

                $recipients[$recipient]["tickets"][] = $ticket;
            }

            return $recipients;
        } else {
            return array();
        }
    }

    /**
     * Возвращает массив содержащий в себе информацию о всех доступных диспетчерах и привязанных к ним машин
     * @return array [id диспетчера => [id, name, car_number[]]]
     */
    public function getDispatchersJsonData()
    {
        $dispatchers = RecipientFactory::getInstance()->getDispatcherRecipient()->getAvailableDispatcherIds();

        if(count($dispatchers)) {
            foreach($dispatchers as $dispatcherId => $dispatcherName) {
                $dispatchers[$dispatcherId] = array(
                    'id' => $dispatcherId,
                    'name' => $dispatcherName,
                    'car_numbers' => App_Helper_Driver::get(array('userId' => $dispatcherId))
                );
            }
        }

        return $dispatchers;
    }

    /**
     * Возвращает активный тикет в складской саппорт для указанной заявки либо FALSE в случаи его отсутствия
     * @param int $claimId
     * @return null|App_Finance_Ticket_Type_Abstract_Ticket
     */
    public static function getActiveDepotTicket($claimId)
    {
        $ticketFinder = new App_Finance_Ticket_Finder();
        $ticketFinder->setClaimIds($claimId);
        $ticketFinder->setTicketTypeIds('depot_verify');
        $ticketFinder->setTicketStatus(array(
            App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_ACTIVE,
            App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_PENDING_CANCEL
        ));

        $result = $ticketFinder->find();

        if($result->size()) {
            return $result{0};
        }else{
            return null;
        }
    }
}