<?php

interface App_Finance_Service_CountService_CountStrategyInterface
{
    /**
     * Возвращает количество активных запросов
     * @return int
     */
    public function getNumActiveTickets();

    /**
     * Возвращает количество ожидающих отмены запросов
     * @return int
     */
    public function getNumPendingCancelTickets();

    /**
     * Возвращает количество отмененных запросов
     * @return int
     */
    public function getNumCanceledTickets();

    /**
     * Возвращает количество архивных запросов
     * @return int
     */
    public function getNumArchivedTickets();
}