<?php
use App_Finance_Service_CountService_Strategy_Default as DefaultCountStrategy;

class App_Finance_Service_CountService_Strategy_Payment extends DefaultCountStrategy
{
    /**
     * Сервис подсчета запросов для запросов на оплату
     * Берутся данные из фильтров для активных, ожидающих отмены запросов
     */
    public function __construct()
    {
        $this->_recipient = App_Finance_Recipient_Factory::getInstance()->createFromRecipientName('payment');
    }

    /**
     * {@inheritdoc}
     */
    public function getNumActiveTickets()
    {
        return $this->_fetchByFilter(App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_ACTIVE);
    }

    /**
     * {@inheritdoc}
     */
    public function getNumPendingCancelTickets()
    {
        return $this->_fetchByFilter(App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_PENDING_CANCEL);
    }
}