<?php
use App_Finance_Ticket_Component_Status_Type_Default_Status as TicketStatus;
use App_Finance_Service_CountService_CountStrategyInterface as CountStrategyInterface;

class App_Finance_Service_CountService_Strategy_Default implements CountStrategyInterface
{
    /**
     * @var App_Finance_Recipient_RecipientInterface
     */
    protected $_recipient;

    /**
     * Кэш "n1,n2,n3" => (int) count, где n1,n2,n3 - список статусов, count - количество запросов
     * @var array
     */
    protected $_cached = array();

    /**
     * Дефолтная стратегия подсчета количество запросов в тот или иной раздел саппорта
     * @param App_Finance_Recipient_RecipientInterface $recipient
     */
    public function __construct(App_Finance_Recipient_RecipientInterface $recipient) {
        $this->_recipient = $recipient;
    }

    /**
     * Возвращает переданный конструктору получателя
     * @return App_Finance_Recipient_RecipientInterface
     */
    public final function getRecipient() {
        return $this->_recipient;
    }

    public final function clearCache() {
        $this->_cached = array();
    }

    /**
     * Возвращает количество запросов по получателю и статусу запроса
     * Используется простой подсчет запросов в таблице ticket
     * @param $status
     * @return int
     */
    protected function _fetchByTableCount($status)
    {
        if($this->_getCached($status) === false) {
            $ticketTypeIds = implode(',', $this->getRecipient()->getTicketTypeIds());

            $sqlQuery = <<<SQL
              SELECT
                count(*)
              FROM ticket
              WHERE ticket.ticket_type_id IN ({$ticketTypeIds}) AND ticket.status IN ({$status})
SQL;

            $result = (int) Zend_Db_Table_Abstract::getDefaultAdapter()->query($sqlQuery)->fetch(Zend_Db::FETCH_COLUMN);

            $this->_setCache($status, $result);
            $this->_setMemcached($status, $result);
        }

        return $this->_getCached($status);
    }

    /**
     * Возвращает количество строк по получателю и статусу запроса
     * Используется подсчет строк с помощью фильтра
     * @param $status
     * @return int
     */
    protected function _fetchByFilter($status)
    {
        if($this->_getCached($status) === false) {
            $ticketTypeIds = implode(',', $this->getRecipient()->getTicketTypeIds());

            $filter = Finance_Model_Filter_Factory::getFilter($this->getRecipient()->getName(), array(
                'countOnly' => true
            ));
            $filter->setPostData(array());
            $filter->getSqlFormatter()->getWhereConditions()->add("status IN({$status})");
            $filter->getSqlFormatter()->getWhereConditions()->add("ticket_type_id IN ({$ticketTypeIds})");
            $filterResult = $filter->getResult();

            $this->_setCache($status, (int) $filterResult->total);
            $this->_setMemcached($status, (int) $filterResult->total);
        }

        return $this->_getCached($status);
    }

    /**
     * Возвращает количество строк по получателю и статусу запроса
     * Возвращает данные из memcached либо FALSE в случае отсутсвия данных в кеше
     * @param $status
     * @return int|false
     */
    protected function _fetchFromMemcached($status)
    {
        $memcached = App_Memcached::get();

        return false;

        $ticketTypeIds = $this->getRecipient()->getTicketTypeIds();

        $keys = App_Finance_Service::getInstance()->getCountService()->getMemcachedKeys(
            $ticketTypeIds[0],
            $this->getRecipient()->getName(),
            App_Finance_Ticket_Component_Status_Type_Default_Status::getStatusNameById($status)
        );
        $keyAccess = $keys['access'];
        $keyValue = $keys['value'];

        if($memcached->load($keyAccess) === false) {
            return false;
        }else{
            $memcached->save($memcached->load($keyAccess)+1, $keyAccess);
            return (int) $memcached->load($keyValue);
        }
    }

    /**
     * Возвращает значение счетчика по статусу из внутреннего кеша
     * @param $status
     * @return bool
     */
    protected function _getCached($status)
    {
        if(!(isset($this->_cached[$status]))) {
            $memcachedValue = $this->_fetchFromMemcached($status);

            if($memcachedValue === false) {
                return false;
            }else{
                $this->_cached[$status] = $memcachedValue;
            }
        }

        return isset($this->_cached[$status]) ? $this->_cached[$status] : false;
    }

    /**
     * Сохраняет во внутреннем кеше значение
     * @param $status
     * @param $num
     * @return int
     */
    protected function _setCache($status, $num)
    {
        $this->_cached[$status] = (int) $num;

        return $this->_cached[$status];
    }

    /**
     * Сохраняет в memcached значение счетчика
     * @param $status
     * @param $num
     */
    protected function _setMemcached($status, $num)
    {
        $memcached = App_Memcached::get();
        $ticketTypeIds = $this->getRecipient()->getTicketTypeIds();

        $keys = App_Finance_Service::getInstance()->getCountService()->getMemcachedKeys(
            $ticketTypeIds[0],
            $this->getRecipient()->getName(),
            App_Finance_Ticket_Component_Status_Type_Default_Status::getStatusNameById($status)
        );
        $keyAccess = $keys['access'];
        $keyValue = $keys['value'];

        $memcached->save(1, $keyAccess);
        $memcached->save($num, $keyValue);
    }

    /**
     * {@inheritdoc}
     */
    public function getNumActiveTickets()
    {
        return $this->_fetchByTableCount(TicketStatus::STATUS_ACTIVE);
    }

    /**
     * {@inheritdoc}
     */
    public function getNumPendingCancelTickets()
    {
        return $this->_fetchByTableCount(TicketStatus::STATUS_PENDING_CANCEL);
    }

    /**
     * {@inheritdoc}
     */
    public function getNumCanceledTickets()
    {
        return $this->_fetchByTableCount(TicketStatus::STATUS_CANCELED);
    }

    /**
     * {@inheritdoc}
     */
    public function getNumArchivedTickets()
    {
        return $this->_fetchByTableCount(TicketStatus::STATUS_PENDING_CANCEL);
    }
}