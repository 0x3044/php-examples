<?php
use App_Finance_Service_CountService_Strategy_Default as DefaultCountStrategy;

class App_Finance_Service_CountService_Strategy_Null extends DefaultCountStrategy
{
    /**
     * Тестовая стратегия
     * Возврашает 0 на все запросы.
     */
    public function __construct()
    {
        $this->_recipient = App_Finance_Recipient_Factory::getInstance()->createFromRecipientName('dispatcher');
    }

    /**
     * {@inheritdoc}
     * @return int
     */
    public function getNumActiveTickets()
    {
        return 0;
    }

    /**
     * {@inheritdoc}
     * @return int
     */
    public function getNumPendingCancelTickets()
    {
        return 0;
    }

    /**
     * {@inheritdoc}
     * @return int
     */
    public function getNumCanceledTickets()
    {
        return 0;
    }

    /**
     * {@inheritdoc}
     * @return int
     */
    public function getNumArchivedTickets()
    {
        return 0;
    }
}