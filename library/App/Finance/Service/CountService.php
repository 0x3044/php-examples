<?php
use App_Finance_Service_CountService_Strategy_Default as DefaultCountService;
use App_Finance_Service_CountService_Strategy_Payment as PaymentCountService;
use App_Finance_Service_CountService_Strategy_Dispatcher as DispatcherCountService;

class App_Finance_Service_CountService
{
    /**
     * Возвращает стратегию подсчета ссылок
     * @param $recipient
     * @return App_Finance_Service_CountService_Strategy_Default|App_Finance_Service_CountService_Strategy_Dispatcher|App_Finance_Service_CountService_Strategy_Payment
     * @throws Exception
     */
    public function getStrategy($recipient)
    {
        if($recipient instanceof App_Finance_Recipient_RecipientInterface) {
            $recipient = $recipient->getName();
        }

        switch($recipient) {
            default:
                if(is_string($recipient)) {
                    $recipient = App_Finance_Recipient_Factory::getInstance()->createFromRecipientName($recipient);
                }else if(!($recipient instanceof App_Finance_Recipient_RecipientInterface)) {
                    throw new \Exception('Invalid argument');
                }

                return new DefaultCountService($recipient);
                break;

            case 'payment':
                return new PaymentCountService();
        }
    }

    /**
     * Возвращает названия ключей из кеша, которые хранят(или не хранят) информацию о количестве запросов
     * @param $ticketTypeId
     * @param $recipientName
     * @param $statusName
     * @return array
     */
    public function getMemcachedKeys($ticketTypeId, $recipientName, $statusName)
    {
        $keyFilter = new Zend_Filter_PregReplace(array(
            'match' => '/[^a-zA-Z0-9_]/',
            'replace' => '_'
        ));

        $recipientName = $keyFilter->filter($recipientName);
        $statusName = $keyFilter->filter($statusName);

        /** @var $ticketTypeDbTable App_Db_TicketType */
        $ticketTypeDbTable = App_Db::get(DB_TICKET_TYPE);
        $memcached = App_Memcached::get();

        if(!($memcached->load("finance_{$recipientName}_session"))) {
            $memcached->save(uniqid('fsessid'), "finance_{$recipientName}_session");
        }

        $session = $memcached->load("finance_{$recipientName}_session");

        $userBindTicketTypes = array(
            $ticketTypeDbTable->getTicketTypeByName('dispatcher_verify')->getId(),
            $ticketTypeDbTable->getTicketTypeByName('payment_request')->getId()
        );

        if(in_array($ticketTypeId, $userBindTicketTypes)) {
            $userId = Zend_Auth::getInstance()->getIdentity()->id;

            return array(
                "session" => "finance_{$recipientName}_session",
                "access" => "finance_{$recipientName}_{$statusName}_{$session}_{$userId}_counter_access",
                "value" => "finance_{$recipientName}_{$statusName}_{$session}_{$userId}_counter_value"
            );
        }else{
            return array(
                "session" => "finance_{$recipientName}_session",
                "access" => "finance_{$recipientName}_{$statusName}_counter_access",
                "value" => "finance_{$recipientName}_{$statusName}_counter_value"
            );
        }
    }
}