<?php
use App_Finance_Service_Util as Util;
use App_Finance_Service_CountService as CountService;

class App_Finance_Service
{
    /**
     * Инстанс сервиса
     * @var App_Finance_Service
     */
    protected static $_instance;

    /**
     * Утилитарные функции
     * @var Util
     */
    protected $_util;

    /**
     * Сервис подсчета запросов
     * @var CountService
     */
    protected $_countService;

    /**
     * Возвращает сервис
     * @return App_Finance_Service
     */
    public static function getInstance()
    {
        if(is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * Возвращает объект с коллекцией методов, которым не нашлось другого места, кроме этого
     * @return \App_Finance_Service_Util
     */
    public function getUtil()
    {
        if(!($this->_util)) {
            $this->_util = new Util();
        }

        return $this->_util;
    }

    /**
     * Возвращает сервис подсчета строк
     * @return \App_Finance_Service_CountService
     */
    public function getCountService()
    {
        if(!($this->_countService)) {
            $this->_countService = new CountService();
        }

        return $this->_countService;
    }
}