<?php
use App_Finance_Utils_ShowFooter_Strategy_Support as SupportStrategy;
use App_Finance_Utils_ShowFooter_Strategy_Payment as PaymentStrategy;

class App_Finance_Utils_ShowFooter
{
    /**
     * Создает раздел "финансовый саппорт" в блоке ссылок
     */
    public function render()
    {
        $blinkEnabled = false;
        $numLinks = 0;
        $strategies = array(
            'support' => new SupportStrategy(array('accounting', 'dispatcher', 'depot')),
            'payment' => new PaymentStrategy(array('payment'))
        );

        /** @var $strategy App_Finance_Utils_ShowFooter_Strategy_Abstract */
        foreach($strategies as $strategy) {
            $strategy->loadLinks();
            $numLinks += count($strategy->getLinks());
        }

        if($numLinks > 0) {
            $links[] = '';

            /** @var $strategy App_Finance_Utils_ShowFooter_Strategy_Abstract */
            foreach($strategies as $strategy) {
                $html = '';

                foreach($strategy->getLinks() as $link) {
                    $html .= $link->render();

                    if($link->enabledBlink()) {
                        $blinkEnabled = true;
                    }
                }

                if(strlen($html)) {
                    $links[] = '<div class="strategy">'.$html.'</div>';
                }
            }

            $links = implode("\n", $links);

            echo <<<HTML
                <section class="finance-support">
                    <div class="blocks">
                        <label>Финансовый саппорт:</label>
                        <div class="links">
                            {$links}
                        </div>
                    </div>
                </section>
HTML;
        }

        if($blinkEnabled) {
            /** @var $headScriptHelper App_View_Helper_HeadScript */
            $headScriptHelper = Zend_Layout::getMvcInstance()->getView()->headScript();
            $headScriptHelper->appendJsFiles(array('/js/jquery/jquery.blinking.js'), true);
            echo "<script>$(function(){ $('.finance-support-link-blink').blinking(); });</script>";
        }
    }
}