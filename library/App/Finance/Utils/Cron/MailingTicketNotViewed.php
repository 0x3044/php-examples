<?php
use App_Finance_Ticket_Factory as TicketFactory;

/**
 * Рассылка "В саппорте появился новый тикет"
 * При создании экземпляра тикета автоматически триггерися рассылка. Все, что делает данный метод - получает
 * экземпляры всех тикетов на текущий день.
 */
class App_Finance_Utils_Cron_MailingTicketNotViewed
{
    /**
     * @see App_Finance_Ticket_Component_Observer_Type_MailingTicketNotViewed_Observer
     * @see App_Finance_Ticket_Component_Observer_Type_MailingTicketNotViewed_Observer::onPerformMailing
     */
    public function perform()
    {
        /** @var $dbTable App_Db_Ticket */
        $dbTable = App_Db::get(DB_TICKET);
        $tickets = $dbTable->getDailyTickets();

        foreach($tickets as $ticketRow) {
            try {
                $ticket = TicketFactory::getInstance()->createFromZendRow($ticketRow);
                $ticket->performMailing();
            }
            catch(\Exception $e) {
                echo "ERROR: {$e->getMessage()}\n";
            }
        }
    }
}