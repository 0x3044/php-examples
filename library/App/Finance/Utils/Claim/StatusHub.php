<?php
use App_Finance_Ticket_Type_Abstract_Ticket as Ticket;
use App_Finance_Ticket_Component_Validation_Exception as ValidationException;
use App_Finance_Ticket_Component_Observer_ObserverException as ObserverException;
use App_Finance_Ticket_Finder as TicketFinder;
use App_Claim_Exception as ClaimException;

class App_Finance_Utils_Claim_StatusHub
{
    /**
     * Переданный конструктору ID заявки
     * @var int
     */
    protected $_claimId;

    /**
     * Исключения, которые произошли при автоподтверждении запросов
     * @var \Exception[]
     */
    protected $_exceptions = array();

    /**
     * Статус диспетчера
     * @var int[]|null
     */
    protected $_approuveDispatcherStatus;

    /**
     * Статус менеджера
     * @var int[]|null
     */
    protected $_approuveDepotStatus;

    /**
     * Автоматическое подтверждение запросов при нахождении заявки в определенных статусах
     * @param int $claimId
     */
    public function __construct($claimId)
    {
        App_Spl_TypeCheck::getInstance()->positiveNumeric($claimId);

        $this->_claimId = $claimId;
    }

    /**
     * Возвращает переданный констуктору ID заявки
     * @return int
     */
    public function getClaimId()
    {
        return $this->_claimId;
    }

    /**
     * Запуск автоподтверждения запросов
     */
    public function perform()
    {
        /** @var $mapper App_Claim_Mapper_Abstract */
        /** @var $claim Claim_Model_Abstract */
        $claimId = $this->getClaimId();

        if($this->_claimOnDepotStatus()) {
            $tickets = App_Finance_Ticket_Factory::getInstance()->getActiveDispatcherTicketsByClaimId($claimId);

            if($tickets->size()) {
                /** @var App_Finance_Ticket_TicketInterface $ticket */
                foreach($tickets as $ticket) {
                    /** @var $ticket Ticket */
                    try {
                        $ticket->getStatusHandler()->setArchived();
                        $ticket->save();
                    }
                    catch(ValidationException $e) {
                        $this->_exceptions[] = $e;
                    }
                    catch(ObserverException $e) {
                        $this->_exceptions[] = $e;
                    }
                }
            }
        }

        if($this->_claimOnManagerStatus()) {
            $tickets = App_Finance_Ticket_Factory::getInstance()->getActiveDepotTicketsByClaimId($claimId);

            if($tickets->size()) {
                /** @var App_Finance_Ticket_TicketInterface $ticket */
                foreach($tickets as $ticket) {
                    /** @var $ticket Ticket */
                    try {
                        $ticket->getStatusHandler()->setArchived();
                        $ticket->save();
                    }
                    catch(ValidationException $e) {
                        $this->_exceptions[] = $e;
                    }
                    catch(ObserverException $e) {
                        $this->_exceptions[] = $e;
                    }
                }
            }
        }
    }

    /**
     * Валидация статуса
     * Запрет перевода на статус кладовщика/менеджера/закрытия, если есть запрос в диспетчерский саппорт
     * Запрет перевода на статус менеджера/закрытия, если есть запрос в складской саппорт
     */
    public function validate($claimStatus)
    {
        $hasTickets = $this->_hasActiveTickets();

        if($hasTickets && $claimStatus == App_Claim_Status_Out::CLOSE_STATUS) {
            throw new ClaimException('Невозможно закрыть заявку, т.к. по заявке имеются нерассмотренные запросы в финансовом саппорте');
        }
    }

    /**
     * @return \Exception[]
     */
    public function getExceptions()
    {
        return $this->_exceptions;
    }

    /**
     * Возвращает true, если заявка находится на статусе начальника склада или выше
     * @return bool
     */
    protected function _claimOnDepotStatus()
    {
        return $this->getClaim()->dispetcherFlag || in_array($this->getClaim()->claim_status, $this->_approuveDispatcherStatus);
    }

    /**
     * Возвращает true, если заявка находится на статусе подтверждения менеджером или выше
     * @return bool
     */
    protected function _claimOnManagerStatus()
    {
        return $this->getClaim()->talibFlag || in_array($this->getClaim()->claim_status, $this->_approuveDepotStatus);
    }

    /**
     * Возвращает данные заявки
     * DO NOT CACHE!
     * @return \Claim_Model_Abstract
     */
    public function getClaim()
    {
        list(, $claim) = App_Claim_Factory::getInstance()->createMapperByClaimId($this->getClaimId());
        $this->_isClaimSupported($claim);

        return $claim;
    }

    /**
     * Возвращает true, если заявка данного типа поддерживаемя
     * @throws Exception
     */
    protected function _isClaimSupported($claim)
    {
        switch($className = get_class($claim)) {
            default:
                throw new \Exception("StatusHub does not support `{$className}`");

            case 'Claim_Model_Out':
                $this->_approuveDispatcherStatus = array(
                    App_Claim_Status_Out::TALIB_STATUS,
                    App_Claim_Status_Out::MANAGER_STATUS,
                );

                $this->_approuveDepotStatus = array(
                    App_Claim_Status_Out::MANAGER_STATUS,
                );

                break;

            case 'Claim_Model_In':
                $this->_approuveDispatcherStatus = array(
                    App_Claim_Status_In::TALIB_STATUS
                );

                $this->_approuveDepotStatus = array(
                    App_Claim_Status_In::MANAGER_STATUS
                );

                break;

            case 'Claim_Model_InOut_In':
                $this->_approuveDispatcherStatus = array(
                    App_Claim_Status_InOut_In::MANAGER_STATUS
                );

                $this->_approuveDepotStatus = array(
                    App_Claim_Status_InOut_In::MANAGER_STATUS
                );

                break;

            case 'Claim_Model_InOut_Out':
                $this->_approuveDispatcherStatus = array(
                    App_Claim_Status_InOut_Out::MANAGER_STATUS
                );

                $this->_approuveDepotStatus = array(
                    App_Claim_Status_InOut_Out::MANAGER_STATUS
                );

                break;

            case 'Claim_Model_ReturnOut':
                $this->_approuveDispatcherStatus = array(
                    App_Claim_Status_ReturnOut::TALIB_STATUS,
                    App_Claim_Status_ReturnOut::MANAGER_STATUS
                );

                $this->_approuveDepotStatus = array(
                    App_Claim_Status_ReturnOut::MANAGER_STATUS
                );

                break;

            case 'Claim_Model_Supply':
                $this->_approuveDispatcherStatus = array(
                    App_Claim_Status_Supply::TALIB_STATUS,
                    App_Claim_Status_Supply::MANAGER_STATUS,
                );

                $this->_approuveDepotStatus = array(
                    App_Claim_Status_Supply::MANAGER_STATUS,
                );

                break;

            case 'Claim_Model_InSupply_In':
                $this->_approuveDispatcherStatus = array(
                    App_Claim_Status_InSupply_In::MANAGER_STATUS,
                );

                $this->_approuveDepotStatus = array(
                    App_Claim_Status_InSupply_In::MANAGER_STATUS,
                );

                break;

            case 'Claim_Model_InSupply_Supply':
                $this->_approuveDispatcherStatus = array(
                    App_Claim_Status_InSupply_Supply::MANAGER_STATUS,
                );

                $this->_approuveDepotStatus = array(
                    App_Claim_Status_InSupply_Supply::MANAGER_STATUS,
                );

                break;

            case 'Claim_Model_Income':
                $this->_approuveDispatcherStatus = array(
                    App_Claim_Status_Income::TALIB_STATUS,
                    App_Claim_Status_Income::MANAGER_STATUS,
                );

                $this->_approuveDepotStatus = array(
                    App_Claim_Status_Income::MANAGER_STATUS,
                );

                break;

            case 'Claim_Model_IncomeOut_Income':
                $this->_approuveDispatcherStatus = array(
                    App_Claim_Status_IncomeOut_Income::MANAGER_STATUS,
                );

                $this->_approuveDepotStatus = array(
                    App_Claim_Status_IncomeOut_Income::MANAGER_STATUS,
                );

                break;

            case 'Claim_Model_IncomeOut_Out':
                $this->_approuveDispatcherStatus = array(
                    App_Claim_Status_IncomeOut_Out::MANAGER_STATUS,
                );

                $this->_approuveDepotStatus = array(
                    App_Claim_Status_IncomeOut_Out::MANAGER_STATUS,
                );

                break;

            case 'Claim_Model_ReturnSupply':
                $this->_approuveDispatcherStatus = array(
                    App_Claim_Status_ReturnSupply::TALIB_STATUS,
                    App_Claim_Status_ReturnSupply::MANAGER_STATUS,
                );

                $this->_approuveDepotStatus = array(
                    App_Claim_Status_ReturnSupply::MANAGER_STATUS,
                );

                break;

            case 'Claim_Model_Spares':
                $this->_approuveDispatcherStatus = array(
                    Claim_Model_Spares::STATUS_TALIB,
                    Claim_Model_Spares::STATUS_MANAGER,
                );

                $this->_approuveDepotStatus = array(
                    Claim_Model_Spares::STATUS_MANAGER,
                );

                break;

            case 'Claim_Model_RepairOut':
                $this->_approuveDispatcherStatus = array(
                    Claim_Model_RepairOut::STATUS_MASTER,
                );

                $this->_approuveDepotStatus = array(
                );

                break;
        }
    }

    /**
     * Возвращает true, если по заявке есть активные запросы
     * @return bool
     */
    protected function _hasActiveTickets()
    {
        $finder = new TicketFinder();
        $finder->setClaimIds($this->getClaimId());
        $finder->setTicketStatus(array("active", "pending-cancel"));

        return $finder->findOne() !== false;
    }

    /**
     * Возвращает true, если у заявки есть активные запросы в диспетчерский саппорт
     * @return bool
     */
    protected function _hasDispatcherTickets()
    {
        $finder = new TicketFinder();
        $finder->setClaimIds($this->getClaimId());
        $finder->setTicketStatus("active");
        $finder->setRecipients("dispatcher");

        return $finder->findOne() !== false;
    }

    /**
     * Возвращает true, если у заявки есть активные запросы в складской саппорт
     * @return bool
     */
    protected function _hasDepotTickets()
    {
        $finder = new TicketFinder();
        $finder->setClaimIds($this->getClaimId());
        $finder->setTicketStatus("active");
        $finder->setRecipients("depot");

        return $finder->findOne() !== false;
    }
}