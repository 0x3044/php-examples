<?php
use App_Finance_Ticket_TicketInterface as Ticket;
use App_Diff_Type_Claim_Component_Changes_Products_Product as Product;
use App_Diff_Component_Field as Field;

class App_Finance_Utils_Claim_MapperRollback
{
    /**
     * @var Ticket
     */
    protected $_ticket;

    /**
     * @var Claim_Model_Abstract
     */
    protected $_mapper;

    /**
     * Создает роллбак-объект
     * @param $claimId
     * @param Claim_Model_Abstract $mapper
     * @return App_Finance_Utils_Claim_MapperRollback|null
     */
    public static function create($claimId, Claim_Model_Abstract $mapper)
    {
        $ticket = App_Finance_Service::getInstance()->getUtil()->getActiveDepotTicket($claimId);

        if($ticket instanceof App_Finance_Ticket_TicketInterface) {
            return new App_Finance_Utils_Claim_MapperRollback($ticket, $mapper);
        } else {
            return NULL;
        }
    }

    /**
     * Откатывает изменения в заявке
     * @param App_Finance_Ticket_TicketInterface $ticket
     * @param $mapper
     */
    public function __construct(Ticket $ticket, Claim_Model_Abstract $mapper)
    {
        $this->_ticket = $ticket;
        $this->_mapper = $mapper;
    }

    /**
     * Откат изменений
     */
    public function rollback()
    {
        $mapper = $this->_mapper;
        $ticket = $this->_ticket;
        $modificationHandler = $ticket->getModificationHandler()->getClaim();

        if(!(is_object($mapper))) {
            throw new \InvalidArgumentException("Invalid mapper");
        }

        if($modificationHandler->hasModifications()) {
            $this->rollbackFields();
            $this->rollbackProducts();
        }
    }

    /**
     * Откатывает изменения в полях заявки
     */
    public function rollbackFields()
    {
        $mapper = $this->_mapper;
        $ticket = $this->_ticket;
        $modificationHandler = $ticket->getModificationHandler()->getClaim();
        $diff = $modificationHandler->getDiff()->getClaimChanges()->getClaimFieldsChanges()->getDiff();

        $rowData = array();

        /** @var $field Field */
        foreach($diff->getFields() as $field) {
            if($field->getItemId() == 0 && $field->getFieldName() != "products") {
                $this->_rollbackField($field, $rowData);
            }
        }

        $mapper->setOptions($rowData);
    }

    /**
     * Откатывает изменения в товарах
     * Метод не может откатить удаленные товары!
     */
    public function rollbackProducts()
    {
        $mapper = $this->_mapper;
        $ticket = $this->_ticket;
        $modificationHandler = $ticket->getModificationHandler()->getClaim();
        $diff = $modificationHandler->getDiff()->getClaimChanges()->getProductChanges();

        $rowData = $mapper->products;

        // Откатываем изменения в товарах и "удаляем" добавленные товары
        if(is_array($rowData) && count($rowData)) {
            /** @var $product Product */
            foreach($diff->getAllProducts() as $product) {
                foreach($rowData as $key => &$productData) {
                    if($product->getItemId() == $productData['product_id']) {
                        if($product->isAddedProduct()) {
                            unset($rowData[$key]);
                        } else if($product->isModifiedProduct()) {
                            foreach($product->getDiff()->getFields() as $field) {
                                $productData[$field->getFieldName()] = $field->getOldValue();
                            }
                        }
                    }
                }
            }
        }

        ksort($rowData);

        $mapper->setOptions(array('products' => $rowData));
    }

    /**
     * Откат изменений в товарах для заявки запчастей
     * @throws Exception
     */
    public function rollbackSparesProducts()
    {
        $mapper = $this->_mapper;
        $ticket = $this->_ticket;
        $modificationHandler = $ticket->getModificationHandler()->getClaim();
        $diff = $modificationHandler->getDiff()->getClaimChanges()->getProductChanges();

        $rowData = $mapper->products;

        if(is_array($rowData) && count($rowData)) {
            /** @var $product Product */
            foreach($diff->getAllProducts() as $product) {
                foreach($rowData as $key => &$productData) {
                    if($product->getItemId() == $productData['product_id']) {
                        foreach($productData['sections'] as &$sectionData) {
                            if($sectionData['item_id'] == $product->getItemId()) {
                                if($product->isAddedProduct()) {
                                    unset($rowData[$key]);
                                } else if($product->isModifiedProduct()) {
                                    foreach($product->getDiff()->getFields() as $field) {
                                        $sectionData[$field->getFieldName()] = $field->getOldValue();
                                    }
                                } else {
                                    throw new \Exception("Произошла ошибка: во время нахождения запроса в складском саппорте из заявки был удален товар");
                                }
                            }
                        }
                    }
                }
            }
        }

        return $rowData;
    }

    /**
     * Откатывает изменения в товарах
     * Специальная версия для заявок на отгрузку
     * @param array $products
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function rollbackProductsSections(array &$products)
    {
        $mapper = $this->_mapper;
        $ticket = $this->_ticket;
        $modificationHandler = $ticket->getModificationHandler()->getClaim();

        if(!(is_object($mapper))) {
            throw new \InvalidArgumentException("Invalid mapper");
        }

        if($modificationHandler->hasModifications()) {
            $diff = $modificationHandler->getDiff()->getClaimChanges()->getProductChanges();

            if(is_array($products) && count($products)) {
                /** @var $product Product */
                foreach($diff->getAllProducts() as $product) {
                    foreach($products as $key => &$productData) {
                        if($product->getItemId() == (int) $productData->product_id) {
                            if($product->isAddedProduct()) {
                                unset($products[$key]);
                            } else if($product->isModifiedProduct() || $product->isRemovedProduct()) {
                                foreach($product->getDiff()->getFields() as $field) {
                                    $productData->{$field->getFieldName()} = $field->getOldValue();
                                }
                            } else {
                                throw new \Exception("Произошла ошибка: во время нахождения запроса в складском саппорте из заявки был удален товар");
                            }
                        }
                    }
                }
            }
        }

        ksort($products);
    }

    /**
     * @param $field
     * @param $rowData
     * @return mixed
     */
    protected function _rollbackField(Field $field, &$rowData)
    {
        $fieldName = $field->getFieldName();

        $fieldName = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $fieldName));
        $fieldValue = $field->getOldValue();

        // Исключения из стандартного правила
        switch($fieldName) {
            case 'client':
                $fieldName = 'client_id';
                break;

            case 'manager':
                $fieldName = 'manager_id';
                break;

            case 'date':
                $fieldValue = strtotime($fieldValue);
                break;
        }

        $rowData[$fieldName] = $fieldValue;

        return $rowData;
    }
}