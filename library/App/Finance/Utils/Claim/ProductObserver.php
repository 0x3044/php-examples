<?php
use App_Finance_Utils_Claim_ProductObserver_Exception as ProductObserverException;
use App_Diff_Type_Claim_Factory as ClaimDiffFactory;
use App_Diff_Type_Claim_Component_Factory_EventIdsException as EventIdsException;

class App_Finance_Utils_Claim_ProductObserver_Exception extends \Exception
{
}

class App_Finance_Utils_Claim_ProductObserver
{
    /**
     * @var int
     */
    protected $_claimId;

    /**
     * @var int
     */
    protected $_itemId;

    const EXCEPTION_RESTRICT_LOWER = 'Вы не можете уменьшать количество товара в заявке при действующем запросе в складской саппорт';

    const EXCEPTION_RESTRICT_REMOVE = 'Нельзя удалить товар из заявки при действующем запросе в складской саппорт';

    /**
     * Обслуживает валидацию товаров - запрет на уменьшение/удаление товаров в заявки при наличии запроса
     * в складском саппорте
     * @param int $claimId
     * @param int $itemId
     */
    public function __construct($claimId, $itemId = NULL)
    {
        $this->_claimId = $claimId;
        $this->_itemId = $itemId;
    }

    /**
     * Запрет на уменьшение товара в заявке при действующем запросе в складской саппорт
     * @param array $postData
     * @return bool
     * @throws App_Finance_Utils_Claim_ProductObserver_Exception
     */
    public function validateSetBlock(array $postData)
    {
        $claimId = $this->getClaimId();

        if(!(App_Finance_Service::getInstance()->getUtil()->getActiveDepotTicket($claimId))) {
            return true;
        }

        if($this->userCanIgnoreRestrictions()) {
            return true;
        }

        foreach($postData as $productData) {
            foreach(array('lastBlockAmount', 'lastBlockBoxes', 'curBlockAmount', 'curBlockBoxes') as $requiredParam) {
                if(!(isset($productData[$requiredParam]))) {
                    continue;
                }

                $confirmBlockAmount = (float) $productData['curBlockAmount'];
                $confirmBlockBoxes = (float) $productData['curBlockBoxes'];
                $lastBlockAmount = (float) $productData['lastBlockAmount'];
                $lastBlockBoxes = (float) $productData['lastBlockBoxes'];

                if($confirmBlockAmount < $lastBlockAmount || $confirmBlockBoxes < $lastBlockBoxes) {
                    throw new ProductObserverException(self::EXCEPTION_RESTRICT_LOWER);
                }
            }
        }

        return true;
    }

    /**
     * Запрет на уменьшение товара в заявке при действующем запросе в складском саппорте
     * @param int $startEventId
     * @throws App_Finance_Utils_Claim_ProductObserver_Exception
     * @return bool
     */
    public function validateSetBlockByStartEventId($startEventId)
    {
        $claimId = $this->getClaimId();

        if(!(App_Finance_Service::getInstance()->getUtil()->getActiveDepotTicket($claimId))) {
            return true;
        }

        if($this->userCanIgnoreRestrictions()) {
            return true;
        }

        App_Spl_TypeCheck::getInstance()->positiveNumeric($startEventId);

        try {
            $claimDiffFactory = new ClaimDiffFactory();
            $claimDiff = $claimDiffFactory->createFromClaimId($claimId, $startEventId);
            $claimDiffProducts = $claimDiff->getClaimChanges()->getProductChanges()->getAllProducts();
        }
        catch(EventIdsException $e) {
            return true;
        }

        if($claimDiffProducts->size()) {
            /** @var $product App_Diff_Type_Claim_Component_Changes_Products_Product */
            foreach($claimDiffProducts as $product) {
                $diff = $product->getDiff();

                if($diff->hasField('amount')) {
                    $field = $diff->getField('amount');
                    $oldValue = (float) $field->getOldValue();
                    $newValue = (float) $field->getNewValue();

                    if($newValue < $oldValue) {
                        throw new ProductObserverException(self::EXCEPTION_RESTRICT_LOWER);
                    }
                }

                if($diff->hasField('boxes')) {
                    $field = $diff->getField('boxes');
                    $oldValue = (float) $field->getOldValue();
                    $newValue = (float) $field->getNewValue();

                    if($newValue < $oldValue) {
                        throw new ProductObserverException(self::EXCEPTION_RESTRICT_LOWER);
                    }
                }
            }
        }

        return true;
    }

    /**
     * Запрет на удаление товаров при действующем запросе в складской саппорт
     * @throws App_Finance_Utils_Claim_ProductObserver_Exception
     */
    public function validateRemove()
    {
        if($this->userCanIgnoreRestrictions()) {
            return true;
        }

        if(App_Finance_Service::getInstance()->getUtil()->getActiveDepotTicket($this->getClaimId())) {
            throw new ProductObserverException(self::EXCEPTION_RESTRICT_REMOVE);
        }
    }

    /**
     * Возвращает переданный конструктору ID заявки
     * @return int
     */
    public function getClaimId()
    {
        return $this->_claimId;
    }

    /**
     * Возвращает переданный конструктору ItemId(products)
     * @return int
     */
    public function getItemId()
    {
        return $this->_itemId;
    }

    /**
     * Возвращает true, если пользователь имеет право на игнорирование ограничений, которые накладываются на товары
     * в заявке при наличии активного/архивного запроса в складской саппорт
     * @return bool
     */
    public function userCanIgnoreRestrictions()
    {
        return App_Access::get('access', 'finance>depot>products_changes');
    }
}