<?php
use App_Finance_Ticket_TicketInterface as Ticket;

class App_Finance_Utils_DocumentViewRegister extends App_Finance_Ticket_Extension
{
    /**
     * Регистрирует просмотр документов пользователем
     * @param string|int $userId
     */
    public function register($userId = 'auto')
    {
        if($userId == 'auto') {
            $userId = Zend_Auth::getInstance()->getIdentity()->id;
        }

        App_Spl_TypeCheck::getInstance()->positiveNumeric($userId);

        $ticket = $this->getTicket();
        $jsonHandler = $ticket->getJsonDataHandler();

        if(!(isset($jsonHandler['document_views']))) {
            $jsonHandler['document_views'] = array();
        }

        $jsonHandler['document_views'][$userId] = true;
    }
}