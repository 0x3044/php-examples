<?php
use App_Finance_Utils_ShowFooter_Strategy_Abstract as AbstractStrategy;
use App_Finance_Recipient_RecipientInterface as Recipient;
use App_Finance_Utils_ShowFooter_View_Link as Link;
use App_Finance_Ticket_Component_Status_Type_Default_Status as TicketStatus;

class App_Finance_Utils_ShowFooter_Strategy_Payment extends AbstractStrategy
{
    /**
     * Загрузка ссылок для данной стратегии
     */
    public function loadLinks()
    {
        $recipients = $this->_recipients;

        foreach($recipients as $recipientName) {
            $recipient = App_Finance_Recipient_Factory::getInstance()->createFromRecipientName($recipientName);

            if($recipient->hasAuthorization()) {
                $this->_links[] = $this->_loadSupportLink($recipient);
            }
        }
    }

    /**
     * Загружает ссылку
     * @param App_Finance_Recipient_RecipientInterface $recipient
     * @return App_Finance_Utils_ShowFooter_View_Link
     */
    protected function _loadSupportLink(Recipient $recipient)
    {
        $link = new Link();
        $link->setTitle($recipient->getDescription());
        $link->setUrl($recipient->getUrl());
        $link->setPriority(array(TicketStatus::STATUS_PENDING_CANCEL, TicketStatus::STATUS_ACTIVE, TicketStatus::STATUS_ARCHIVED));
        $link->invertCounterColors();

        $countStrategy = App_Finance_Service::getInstance()->getCountService()->getStrategy('payment');
        $numPendingCancelTickets = $countStrategy->getNumPendingCancelTickets();
        $numActiveTickets = $countStrategy->getNumActiveTickets();

        if($numActiveTickets > 0 && $numActiveTickets > 0) {
            $link->setCustomContainerClass('payment-super-blink');
        }

        if($numPendingCancelTickets>0 && App_Access::get('access', "finance>{$recipient->getName()}>status>pending-cancel")) {
            $link->setNumTickets(TicketStatus::STATUS_PENDING_CANCEL, $numPendingCancelTickets);
        }

        if($numActiveTickets>0 && App_Access::get('access', "finance>{$recipient->getName()}>status>active")) {
            $link->setNumTickets(TicketStatus::STATUS_ACTIVE, $numActiveTickets);
        }

        if($numActiveTickets > 0 && $numActiveTickets) {
            $link->enableBackdrop('green');
        }

        if(App_Access::get('access', "finance>{$recipient->getName()}>blink")) {
            if($numActiveTickets > 0 || $numPendingCancelTickets > 0) {
                $link->enableBackground();
            }

            if($numActiveTickets> 0 && $numPendingCancelTickets > 0) {
                $link->enableBlink();
                $link->setBlinkAdditionalColor('#FA5858');
            }
        }

        if($numActiveTickets > 0 && $numPendingCancelTickets > 0) {
            $link->setAdditionalCounter(array(
                'color' => 'red',
                'count' => $numActiveTickets
            ));
        }

        return $link;
    }
}