<?php
use App_Finance_Utils_ShowFooter_Strategy_Abstract as AbstractStrategy;
use App_Finance_Recipient_RecipientInterface as Recipient;
use App_Finance_Utils_ShowFooter_View_Link as Link;
use App_Finance_Ticket_Component_Status_Type_Default_Status as TicketStatus;

class App_Finance_Utils_ShowFooter_Strategy_Support extends AbstractStrategy
{
    /**
     * Загрузка ссылок для данной стратегии
     */
    public function loadLinks()
    {
        $recipients = $this->_recipients;

        foreach($recipients as $recipientName) {
            $recipient = App_Finance_Recipient_Factory::getInstance()->createFromRecipientName($recipientName);

            if($recipient->hasAuthorization()) {
                $this->_links[] = $this->_loadSupportLink($recipient);
            }
        }
    }

    /**
     * Загружает ссылку
     * @param App_Finance_Recipient_RecipientInterface $recipient
     * @return App_Finance_Utils_ShowFooter_View_Link
     */
    protected function _loadSupportLink(Recipient $recipient)
    {
        $link = new Link();
        $link->setTitle($recipient->getDescription());
        $link->setUrl($recipient->getUrl());
        $link->invertCounterColors();
        $link->setPriority(array(
            TicketStatus::STATUS_PENDING_CANCEL,
            TicketStatus::STATUS_ACTIVE,
        ));

        $sessionWatcher = $recipient->getSessionHandler();

        $countStrategy = App_Finance_Service::getInstance()->getCountService()->getStrategy($recipient);
        $numActiveTickets = count($sessionWatcher->getActualTicketIds());
        $numPendingCancelTickets = $countStrategy->getNumPendingCancelTickets();

        /** @var $ticketViewedDb App_Db_TicketViewed */
        $ticketViewedDb = App_Db::get(DB_TICKET_VIEWED);
        $viewedTicketsIds = $ticketViewedDb->getViewedTicketsIds('depot');
        if ($viewedTicketsIds && is_array($viewedTicketsIds) && count($viewedTicketsIds)) {
            foreach ($viewedTicketsIds as $viewedTicketId) {
                if (in_array($viewedTicketId, $sessionWatcher->getActualTicketIds()))
                    $numActiveTickets --;
            }
        }

        if($numActiveTickets>0 && App_Access::get('access', "finance>{$recipient->getName()}>status>active")) {
            $link->setNumTickets(TicketStatus::STATUS_ACTIVE, $numActiveTickets);
        }

        if($numPendingCancelTickets>0 && App_Access::get('access', "finance>{$recipient->getName()}>status>pending-cancel")) {
            $link->setNumTickets(TicketStatus::STATUS_PENDING_CANCEL, $numPendingCancelTickets);
        }

        if($sessionWatcher->hasBlinkingAccess() && count($sessionWatcher->getExceedActualTicketIds())) {
            $link->enabledBlink();
            $link->enableBackground();
        }

        return $link;
    }
}