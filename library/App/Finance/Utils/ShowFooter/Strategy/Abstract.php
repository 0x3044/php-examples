<?php
use App_Finance_Utils_ShowFooter_View_Link as Link;
use App_Finance_Recipient_RecipientInterface as Recipient;

abstract class App_Finance_Utils_ShowFooter_Strategy_Abstract
{
    /**
     * Сссылки
     * @var Link[]
     */
    protected $_links;

    /**
     * Получатели
     * @var string[]
     */
    protected $_recipients;

    /**
     * Стратегия
     * @param array $recipients
     */
    public final function __construct(array $recipients)
    {
        $this->_recipients = $recipients;
    }

    /**
     * Загрузка ссылок для данной стратегии
     */
    abstract public function loadLinks();

    /**
     * Возвращает ссылки
     * @return \App_Finance_Utils_ShowFooter_View_Link[]
     */
    public final  function getLinks()
    {
        return $this->_links;
    }
}