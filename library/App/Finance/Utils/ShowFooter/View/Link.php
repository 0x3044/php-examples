<?php
use App_Finance_Ticket_Component_Status_Type_Default_Status as TicketStatus;

/**
 * Ссылка для финансового саппорта
 */
class App_Finance_Utils_ShowFooter_View_Link
{
    /**
     * Название ссылки
     * @var string
     */
    protected $_title;

    /**
     * URL ссылки
     * @var string
     */
    protected $_url;

    /**
     * Дополнительный CSS-класс к контейнеру
     * @var string
     */
    protected $_customContainerClass;

    /**
     * Список приоритетов
     * @var array
     */
    protected $_priority = array(TicketStatus::STATUS_ACTIVE, TicketStatus::STATUS_PENDING_CANCEL);

    /**
     * Количество тикетов, <Статус => Количество>[]
     * @var array
     */
    protected $_numTickets = array();

    /**
     * Включено/выключено подсветку ссылки
     * @var bool
     */
    protected $_enableBackground = false;

    /**
     * Включено/выключено мигание ссылки
     * @var bool
     */
    protected $_enableBlink = false;

    /**
     * Флаг "инвертировать цвета счетика"
     * @var bool
     */
    protected $_invertCounterColors = false;

    /**
     * Цвет подложки
     * @var string|null
     */
    protected $_backdropColor = NULL;

    /**
     * Дополнительный цвет мигания
     * @var string
     */
    protected $_blinkAdditionalColor = null;

    /**
     * Дополнительный счетчик
     * @var array color, count
     */
    protected $_additionalCounter = null;

    /**
     * Установить приоритет
     * @param array $priority
     */
    public function setPriority(array $priority)
    {
        $this->_priority = $priority;
    }

    /**
     * Установить количество тикетов в разделе саппорта
     * @param $ticketStatus
     * @param $numTickets
     * @throws Exception
     */
    public function setNumTickets($ticketStatus, $numTickets)
    {
        if(!(in_array($ticketStatus, TicketStatus::getDefaultAvailableStatuses()))) {
            throw new \Exception("Unknown ticket status `{$ticketStatus}`");
        }

        $this->_numTickets[$ticketStatus] = (int) $numTickets;
    }

    /**
     * Возвращает HTML-код ссылки
     */
    public function render()
    {
        $containerClass = array('finance-link');
        $containerCss = array();

        $counterClass = array('counter');
        $counterCss = array();

        $backdropCss = array();
        $linkCss = array();
        $linkClass = array();

        $linkTitle = htmlspecialchars($this->getTitle());
        $linkUrl = htmlspecialchars($this->getUrl());

        $numTickets = 0;

        foreach($this->_priority as $ticketStatus) {
            if(isset($this->_numTickets[$ticketStatus]) && $this->_numTickets[$ticketStatus] >= -1) {
                $numTickets = $this->_numTickets[$ticketStatus];

                $containerClass[] = 'status-'.TicketStatus::getStatusNameById($ticketStatus);

                if($this->_invertCounterColors) {
                    $containerClass[] = 'inverted';
                    $counterCss[] = 'color: '.$this->_getCounterBackgroundColor($ticketStatus);
                    $counterCss[] = 'background-color: '.$this->_getCounterTextColor($ticketStatus);
                } else {
                    $counterCss[] = 'background-color: '.$this->_getCounterBackgroundColor($ticketStatus);
                    $counterCss[] = 'color: '.$this->_getCounterTextColor($ticketStatus);
                }

                if($this->enabledBackground()) {
                    $containerCss[] = 'background-color: '.$this->_getBackgroundColor($ticketStatus);
                }

                break;
            }
        }

        if($this->enabledBlink() && $numTickets > 0) {
            if($this->enabledBackdrop()) {
                $linkClass[] = 'finance-support-link-blink';
            }else{
                $containerClass[] = 'finance-support-link-blink';
            }
        }

        if($this->enabledBackdrop()) {
            $backdrop = $this->_backdropColor;
        }else{
            $backdrop = null;
        }

        if($numTickets > 0) {
            $counterClass = implode(' ', $counterClass);
            $counterCss = implode('; ', $counterCss);

            $htmlCounter = <<<HTML
                <span class="{$counterClass}" style="{$counterCss}">{$numTickets}</span>
HTML;

            if($additionalCounter = $this->getAdditionalCounter()) {
                $numTickets = $additionalCounter['count'];
                $counterCss = implode('; ', array(
                    'color: '.$additionalCounter['color']
                ));

                $htmlCounter = <<<HTML
                    <span class="{$counterClass}" style="{$counterCss}">{$numTickets}</span>
                    {$htmlCounter}
HTML;
            }
        } else {
            $htmlCounter = '';
        }

        if(strlen($this->getCustomContainerClass())) {
            $containerClass[] = $this->getCustomContainerClass();
        }

        $containerClass = implode(' ', $containerClass);
        $containerCss = implode('; ', $containerCss);
        $linkCss = implode(';', $linkCss);
        $linkClass = implode(';', $linkClass);

        $blinkAdditionalColor = strlen($this->_blinkAdditionalColor) ? "data-attr-blink-additional-color='{$this->_blinkAdditionalColor}'" : null;

        return <<<HTML
            <div class="{$containerClass}">
                <a target="_blank" href="{$linkUrl}" class="{$linkClass}" style="{$linkCss}"><span style="{$containerCss}" class="title" data-blink-attr="backdrop" {$blinkAdditionalColor}><span class="text">{$linkTitle}</span></span></a>
                {$htmlCounter}
            </div>
HTML;
    }

    /**
     * Возвращает текст ссылки
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Задать текст ссылки
     * Задать текст ссылки
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->_title = $title;
    }

    /**
     * Возвращает URL ссылки
     * @return string
     */
    public function getUrl()
    {
        return $this->_url;
    }

    /**
     * Задать URL ссылки
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->_url = $url;
    }

    /**
     * Инвертировать цвет текста/цвет подложки для счетчика
     */
    public function invertCounterColors()
    {
        $this->_invertCounterColors = true;
    }

    /**
     * Возвращает цвет текста для счетчика
     * @param $ticketStatus
     * @return string
     */
    protected function _getCounterTextColor($ticketStatus)
    {
        switch($ticketStatus) {
            default:
                return 'white';
        }
    }

    /**
     * Возвращает цвет подсветки для счетчика
     * @param $ticketStatus
     * @return string
     */
    protected function _getCounterBackgroundColor($ticketStatus)
    {
        switch($ticketStatus) {
            default:
                return 'inherit';

            case TicketStatus::STATUS_ACTIVE:
                return 'red';

            case TicketStatus::STATUS_PENDING_CANCEL:
                return '#DF7401';

            case TicketStatus::STATUS_CANCELED:
                return 'darkkgray';

            case TicketStatus::STATUS_ARCHIVED:
                return 'green';
        }
    }

    /**
     * Возвращает цвет подсветки ссылки
     * @param $ticketStatus
     * @return string
     */
    protected function _getBackgroundColor($ticketStatus)
    {
        switch($ticketStatus) {
            default:
                return 'inherit';

            case TicketStatus::STATUS_ACTIVE:
                return '#FA5858';

            case TicketStatus::STATUS_PENDING_CANCEL:
                return 'darkorange';

            case TicketStatus::STATUS_CANCELED:
                return 'darkkgray';

            case TicketStatus::STATUS_ARCHIVED:
                return '#58FA58';
        }
    }

    /**
     * Включить подсветку ссылки
     */
    public function enableBackground()
    {
        $this->_enableBackground = true;
    }

    /**
     * Выключить подсветку ссылки
     */
    public function disableBackground()
    {
        $this->_enableBackground = false;
    }

    /**
     * Возвращает true, если включена подсветка ссылки
     * @return bool
     */
    public function enabledBackground()
    {
        return $this->_enableBackground;
    }

    /**
     * Включить мигание ссылки
     */
    public function enableBlink()
    {
        $this->_enableBlink = true;
    }

    /**
     * Отключить мигание ссылки
     */
    public function disableBlink()
    {
        $this->_enableBlink = false;
    }

    /**
     * Возвращает true, если включено мигание ссылки
     * @return bool
     */
    public function enabledBlink()
    {
        return $this->_enableBlink;
    }

    /**
     * Отключить цвет подложки
     */
    public function disableBackdrop()
    {
        $this->_backdropColor = NULL;
    }

    /**
     * Возвращает true, если цвет подложки включен
     * @return bool
     */
    public function enabledBackdrop()
    {
        return $this->enabledBackground() && !(is_null($this->_backdropColor));
    }

    /**
     * Включить цвет подложки
     * @param $color
     */
    public function enableBackdrop($color)
    {
        $this->_backdropColor = $color;
    }

    /**
     * Задать дополнительный css-класс к контейнеру
     * @param string $customContainerClass
     */
    public function setCustomContainerClass($customContainerClass)
    {
        $this->_customContainerClass = $customContainerClass;
    }

    /**
     * Возвращает дополнительный css-класс к контейнеру
     * @return string
     */
    public function getCustomContainerClass()
    {
        return $this->_customContainerClass;
    }

    /**
     * Установить дополнительный цвет для мигания
     * @param string $blinkAdditionalColor
     */
    public function setBlinkAdditionalColor($blinkAdditionalColor)
    {
        $this->_blinkAdditionalColor = $blinkAdditionalColor;
    }

    /**
     * Вернуть дополнительный цвет для мигания
     * @return string
     */
    public function getBlinkAdditionalColor()
    {
        return $this->_blinkAdditionalColor;
    }

    /**
     * Установить параметры дополнительного счетчика
     * @param array $additionalCounter
     */
    public function setAdditionalCounter(array $additionalCounter = null)
    {
        $this->_additionalCounter = $additionalCounter;
    }

    /**
     * Возвращает параметры дополнительного счетчика
     * @return array
     */
    public function getAdditionalCounter()
    {
        return $this->_additionalCounter;
    }
}