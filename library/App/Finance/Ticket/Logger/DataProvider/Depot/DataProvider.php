<?php
use App_Finance_Ticket_Logger_DataProvider_AbstractDataProvider as AbstractDataProvider;

class App_Finance_Ticket_Logger_DataProvider_Depot_DataProvider extends AbstractDataProvider
{
    /**
     * Добавляет дополнительные данные к снэпшоту
     */
    protected function _getAdditionalSnapshotData(stdClass $snapshot)
    {
        $ticket = $this->getTicket();

        if($ticket->isNewTicket()) {
            throw new \Exception("Cant get additional snapshot data for new ticket");
        }

        $sqlQuery = <<<SQL
          SELECT
            o.`id` AS claim_orgstructure_id,
            clients.`id` AS claim_client_id,
            claims.`date` claim_date,
            claims.`manager_id` AS claim_manager_id,
            claims.`claim_status` AS claim_status,
            claims.`car` claim_has_car,
            claims.`car_type` AS claim_car_type,
            claims.`car_number` claim_car_number,
            claims.`transport_delivery_time` claim_transport_delivery_time
          FROM ticket t
          LEFT JOIN claims ON t.`claim_id` = claims.`id`
          LEFT JOIN clients ON claims.`client_id` = clients.`id`
          LEFT JOIN orgstructure_users ou ON claims.`manager_id` = ou.`userId`
          LEFT JOIN orgstructure o ON o.`id` = ou.`oId`
          WHERE t.`id`={$ticket->getId()}
SQL;

        if(($result = Zend_Db_Table_Abstract::getDefaultAdapter()->query($sqlQuery)->fetch(Zend_Db::FETCH_ASSOC)) && count($result)) {
            foreach($result as $fieldName => $value) {
                $snapshot->{$fieldName} = $value;
            }
        }
    }
}