<?php
use App_Finance_Ticket_Logger_DataProvider_AbstractDataProvider as AbstractDataProvider;

class App_Finance_Ticket_Logger_DataProvider_Accounting_DataProvider extends AbstractDataProvider
{
    /**
     * Добавляет дополнительные данные к снэпшоту
     */
    protected function _getAdditionalSnapshotData(stdClass $snapshot)
    {
        $ticket = $this->getTicket();

        if($ticket->isNewTicket()) {
            throw new \Exception("Cant get additional snapshot data for new ticket");
        }

        $sqlQuery = <<<SQL
          SELECT
            claims.`manager_id` AS claim_manager_id,
            claims.`claim_status` AS claim_status,
            claims.date AS claim_date,
            clients.`id` AS client_id,
            claims.`credit_limit_exceed` AS claim_credit_limit_exceed,
            o.`id` AS claim_orgstructure_id
          FROM ticket t
          LEFT JOIN claims ON t.`claim_id` = claims.`id`
          LEFT JOIN clients ON claims.`client_id` = clients.`id`
          LEFT JOIN orgstructure_users ou ON claims.`manager_id` = ou.`userId`
          LEFT JOIN orgstructure o ON o.`id` = ou.`oId`
          WHERE t.`id`={$ticket->getId()}
SQL;

        if(($result = Zend_Db_Table_Abstract::getDefaultAdapter()->query($sqlQuery)->fetch(Zend_Db::FETCH_ASSOC)) && count($result)) {
            foreach($result as $fieldName => $value) {
                $snapshot->{$fieldName} = $value;
            }
        }
    }
}