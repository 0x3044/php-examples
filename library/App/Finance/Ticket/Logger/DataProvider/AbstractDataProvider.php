<?php
use App_Finance_Ticket_TicketInterface as Ticket;
use App_Finance_Ticket_Component_Document_FileNotFoundException as FileNotFoundException;

abstract class App_Finance_Ticket_Logger_DataProvider_AbstractDataProvider
{
    /**
     * Переданный конструктору тикет
     * @var Ticket
     */
    protected $_ticket;

    public function __construct(Ticket $ticket)
    {
        $this->_ticket = $ticket;
    }

    /**
     * Возвращает переданный конструктору тикет
     * @return \App_Finance_Ticket_TicketInterface
     */
    public function getTicket()
    {
        return $this->_ticket;
    }

    /**
     * Возвращает снимок тикета
     * @return stdClass
     */
    public final function getSnapshot()
    {
        if($this->getTicket()->isNewTicket()) {
            return new stdClass();
        }

        $snapshot = $this->_getTicketSnapshot();
        $snapshot->documents = $this->_getTicketDocumentsSnapshot();

        $this->_getAdditionalSnapshotData($snapshot);

        return $snapshot;
    }

    /**
     * Добавляет дополнительные данные к снэпшоту
     * @param stdClass $snapshot
     * @return mixed
     */
    abstract protected function _getAdditionalSnapshotData(stdClass $snapshot);

    /**
     * Возврашает снэпшот тикета
     * @return stdClass
     */
    protected final function _getTicketSnapshot()
    {
        /** @var $dbTable App_Db_Ticket */
        $dbTable = App_Db::get(DB_TICKET);
        $ticket = $this->getTicket();
        $snapshot = new stdClass();

        if($ticket->isNewTicket()) {
            return $snapshot;
        }

        $ticketRow = $dbTable->getTicketById($ticket->getId());

        $snapshot->id = $ticketRow->getId();
        $snapshot->date_created = $ticketRow->getDateCreated();
        $snapshot->date_to_process = $ticketRow->getDateToProcess();
        $snapshot->date_processed = $ticketRow->getDateProcessed();
        $snapshot->ticket_type_id = $ticketRow->getTicketTypeId();
        $snapshot->author_user_id = $ticket->getAuthorUserId();
        $snapshot->processed_user_id = $ticketRow->getProcessedUserId();
        $snapshot->claim_id = $ticketRow->getClaimId();
        $snapshot->status = $ticketRow->getStatus();
        $snapshot->is_completed = $ticketRow->getIsCompleted();
        $snapshot->json_data = $ticketRow->getJsonData();

        foreach(array('date_created', 'date_to_process', 'date_processed') as $date) {
            /** @var $date Zend_Date|string */
            if($snapshot->$date instanceof Zend_Date) {
                $snapshot->$date = $date->toString(App_Db::ZEND_DATE_FORMAT);
            }
        }

        return $snapshot;
    }

    /**
     * Возвращает снэпшот документов
     * @return array
     */
    protected final function _getTicketDocumentsSnapshot()
    {
        $ticket = $this->getTicket();
        /** @var $snapshots stdClass[] */
        $snapshots = array();

        if(!($ticket->attachmentsAllowed())) {
            return $snapshots;
        }

        if($ticket->getDocumentsHandler()->getDocuments()->size() == 0) {
            return $snapshots;
        }

        /** @var $document App_Finance_Ticket_Component_Document */
        foreach($ticket->getDocumentsHandler()->getDocuments() as $number => $document) {
            $snapshot = array();

            try {
                $snapshot = $document->toArray();
            }
            catch(FileNotFoundException $e) {
                $snapshot['fileExists'] = false;
            }

            $snapshots[] = array_merge($snapshot, array(
                'product_id' => $document->getRow()->getId(),
                'number' => $number
            ));
        }

        return $snapshots;
    }
}