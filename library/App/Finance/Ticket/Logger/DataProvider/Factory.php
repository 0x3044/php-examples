<?php
use App_Finance_Ticket_TicketInterface as Ticket;

class App_Finance_Ticket_Logger_DataProvider_Factory
{
    /**
     * Возвращает сериализатор запроса для логгера
     * @param App_Finance_Ticket_TicketInterface $ticket
     * @return App_Finance_Ticket_Logger_DataProvider_AbstractDataProvider
     * @throws Exception
     */
    public function createDataProvider(Ticket $ticket)
    {
        if($ticket instanceof App_Finance_Ticket_Type_Accounting_Abstract) {
            return new App_Finance_Ticket_Logger_DataProvider_Accounting_DataProvider($ticket);
        } else if($ticket instanceof App_Finance_Ticket_Type_Dispatcher_Abstract) {
            return new App_Finance_Ticket_Logger_DataProvider_Dispatcher_DataProvider($ticket);
        } else if($ticket instanceof App_Finance_Ticket_Type_Depot_Abstract) {
            return new App_Finance_Ticket_Logger_DataProvider_Depot_DataProvider($ticket);
        } else if($ticket instanceof App_Finance_Ticket_Type_Payment_Abstract) {
            return new App_Finance_Ticket_Logger_DataProvider_Payment_DataProvider($ticket);
        } else {
            throw new \Exception("No dataProvider available for ticket `".get_class($ticket)."`");
        }
    }
}