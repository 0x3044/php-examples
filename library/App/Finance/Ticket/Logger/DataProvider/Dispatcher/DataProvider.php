<?php
use App_Finance_Ticket_Logger_DataProvider_AbstractDataProvider as AbstractDataProvider;

class App_Finance_Ticket_Logger_DataProvider_Dispatcher_DataProvider extends AbstractDataProvider
{
    /**
     * Добавляет дополнительные данные к снэпшоту
     */
    protected function _getAdditionalSnapshotData(stdClass $snapshot)
    {
        $ticket = $this->getTicket();

        if($ticket->isNewTicket()) {
            throw new \Exception("Cant get additional snapshot data for new ticket");
        }

        $sqlQuery = <<<SQL
          SELECT
            o.`id` AS claim_orgstructure_id,
            clients.`id` AS claim_client_id,
            claims.`date` claim_date,
            claims.`manager_id` AS claim_manager_id,
            claims.`claim_status` AS claim_status,
            claims.`dispetcher` AS claim_dispatcher_id,
            claims.`car` claim_has_car,
            claims.`car_type` AS claim_car_type,
            claims.`car_number` claim_car_number,
            claims.`rent_price` AS claim_rent_price,
            claims.`aprox_claim_weight` AS claim_aprox_claim_weight,
            claims.`transport_delivery_time` claim_transport_delivery_time,
            claims.unload_region AS claim_unload_region,
            claims.unload_city AS claim_unload_city,
            claims.unload_street AS claim_unload_street,
            claims.unload_building claim_unload_building,
            claims.unload_address claim_unload_address,
            claims.`unload_contact_name` AS claim_unload_contact_name
          FROM ticket t
          LEFT JOIN claims ON t.`claim_id` = claims.`id`
          LEFT JOIN clients ON claims.`client_id` = clients.`id`
          LEFT JOIN orgstructure_users ou ON claims.`manager_id` = ou.`userId`
          LEFT JOIN orgstructure o ON o.`id` = ou.`oId`
          WHERE t.`id`={$ticket->getId()}
SQL;

        if(($result = Zend_Db_Table_Abstract::getDefaultAdapter()->query($sqlQuery)->fetch(Zend_Db::FETCH_ASSOC)) && count($result)) {
            foreach($result as $fieldName => $value) {
                $snapshot->{$fieldName} = $value;
            }
        }
    }
}