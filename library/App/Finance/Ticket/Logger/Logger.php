<?php
use App_Finance_Ticket_TicketInterface as Ticket;
use App_Finance_Ticket_Logger_DataProvider_Factory as DataProviderFactory;
use App_Finance_Ticket_Logger_DataProvider_AbstractDataProvider as DataProvider;
use App_Logger_Adapter_Finance_Edit as FinanceLoggerEdit;
use App_Logger_Adapter_Finance_Create as FinanceLoggerCreate;

class App_Finance_Ticket_Logger_Logger
{
    /**
     * @var App_Finance_Ticket_TicketInterface
     */
    protected $_ticket;

    /**
     * @var DataProvider
     */
    protected $_dataProvider;

    /**
     * @var stdClass
     */
    protected $_oldSnapshot;

    /**
     * @var stdClass
     */
    protected $_newSnapshot;

    /**
     * Флаг "новый запрос"
     * @var bool
     */
    protected $_isNewTicket;

    public function __construct(Ticket $ticket)
    {
        $dpFactory = new DataProviderFactory();

        $this->_ticket = $ticket;
        $this->_isNewTicket = $ticket->isNewTicket();
        $this->_dataProvider = $dpFactory->createDataProvider($ticket);
    }

    /**
     * @return \App_Finance_Ticket_Logger_DataProvider_AbstractDataProvider
     */
    public function getDataProvider()
    {
        return $this->_dataProvider;
    }

    public function oldSnapshot()
    {
        $this->_oldSnapshot = $this->getDataProvider()->getSnapshot();
    }

    public function newSnapshot()
    {
        $this->_newSnapshot = $this->getDataProvider()->getSnapshot();
    }

    public function writeLog()
    {
        if($this->_isNewTicket) {
            $financeLogger = new FinanceLoggerEdit(App_Logger_Abstract::EVENT_ADD);
            $financeLogger->setObjectId($this->getTicket()->getId());
            $financeLogger->setOldObject($this->getNewSnapshot());
            $financeLogger->setNewObject($this->getOldSnapshot());
        }else{
            $financeLogger = new FinanceLoggerEdit(App_Logger_Abstract::EVENT_EDIT);
            $financeLogger->setObjectId($this->getTicket()->getId());
            $financeLogger->setOldObject($this->getOldSnapshot());
            $financeLogger->setNewObject($this->getNewSnapshot());
        }

        $financeLogger->writeLog();
    }

    /**
     * @return \stdClass
     */
    public function getNewSnapshot()
    {
        return $this->_newSnapshot;
    }

    /**
     * @return \stdClass
     */
    public function getOldSnapshot()
    {
        return $this->_oldSnapshot;
    }

    /**
     * @return \App_Finance_Ticket_TicketInterface
     */
    public function getTicket()
    {
        return $this->_ticket;
    }

    /**
     * Отмечает, что запрос добавляется в базу данных
     */
    public function setIsExistedTicket()
    {
        $this->_isNewTicket = false;
    }

    /**
     * Отмечается, что запрос уже существует и у него обновляются данные
     */
    public function setIsNewTicket()
    {
        $this->_isNewTicket = true;
    }

    /**
     * Возвращает true, если запрос отмечен как новый
     * @return bool
     */
    public function isNewTicket()
    {
        return $this->_isNewTicket;
    }
}