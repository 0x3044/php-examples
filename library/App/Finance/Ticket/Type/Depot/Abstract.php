<?php
use App_Finance_Ticket_Type_Abstract_Ticket as AbstractTicket;
use App_Finance_Ticket_Component_Observer_Type_ClaimNextState_DepotObserver as ClaimNextStageObserver;
use App_Finance_Ticket_Component_Observer_Type_ClaimDate_Observer as ClaimDateObserver;
use App_Finance_Ticket_Component_Validation_Type_HasProducts_Validator as HasProductsValidator;

abstract class App_Finance_Ticket_Type_Depot_Abstract extends AbstractTicket
{
    /**
     * Код в данном методе запускаетсяя после построения объекта.
     * Наблюдатели отключены.
     */
    protected function _init()
    {
        $this->getObserversHandler()->add(new ClaimNextStageObserver());
        $this->getObserversHandler()->add(new ClaimDateObserver());
        $this->getValidationObserver()->getValidationHandler()->add(new HasProductsValidator());
    }

    /**
     * Возвращает основного получателя
     * @return string
     */
    public function getMainRecipient()
    {
        return "depot";
    }
}