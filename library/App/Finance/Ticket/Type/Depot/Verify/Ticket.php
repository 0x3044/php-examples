<?php
use App_Finance_Ticket_Type_Depot_Abstract as AbstractDepotTicket;

class App_Finance_Ticket_Type_Depot_Verify_Ticket extends AbstractDepotTicket
{
    /**
     * {@inheritdoc}
     * @return string
     */
    public function getTicketTypeName()
    {
        return 'depot_verify';
    }

    /**
     * {@inheritdoc}
     * @return bool|mixed
     */
    public function attachmentsAllowed()
    {
        return false;
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function getRecipientNames()
    {
        return array('depot');
    }
}