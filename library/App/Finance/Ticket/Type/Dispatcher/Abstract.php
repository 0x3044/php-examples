<?php
use App_Finance_Ticket_Type_Abstract_Ticket as AbstractTicket;
use App_Finance_Ticket_Component_Observer_Type_MailingDispatcher_Observer as MailingDispatcherObserver;
use App_Finance_Ticket_Component_Validation_Type_OnlyOwnCar_Validator as OnlyOwnCarValidator;
use App_Finance_Ticket_Component_Observer_Type_ProcessDelayViolation_Observer as ProcessDelayViolationObserver;
use App_Finance_Ticket_Component_Observer_Type_ClaimNextState_DispatcherObserver as ClaimNextStageObserver;
use App_Finance_Ticket_Component_Observer_Type_DispatcherRequiredFields_Observer as DispatcherRequiredFieldsObserver;
use App_Finance_Ticket_Component_Observer_Type_MailingPenaltyForCarWaste_Observer as MailingPenaltyForCarWasteObserver;

abstract class App_Finance_Ticket_Type_Dispatcher_Abstract extends AbstractTicket
{
    /**
     * Код в данном методе запускаетсяя после построение объекта.
     * Наблюдатели отключены.
     */
    protected function _init()
    {
        $this->getObserversHandler()->add(new ProcessDelayViolationObserver());
        $this->getObserversHandler()->add(new MailingDispatcherObserver());
        $this->getObserversHandler()->add(new ClaimNextStageObserver());
        $this->getObserversHandler()->add(new DispatcherRequiredFieldsObserver());
        $this->getObserversHandler()->add(new MailingPenaltyForCarWasteObserver());

        $this->getValidationObserver()->getValidationHandler()->add(new OnlyOwnCarValidator());
    }

    /**
     * Возвращает основного получателя
     * @return string
     */
    public function getMainRecipient()
    {
        return "dispatcher";
    }
}