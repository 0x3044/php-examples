<?php
use App_Finance_Ticket_Type_Dispatcher_Abstract as AbstractDispatcherTicket;

class App_Finance_Ticket_Type_Dispatcher_Verify_Ticket extends AbstractDispatcherTicket
{
    /**
     * {@inheritdoc}
     * @return string
     */
    public function getTicketTypeName()
    {
        return 'dispatcher_verify';
    }

    /**
     * {@inheritdoc}
     * @return bool|mixed
     */
    public function attachmentsAllowed()
    {
        return false;
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function getRecipientNames()
    {
        return array('dispatcher');
    }
}