<?php
use App_Db_Row_Ticket as TicketZendRow;
use App_Finance_Recipient_Collection as RecipientCollection;
use App_Finance_Ticket_TicketInterface as TicketInterface;
use App_Finance_Ticket_Component_JsonData as JsonDataHandler;
use App_Finance_Ticket_Component_Observer_ObserversHandler as ObserverHandler;
use App_Finance_Ticket_Component_Observer_Type_Validation_Observer as ValidationObserver;
use App_Finance_Ticket_Component_Status_Type_Default_Status as StatusHandler;
use App_Finance_Ticket_Component_Document_Handler as DocumentsHandler;
use App_Finance_Ticket_Component_Behaviour_Save as SaveBehaviour;
use App_Finance_Ticket_Component_Decorator_AbstractDecorator as Decorator;
use App_Finance_Ticket_Component_Decorator_Type_DefaultObservers as DefaultObserversDecorator;
use App_Finance_Ticket_Component_Decorator_Type_DefaultValidators as DefaultValidatorsDecorator;
use App_Finance_Ticket_Component_Acl_Default as Acl;
use App_Finance_Ticket_Component_Acl_Dispatcher as AclDispatcher;
use App_Finance_Ticket_Component_Acl_Factory as AclFactory;
use App_Finance_Ticket_Component_Modification_Handler as ModificationHandler;
use App_Finance_Ticket_Component_Attachment_ServiceNote_Document as ServiceNoteHandler;

abstract class App_Finance_Ticket_Type_Abstract_Ticket implements TicketInterface
{
    /**
     * Запись, представляющая тикет
     * @var TicketZendRow
     */
    protected $_zendRow;

    /**
     * Объект для получения информации о правах доступах
     * @var Acl
     */
    protected $_acl;

    /**
     * Объект управления наблюдателями тикета
     * @var ObserverHandler;
     */
    protected $_observersHandler;

    /**
     * Объект управления статусом тикета
     * @var StatusHandler
     */
    protected $_statusHandler;

    /**
     * Объект управления дополнительными JSON-данными тикета
     * @var JsonDataHandler
     */
    protected $_jsonDataHandler;

    /**
     * Объект управления прикрепленными к тикету файлами
     * @var DocumentsHandler
     */
    protected $_documentsHandler;

    /**
     * Объект для получения информации об изменениях в заявке
     * @var ModificationHandler
     */
    protected $_modificationHandler;

    /**
     * Служебная записка
     * @var ServiceNoteHandler
     */
    protected $_serviceNoteHandler;

    /**
     * Список получателей тикета
     * @var RecipientCollection
     */
    protected $_recipients;

    /**
     * Список валидаторов
     * @var ValidationObserver
     */
    protected $_validationObserver;

    /**
     * Создает тикет
     * При указании zendRow заполняет объект данными из zendRow
     * @param App_Db_Row_Ticket $zendRow
     */
    public final function __construct(App_Db_Row_Ticket $zendRow = NULL)
    {
        $this->_jsonDataHandler = new JsonDataHandler();
        $this->_observersHandler = new ObserverHandler($this);
        $this->_statusHandler = new StatusHandler($this);
        $this->_documentsHandler = new DocumentsHandler($this);
        $this->_modificationHandler = new ModificationHandler($this);

        if($zendRow) {
            $this->_setupTicketFromZendRow($zendRow);
        } else {
            $this->_setupNewTicket();
        }

        $this->appendValidationObserver();
        $this->_defaultDecoration();
        $this->_init();
        $this->getObserversHandler()->enableObservers();
    }

    /**
     * Декорирование дефолтными валидаторами/наблюдателями
     */
    protected function _defaultDecoration()
    {
        $this->decorate(new DefaultObserversDecorator());
        $this->decorate(new DefaultValidatorsDecorator());
    }

    /**
     * Возвращает объект для получения информации о правах доступа к тем или иным операциям с тикетом
     * @return Acl|AclDispatcher
     */
    public final function getAcl()
    {
        if(!($this->_acl instanceof Acl)) {
            $aclFactory = new AclFactory();
            $this->_acl = $aclFactory->createAclTree($this);
        }

        return $this->_acl;
    }

    /**
     * Код в данном методе запускаетсяя после построение объекта.
     * Наблюдатели отключены.
     */
    abstract protected function _init();

    /**
     * Декорация тикета
     * @param App_Finance_Ticket_Component_Decorator_AbstractDecorator $decorator
     */
    public final function decorate(Decorator $decorator)
    {
        $decorator->decorate($this);
    }

    /**
     * Возвращает true, если тикет является новым и при сохранении будет создана новая запись в базе данных
     * @return bool
     */
    public function isNewTicket()
    {
        return $this->getId() == 0;
    }

    /**
     * Возвращает true, если тикет представляет уже существующую запись в базе данных
     * @return bool
     */
    public function isExistedTicket()
    {
        return $this->getId() != 0;
    }

    /**
     * Создает тикет из объекта Zend_Row
     * @param App_Db_Row_Ticket $ticketZendRow
     * @throws Exception
     */
    private final function _setupTicketFromZendRow(TicketZendRow $ticketZendRow)
    {
        if($ticketZendRow->getTicketTypeId() !== $this->getTicketTypeId()) {
            throw new \Exception("Ticket type \"{$this->getTicketTypeId()}\" does not match \"{$ticketZendRow->getTicketTypeId()}\"");
        }

        $this->_setZendRow($ticketZendRow);
        $this->getStatusHandler()->set($ticketZendRow->getStatus());
        $this->getJsonDataHandler()->set(json_decode($ticketZendRow->getJsonData(), true));
    }

    /**
     * Создает новый тикет
     */
    private function _setupNewTicket()
    {
        /** @var $newTicketRow App_Db_Row_Ticket */
        $newTicketRow = App_Db::get(DB_TICKET)->createRow();
        $this->_setZendRow($newTicketRow);
        $this->getStatusHandler()->setDefaultStatus();
    }

    /**
     * Возвращает Zend_Row, представляющей тикет
     * @return App_Db_Row_Ticket
     */
    public final function getZendRow()
    {
        return $this->_zendRow;
    }

    /**
     * Устанавливает Zend_Row, представляющий тикет
     * @param App_Db_Row_Ticket $zendRow
     */
    private function _setZendRow(App_Db_Row_Ticket $zendRow)
    {
        $this->_zendRow = $zendRow;
    }

    /**
     * Возвращает Id типа тикета
     * @return int
     */
    public final function getTicketTypeId()
    {
        if(!($this->getZendRow())) {
            $ticketTypeId = NULL;
        } else {
            $ticketTypeId = $this->getZendRow()->getTicketTypeId();
        }

        if(!($ticketTypeId)) {
            /** @var $dbTicketTypeDbTable App_Db_TicketType */
            $dbTicketTypeDbTable = App_Db::get(DB_TICKET_TYPE);
            $ticketTypeId = $dbTicketTypeDbTable->getTicketTypeIdByName($this->getTicketTypeName());
        }

        return $ticketTypeId;
    }

    /**
     * Возвращает название(name) типа тикета
     * @return string
     */
    abstract public function getTicketTypeName();

    /**
     * Возвращает true, если к данному тикету разрешено прикреплять файлы
     * @return mixed
     */
    abstract public function attachmentsAllowed();

    /**
     * Возвращает объект для управления статусом тикета
     * @return StatusHandler
     */
    public final function getStatusHandler()
    {
        return $this->_statusHandler;
    }

    /**
     * Возвращает объект для управления дополнительными JSON-данными тикета
     * @return JsonDataHandler
     */
    public final function getJsonDataHandler()
    {
        return $this->_jsonDataHandler;
    }

    /**
     * Возвращает объект для управления файлами, прикрепленными к тикету
     * @throws Exception
     * @return DocumentsHandler
     */
    public function getDocumentsHandler()
    {
        if(!($this->attachmentsAllowed())) {
            throw new \Exception("Attachments not allowed for this ticket type");
        }

        return $this->_documentsHandler;
    }

    /**
     * Возвращает объект для получения информации о модифицированных полях в заявке
     * @return ModificationHandler
     */
    public function getModificationHandler()
    {
        return $this->_modificationHandler;
    }

    /**
     * Возвращает служебную записку
     * @return App_Finance_Ticket_Component_Attachment_ServiceNote_Document
     */
    public function getServiceNoteHandler()
    {
        if(!($this->_serviceNoteHandler)) {
            $this->_serviceNoteHandler = new ServiceNoteHandler($this);
        }

        return $this->_serviceNoteHandler;
    }

    /**
     * Прикрепляет валидатор к тикету
     */
    public function appendValidationObserver()
    {
        $observers = $this->getObserversHandler();
        $validationObserver = new ValidationObserver();

        $observers->add($validationObserver);

        $this->_validationObserver = $validationObserver;
    }

    /**
     * Возвращает коллекцию наблюдателей
     * @return ObserverHandler;
     */
    public final function getObserversHandler()
    {
        return $this->_observersHandler;
    }

    /**
     * Возвращает список всех получателей, к которым может быть адресован данный тикет
     * @return RecipientCollection
     */
    public final function getRecipients()
    {
        if(!($this->_recipients)) {
            $this->_recipients = new RecipientCollection();
            $recipientNames = $this->getRecipientNames();

            if(is_array($recipientNames) && count($recipientNames)) {
                foreach($recipientNames as $recipientName) {
                    $this->_recipients->addRecipientByName($recipientName);
                }
            }
        }

        return $this->_recipients;
    }

    /*
     * Возвращает список всех получателей(в виде массива строк), к которым может быть адресован данный тикет
     * @return string[]
     */
    abstract public function getRecipientNames();

    /**
     * Возвращает true, если тикеты идентичны
     * @param App_Finance_Ticket_TicketInterface $compareObject
     * @throws InvalidArgumentException
     * @return bool
     */
    public function equals(App_Finance_Ticket_TicketInterface $compareObject)
    {
        if(!(is_object($compareObject)) || !($compareObject instanceof TicketInterface)) {
            throw new \InvalidArgumentException('compareObject must be instance of TicketInterface object, got '.gettype($compareObject));
        }

        $isEquals = ($this->getId() == $compareObject->getId()) &&
            ($this->getTicketTypeId() == $compareObject->getTicketTypeId()) &&
            ($this->getClaimId() == $compareObject->getClaimId()) &&
            (get_class($this->getStatusHandler()) === get_class($compareObject->getStatusHandler())) &&
            ($this->getStatusHandler()->toInt() === $compareObject->getStatusHandler()->toInt()) &&
            ($this->isCompleted() == $compareObject->isCompleted()) &&
            ($this->getAuthorUserId() == $compareObject->getAuthorUserId()) &&
            ($this->getProcessedUserId() == $compareObject->getProcessedUserId()) &&
            ($this->getJsonDataHandler()->equals($compareObject->getJsonDataHandler()));

        return $isEquals;
    }

    /**
     * Сохраняет изменения в тикете
     * @param array $options
     */
    public final function save(array $options = null)
    {
        $saveBehaviour = new SaveBehaviour($this, $options);
        $saveBehaviour->perform();
    }

    /**
     * Удаляет тикет и всю связанную с ним информацию
     */
    public final function destroy()
    {
        Zend_Db_Table_Abstract::getDefaultAdapter()->beginTransaction();

        if(!($this->isExistedTicket())) {
            throw new \Exception('Удалять можно только уже существующие в базе данных запросы');
        }

        try {
            $observers = $this->getObserversHandler();
            $observers->on('onAfterDestroy');

            $dbTicket = App_Db::get(DB_TICKET);
            $dbTicket->delete('id = '.$this->getId());

            $observers->on('onBeforeDestroy');
            Zend_Db_Table_Abstract::getDefaultAdapter()->commit();
        }catch(\Exception $e){
            Zend_Db_Table_Abstract::getDefaultAdapter()->rollBack();
            throw $e;
        }
    }

    /**
     * Рассылка почтовых сообщений
     */
    public function performMailing()
    {
        $observers = $this->getObserversHandler();

        $observers->on('onPerformMailing');
    }

    /**
     * Возвращает URL тикета (соотвествующая страница саппорта+ticketId=?)
     * @param null|string $specifiedRecipient Указать получателя(если у тикетов данного типа может быть несколько получателей)
     * @throws Exception
     * @return string
     */
    public function getUrl($specifiedRecipient = NULL)
    {
        $recipients = $this->getRecipientNames();
        $linkPrefix = Zend_Layout::getMvcInstance()->getView()->linkPrefix();

        if((!strlen($specifiedRecipient)) && count($recipients) != 1) {
            throw new \Exception("Ticket with ticketType `{$this->getTicketTypeName()}` has more than 1 recipients and no specified recipient was set");
        }

        if(strlen($specifiedRecipient) && !(in_array($specifiedRecipient, $recipients))) {
            throw new \Exception("Specified recipient `{$specifiedRecipient}` is not available for ticketType `{$this->getTicketTypeName()}`");
        }

        $specifiedRecipient = strlen($specifiedRecipient) ? $specifiedRecipient : $recipients[0];
        $status = $this->getStatusHandler()->toString();

        if($status == 'deleted') {
            $status = 'archived';
        }

        return "{$linkPrefix}/finance/list/{$specifiedRecipient}/{$status}/?ticketId=".$this->getId();
    }

    /**
     * Возвращает Id тикета либо 0, если не указан
     * @return int
     */
    public final function getId()
    {
        return $this->getZendRow()->getId();
    }

    /**
     * Устанавливает Id заявки, к которой принадлежит тикет
     * @param $claimId
     */
    public final function setClaimId($claimId)
    {
        $this->getZendRow()->setClaimId($claimId);
    }

    /**
     * Возвращает Id заявки, к которой принадлежит тикет
     * @return int
     */
    public final function getClaimId()
    {
        return $this->getZendRow()->getClaimId();
    }

    /**
     * Возвращает true, если к запросу прикреплена заявка
     * @return bool
     */
    public function hasClaimId()
    {
        return $this->getClaimId() > 0;
    }

    /**
     * Возвращает Id пользователя, обработавшего тикет
     * @return int|null
     */
    public final function getProcessedUserId()
    {
        return $this->getZendRow()->getProcessedUserId();
    }

    /**
     * Устанавливает Id пользователя, обработавшего тикет
     * @param $processedUserId
     */
    public final function setProcessedUserId($processedUserId)
    {
        $this->getZendRow()->setProcessedUserId($processedUserId);
    }

    /**
     * Возвращает дату создания тикета
     * @return Zend_Date
     */
    public final function getDateCreated()
    {
        $dateCreated = $this->getZendRow()->getDateCreated();

        if(!($dateCreated instanceof Zend_Date)) {
            if(!(is_null($dateCreated))) {
                $dateCreated = new Zend_Date($dateCreated, App_Db::ZEND_DATETIME_FORMAT);
            }
        }

        if($dateCreated instanceof Zend_Date) {
            $dateCreated = clone $dateCreated;
        }

        return $dateCreated;
    }

    /**
     * Устанавливает дату создания тикета
     * @param Zend_Date $dateCreated
     */
    public final function setDateCreated(Zend_Date $dateCreated)
    {
        $this->getZendRow()->setDateCreated($dateCreated);
    }

    /**
     * Возвращает дату, к которой должен быть обработан тикет
     * @return Zend_Date
     */
    public final function getDateToProcess()
    {
        $dateToProcess = $this->getZendRow()->getDateToProcess();

        if(!($dateToProcess instanceof Zend_Date)) {
            if(!(is_null($dateToProcess))) {
                $dateToProcess = new Zend_Date($dateToProcess, App_Db::ZEND_DATETIME_FORMAT);
            }
        }

        if($dateToProcess instanceof Zend_Date) {
            $dateToProcess = clone $dateToProcess;
        }

        return $dateToProcess;
    }

    /**
     * Устанавливает дату, к которой должен быть обработан тикет
     * @param Zend_Date $dateToProcess
     */
    public final function setDateToProcess(Zend_Date $dateToProcess = NULL)
    {
        $this->getZendRow()->setDateToProcess($dateToProcess);
        $this->getObserversHandler()->on("onChange", "dateToProcess");
    }

    /**
     * Устанавливает дату, к которой должен быть обработан тикет
     * Допускается:
     *  - Zend_Date
     *  - int(0-60): стартовая дата + n минут
     *  - int(>60): UNIX timestamp
     *  - string: попытка парсинга даты средствами Zend_Date
     * @param $dateToProcess
     * @throws Exception
     */
    public final function setDateToProcessFromMixed($dateToProcess)
    {
        if($this->getJsonDataHandler()->offsetExists('clientDeliveryFact')) {
            $this->getJsonDataHandler()->offsetSet('clientDeliveryFact', false);
        }

        // 0-60: Текущая дата + количество минут
        if(is_numeric($dateToProcess) && $dateToProcess >= 0 && $dateToProcess <= 60) {
            $nDateToProcess = new Zend_Date();
            $nDateToProcess->add((int) $dateToProcess, Zend_Date::MINUTE);
            $dateToProcess = $nDateToProcess;
        // -1: факт клиента
        }else if(is_numeric($dateToProcess) && (int) $dateToProcess == self::DATE_CLIENT_DELIVERY_FACT){
            $this->getJsonDataHandler()->offsetSet('clientDeliveryFact', true);
            $dateToProcess = new Zend_Date();
        } // UNIX time
        else if(is_numeric($dateToProcess) && $dateToProcess > 60) {
            $dateToProcess = new Zend_Date((int) $dateToProcess, Zend_Date::TIMESTAMP);
        } // Строка - попытка распарсить дату
        else if(is_string($dateToProcess)) {
            $dateToProcess = new Zend_Date((string) $dateToProcess);
        }

        if(!($dateToProcess instanceof Zend_Date)) {
            throw new \Exception("Invalid dateToProcess value");
        }

        $this->setDateToProcess($dateToProcess);
    }

    /**
     * Устанавливает дату обработки запроса
     * @param Zend_Date $dateProcessed
     */
    public final function setDateProcessed(Zend_Date $dateProcessed = NULL)
    {
        $this->getZendRow()->setDateProcessed($dateProcessed);
    }

    /**
     * Возвращает дату обработки запроса
     * Внимание! Данный метод может вернуть NULL!
     * @return Zend_Date|null
     */
    public final function getDateProcessed()
    {
        $dateProcessed = $this->getZendRow()->getDateProcessed();

        if(!($dateProcessed instanceof Zend_Date)) {
            if(!(is_null($dateProcessed))) {
                $dateProcessed = new Zend_Date($dateProcessed, App_Db::ZEND_DATETIME_FORMAT);
            }
        }

        if($dateProcessed instanceof Zend_Date) {
            $dateProcessed = clone $dateProcessed;
        }

        return $dateProcessed;
    }

    /**
     * Устанавливает Id пользователя, создавшего тикет
     * @param $authorUserId
     */
    public final function setAuthorUserId($authorUserId)
    {
        $this->getZendRow()->setAuthorUserId($authorUserId);
    }

    /**
     * Возвращает Id пользователя, создавшего тикет
     * @return int|null
     */
    public final function getAuthorUserId()
    {
        return $this->getZendRow()->getAuthorUserId();
    }

    /**
     * Отмечает тикет как завершенный
     */
    public final function setCompleted()
    {
        $this->getZendRow()->setIsCompleted(true);
        $this->getObserversHandler()->on('onSetCompleted');
    }

    /**
     * Отмечает тикет как незавершенный
     */
    public final function setUncompleted()
    {
        if($this->isCompleted()) {
            $this->setProcessedUserId(NULL);
        }

        $this->getZendRow()->setIsCompleted(false);
    }

    /**
     * Возвращает true, если тикет отмечен как завершенный
     * @return bool
     */
    public final function isCompleted()
    {
        return (bool) $this->getZendRow()->getIsCompleted();
    }

    /**
     * Возвращает валидатор
     * @return \App_Finance_Ticket_Component_Observer_Type_Validation_Observer
     */
    public function getValidationObserver()
    {
        return $this->_validationObserver;
    }
}