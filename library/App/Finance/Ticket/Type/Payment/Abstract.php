<?php
use App_Finance_Ticket_Type_Abstract_Ticket as AbstractTicket;

use App_Finance_Ticket_Component_Observer_Type_MailingSetCanceled_Observer as MailingObserver;
use App_Finance_Ticket_Component_Observer_Type_UploadDocument_Observer as UploadDocumentObserver;
use App_Finance_Ticket_Component_Observer_Type_EndState_Observer as EndStateObserver;
use App_Finance_Ticket_Component_Observer_Type_DateProcessed_Observer as DateProcessedObserver;
use App_Finance_Ticket_Component_Observer_Type_ProcessedUserId_Observer as ProcessedUserIdObserver;
use App_Finance_Ticket_Component_Observer_Type_MailingTicketNotViewed_Observer as MailingTicketNotViewedObserver;
use App_Finance_Ticket_Component_Observer_Type_Dates_Observer as DatesObserver;
use App_Finance_Ticket_Component_Observer_Type_SetCompletedOnStatusChange_Observer as SetCompletedOnStatusChange;
use App_Finance_Ticket_Component_Observer_Type_MemcachedCounter_Observer as MemcachedCounterObserver;
use App_Finance_Ticket_Component_Observer_Type_CancellationReasonRequired_Observer as CancellationReasonRequiredObserver;

abstract class App_Finance_Ticket_Type_Payment_Abstract extends AbstractTicket
{
    /**
     * Код в данном методе запускаетсяя после построение объекта.
     * Наблюдатели отключены.
     */
    protected function _init()
    {
    }

    protected function _defaultDecoration()
    {
        $observers = $this->getObserversHandler();

        $observers->add(new SetCompletedOnStatusChange());
        $observers->add(new SetCompletedOnStatusChange());
        $observers->add(new DatesObserver());
        $observers->add(new UploadDocumentObserver());
        $observers->add(new MailingObserver());
        $observers->add(new DateProcessedObserver());
        $observers->add(new DateProcessedObserver());
        $observers->add(new EndStateObserver());
        $observers->add(new ProcessedUserIdObserver());
        $observers->add(new MailingTicketNotViewedObserver());
        $observers->add(new MemcachedCounterObserver());
        $observers->add(new CancellationReasonRequiredObserver());
    }

    /**
     * Возвращает основного получателя
     * @return string
     */
    public function getMainRecipient()
    {
        return "payment";
    }

    /**
     * Возвращает true, если к данному тикету разрешено прикреплять файлы
     */
    public function attachmentsAllowed()
    {
        return true;
    }
}