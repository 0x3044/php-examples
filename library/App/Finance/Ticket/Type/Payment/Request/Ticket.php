<?php
use App_Finance_Ticket_Type_Payment_Abstract as AbstractPaymentTicket;

class App_Finance_Ticket_Type_Payment_Request_Ticket extends AbstractPaymentTicket
{
    /**
     * {@inheritdoc}
     * @return string
     */
    public function getTicketTypeName()
    {
        return 'payment_request';
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function getRecipientNames()
    {
        return array('payment');
    }

    public function getPaymentDetails()
    {

    }

    /**
     * Возвращает true, если для запроса есть сохраненные платежные данные
     * @return bool
     */
    public function isPaymentDetailsExists()
    {
        return true;
    }
}