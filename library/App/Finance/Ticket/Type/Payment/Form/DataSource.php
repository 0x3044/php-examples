<?php
use App_Finance_Ticket_Type_Payment_Form_Data_PaymentTypes as PaymentTypes;
use App_Finance_Ticket_Type_Payment_Form_DataSource_ClientSelect as ClientSelect;
use App_Finance_Ticket_Type_Payment_Form_DataSource_ClaimSelect as ClaimSelect;
use App_Finance_Ticket_TicketInterface as Ticket;

class App_Finance_Ticket_Type_Payment_Form_DataSource
{
    /**
     * @var App_Finance_Ticket_Type_Payment_Form_DataSource
     */
    protected static $_instance;

    /**
     * @var PaymentTypes
     */
    protected $_paymentTypes;

    public function __construct() {
        $this->_paymentTypes = new PaymentTypes();
    }

    /**
     * Синглтон
     * @return App_Finance_Ticket_Type_Payment_Form_DataSource
     */
    public static function getInstance()
    {
        if(is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * Возврашает информацию о доступных типах оплаты
     * @return \App_Finance_Ticket_Type_Payment_Form_Data_PaymentTypes
     */
    public function getPaymentTypes()
    {
        return $this->_paymentTypes;
    }

    /**
     * Возвращает список доступных клиентов
     * @param int|null $paymentTypeId
     * @return array
     */
    public function getAvailableClients($paymentTypeId = null)
    {
        $clientSelect = new ClientSelect();
        $clientSelect->setFilterClientIds(App_Access::get('clients', 'finance>payment>payment_request>clients', array('noAutoAppend' => true)));

        if($paymentTypeId) {
            $clientSelect->setFilterPaymentType($paymentTypeId);
        }

        return $clientSelect->fetch();
    }

    /**
     * Возвращает список заявок по указанному типу оплаты и указанному клиенту в виде HTML-селекта
     * @param $paymentTypeId
     * @param $clientId
     * @param array $options [monthRange => (int), hasSum => (bool), hasNoSum => (bool)
     * @return string
     */
    public function getClaims($paymentTypeId, $clientId, array $options = null)
    {
        $claimSelect = new ClaimSelect();
        $claimSelect->setClientId($clientId);
        $claimSelect->setPaymentTypeId($paymentTypeId);
        $claimSelect->setOptions($options);

        return $claimSelect->getHtml();
    }

    /**
     * Возвращает права пользователя на операции с файлами по указанному запросу
     * @param App_Finance_Ticket_TicketInterface $ticket
     * @return array [read => (bool), write => (bool)]
     */
    public function getDocumentsAcl(Ticket $ticket)
    {
        return array(
            'read' => $ticket->getAcl()->hasAccessListDocuments(),
            'write' => $ticket->getAcl()->hasAccessAttachDocuments(),
        );
    }

    /**
     * Возвращает права пользователя на флаг "за отходы"
     * @param App_Finance_Ticket_TicketInterface $ticket
     * @return array
     */
    public function getWasteAcl(Ticket $ticket)
    {
        return array(
            'enabled' => $ticket->getAcl()->hasAccess('iswasteenabled')
        );
    }
}