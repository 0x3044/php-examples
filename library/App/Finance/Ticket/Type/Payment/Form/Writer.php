<?php
use App_Finance_Ticket_Type_Payment_Form_Data_Form as PaymentForm;
use App_Finance_Ticket_Type_Payment_Form_DataSource as DataSource;

class App_Finance_Ticket_Type_Payment_Form_Writer
{
    /**
     * @var PaymentForm
     */
    protected $_paymentForm;

    public function __construct(PaymentForm $paymentForm)
    {
        $this->_paymentForm = $paymentForm;
    }

    /**
     * @return \App_Finance_Ticket_Type_Payment_Form_Data_Form
     */
    public function getPaymentForm()
    {
        return $this->_paymentForm;
    }

    /**
     * Сохраняет данные об оплате и расшифровке
     */
    public function save()
    {
        $form = $this->getPaymentForm();
        $paymentTypeData = DataSource::getInstance()->getPaymentTypes()->getPaymentTypeById($form->getPaymentType());

        $this->_savePaymentData();

        if($paymentTypeData['details']) {
            $this->_savePaymentDetails($form);
        }else{
            $this->_destroyPaymentDetails();
        }

        $form->getTicket()->save(array(
            'enableTransaction' => false
        ));
    }

    /**
     * Сохраняет данные о расшифровке
     * @throws Exception
     */
    protected function _savePaymentDetails()
    {
        /** @var $dbTicketPaymentDetails App_Db_TicketPaymentDetails */
        $dbTicketPaymentDetails = App_Db::get(DB_TICKET_PAYMENT_DETAILS);
        $form = $this->getPaymentForm();

        $form->validate();
        $this->_destroyPaymentDetails();

        foreach($form->getDetails() as $detailsRecord) {
            foreach($detailsRecord->getClaimSumForms() as $claimSumRecord) {
                $dbTicketPaymentDetails->insert(array(
                    'ticket_id' => $form->getTicket()->getId(),
                    'client_id' => $detailsRecord->getClientId(),
                    'claim_id' => $claimSumRecord->getClaimId(),
                    'claim_sum' => $claimSumRecord->getClaimSum()
                ));
            }
        }
    }

    /**
     * Сохранение данных об оплате
     */
    protected function _savePaymentData()
    {
        /** @var $dbPayment App_Db_TicketPayment */
        $dbPayment = App_Db::get(DB_TICKET_PAYMENT);
        $form = $this->getPaymentForm();

        $paymentData = array(
            'payment_type' => $form->getPaymentType(),
            'sum' => $form->getSum(),
            'client_id' => $form->getClientId(),
            'annotation' => $form->getAnnotation(),
            'from_date' => $form->getFromDate()->toString(App_Db::ZEND_DATE_FORMAT),
            'document_number' => $form->getDocumentNumber(),
            'is_waste' =>  $form->getIsWaste()
        );

        $dbPayment->delete('ticket_id = '.(int) $form->getTicket()->getId());
        $dbPayment->insert(array('ticket_id' => $form->getTicket()->getId()) + $paymentData);
    }

    /**
     * Удаляет данные по платежу
     */
    public function destroy()
    {
        $this->_destroyPaymentData();
        $this->_destroyPaymentDetails();
    }

    /**
     * Удаляет данные по платежу
     */
    protected function _destroyPaymentData()
    {
        /** @var $dbPayment App_Db_TicketPayment */
        $dbPayment = App_Db::get(DB_TICKET_PAYMENT);
        $dbPayment->delete('ticket_id = '.(int) $this->getPaymentForm()->getTicket()->getId());
    }

    /**
     * Уничтожает данные о расшифровке
     */
    protected function _destroyPaymentDetails()
    {
        $paymentTicket = $this->getPaymentForm()->getTicket();

        /** @var $dbTicketPaymentDetails App_Db_TicketPaymentDetails */
        $dbTicketPaymentDetails = App_Db::get(DB_TICKET_PAYMENT_DETAILS);
        $dbTicketPaymentDetails->destroyByTicketId($paymentTicket->getId());
    }
}