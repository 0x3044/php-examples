<?php
use App_Finance_Ticket_Type_Payment_Form_Data_PaymentTypes as PaymentTypes;

class App_Finance_Ticket_Type_Payment_Form_DataSource_ClaimSelect
{
    /**
     * Id клиента
     * @var int
     */
    protected $_clientId;

    /**
     * Id типа оплаты
     * @var int
     */
    protected $_paymentTypeId;

    /**
     * Опции
     * @var array
     */
    protected $_options;

    /**
     * Задать Id клиента
     * @param int $clientId
     */
    public function setClientId($clientId)
    {
        $this->_clientId = $clientId;
    }

    /**
     * Возвращает Id клиента
     * @return int
     */
    public function getClientId()
    {
        return $this->_clientId;
    }

    /**
     * Задать опции
     * @param array $options
     */
    public function setOptions($options)
    {
        $this->_options = $options;
    }

    /**
     * Возвращает заданные опции
     * @return array
     */
    public function getOptions()
    {
        return $this->_options;
    }

    /**
     * Задать Id типа оплаты
     * @param int $paymentTypeId
     */
    public function setPaymentTypeId($paymentTypeId)
    {
        $this->_paymentTypeId = $paymentTypeId;
    }

    /**
     * Возвращает Id типа оплаты
     * @return int
     */
    public function getPaymentTypeId()
    {
        return $this->_paymentTypeId;
    }

    /**
     * Возвращает HTML-код
     * @return string
     */
    public function getHtml()
    {
        $paymentTypeId = $this->getPaymentTypeId();

        if($paymentTypeId == PaymentTypes::PAYMENT_TYPE_TRANSPORT_COSTS) {
            return $this->_getTransportOptions();
        } else {
            return $this->_getDefaultOptions();
        }
    }

    /**
     * Возвращает html-код селекта заявок
     * Используется стандартная логика из кассы
     * @return string
     */
    protected function _getDefaultOptions()
    {
        $clientId = $this->getClientId();
        $paymentTypeId = $this->getPaymentTypeId();
        $options = $this->getOptions();

        $monthRange = $options['monthRange'];
        $flagHasClaimSum = $options['flagHasClaimSum'];
        $flagNoClaimSum = $options['flagNoClaimSum'];
        $fromTime = $this->_getStartTime($monthRange);

        App_Spl_TypeCheck::getInstance()->positiveNumeric($clientId, $paymentTypeId);
        $paymentType = App_Finance_Ticket_Type_Payment_Form_DataSource::getInstance()->getPaymentTypes()->getPaymentTypeById($paymentTypeId);

        /** @var $cashModel Cash_Model_Extended_Abstract */
        $cashModel = Cash_Model_Extended_Abstract::factory($paymentType['id']);

        return $cashModel->getClaims($clientId, $fromTime, $flagHasClaimSum, $flagNoClaimSum, true);
    }

    /**
     * Возвращает html-код селекта заявок
     * Используется специфичная для данного модуля логика
     * @return string
     */
    protected function _getTransportOptions()
    {
        $clientId = $this->getClientId();
        $paymentTypeId = $this->getPaymentTypeId();
        $options = $this->getOptions();

        $monthRange = $options['monthRange'];
        $flagHasClaimSum = $options['flagHasClaimSum'];
        $flagNoClaimSum = $options['flagNoClaimSum'];
        $fromTime = $this->_getStartTime($monthRange);

        App_Spl_TypeCheck::getInstance()->positiveNumeric($clientId, $paymentTypeId);
        $paymentType = App_Finance_Ticket_Type_Payment_Form_DataSource::getInstance()->getPaymentTypes()->getPaymentTypeById($paymentTypeId);

        // Выбрать все заявки по данному клиенту
        // По которой есть/нет подтвержденных запросов в саппорте
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $ticketTypeId = implode(',', App_Finance_Recipient_Factory::getInstance()->createFromRecipientName('payment')->getTicketTypeIds());
        $archivedStatus = App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_ARCHIVED;

        $select = $db->select()
            ->distinct(true)
            ->from(DB_CLAIMS, array('cid' => 'claims.id', 'full_id'))
            ->joinLeft(DB_TICKET_PAYMENT_DETAILS, 'ticket_payment_details.claim_id = claims.id', array())
            ->joinLeft(DB_TICKET, "ticket.ticket_type_id IN ({$ticketTypeId}) AND ticket.status = {$archivedStatus}", array())
            ->where('claims.client_id = ?', $clientId)
            ->where('claims.car = 1 or claims.tk = "1"')
            ->order('claims.id DESC')
        ;

        if($flagHasClaimSum && !($flagNoClaimSum)) {
            $select->where('ticket_payment_details.id IS NOT NULL and ticket.id IS NOT NULL');
        }else if(!($flagHasClaimSum) && $flagNoClaimSum) {
            $select->where('ticket_payment_details.id IS NULL');
        }

        // Фильтрации по времени
        if($fromTime > 0) {
            $select->where('claims.date >= ?', $fromTime);
        }

        // Создание инпута
        $html = '';

        foreach($db->fetchAll($select) as $row) {
            $claimId = $row->cid;
            $claimFullId = $row->full_id;

            $html .= <<<HTML
                <option value="{$claimId}">{$claimFullId}</option>
HTML;
        }

        return $html;
    }

    /**
     * Возвращает timestamp, с какого времени брать заявки
     * @param $monthRange
     * @return int|string
     * @throws Exception
     */
    protected function _getStartTime($monthRange)
    {
        if(is_null($monthRange)) {
            $monthRange = 3;
        }

        if(!(in_array($monthRange, array(0, 1, 3, 6, 9)))) {
            throw new \Exception('Вы можете указать для выборки промежуток только до 9, 6, 3 и до 1 месяца');
        }

        $fromTime = 0;
        $monthRange = (int) $monthRange;

        if($monthRange > 0) {
            $date = new Zend_Date();
            $date->setHour(0)->setMinute(0)->setSecond(0)->sub($monthRange, 'MM');

            $fromTime = $date->getTimestamp();
        }

        return $fromTime;
    }
}