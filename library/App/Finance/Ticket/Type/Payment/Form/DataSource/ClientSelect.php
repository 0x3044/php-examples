<?php
use App_Finance_Ticket_Type_Payment_Form_Data_PaymentTypes as PaymentTypes;

class App_Finance_Ticket_Type_Payment_Form_DataSource_ClientSelect
{
    /**
     * @var int
     */
    protected $_filterPaymentType = NULL;

    /**
     * @var array
     */
    protected $_filterClientIds = array();

    public function fetch()
    {
        /** @var $clientDbTable App_Db_Clients */
        $clientDbTable = App_Db::get(DB_CLIENTS);
        $filterClientIds = $this->getFilterClientIds();
        $filterPaymentType = $this->getFilterPaymentType();

        $select = $clientDbTable->select();
        $select->from('clients', array('id', 'name' => 's_title'))->order('name ASC');

        if(is_array($filterClientIds)) {
            if(count($filterClientIds)) {
                $select->where('clients.id IN ('.implode(',', $filterClientIds).')');
            }else{
                $select->where('0 = 1');
            }

        }

        if($filterPaymentType) {
            switch($filterPaymentType) {
                default:
                    throw new \Exception("Unknown payment type `{$filterPaymentType}`");

                case NULL:
                case PaymentTypes::PAYMENT_TYPE_BUY:
                case PaymentTypes::PAYMENT_RETURN:
                    break;

                case PaymentTypes::PAYMENT_TYPE_TRANSPORT_COSTS:
                    $select->where("`type` LIKE '____1%'");
                    break;

                case PaymentTypes::PAYMENT_TYPE_CONSUMABLES:
                    $select->where("`type` LIKE '____1%'");
                    break;

                case PaymentTypes::PAYMENT_TYPE_OTHER:
                    $select->where("`type` LIKE '________1%'");
                    break;
            }
        }

        $result = $clientDbTable->fetchAll($select)->toArray();

        return $result;
    }

    /**
     * @param array $filterClientIds
     */
    public function setFilterClientIds($filterClientIds)
    {
        $this->_filterClientIds = $filterClientIds;
    }

    /**
     * @return array
     */
    public function getFilterClientIds()
    {
        return $this->_filterClientIds;
    }

    /**
     * @param int $filterPaymentType
     */
    public function setFilterPaymentType($filterPaymentType)
    {
        $this->_filterPaymentType = $filterPaymentType;
    }

    /**
     * @return int
     */
    public function getFilterPaymentType()
    {
        return $this->_filterPaymentType;
    }
}