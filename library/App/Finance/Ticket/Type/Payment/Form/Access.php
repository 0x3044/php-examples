<?php
class App_Finance_Ticket_Type_Payment_Form_Access
{
    /**
     * Возвращает true, если пользователь имеет право на создание запросов для данного клиента
     * @param $clientId
     * @return bool
     */
    public static function hasCreateAccessForClient($clientId)
    {
        $ids = array();
        $clients = App_Finance_Ticket_Type_Payment_Form_DataSource::getInstance()->getAvailableClients();

        if(is_array($clients) && count($clients)) {
            foreach($clients as $client) {
                $ids[] = $client['id'];
            }
        }

        return in_array($clientId, $ids);
    }
}