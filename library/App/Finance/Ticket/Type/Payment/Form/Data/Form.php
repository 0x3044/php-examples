<?php
use App_Finance_Ticket_Type_Payment_Request_Ticket as PaymentRequestTicket;
use App_Finance_Ticket_Type_Payment_Form_Data_DetailsForm as Details;

class App_Finance_Ticket_Type_Payment_Form_Data_Form
{
    /**
     * Запрос, с которым работает форма
     * @var PaymentRequestTicket
     */
    protected $_ticket;

    /**
     * Расшифровка
     * @var Details[]
     */
    protected $_details;

    /**
     * Сумма
     * @var float
     */
    protected $_sum;

    /**
     * Id клиента
     * @var int
     */
    protected $_clientId;

    /**
     * Тип оплаты
     * @var int
     */
    protected $_paymentType;

    /**
     * Номер документа
     * @var string
     */
    protected $_documentNumber;

    /**
     * От даты
     * @var Zend_Date
     */
    protected $_fromDate;

    /**
     * Примечание
     * @var string
     */
    protected $_annotation;

    /**
     * Флаг "за отходы"
     * @var string
     */
    protected $_isWaste;

    /**
     * Проверка на права пользователя
     * @var bool
     */
    protected $_aclEnabled = false;

    /**
     * Список клиентов, доступных для данного пользователя (кэш)
     * @var array|null
     */
    protected static $_cachedClients;

    /**
     * Форма создания запроса
     * @param App_Finance_Ticket_Type_Payment_Request_Ticket $ticket
     */
    public function __construct(PaymentRequestTicket $ticket)
    {
        $this->_ticket = $ticket;
        $this->_fromDate = new Zend_Date();

        if($ticket->isExistedTicket()) {
            $reader = new App_Finance_Ticket_Type_Payment_Form_Reader();
            $reader->formFromTicket($this, $ticket);
        }
    }

    /**
     * @return \App_Finance_Ticket_Type_Payment_Request_Ticket
     */
    public function getTicket()
    {
        return $this->_ticket;
    }

    /**
     * Возвращает true, если запрос имеет расшифровку
     * @return bool
     */
    public function detailsExists()
    {
        return $this->getTicket()->isExistedTicket();
    }

    /**
     * Валидация формы
     * @return bool
     */
    public function validate()
    {
        $validator = new App_Finance_Ticket_Type_Payment_Form_Data_Validator();
        return $validator->validate($this, $this->isAclEnabled());
    }

    /**
     * Очистить список расшифровки
     */
    public function clearDetails()
    {
        $this->_details = array();
    }

    /**
     * Включить проверку на права пользователя
     */
    public function enableAcl()
    {
        $this->_aclEnabled = true;
    }

    /**
     * Отключить проверку на права пользователя
     */
    public function disableAcl()
    {
        $this->_aclEnabled = false;
    }

    /**
     * Возвращает true, если для формы включена пров    ерка на права пользователя
     * @return bool
     */
    public function isAclEnabled()
    {
        return $this->_aclEnabled;
    }

    /**
     * Конвертация формы в массив
     * @param bool $additionalTicketData
     * @return array
     */
    public function toArray($additionalTicketData = false)
    {
        $clientId = $this->getClientId();
        $clientName = '';

        if($clientId > 0) {
            /** @var $dbClient App_Db_Clients */
            try {
                $dbClient = App_Db::get(DB_CLIENTS);
                $clientName = $dbClient->getClientName($clientId);
            }catch (\Exception $e){
                $clientName = null;
            }
        }

        $result = array(
            'clientId' => $this->getClientId(),
            'clientName' => $clientName,
            'paymentType' => $this->getPaymentType(),
            'annotation' => $this->getAnnotation(),
            'sum' => $this->getSum(),
            'documentNumber' => $this->getDocumentNumber(),
            'fromDate' => $this->getFromDate()->toString(App_Db::ZEND_DATE_RU_FORMAT),
            'isWaste' => $this->getIsWaste(),
            'details' => array()
        );

        if($additionalTicketData && $this->getTicket()->isExistedTicket()) {
            $files = array();

            /** @var $document App_Finance_Ticket_Component_Document */
            foreach($this->getTicket()->getDocumentsHandler()->getDocuments() as $document) {
                $fileHelper = new App_Filemanager_Preview_FileTypeDetector($document->getFilePath());

                $files[] = array(
                    'id' => $document->getRow()->getId(),
                    'name' => $document->getFileName(),
                    'size' => $document->getSize(),
                    'url' => $document->getDownloadLink(),
                    'real' => $document->getRealPath(),
                    'browsable' => $fileHelper->isBrowsable(),
                    'browsableType' => $fileHelper->getBrowsableType()
                );
            }

            $result['files'] = $files;
        }

        if(is_array($this->_details) && count($this->_details)) {
            /** @var $details Details */
            foreach($this->_details as $details) {
                $result['details'][] = $details->toArray();
            }
        }

        return $result;
    }

    /**
     * Установить примечание
     * @param string $annotation
     */
    public function setAnnotation($annotation)
    {
        $this->_annotation = $annotation;
    }

    /**
     * Возвращает примечание
     * @return string
     */
    public function getAnnotation()
    {
        return $this->_annotation;
    }

    /**
     * Установить Id клиента
     * @param int $clientId
     */
    public function setClientId($clientId)
    {
        $this->_clientId = $clientId;
    }

    /**
     * Возвращает Id клиента
     * @return int
     */
    public function getClientId()
    {
        return $this->_clientId;
    }

    /**
     * Установить тип оплаты
     * @param int $paymentType
     */
    public function setPaymentType($paymentType)
    {
        $this->_paymentType = $paymentType;
    }

    /**
     * Возвращает тип оплаты
     * @return int
     */
    public function getPaymentType()
    {
        return $this->_paymentType;
    }

    /**
     * Установить сумму
     * @param float $sum
     * @throws Exception
     */
    public function setSum($sum)
    {
        $this->_sum = $sum;
    }

    /**
     * Возвращает сумму
     * @return float
     */
    public function getSum()
    {
        return $this->_sum;
    }

    /**
     * @param string $documentNumber
     */
    public function setDocumentNumber($documentNumber)
    {
        $this->_documentNumber = $documentNumber;
    }

    /**
     * @return string
     */
    public function getDocumentNumber()
    {
        return $this->_documentNumber;
    }

    /**
     * @param \Zend_Date|string $fromDate
     */
    public function setFromDate($fromDate)
    {
        if(is_string($fromDate)) {
            $fromDate = new Zend_Date($fromDate, App_Db::ZEND_DATE_FORMAT);
        }

        $this->_fromDate = $fromDate;
    }

    /**
     * @return \Zend_Date
     */
    public function getFromDate()
    {
        return $this->_fromDate;
    }

    /**
     * @param string $isWaste
     */
    public function setIsWaste($isWaste)
    {
        $this->_isWaste = $isWaste;
    }

    /**
     * @return string
     */
    public function getIsWaste()
    {
        return $this->_isWaste;
    }

    /**
     * Возвращает расшифровку
     * @return \App_Finance_Ticket_Type_Payment_Form_Data_DetailsForm[]
     */
    public function &getDetails()
    {
        return $this->_details;
    }

    /**
     * Добавить расшифровку
     * @param App_Finance_Ticket_Type_Payment_Form_Data_DetailsForm $details
     */
    public function addDetails(Details $details)
    {
        $this->_details[] = $details;
    }
}