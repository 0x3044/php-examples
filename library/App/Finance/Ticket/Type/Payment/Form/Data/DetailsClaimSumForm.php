<?php

/**
 * Подформа номер <заявки - сумма счета>, используется в расшифровке
 */
class App_Finance_Ticket_Type_Payment_Form_Data_DetailsClaimSumForm
{
    /**
     * Id заявки
     * @var int
     */
    protected $_claimId;

    /**
     * Сумма по заявке
     * @var float
     */
    protected $_claimSum;

    /**
     * Установить Id заявки
     * @param int $claimId
     */
    public function setClaimId($claimId)
    {
        $this->_claimId = $claimId;
    }

    /**
     * Возвращает Id заяки
     * @return int
     */
    public function getClaimId()
    {
        return $this->_claimId;
    }

    /**
     * Установить сумму по заявки
     * @param float $claimSum
     */
    public function setClaimSum($claimSum)
    {
        $this->_claimSum = $claimSum;
    }

    /**
     * Возвращает сумма по заявке
     * @return float
     */
    public function getClaimSum()
    {
        return $this->_claimSum;
    }

    /**
     * Конвертация в массив
     * @return array
     */
    public function toArray()
    {
        return array(
            'claimId' => $this->getClaimId(),
            'claimSum' => $this->getClaimSum()
        );
    }
}