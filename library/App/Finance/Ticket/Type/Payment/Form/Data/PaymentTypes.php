<?php
class App_Finance_Ticket_Type_Payment_Form_Data_PaymentTypes
{
    const PAYMENT_TYPE_BUY = 1;
    const PAYMENT_TYPE_TRANSPORT_COSTS = 4;
    const PAYMENT_TYPE_OTHER = 8;
    const PAYMENT_TYPE_CONSUMABLES = 11;
    const PAYMENT_RETURN = 3;

    /**
     * Типы оплаты
     * @var array
     */
    protected static $_paymentTypes = array(
        self::PAYMENT_TYPE_BUY =>
            array(
                'id' => self::PAYMENT_TYPE_BUY,
                'name' => 'buy',
                'title' => 'Покупка',
                'details' => false,
                'acl' => 'buy',
                'onlyOwnCar' => false
            ),

        self::PAYMENT_TYPE_TRANSPORT_COSTS =>
            array(
                'id' => self::PAYMENT_TYPE_TRANSPORT_COSTS,
                'name' => 'transport_costs',
                'title' => 'Транспортные расходы',
                'details' => true,
                'acl' => 'transport_costs',
                'onlyOwnCar' => true),

        self::PAYMENT_TYPE_OTHER =>
            array(
                'id' => self::PAYMENT_TYPE_OTHER,
                'name' => 'other',
                'title' => 'Прочее',
                'details' => false,
                'acl' => 'other',
                'onlyOwnCar' => false),

        self::PAYMENT_TYPE_CONSUMABLES =>
            array(
                'id' => self::PAYMENT_TYPE_CONSUMABLES,
                'name' => 'consumables',
                'title' => 'Расходники на товар',
                'details' => true,
                'acl' => 'consumables',
                'onlyOwnCar' => false),

        self::PAYMENT_RETURN =>
            array(
                'id' => self::PAYMENT_RETURN,
                'name' => 'return',
                'title' => 'Возврат денежных средств',
                'details' => false,
                'acl' => 'return',
                'onlyOwnCar' => false
            ),
    );

    /**
     * Возвращае всем типы оплаты
     * @return array
     */
    public function getAvailablePaymentTypes() {
        return self::$_paymentTypes;
    }

    /**
     * Возвращает описание типа оплаты по Id
     * @param $paymentTypeId
     * @return array
     * @throws Exception
     */
    public function getPaymentTypeById($paymentTypeId) {
        App_Spl_TypeCheck::getInstance()->positiveNumeric($paymentTypeId);

        if(!($this->isPaymentTypeIdExists($paymentTypeId))) {
            throw new \Exception("Unknown payment type `{$paymentTypeId}`");
        }

        return self::$_paymentTypes[$paymentTypeId];
    }

    /**
     * Возвращает описания типа оплаты по имени
     * @param $paymentTypeName
     * @return array
     * @throws Exception
     */
    public function getPaymentTypeByName($paymentTypeName) {
        foreach(self::$_paymentTypes as $paymentTypeData) {
            if($paymentTypeData['name'] == $paymentTypeName) {
                return $paymentTypeData;
            }
        }

        throw new \Exception("Unknown payment type `{$paymentTypeName}`");
    }

    /**
     * Возвращает true, если тип оплаты с указанным Id существует
     * @param $paymentTypeId
     * @return bool
     */
    public function isPaymentTypeIdExists($paymentTypeId) {
        return array_key_exists($paymentTypeId, self::$_paymentTypes);
    }

    /**
     * Возвращает true, если пользователь имеет право просматривать запросы указанного типа оплаты
     * @param $paymentTypeAcl
     * @return bool
     * @throws Exception
     */
    public function hasAccessReadPaymentType($paymentTypeAcl)
    {
        if(is_numeric($paymentTypeAcl)) {
            $paymentType = $this->getPaymentTypeById($paymentTypeAcl);

            $paymentTypeAcl = $paymentType['acl'];
        }

        return App_Access::get('access', 'finance>payment>payment_request>types>'.$paymentTypeAcl) == App_Access_Field_Type2::ACCESS_VIEW;
    }

    /**
     * Возвращает true, если пользователь имеет право создавать запросы указанного типа оплаты
     * @param $paymentTypeAcl
     * @return bool
     * @throws Exception
     */
    public function hasAccessWritePaymentType($paymentTypeAcl)
    {
        if(is_numeric($paymentTypeAcl)) {
            $paymentType = $this->getPaymentTypeById($paymentTypeAcl);

            $paymentTypeAcl = $paymentType['acl'];
        }

        return App_Access::get('access', 'finance>payment>payment_request>types>'.$paymentTypeAcl) == App_Access_Field_Type2::ACCESS_EDIT;
    }
}