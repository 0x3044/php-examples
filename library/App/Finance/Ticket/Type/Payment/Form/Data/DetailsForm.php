<?php
use App_Finance_Ticket_Type_Payment_Form_Data_DetailsClaimSumForm as ClaimSumForm;

/**
 * Расшифровка запроса
 */
class App_Finance_Ticket_Type_Payment_Form_Data_DetailsForm
{
    /**
     * Id клиента
     * @var int
     */
    protected $_clientId;

    /**
     * Заявки-суммы
     * @var ClaimSumForm[]
     */
    protected $_claimSumForms = array();

    /**
     * Установить Id клиента
     * @param int $clientId
     */
    public function setClientId($clientId)
    {
        $this->_clientId = $clientId;
    }

    /**
     * Возвращает Id клиента
     * @return int
     */
    public function getClientId()
    {
        return $this->_clientId;
    }

    /**
     * Возвращает формы заявка-сумма
     * @return \App_Finance_Ticket_Type_Payment_Form_Data_DetailsClaimSumForm[]
     */
    public function &getClaimSumForms()
    {
        return $this->_claimSumForms;
    }

    /**
     * Добавить форму заявка-сумма
     * @param App_Finance_Ticket_Type_Payment_Form_Data_DetailsClaimSumForm $form
     */
    public function addClaimSumForm(ClaimSumForm $form)
    {
        $this->_claimSumForms[] = $form;
    }

    /**
     * Конвертация в массив
     * @return array
     */
    public function toArray()
    {
        $clientName = null;
        $clientId = $this->getClientId();

        if($clientId > 0) {
            /** @var $dbClient App_Db_Clients */
            try {
                $dbClient = App_Db::get(DB_CLIENTS);
                $clientName = $dbClient->getClientName($clientId);
            }catch (\Exception $e){
                $clientName = null;
            }
        }

        $result = array(
            'clientId' => $clientId,
            'clientName' => $clientName,
            'claims' => array()
        );

        if(count($this->_claimSumForms) && count($this->_claimSumForms)) {
            foreach($this->_claimSumForms as $claimForm) {
                $result['claims'][] = $claimForm->toArray();
            }
        }

        return $result;
    }
}