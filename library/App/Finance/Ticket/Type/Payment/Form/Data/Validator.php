<?php
use App_Finance_Ticket_Type_Payment_Form_Data_Form as Form;

class App_Finance_Ticket_Type_Payment_Form_Data_Validator
{
    /**
     * Возвращает true, если форма валидна, либо выбрасывает исключение в ином случае
     * @param App_Finance_Ticket_Type_Payment_Form_Data_Form $form
     * @param bool $enableAcl
     * @throws Exception
     * @return bool
     */
    public function validate(Form $form, $enableAcl = false)
    {
        $paymentTypes = App_Finance_Ticket_Type_Payment_Form_DataSource::getInstance()->getPaymentTypes();

        if(!(is_float($form->getSum())) || $form->getSum() < 0) {
            throw new \Exception('Сумма должна быть положительным числом');
        }

        if(!($paymentTypes->isPaymentTypeIdExists($form->getPaymentType()))) {
            throw new \Exception('Неизвестный тип оплаты');
        }

        /** @var $clientDbTable App_Db_Clients */
        $clientDbTable = App_Db::get(DB_CLIENTS);
        $clientId = $form->getClientId();

        App_Spl_TypeCheck::getInstance()->positiveNumeric($clientId);

        if($clientData = $clientDbTable->getClientById($clientId)) {
            if($enableAcl) {
                $hasCreateAccess = App_Finance_Ticket_Type_Payment_Form_Access::hasCreateAccessForClient($form->getClientId());

                if(!($hasCreateAccess)) {
                    throw new \Exception('Вы не имеете прав создавать запрос на оплату для указанного клиента');
                }
            }
        } else {
            throw new \Exception("Отсутствует клиент с id:`{$clientId}`");
        }

        if($form->getDetails() && count($form->getDetails())) {
            foreach($form->getDetails() as $details) {
                $this->validateDetails($details, $enableAcl);
            }
        }

        return true;
    }

    /**
     * Валидаци расшифровки
     * @param App_Finance_Ticket_Type_Payment_Form_Data_DetailsForm $details
     * @param bool $enableAcl
     * @throws Exception
     */
    public function validateDetails(App_Finance_Ticket_Type_Payment_Form_Data_DetailsForm $details, $enableAcl = false) {
        App_Spl_TypeCheck::getInstance()->positiveNumeric($details->getClientId());

        $hasCreateAccess = App_Finance_Ticket_Type_Payment_Form_Access::hasCreateAccessForClient($details->getClientId());

        if($enableAcl) {
            if(!($hasCreateAccess)) {
                throw new \Exception('Вы не имеете прав создавать запрос на оплату для указанного клиента');
            }
        }

        if($details->getClaimSumForms() && count($details->getClaimSumForms())) {
            foreach($details->getClaimSumForms() as $claimSumForm) {
                $this->validateClaimSumForm($claimSumForm);
            }
        }
    }

    /**
     * Валидация суммы по заявке
     * @param App_Finance_Ticket_Type_Payment_Form_Data_DetailsClaimSumForm $claimSumForm
     * @throws Exception
     */
    public function validateClaimSumForm(App_Finance_Ticket_Type_Payment_Form_Data_DetailsClaimSumForm $claimSumForm)
    {
        App_Spl_TypeCheck::getInstance()->positiveNumeric($claimSumForm->getClaimId());

        if(!(is_float($claimSumForm->getClaimSum()) || $claimSumForm->getClaimSum() <= 0)) {
            throw new \Exception('Сумма должна быть больше нуля');
        }
    }
}