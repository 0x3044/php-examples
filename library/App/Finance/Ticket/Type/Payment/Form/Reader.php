<?php
use App_Finance_Ticket_Type_Payment_Form_Data_Form as PaymentForm;
use App_Finance_Ticket_Type_Payment_Form_Data_DetailsForm as Details;
use App_Finance_Ticket_Type_Payment_Form_Data_DetailsClaimSumForm as ClaimSum;

class App_Finance_Ticket_Type_Payment_Form_Reader
{
    /**
     * Создает форму, заполняет ее данными из массива и возвращает ее
     * @param App_Finance_Ticket_Type_Payment_Form_Data_Form $form
     * @param array $formData
     * @throws Exception
     * @return App_Finance_Ticket_Type_Payment_Form_Data_Form
     */
    public function formFromArray(PaymentForm $form, array $formData)
    {
        foreach(array('clientId', 'sum', 'paymentType', 'annotation') as $required) {
            if(!(array_key_exists($required, $formData))) {
                throw new \Exception("Произошла ошибка: не удалось получить все требуемые данные формы");
            }
        }

        $form->setClientId((int) $formData['clientId']);
        $form->setSum((float) $formData['sum']);
        $form->setPaymentType((int) $formData['paymentType']);
        $form->setAnnotation($formData['annotation']);
        $form->setFromDate($formData['fromDate']);
        $form->setDocumentNumber($formData['documentNumber']);
        $form->setIsWaste((int) $formData['isWaste']);

        if(isset($formData['details']) && is_array($formData['details']) && count($formData['details'])) {
            foreach($formData['details'] as $n => $detailsData) {
                $detailsForm = new Details();

                foreach(array('clientId', 'claims') as $required) {
                    if(!(array_key_exists($required, $detailsData))) {
                        throw new \Exception("FormData[details][$n]: `{$required}` required");
                    }
                }

                $detailsForm->setClientId((int) $detailsData['clientId']);

                if(is_array($detailsData['claims']) && count($detailsData['claims'])) {
                    foreach($detailsData['claims'] as $m => $claimSumData) {
                        foreach(array('claimId', 'claimSum') as $required) {
                            if(!(array_key_exists($required, $claimSumData))) {
                                throw new \Exception("FormData[details][{$n}][claims][{$m}][claimSum]: `{$required}` required");
                            }
                        }

                        $claimSumSubForm = new ClaimSum();
                        $claimSumSubForm->setClaimId((int) $claimSumData['claimId']);
                        $claimSumSubForm->setClaimSum((float) $claimSumData['claimSum']);

                        $detailsForm->addClaimSumForm($claimSumSubForm);
                    }
                } else {
                    throw new \Exception("FormData[details]: no claims sum data available for #{$n}(clientId: {$detailsForm->getClientId()})");
                }

                $form->addDetails($detailsForm);
            }
        }

        return $form;
    }

    /**
     * Создает форму из существующего запроса на оплату
     * * @param App_Finance_Ticket_Type_Payment_Form_Data_Form $form
     * @param App_Finance_Ticket_Type_Payment_Request_Ticket $ticket
     * @return App_Finance_Ticket_Type_Payment_Form_Data_Form
     */
    public function formFromTicket(PaymentForm $form, App_Finance_Ticket_Type_Payment_Request_Ticket $ticket)
    {
        /** @var App_Db_TicketPayment $ticketPaymentTable */
        $ticketPaymentTable = App_Db::get(DB_TICKET_PAYMENT);

        // Платежные данные
        if($paymentData = $ticketPaymentTable->getTicketPayment($ticket->getId())) {
            $filter = new Zend_Filter_Word_UnderscoreToCamelCase();

            // underscore to camelcase
            $paymentData = $this->_underscoreToCamecaseRecursive($paymentData);

            // setUp form
            self::formFromArray($form, $paymentData);
        }

        // Детализация
        $paymentData['details'] = array();

        if($paymentDetailsData = $ticketPaymentTable->getTicketPaymentDetails($ticket->getId())) {
            foreach($paymentDetailsData as $details) {
                $clientId = (int) $details['client_id'];

                if(!(isset($paymentData['details'][$clientId]))) {
                    $paymentData['details'][$clientId] = array(
                        'clientId' => $clientId,
                        'clientName' => $details['client_name'],
                        'claims' => array()
                    );
                }

                $paymentData['details'][$clientId]['claims'][] = array(
                    'claimId' => (int) $details['claim_id'],
                    'claimSum' => bcadd(0, (float) $details['claim_sum'], 2)
                );
            }

            // destroy hash kets
            $paymentData['details'] = array_values($paymentData['details']);
        }

        $this->formFromArray($form, $paymentData);
    }

    /**
     * Under_score > CamelKeys для ключей массива
     * @param array $input
     * @return array
     */
    protected function _underscoreToCamecaseRecursive(array $input)
    {
        $filter = new Zend_Filter_Word_UnderscoreToCamelCase();
        $filteredPaymentData = array();

        foreach($input as $key => $value) {
            $key = lcfirst($filter->filter($key));

            if(is_array($value)) {
                $value = $this->_underscoreToCamecaseRecursive($value);
            }

            $filteredPaymentData[$key] = $value;
        }

        return $filteredPaymentData;
    }
}