<?php
use App_Finance_Ticket_Type_Accounting_Abstract as AbstractAccountingTicket;

class App_Finance_Ticket_Type_Accounting_ActOfReconciliation_Ticket extends AbstractAccountingTicket
{
    /**
     * {@inheritdoc}
     * @return string
     */
    public function getTicketTypeName()
    {
        return 'accounting_act_of_reconciliation';
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function getRecipientNames()
    {
        return array('accounting');
    }

    /**
     * {@inheritdoc}
     * @return bool|mixed
     */
    public function attachmentsAllowed()
    {
        return true;
    }
}