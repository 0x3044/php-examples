<?php
use App_Finance_Ticket_Type_Abstract_Ticket as AbstractTicket;
use App_Finance_Ticket_Component_Validation_Type_CashAccountOnly_Validator as CashAccountOnlyValidator;
use App_Finance_Ticket_Component_Validation_Type_InvoicesAvailable_Validator as InvoicesValidator;
use App_Finance_Ticket_Component_Observer_Type_AccountingDocuments_Observer as ArchiveOnHasUploadedDocumentsObserver;
use App_Finance_Ticket_Component_Observer_Type_ServiceNote_Observer as ServiceNoteObserver;
use App_Finance_Ticket_Component_Observer_Type_ExplanatoryNote_Observer as ExplanatoryNoteObserver;
use App_Finance_Ticket_Component_Observer_Type_MailingServiceNote_Observer as MailingServiceNoteObserver;
use App_Finance_Ticket_Component_Validation_Type_ExplanatoryNote_Validator as ExplanatoryNoteValidator;

abstract class App_Finance_Ticket_Type_Accounting_Abstract extends AbstractTicket
{
    /**
     * Код в данном методе запускаетсяя после построение объекта.
     * Наблюдатели отключены.
     */
    protected function _init()
    {
        $this->getObserversHandler()->add(new ArchiveOnHasUploadedDocumentsObserver());
        $this->getObserversHandler()->add(new ServiceNoteObserver());

        $this->getObserversHandler()->add(new MailingServiceNoteObserver());

        $this->getValidationObserver()->getValidationHandler()->add(new CashAccountOnlyValidator());
        $this->getValidationObserver()->getValidationHandler()->add(new InvoicesValidator());

        $this->getObserversHandler()->add(new ExplanatoryNoteObserver());
        $this->getValidationObserver()->getValidationHandler()->add(new ExplanatoryNoteValidator());
    }

    /**
     * Возвращает основного получателя
     * @return string
     */
    public function getMainRecipient()
    {
        return "accounting";
    }
}
