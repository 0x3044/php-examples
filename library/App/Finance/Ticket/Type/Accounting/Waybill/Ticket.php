<?php
use App_Finance_Ticket_Type_Accounting_Abstract as AbstractAccountingTicket;

class App_Finance_Ticket_Type_Accounting_Waybill_Ticket extends AbstractAccountingTicket
{
    /**
     * {@inheritdoc}
     * @return string
     */
    public function getTicketTypeName()
    {
        return 'accounting_waybill';
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function getRecipientNames()
    {
        return array('accounting');
    }

    /**
     * {@inheritdoc}
     * @return bool|mixed
     */
    public function attachmentsAllowed()
    {
        return true;
    }
}