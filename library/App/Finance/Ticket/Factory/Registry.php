<?php
use App_Finance_Ticket_TicketInterface as Ticket;

class App_Finance_Ticket_Factory_Registry
{
    /**
     * Уже загруженные запросы
     * @var Ticket[]
     */
    protected $_tickets = array();

    /**
     * Возвращает запрос по его Id
     * Если запрос уже загружен
     * @param $ticketId
     * @return \App_Finance_Ticket_TicketInterface
     */
    public function get($ticketId)
    {
        if(!(isset($this->_tickets[$ticketId]))) {
            $this->_tickets[$ticketId] = App_Finance_Ticket_Factory::getInstance()->createFromTicketId($ticketId);
        }

        return $this->_tickets[$ticketId];
    }

    /**
     * Сохраняет в реестре загруженную запись
     * @param App_Finance_Ticket_TicketInterface $ticket
     */
    public function save(Ticket $ticket)
    {
        $this->_tickets[$ticket->getId()] = $ticket;
    }
}