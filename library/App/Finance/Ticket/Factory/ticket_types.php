<?php
/**
 * Список ТИП_ТИКЕТА(строка) => КЛАСС_ТИКЕТА для фабрики App_Finance_Ticket_Factory
 */
return array(
    'accounting_act_of_reconciliation' => 'App_Finance_Ticket_Type_Accounting_ActOfReconciliation_Ticket',
    'accounting_waybill' => 'App_Finance_Ticket_Type_Accounting_Waybill_Ticket',
    'dispatcher_verify' => 'App_Finance_Ticket_Type_Dispatcher_Verify_Ticket',
    'depot_verify' => 'App_Finance_Ticket_Type_Depot_Verify_Ticket',
    'payment_request' => 'App_Finance_Ticket_Type_Payment_Request_Ticket'
);