<?php
use App_Finance_Ticket_Component_Document_Collection as DocumentsCollection;
use App_Finance_Ticket_Component_Document as Document;
use App_Finance_Ticket_TicketInterface as Ticket;

class App_Finance_Ticket_Component_Document_Handler
{
    /**
     * Список прикрепленных документов
     * @var DocumentsCollection
     */
    protected $_documents;

    /**
     * Тикет, к котором прикреплены документы
     * @var Ticket
     */
    protected $_ticket;

    /**
     * Флаг "список документов тикета получен"
     * @var bool
     */
    protected $_wasPulled = false;

    /**
     * DocumentHandler, управление документами, прикрепленными к тикету
     * @param App_Finance_Ticket_TicketInterface $ticket
     */
    public function __construct(Ticket $ticket)
    {
        $this->_ticket = $ticket;
        $this->_documents = new DocumentsCollection();
    }

    /**
     * Возвращает список прикрепленных к тикету документов в виде коллекции
     * @return App_Finance_Ticket_Component_Document_Collection
     */
    public function getDocuments()
    {
        if(!($this->_wasPulled)) {
            $this->pull();
        }

        return $this->_documents;
    }

    /**
     * Получает список документов из базы данных
     */
    public function pull()
    {
        $ticket = $this->getTicket();
        $documents = $this->_documents;

        if($ticket->getId()) {
            $documents->clear();
            $dbTable = App_Db::get(DB_TICKET_DOCUMENT);
            $select = $dbTable->select();
            $select->where('ticket_id = ?', (int) $ticket->getId());
            $records = $dbTable->fetchAll($select);

            if($records && count($records)) {
                foreach($records as $record) {
                    $documents->add(new Document($record));
                }
            }
        }

        $this->_wasPulled = true;
    }

    /**
     * Сохраняет информацию о всех документах, которые есть в коллекции
     */
    public function push()
    {
        $ticket = $this->getTicket();

        /** @var $document Document */
        foreach($this->getDocuments() as $document) {
            $ticket->getObserversHandler()->on('onBeforeDocumentSave');
            $document->save();
            $ticket->getObserversHandler()->on('onAfterDocumentSave');
        }
    }

    /**
     * Создает и возврашает документ, готовый для загрузки и прикрепления к нему файла
     * @return Document
     */
    public function createDocument()
    {
        $document = new Document();
        $document->setUploadTicket($this->getTicket());

        return $this->getDocuments()->add($document);
    }

    /**
     * Удаляет документ по его Id
     * @param int $documentId
     */
    public function removeDocument($documentId)
    {
        $document = $this->getDocuments()->getDocumentById($documentId);
        $this->getDocuments()->remove($document);
        $document->remove();
    }

    /**
     * Удаляет все документы, привязанные к тикету
     */
    public function destroyDocuments()
    {
        $documents = $this->getDocuments();

        if($documents->size()) {
            /** @var $item Document */
            while($item = $documents->eachItem()) {
                $item->remove();
            }

            $documents->clear();
        }
    }

    /**
     * Returnss ticket
     * @return App_Finance_Ticket_TicketInterface
     */
    public function getTicket()
    {
        return $this->_ticket;
    }
}