<?php
use App_Finance_Ticket_Component_Document_PathStrategy_Abstract as AbstractPathStrategy;

/**
 * Класс для получения пути к директории для сохранения документов тикета
 */
class App_Finance_Ticket_Component_Document_PathStrategy_Payment extends AbstractPathStrategy
{
    protected $_pathData = array();

    /**
     * Возвращает путь к директории для сохранения прикрепленных к запросу документов
     * @param array $options
     * @return string
     */
    public function getPath(array $options = NULL)
    {
        $options = array_merge(array(
            'realPath' => false,
            'allocate' => false,
        ), (array) $options);

        $pathData = $this->_getPathData();

        $path = 'payment/'.$pathData['year'].'/'.$pathData['month'].'/'.$pathData['id'];

        if($options['realPath']) {
            $path = $this->getUploadDir().'/'.$path;
        }

        if($options['allocate']) {
            $this->allocate();
        }

        return $path;
    }


    /**
     * Возвращает данные для формирования пути к директории
     * @return array
     * @throws Exception
     */
    protected function _getPathData()
    {
        if(!($this->_pathData)) {
            $ticket = $this->getTicket();

            $id = $ticket->getId();
            $year = date('Y');
            $month = date('m');

            $this->_pathData = array(
                'year' => $year,
                'month' => $month,
                'id' => $id
            );
        }

        return $this->_pathData;
    }

    /**
     * Создает директорию для сохранения прикрепленных к запросу документов
     */
    public function allocate()
    {
        $uploadDir = $this->getUploadDir();
        $pathData = $this->_getPathData();

        $year = $pathData['year'];
        $month = $pathData['month'];
        $id = $pathData['id'];

        $this->_touchDir('payment', $uploadDir);
        $this->_touchDir($year, $uploadDir.'/payment');
        $this->_touchDir($month, $uploadDir.'/payment/'.$year);
        $this->_touchDir($id, $uploadDir.'/payment/'.$year.'/'.$month);
    }
}