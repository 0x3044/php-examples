<?php
abstract class App_Finance_Ticket_Component_Document_PathStrategy_Abstract extends App_Finance_Ticket_Extension
{
    /**
     * Возвращает стратегию выбора пути для загружаемого файла
     * @param \App_Finance_Ticket_TicketInterface|\App_Finance_Ticket_Type_Abstract_Ticket $ticket
     * @return App_Finance_Ticket_Component_Document_PathStrategy_Claim
     */
    public static function create(App_Finance_Ticket_TicketInterface $ticket)
    {
        if($ticket->getMainRecipient() == "payment") {
            return new App_Finance_Ticket_Component_Document_PathStrategy_Payment($ticket);
        }else{
            return new App_Finance_Ticket_Component_Document_PathStrategy_Claim($ticket);
        }
    }

    /**
     * Создает директорию для сохранения прикрепленных к запросу документов
     */
    abstract public function allocate();

    /**
     * Возвращает путь к директории для сохранения прикрепленных к запросу документов
     * @param array $options
     * @return string
     */
    abstract public function getPath(array $options = null);

    /**
     * Возвращает корневую директорию для сохранения документов
     * @return string
     */
    public function getUploadDir()
    {
        return APPLICATION_PATH.'/../public/uploads/finance/documents';
    }

    /**
     * Создает директорию, если она не сушествует
     * @param $dirName
     * @param $destination
     * @throws Exception
     */
    protected function _touchDir($dirName, $destination)
    {
        $dirPath = $destination.'/'.$dirName;

        if(!(is_dir($destination))) {
            throw new \Exception("Path `{$destination}` is not a dir");
        }

        if(!(file_exists($dirPath))) {
            mkdir($destination.'/'.$dirName, 0755);
        }

        if(!(is_dir($dirPath))) {
            throw new \Exception("File `{$dirPath}` exists");
        }
    }
}