<?php
use App_Finance_Ticket_Component_Document_PathStrategy_Abstract as AbstractPathStrategy;

/**
 * Класс для получения пути к директории для сохранения документов тикета
 */
class App_Finance_Ticket_Component_Document_PathStrategy_Claim extends AbstractPathStrategy
{
    protected $_pathData = array();

    /**
     * Возвращает путь к директории для сохранения прикрепленных к запросу документов
     * @param array $options
     * @return string
     */
    public function getPath(array $options = null)
    {
        $options = array_merge(array(
            'realPath' => false,
            'allocate' => false,
        ), (array) $options);

        $pathData = $this->_getPathData();
        $path = $pathData['year'].'/'.$pathData['month'].'/'.$pathData['claimId'];

        if($options['realPath']) {
            $path = $this->getUploadDir().'/'.$path;
        }

        if($options['allocate']) {
            $this->allocate();
        }

        return $path;
    }

    /**
     * Создает директорию для сохранения прикрепленных к запросу документов
     */
    public function allocate()
    {
        $uploadDir = $this->getUploadDir();
        $pathData = $this->_getPathData();

        $year = $pathData['year'];
        $month = $pathData['month'];
        $claimId = $pathData['claimId'];

        $this->_touchDir($year, $uploadDir);
        $this->_touchDir($month, $uploadDir.'/'.$year);
        $this->_touchDir($claimId, $uploadDir.'/'.$year.'/'.$month);
    }

    /**
     * Возвращает данные для формирования пути к директории
     * @return array
     * @throws Exception
     */
    protected function _getPathData()
    {
        if(!($this->_pathData)) {
            $ticket = $this->getTicket();
            $claimId = $ticket->getClaimId();

            if($claimId > 0) {
                if(!($claimDateCreatedTimestamp = (int) App_Claim_Factory::getInstance()->getClaimData($claimId)->date_begin)) {
                    throw new \Exception('No claimDateCreatedTimestamp available');
                }

                $year = date('Y', $claimDateCreatedTimestamp);
                $month = (int) date('m', $claimDateCreatedTimestamp);

                if($month < 10) {
                    $month = '0'.$month;
                }
            }else{
                $claimId = 'payment';
                $year = date('Y');
                $month = date('m');
            }

            $this->_pathData = array(
                'year' => $year,
                'month' => $month,
                'claimId' => $claimId
            );
        }

        return $this->_pathData;
    }
}