<?php
use App_Spl_Collection_Abstract as SplCollection;
use App_Finance_Ticket_Component_DocumentInterface as Document;

class App_Finance_Ticket_Component_Document_Collection extends SplCollection
{
    /**
     * Returns true if item is allowed for this collection
     * @param $item
     * @return bool
     */
    public function isItemAllowed($item)
    {
        return $item instanceof Document;
    }

    /**
     * Возвращает документ по его Id
     * @param int $documentId
     * @throws Exception
     * @return \App_Finance_Ticket_Component_DocumentInterface
     */
    public function getDocumentById($documentId)
    {
        /** @var $document Document */
        foreach($this->getItems() as $document) {
            if($document->getRow()->getId() == (int) $documentId) {
                return $document;
            }
        }

        throw new \Exception("document(id:`{$documentId}`) not found");
    }
}