<?php
use App_Finance_Ticket_Component_Attachment_AbstractAttachment as AbstractAttachDocument;

/**
 * Пояснительная записка
 * @see ticket 1167
 */
class App_Finance_Ticket_Component_Attachment_ExplanatoryNote_Document extends AbstractAttachDocument
{
    /**
     * Директория, в которую сохраняются служебные записки
     */
    const UPLOAD_DIRECTORY = 'finance/attachments/explanatory';

    /**
     * {@inheritdoc}
     * @return string
     */
    protected function _getUploadDirectory()
    {
        return self::UPLOAD_DIRECTORY;
    }

    /**
     * {@inheritdoc}
     * @return bool
     */
    public function hasReadAccess()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     * @return bool
     */
    public function hasWriteAccess()
    {
        return true;
    }

    /**
     * Возвращает информацию о служебной записке в виде массива
     * @return array
     */
    public function toArray()
    {
        return array(
            'filename' => $this->getFileName(),
            'link' => $this->getLink(),
            'extension' => pathinfo($this->getFileName(), PATHINFO_EXTENSION)
        );
    }

    /**
     * {@inheritdoc}
     * @return \App_Finance_Ticket_Component_Attachment_AbstractAttachmentDbRowInterface
     */
    protected function _fetchDbRow()
    {
        /** @var $dbTable App_Db_TicketExplanatoryNote */
        $dbTable = App_Db::get(DB_TICKET_EXPLANATORY_NOTE);

        return $dbTable->getByTicketId($this->getTicket()->getId());
    }

    /**
     * {@inheritdoc}
     * @param $fileName
     */
    protected function _saveUploadedAttachmentToDb($fileName)
    {
        /** @var $dbTable App_Db_TicketExplanatoryNote */
        $dbTable = App_Db::get(DB_TICKET_EXPLANATORY_NOTE);

        $dbTable->create($this->getTicket()->getId(), $fileName);
    }

    /**
     * {@inheritdoc}
     */
    protected function _destroyAttachmentDbRow()
    {
        /** @var $dbTable App_Db_TicketExplanatoryNote */
        $dbTable = App_Db::get(DB_TICKET_EXPLANATORY_NOTE);

        $dbTable->destroy($this->getTicket()->getId());
    }

    /**
     * {@inheritdoc}
     * @return string
     * @throws Exception
     */
    public function getLink()
    {
        if(!($this->isUploaded())) {
            throw new \Exception('Служебная записка не загружена');
        }

        return '/uploads/'.self::UPLOAD_DIRECTORY.'/'.$this->getTicket()->getId().'/'.$this->getRecordData()->getFilename();
    }
}