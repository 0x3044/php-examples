<?php
use App_Finance_Ticket_TicketInterface as Ticket;
use App_Finance_Ticket_Component_Attachment_AbstractAttachmentDbRowInterface as Row;
use App_Finance_Ticket_Component_AttachDocument_AccessDeniedException as AccessDeniedException;

class App_Finance_Ticket_Component_AttachDocument_AccessDeniedException extends \Exception {}

abstract class App_Finance_Ticket_Component_Attachment_AbstractAttachment
{
    /**
     * Переданный конструктору запрос
     * @var Ticket
     */
    protected $_ticket;

    /**
     * @var Row
     */
    protected $_recordData;

    /**
     * Приложение (attachment) к запросу
     * @param App_Finance_Ticket_TicketInterface $ticket
     */
    public function __construct(Ticket $ticket)
    {
        $this->_ticket = $ticket;
    }

    /**
     * Возвращает директорию, в которую загружаются файлы
     * @return string
     */
    abstract protected function _getUploadDirectory();

    /**
     * Возвращает true, если пользователь имеет права на просмотр аттачмента
     * @return bool
     */
    abstract public function hasReadAccess();

    /**
     * Возвращает true, если пользователь имеета права на создание аттачмента
     * @return bool
     */
    abstract public function hasWriteAccess();

    /**
     * Сохраняет информацию о загруженном аттачменте в базу данных
     * @param $fileName
     */
    abstract protected function _saveUploadedAttachmentToDb($fileName);

    /**
     * Получает запись из базы данных для этого аттачмента
     * @return Row
     */
    abstract protected function _fetchDbRow();

    /**
     * Удаляет запись из базы данных для этого аттачмента
     */
    abstract protected function _destroyAttachmentDbRow();

    /**
     * Возвращает ссылку на служебную записку
     * @return string
     * @throws Exception
     */
    abstract public function getLink();

    /**
     * Возвращает переданный конструктору запрос
     * @return \App_Finance_Ticket_TicketInterface
     */
    public function getTicket()
    {
        return $this->_ticket;
    }

    /**
     * Загрузка на сервер приложения к запросу
     * @param $tmpFile
     * @param $fileName
     * @throws Exception
     */
    public function upload($tmpFile, $fileName)
    {
        $this->_recordData = NULL;

        $dirPath = APPLICATION_PATH.'/../public/uploads/'.$this->_getUploadDirectory().'/'.$this->getTicket()->getId();

        if(!(static::hasWriteAccess())) {
            throw new AccessDeniedException('Вы не имеете прав на прикрепление данного файла');
        }

        if(!(file_exists($tmpFile))) {
            throw new \Exception("Отсутствует файл `{$tmpFile}`");
        }

        if(!($fileName)) {
            throw new \Exception('Отсутствует имя файла');
        }

        if(!(file_exists($dirPath))) {
            if(!(mkdir($dirPath))) {
                throw new \Exception("Не удалось создать директорию `{$dirPath}`");
            }
        } else if(is_file($dirPath)) {
            throw new \Exception("Невозможно создать директорию `{$dirPath}`, т.к. существует файл с данным именем");
        }

        if(!(move_uploaded_file($tmpFile, $dirPath.'/'.$fileName))) {
            throw new \Exception('Не удалось переместить загруженный файл');
        }

        $this->_saveUploadedAttachmentToDb($fileName);
    }

    /**
     * Выгрузка приложения
     */
    public function download()
    {
        $translator = new App_Translit();
        $fileName = $translator->filterFileName($this->getFileName());
        $filePath = $this->getPath();
        $fileContent = file_get_contents($filePath);
        $fileSize = filesize($filePath);

        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header('Content-Disposition: attachment; filename='.$fileName);
        header('Content-Length: '.$fileSize);
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);

        exit($fileContent);
    }

    /**
     * Загрузка файла из $_FILES
     * @param $postParamName
     * @throws Exception
     */
    public function uploadFromPost($postParamName)
    {
        if(isset($_FILES[$postParamName]) && isset($_FILES[$postParamName]['tmp_name']) && strlen($_FILES[$postParamName]['tmp_name'])) {
            $this->upload($_FILES[$postParamName]['tmp_name'], $_FILES[$postParamName]['name']);
        } else {
            throw new \Exception("Запрос требует прикрепления файла");
        }
    }

    /**
     * Удаление аттачмента
     * @throws Exception
     */
    public function destroy()
    {
        $this->_recordData = NULL;

        if(!($this->isUploaded())) {
            throw new \Exception('У запроса нет загруженного приложения');
        }

        $this->_destroyAttachmentDbRow();
    }

    /**
     * Возвращает true, если аттачмент была загружен для данного тикета
     * @return bool
     */
    public function isUploaded()
    {
        return $this->getRecordData() && $this->getRecordData()->getFilename() && $this->fileExists();
    }

    /**
     * Воврашает true, если файл аттачмента существует
     * @return bool
     */
    public function fileExists()
    {
        return file_exists($this->getPath());
    }

    /**
     * Возврашает абсолютный путь к файлу
     * @return string
     * @throws Exception
     */
    public function getPath()
    {
        if(!($recordData = $this->getRecordData()) || !($recordData->getFilename())) {
            throw new \Exception('Приложение к запросу не загружену');
        }

        return APPLICATION_PATH.'/../public/uploads/'.($this->_getUploadDirectory()).'/'.$this->getTicket()->getId().'/'.$recordData->getFilename();
    }

    /**
     * Возвращает имя файла
     * @return string
     * @throws Exception
     */
    public function getFileName()
    {
        if(!($this->isUploaded())) {
            throw new \Exception('Приложение к запросу не загружено');
        }

        return $this->getRecordData()->getFilename();
    }

    /**
     * Возвращает запись об аттачменте
     * @return Row
     */
    public function getRecordData()
    {
        if(is_null($this->_recordData)) {
            $this->_recordData = $this->_fetchDbRow();

        }

        return $this->_recordData;
    }
}