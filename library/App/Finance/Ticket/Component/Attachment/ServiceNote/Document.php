<?php
use App_Finance_Ticket_Component_Attachment_AbstractAttachment as AbstractAttachDocument;
use App_Db_Row_TicketServiceNote as ServiceNoteRow;

/**
 * Служебная записка
 */
class App_Finance_Ticket_Component_Attachment_ServiceNote_Document extends AbstractAttachDocument
{
    /**
     * Директория, в которую сохраняются служебные записки
     */
    const UPLOAD_DIRECTORY = 'finance/attachments/service';

    /**
     * {@inheritdoc}
     * @return string
     */
    protected function _getUploadDirectory()
    {
        return self::UPLOAD_DIRECTORY;
    }

    /**
     * {@inheritdoc}
     * @return bool
     */
    public function hasReadAccess()
    {
        return in_array($this->getTicket()->getAcl()->hasAccessServiceNote(), array(App_Access_Field_Type2::ACCESS_EDIT, App_Access_Field_Type2::ACCESS_VIEW));
    }

    /**
     * {@inheritdoc}
     * @return bool
     */
    public function hasWriteAccess()
    {
        return $this->getTicket()->getAcl()->hasAccessServiceNote() == App_Access_Field_Type2::ACCESS_EDIT;
    }

    /**
     * {@inheritdoc}
     * @return ServiceNoteRow
     */
    protected function _fetchDbRow()
    {
        /** @var $dbTable App_Db_TicketServiceNote */
        $dbTable = App_Db::get(DB_TICKET_SERVICE_NOTE);

        return $dbTable->getByTicketId($this->getTicket()->getId());
    }

    /**
     * {@inheritdoc}
     * @param $fileName
     */
    protected function _saveUploadedAttachmentToDb($fileName)
    {
        /** @var $serviceNoteDbTable App_Db_TicketServiceNote */
        $serviceNoteDbTable = App_Db::get(DB_TICKET_SERVICE_NOTE);
        $serviceNoteDbTable->create($this->getTicket()->getId(), $fileName);
    }

    /**
     * {@inheritdoc}
     */
    protected function _destroyAttachmentDbRow()
    {
        /** @var $serviceNoteDbTable App_Db_TicketServiceNote */
        $serviceNoteDbTable = App_Db::get(DB_TICKET_SERVICE_NOTE);
        $serviceNoteDbTable->destroy($this->getTicket()->getId());
    }

    /**
     * Возвращает ссылку на служебную записку (статический доступ)
     * @param $ticketId
     * @return string
     */
    public static function getStaticLink($ticketId)
    {
        App_Spl_TypeCheck::getInstance()->positiveNumeric($ticketId);

        return '/finance/ticket/service-note/go?ticketId='.$ticketId;
    }

    /**
     * {@inheritdoc}
     * @return string
     * @throws Exception
     */
    public function getLink()
    {
        if(!($this->isUploaded())) {
            throw new \Exception('Служебная записка не загружена');
        }

        return '/uploads/'.self::UPLOAD_DIRECTORY.'/'.$this->getTicket()->getId().'/'.$this->getRecordData()->getFilename();
    }
}