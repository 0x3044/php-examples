<?php
interface App_Finance_Ticket_Component_Attachment_AbstractAttachmentDbRowInterface
{
    public function getFilename();
}