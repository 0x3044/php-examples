<?php
use App_Finance_Ticket_TicketInterface as Ticket;
use App_Finance_Ticket_Component_Modification_Type_Claim as ClaimModification;

class App_Finance_Ticket_Component_Modification_Handler
{
    /**
     * @var Ticket
     */
    protected $_ticket;

    /**
     * Объект для получения информации об изменениях в заявке
     * @var ClaimModification
     */
    protected $_modificationClaim;

    /**
     * Объект для получения информации об изменениях в заявке
     * @param App_Finance_Ticket_TicketInterface $ticket
     */
    public function __construct(Ticket $ticket)
    {
        $this->_ticket = $ticket;
    }

    /**
     * Возвращает объект для получения информации об изменениях в заявке
     * @return App_Finance_Ticket_Component_Modification_Type_Claim
     */
    public function getClaim()
    {
        if(!($this->_modificationClaim)) {
            $this->_modificationClaim = new ClaimModification($this->_getTicket());
        }

        return $this->_modificationClaim;
    }

    /**
     * Возвращает переданный конструктору тикет
     * @return \App_Finance_Ticket_TicketInterface
     */
    protected function _getTicket()
    {
        return $this->_ticket;
    }
}