<?php
use App_Finance_Ticket_TicketInterface as Ticket;

abstract class App_Finance_Ticket_Component_Modification_Type_AbstractType
{
    /**
     * @var Ticket
     */
    protected $_ticket;

    /**
     * @var int
     */
    protected $_fixedEventId;

    /**
     * @var int
     */
    protected $_actualEventId;

    /**
     * @var int
     */
    protected $_fixedTimestamp;

    /**
     * @var int
     */
    protected $_actualTimestamp;

    public function __construct(Ticket $ticket)
    {
        $this->_ticket = $ticket;
    }

    public function getTicket()
    {
        return $this->_ticket;
    }

    public function hasModifications()
    {
        return $this->hasFixedEventId() && ($this->getFixedEventId() !== $this->getActualEventId());
    }

    public final function hasFixedEventId()
    {
        return $this->getFixedEventId() > 0;
    }

    public function getFixedEventId()
    {
        if(is_null($this->_actualEventId)) {
            $this->_fetchData();
        }

        return $this->_fixedEventId;
    }

    protected function _setFixedEventId($fixedEventId)
    {
        $this->_fixedEventId = (int) $fixedEventId;
    }

    public function getActualEventId()
    {
        if(is_null($this->_actualEventId)) {
            $this->_fetchData();
        }

        return $this->_actualEventId;
    }

    protected function _setActualEventId($actualEventId)
    {
        $this->_actualEventId = $actualEventId;
    }

    /**
     * @param int $fixedTimestamp
     */
    protected function _setFixedTimestamp($fixedTimestamp)
    {
        $this->_fixedTimestamp = $fixedTimestamp;
    }

    /**
     * @return int
     */
    public function getFixedTimestamp()
    {
        return $this->_fixedTimestamp;
    }

    /**
     * @param int $actualTimestamp
     */
    protected function _setActualTimestamp($actualTimestamp)
    {
        $this->_actualTimestamp = $actualTimestamp;
    }

    /**
     * @return int
     */
    public function getActualTimestamp()
    {
        return $this->_actualTimestamp;
    }

    /**
     * Получение данных о фиксированной/актуальной версии
     */
    abstract protected function _fetchData();

    /**
     * Обновление данных о фиксированной/актуальной версии
     */
    abstract public function updateFixedEventId();
}