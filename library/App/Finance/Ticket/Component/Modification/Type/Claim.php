<?php
use App_Finance_Ticket_Component_Modification_Type_AbstractType as AbstractType;
use App_Diff_Type_Claim_ClaimDiff as ClaimDiff;
use App_Diff_Type_Claim_Factory as ClaimDiffFactory;

class App_Finance_Ticket_Component_Modification_Type_Claim extends AbstractType
{
    /**
     * Данные об изменениях в заявке
     * @var ClaimDiff
     */
    protected $_claimDiff;

    /**
     * Получение данных о фиксированной/актуальной версии
     */
    protected function _fetchData()
    {
        $ticket = $this->getTicket();

        if($ticket->isNewTicket()) {
            throw new \Exception('Unable to setUp modification handler for new ticket');
        }

        /** @var $db App_Db_TicketModifiedFields */
        $db = App_Db::get(DB_TICKET_MODIFIED_FIELDS);
        $rowData = $db->getByTicketId($ticket->getId());

        if(is_array($rowData) && array_key_exists('claim_modified_event_id', $rowData) && $rowData['claim_modified_event_id'] > 0) {
            $this->_setFixedEventId((int) $rowData['claim_modified_event_id']);
            $this->_setFixedTimestamp((int) $db->getEventTimestamp($rowData['claim_modified_event_id']));
        }

        if($claimActualEventId = $db->getClaimActualEventId($ticket->getClaimId())) {
            $this->_setActualEventId($claimActualEventId);
            $this->_setActualTimestamp($db->getEventTimestamp($this->getActualEventId()));
        }
    }

    /**
     * Обновление данных о фиксированной/актуальной версии
     */
    public function updateFixedEventId($refreshFlag = false)
    {
        $ticket = $this->getTicket();

        if($ticket->isNewTicket()) {
            throw new \Exception('Unable to setUp modified fields for new ticket');
        }

        /** @var $db App_Db_TicketModifiedFields */
        $db = App_Db::get(DB_TICKET_MODIFIED_FIELDS);
        $dbRow = $db->getByTicketId($ticket->getId());

        if($dbRow || (!($dbRow) && $refreshFlag)) {
            $db->updateClaimModifiedFlag($ticket->getClaimId(), $ticket->getId(), $refreshFlag);
        }
    }

    /**
     * Возвращает объект изменений заявки
     * @return ClaimDiff
     */
    public function getDiff()
    {
        if(!($this->_claimDiff instanceof ClaimDiff)) {
            $factory = new ClaimDiffFactory();

            $this->_claimDiff = $factory->createFromClaimId($this->getTicket()->getClaimId(), $this->getFixedEventId());
        }

        return $this->_claimDiff;
    }
}