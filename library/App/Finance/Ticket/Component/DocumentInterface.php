<?php
use App_Finance_Ticket_TicketInterface as Ticket;

interface App_Finance_Ticket_Component_DocumentInterface
{
    /**
     * @param App_Db_Row_TicketDocument $documentRow
     */
    public function __construct(App_Db_Row_TicketDocument $documentRow = NULL);

    /**
     * Возвращает директорию, в которой сохранен/будет сохранен файл
     * @return string
     */
    public function getFileDir();

    /**
     * Возвращает имя файла
     * @return string
     */
    public function getFileName();

    /**
     * Возвращает полный путь к файлу
     * @return string
     */
    public function getFilePath();

    /**
     * Возвращает информацию о файле(path_info)
     * @return array
     */
    public function getFileInfo();

    /**
     * Возвращает путь к файлу (настоящий)
     * @return string
     */
    public function getRealPath();

    /**
     * @param App_Finance_Ticket_TicketInterface $uploadTicket
     */
    public function setUploadTicket(Ticket $uploadTicket);

    /**
     * Возврашает запись в базе данных для этого документа
     * @return \App_Db_Row_TicketDocument
     */
    public function getRow();

    /**
     * Сохраняет изменения
     */
    public function save();

    /**
     * Сохраняет файл
     */
    public function upload($tmpFile);

    /**
     * Выгрузка документа
     * @throws Exception
     */
    public function download();

    /**
     * Удалет файл с жесткого диска и базы данных
     */
    public function remove();

    /**
     * Сериализовать в массив
     * @return array
     */
    public function toArray();
}