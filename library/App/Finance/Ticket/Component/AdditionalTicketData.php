<?php
use App_Finance_Ticket_Extension as TicketExtension;

class App_Finance_Ticket_Component_AdditionalTicketData extends TicketExtension
{
    protected $_claimData;

    /**
     * Возвращает данные по заявки от данного клиента
     * @return stdClass
     * @throws Exception
     */
    public function getClaimData()
    {
        if(!($this->_claimData)) {
            $claimId = $this->getTicket()->getClaimId();

            if(!($claimId)) {
                return array();
            }

            /** @var $claimDbTable App_Db_Claims */
            $claimDbTable = App_Db::get(DB_CLAIMS);
            $claimRecord = $claimDbTable->getById($claimId);

            if(!($claimRecord && is_object($claimRecord))) {
                throw new \Exception('Заявка не найдена');
            }

            $this->_claimData = $claimRecord;
        }

        return $this->_claimData;
    }

    /**
     * Возвращает ID клиента
     * @return int
     */
    public function getClientId()
    {
        return $this->getClaimData()->client_id;
    }

    /**
     * Возвращает URL заявки
     * @return string
     */
    public function getClaimUrl()
    {
        return $this->getClaimData()->url;
    }

    /**
     * Возвращает клиента заявки
     * @return string
     * @throws Exception
     */
    public function getClientName()
    {
        $clientId = $this->getClientId();

        if(!($clientId)) {
            throw new \Exception('Неизвестен клиент');
        }

        /** @var $clientDbTable App_Db_Clients */
        $clientDbTable = App_Db::get(DB_CLIENTS);
        $selectQuery = $clientDbTable->select();
        $selectQuery->where('id='.(int) $clientId);

        $client = $clientDbTable->fetchRow($selectQuery);

        if(!($client) || !(is_object($client))) {
            throw new \Exception('Не найден клиент');
        }

        if(strlen(trim($client->u_title))) {
            return $client->u_title;
        } else {
            return $client->s_title;
        }
    }

    /**
     * Возвращает имя пользователя
     * @param int $userId
     * @throws Exception
     * @return string
     */
    public function getUserFullName($userId)
    {
        /** @var $userDbTable App_Db_Users */
        $userDbTable = App_Db::get(DB_USERS);

        if(!($userId)) {
            throw new \Exception('Неизвестен пользователь, который провел операцию!');
        }

        $record = $userDbTable->getUser($userId);

        if(!($record)) {
            throw new \Exception("Пользователь #id:{$userId} не найден");
        }

        return $record->name;
    }

    /**
     * Возвращает ID менеджера заявки
     * @return int
     */
    public function getManagerId()
    {
        return $this->getClaimData()->manager_id;
    }

    /**
     * Возвращает имя менеджера
     * @return string
     */
    public function getManagerName()
    {
        return $this->getUserFullName($this->getManagerId());
    }
}