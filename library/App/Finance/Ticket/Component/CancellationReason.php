<?php

/**
 * Причина отмены запроса
 */
class App_Finance_Ticket_Component_CancellationReason extends App_Finance_Ticket_Extension
{
    /**
     * Возвращает сохраненную ранее причину отмены запроса
     * @return mixed
     * @throws Exception
     */
    public function getReason()
    {
        if(!($this->hasReason())) {
            throw new \Exception('No cancellation reason found');
        }

        return array(
            'message' => $this->getTicket()->getJsonDataHandler()->offsetGet('cancellation_reason'),
            'user_id' => $this->getTicket()->getJsonDataHandler()->offsetGet('cancellation_reason_user_id'),
        );
    }

    /**
     * Возвращает true, если у запроса сохранена причина отмены хапроса
     * @return bool
     */
    public function hasReason()
    {
        return $this->getTicket()->getJsonDataHandler()->offsetExists('cancellation_reason');
    }

    /**
     * Задать причину отмены запроса
     * @param $message
     * @param int $userId
     * @throws Exception
     */
    public function setUpReason($message, $userId = null)
    {
        $message = trim($message);

        if(is_null($userId)) {
            $userId = (int) Zend_Auth::getInstance()->getIdentity()->id;
        }

        if(!(is_numeric($userId) && $userId > 0)) {
            throw new \Exception('Invalid userId');
        }

        if(strlen($message)) {
            $this->getTicket()->getJsonDataHandler()->offsetSet('cancellation_reason', $message);
            $this->getTicket()->getJsonDataHandler()->offsetSet('cancellation_reason_user_id', $userId);
        }
    }

    /**
     * Удалить причину отмены запроса
     */
    public function destroy()
    {
        $this->getTicket()->getJsonDataHandler()->offsetUnset('cancellation_reason');
        $this->getTicket()->getJsonDataHandler()->offsetUnset('cancellation_reason_user_id');
    }
}