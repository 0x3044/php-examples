<?php
use App_Finance_Ticket_TicketInterface as Ticket;

interface App_Finance_Ticket_Component_Behaviour_SaveInterface
{
    /**
     * Save behaviour
     * @param App_Finance_Ticket_TicketInterface $ticket
     * @param array $options
     */
    public function __construct(Ticket $ticket, array $options = null);

    /**
     * Сохранение переданного конструктору тикет в базу данных
     */
    public function perform();
}