<?php
use App_Finance_Ticket_Component_Behaviour_SaveInterface as SaveInterface;
use App_Finance_Ticket_TicketInterface as Ticket;
use App_Finance_Ticket_Logger_Logger as FinanceLogger;

class App_Finance_Ticket_Component_Behaviour_Save implements SaveInterface
{
    /**
     * @var Ticket
     */
    protected $_ticket;

    /**
     * Опции сохранения
     * @var array|null
     */
    protected $_options;

    /**
     * Режим работы - сохранение(update) либо создание(insert)
     * @var string
     */
    protected $_saveMode;

    /**
     * Включено/отключено логирование
     * @var bool
     */
    protected $_loggerEnabled;

    /**
     * Сохраняет указанный тикет
     * @param App_Finance_Ticket_TicketInterface $ticket
     * @param array $options
     */
    public function __construct(Ticket $ticket, array $options = null)
    {
        $this->_ticket = $ticket;
        $this->_options = array_merge(array(
            'enableTransaction' => true
        ), (array) $options);

        $ticketRow = $ticket->getZendRow();
        $this->_setSaveMode($ticketRow->getId() ? 'update' : 'insert');
    }

    /**
     * Сохранение переданного конструктору тикет в базу данных
     * @throws Exception
     */
    public function perform()
    {
        $enableTransaction = $this->_options['enableTransaction'];

        if($enableTransaction) {
            Zend_Db_Table_Abstract::getDefaultAdapter()->beginTransaction();
        }

        try {
            $ticket = $this->_getTicket();
            $financeLogger = new FinanceLogger($ticket);
            $oldLoggerObject = NULL;
            $newLoggerObject = NULL;

            $financeLogger->oldSnapshot();
            $ticket->getObserversHandler()->on('onBeforeSave');

            if(!($ticket->getStatusHandler()->hasStatus())) {
                throw new \Exception('No status');
            }

            $this->_setUpUserIds();
            $this->_setUpTicketTypeId();
            $this->_setUpJsonData();
            $this->_setUpStatus();
            $this->_setUpDocuments();

            if(!($result = $ticket->getZendRow()->save())) {
                throw new \Exception('Failed to save ticket');
            }

            $ticket->getObserversHandler()->on('onAfterSave');

            $financeLogger->newSnapshot();
            $financeLogger->writeLog();

            if($enableTransaction) {
                Zend_Db_Table_Abstract::getDefaultAdapter()->commit();
            }

            $enableTransaction = false;
            $ticket->performMailing();
        }
        catch(\Exception $e) {
            if($enableTransaction) {
                Zend_Db_Table_Abstract::getDefaultAdapter()->rollBack();
            }

            throw $e;
        }
    }

    /**
     * Поле "author_user_id", "processed_user_id"
     * @throws Exception
     */
    protected function _setUpUserIds()
    {
        $ticket = $this->_getTicket();
        $saveMode = $this->_getSaveMode();
        $zendAuth = Zend_Auth::getInstance();

        if(is_object($zendAuth) && $zendAuth instanceof Zend_Auth) {
            $zendIdentity = $zendAuth->getIdentity();
        } else {
            $zendIdentity = false;
        }

        if(is_object($zendIdentity)) {
            if($saveMode == 'insert' && !($ticket->getAuthorUserId())) {
                $ticket->setAuthorUserId($zendIdentity->id);
            }

            if(!($ticket->getZendRow()->getIsCompleted()) && $ticket->isCompleted()) {
                $ticket->setProcessedUserId($zendIdentity->id);
            }
        } else if(!(defined('PHPUNIT_TEST') && PHPUNIT_TEST)) {
            throw new \Exception('Невозможно сохранить данные из-за отсутствия данных о текущем пользователе');
        }
    }

    /**
     * Поле "json_data"
     */
    protected function _setUpJsonData()
    {
        $ticket = $this->_getTicket();

        $ticket->getZendRow()->setJsonData((string) $ticket->getJsonDataHandler());
    }

    /**
     * Поле "status"
     */
    protected function _setUpStatus()
    {
        $ticket = $this->_getTicket();

        $ticket->getZendRow()->setStatus($ticket->getStatusHandler()->toInt());
    }

    /**
     * Поле "ticket_type_id"
     */
    protected function _setUpTicketTypeId()
    {
        $ticket = $this->_getTicket();

        $ticket->getZendRow()->setTicketTypeId($ticket->getTicketTypeId());
    }

    /**
     * Сохранение прикрепленных к тикеты документов
     */
    protected function _setUpDocuments()
    {
        $ticket = $this->_getTicket();

        if($ticket->attachmentsAllowed()) {
            $ticket->getDocumentsHandler()->push();
        }
    }

    /**
     * Возвращает переданный конструктору тикет
     * @return \App_Finance_Ticket_TicketInterface
     */
    protected function _getTicket()
    {
        return $this->_ticket;
    }

    /**
     * Устанавливает режим работы
     * @param string $saveMode
     * @throws InvalidArgumentException
     */
    protected function _setSaveMode($saveMode)
    {
        if($saveMode != 'update' && $saveMode != 'insert') {
            throw new \InvalidArgumentException('Unknown saveMode');
        }

        $this->_saveMode = $saveMode;
    }

    /**
     * Возвращает установленный режим работы
     * @return string
     */
    protected function _getSaveMode()
    {
        return $this->_saveMode;
    }
}