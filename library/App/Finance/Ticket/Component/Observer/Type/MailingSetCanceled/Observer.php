<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;
use App_Finance_Recipient_Factory as RecipientFactory;
use App_Finance_Ticket_Component_AdditionalTicketData as AdditionalTicketData;

/**
 * Рассылка "Тикет отменен"
 */
class App_Finance_Ticket_Component_Observer_Type_MailingSetCanceled_Observer extends AbstractObserver
{
    protected $_sendMail = false;

    /**
     * Дополнительные данные о запросе
     * @var AdditionalTicketData
     */
    protected $_ticketAdditionalData;

    public function onStatusBeforeChange()
    {
        $this->_sendMail = false;
    }

    public function onStatusSetCanceled()
    {
        $this->_sendMail = true;
    }

    /**
     * Рассылка запроса об отмене после сохранения данных о тикете
     * @throws Exception
     */
    public function onPerformMailing()
    {
        if(!($this->_checkStatusAllowed())) {
            throw new \Exception('StatusHandler \"'.get_class($this->getTicket()->getStatusHandler()).'" should have method "isCanceled"');
        }

        $sendMail = $this->_sendMail
            && $this->getTicket()->getStatusHandler()->isCanceled()
            && $this->getTicket()->getId();

        if($sendMail) {
            $this->_sendMail();
            $this->_sendMail = false;
        }
    }

    /**
     * Проверяет, что StatusHandler имеет определение "отмененного" запроса
     * @return bool
     */
    private function _checkStatusAllowed()
    {
        return method_exists($this->getTicket()->getStatusHandler(), 'isCanceled');
    }

    /**
     * Рассылка
     */
    private function _sendMail()
    {
        /** @var $dbMailListType App_Db_MailListType */
        $dbMailListType = App_Db::get(DB_MAIL_LIST_TYPE);
        $mailListTypeId = $dbMailListType->getMailTypeIdByView('financeSupportTicketCanceled');

        App_Sendmail_Abstract::sendMail($mailListTypeId, $this->_getMailParams());
    }

    /**
     * Параметры для рассылки
     * @return array
     */
    private function _getMailParams()
    {
        $ticket = $this->getTicket();

        $recipientTypeName = $this->_getTicketRecipientType();
        $httpHost = SITE_NAME . '/';

        return array(
            'subjectParams' => array('"'.addslashes($recipientTypeName).'"'),
            'userFullName' => $this->_getUserFullName(),
            'ticketId' => $ticket->getId(),
            'ticketUrl' => $httpHost . $ticket->getUrl(),
            'ticketTypeName' => $recipientTypeName,
            'claimId' => $ticket->getClaimId(),
            'claimUrl' => $httpHost . $this->_getClaimUrl(),
            'clientFullName' => $this->_getClientName()
        );
    }

    private function _getTicketAdditionalData()
    {
        if(!($this->_ticketAdditionalData)) {
            $this->_ticketAdditionalData = new App_Finance_Ticket_Component_AdditionalTicketData($this->getTicket());
        }

        return $this->_ticketAdditionalData;
    }

    /**
     * Возвращает тип запроса
     * @return string
     */
    private function _getTicketRecipientType()
    {
        try {
            $recipients = array();

            foreach($this->getTicket()->getRecipientNames() as $recipientName) {
                $recipients[] = RecipientFactory::getInstance()->createFromRecipientName($recipientName)->getDescription();
            }

            $recipientType = implode(', ', $recipients);
        }
        catch(\Exception $e) {
            $recipientType = "<{$e->getMessage()}>";
        }

        return $recipientType;
    }

    /**
     * Возвращает имя пользователя, отменившего запрос
     * @return string
     */
    protected function _getUserFullName()
    {
        $ticketData = $this->_getTicketAdditionalData();

        try {
            $userFullName = $ticketData->getUserFullName($this->getTicket()->getProcessedUserId());
        }
        catch(\Exception $e) {
            $userFullName = "<{$e->getMessage()}>";
        }

        return $userFullName;
    }

    /**
     * Возвращает ссылку на заявку
     * @return string
     */
    protected function _getClaimUrl()
    {
        $ticketData = $this->_getTicketAdditionalData();

        return $ticketData->getClaimUrl();
    }

    /**
     * Возвращает клиента заявки
     * @return string
     */
    protected function _getClientName()
    {
        $ticketData = $this->_getTicketAdditionalData();

        try {
            $clientName = $ticketData->getClientName();
        }
        catch(\Exception $e) {
            $clientName = "<{$e->getMessage()}>";
        }

        return $clientName;
    }
}
