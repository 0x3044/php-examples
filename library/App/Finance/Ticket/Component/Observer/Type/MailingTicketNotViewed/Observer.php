<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;
use App_Finance_Recipient_RecipientInterface as Recipient;

/**
 * Рассылка "Тикет не был просмотрен"
 */
class App_Finance_Ticket_Component_Observer_Type_MailingTicketNotViewed_Observer extends AbstractObserver
{
    /**
     * Разница в минутах между текущей датой и датой тикета. Если запрос не обработан и до просрочки обработки
     * тикета меньше 15 минут, будет выслана рассылка "Уведомление о новом тикете"
     * @const int
     */
    const DIFFERENCE_IN_MINUTES_TO_SEND_MAIL = 15;

    /**
     * Рассылки при соотвествуещем вызове
     */
    public function onPerformMailing()
    {
        return;

        if($this->getTicket()->getStatusHandler()->equalsToString('active') && !($this->_wasViewed())) {
            $this->_sendMailing();
            $this->getTicket()->getJsonDataHandler()->offsetSet('mailWasViewedSent', true);
            $this->getTicket()->save();
        }
    }

    /**
     * Возвращает true, если тикет считается "просмотренным"
     * @return bool
     */
    protected function _wasViewed()
    {
        $ticket = $this->getTicket();

        if($ticket->getJsonDataHandler()->offsetExists('mailWasViewedSent') && $ticket->getJsonDataHandler()->offsetGet('mailWasViewedSent')) {
            return true;
        }

        $currentDate = new Zend_Date();
        $dateToProcess = $ticket->getDateToProcess();

        /** @var $difference Zend_Date */
        $difference = $dateToProcess->sub($currentDate);

        $measure = new Zend_Measure_Time($difference->toValue(), Zend_Measure_Time::SECOND);
        $measure->convertTo(Zend_Measure_Time::MINUTE);
        $differenceInMinutes = $measure->getValue();

        return ($differenceInMinutes >= self::DIFFERENCE_IN_MINUTES_TO_SEND_MAIL);
    }

    /**
     * Рассылка
     */
    protected function _sendMailing()
    {
        $ticket = $this->getTicket();
        $ticket->getJsonDataHandler()->offsetSet("mailWasViewedSent", true);

        /** @var $dbMailListType App_Db_MailListType */
        $dbMailListType = App_Db::get(DB_MAIL_LIST_TYPE);
        $mailListTypeId = $dbMailListType->getMailTypeIdByView('financeSupportTicketNotViewed');

        App_Sendmail_Abstract::sendMail($mailListTypeId, $this->_getMailParams());
    }

    /**
     * Данные для рассылки
     * @return array
     */
    protected function _getMailParams()
    {
        $ticket = $this->getTicket();

        $recipients = array();

        if(!($ticket->getRecipients()->size())) {
            $recipients = array("[ошибка: недоступен получатель, просим вас сообщить о данной проблеме]");
        } else {
            /** @var $recipient Recipient */
            foreach($ticket->getRecipients() as $recipient) {
                $recipients[] = $recipient->getDescription();
            }
        }

        $recipient = implode(', ', $recipients);

        return array(
            'subjectParams' => array($ticket->getId(), '"'.$recipient.'"'),
            'ticketId' => $ticket->getId(),
            'ticketUrl' => SITE_NAME . '/'.$ticket->getUrl(),
            'expirationTime' => self::DIFFERENCE_IN_MINUTES_TO_SEND_MAIL,
            'recipient' => $recipient
        );
    }
}
