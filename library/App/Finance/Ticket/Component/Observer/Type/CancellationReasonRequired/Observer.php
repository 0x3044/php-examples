<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;
use App_Finance_Ticket_Component_CancellationReason as CancellationReasonHelper;

class App_Finance_Ticket_Component_Observer_Type_CancellationReasonRequired_Observer extends AbstractObserver
{
    public function onBeforeSave()
    {
        if($this->getTicket()->getStatusHandler()->equalsToString('pending-cancel')) {
            $helper = new CancellationReasonHelper($this->getTicket());

            if(!($helper->hasReason())) {
                throw new \Exception('Укажите причину отмены запроса');
            }
        }
    }
}