<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;
use App_Finance_Ticket_Component_Observer_Type_EndState_Exception as EndStateException;

/**
 * Завершенный статус - тикет после достижение определенного статуса не может быть переопределен на другой статус
 */
class App_Finance_Ticket_Component_Observer_Type_EndState_Observer extends AbstractObserver
{
    protected $_oldStatus;

    protected $_newStatus;

    /**
     * Возвращает карту маршрутов смены статуса
     * @return array
     */
    protected function _getRoutes()
    {
        return array(
            'active' => array(
                'canceled' => true,
                'archived' => true,
                'pending-cancel' => true,
            ),
            'canceled' => array(
                'active' => false,
                'archived' => false,
            ),
            'archived' => array(
                'active' => false,
                'canceled' => true,
                'pending-cancel' => true,
            ),
            'pending-cancel' => array(
                'active' => true,
                'canceled' => true,
                'deleted' => true
            ),
            'deleted' => array(
            )
        );
    }

    public function onStatusBeforeChange()
    {
        $this->_oldStatus = $this->getTicket()->getStatusHandler()->toString();
    }

    public function onStatusAfterChange()
    {
        $this->_newStatus = $this->getTicket()->getStatusHandler()->toString();

        if($this->getTicket()->getId()) {
            $this->_validateRoute();
        }
    }

    protected function _validateRoute()
    {
        $routes = $this->_getRoutes();
        $oldStatus = $this->_oldStatus;
        $newStatus = $this->_newStatus;

        if(!(isset($routes[$oldStatus]) && is_array($routes[$oldStatus]))) {
            throw new \Exception("No route found for status `{$oldStatus}`");
        }

        if(!(isset($routes[$oldStatus][$newStatus]) && is_bool($routes[$oldStatus][$newStatus]))) {
            throw new \Exception("No sub-route found for status `{$oldStatus}` -> `{$newStatus}`");
        }

        $isRouteAllowed = $routes[$oldStatus][$newStatus];

        if(!($isRouteAllowed)) {
            throw new EndStateException("Route [{$oldStatus}]->[{$newStatus}] restricted");
        }
    }
}