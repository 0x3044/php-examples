<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;

/**
 * Добавляет в json-data информацию о том, что запрос был архивирован
 * Необходимо для работы служебных записок для бухгалтерскго саппорта
 */
class App_Finance_Ticket_Component_Observer_Type_WasArchivedFlag_Observer extends AbstractObserver
{
    public function onBeforeSave()
    {
        $ticket = $this->getTicket();

        if($ticket->getStatusHandler()->isArchived()) {
            $ticket->getJsonDataHandler()->offsetSet('wasArchived', true);
        }
    }
}