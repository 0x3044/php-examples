<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;

class App_Finance_Ticket_Component_Observer_Type_ClaimDate_Observer extends AbstractObserver
{
    protected $_changeClaimDate;

    public function onStatusSetArchived()
    {
        $this->_changeClaimDate = true;
    }

    /**
     * Обновление даты исполнения заявки 2
     */
    public function onAfterSave()
    {
        if($this->_changeClaimDate) {
            $ticket = $this->getTicket();
            $claimId = $ticket->getClaimId();

            /** @var $claimsDbTable App_Db_Claims */
            $claimsDbTable = App_Db::get(DB_CLAIMS);
            $claimsDbTable->update(array(
                'date' => time()
            ), array('id = ?' => $claimId));
        }
    }
}