<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;

/**
 * Заполняет поле date_processed при переводе тикета в завершенные
 */
class App_Finance_Ticket_Component_Observer_Type_DateProcessed_Observer extends AbstractObserver
{
    protected $_setupDateProcessed = false;

    public function onStatusSetPendingCancel()
    {
        if($this->getTicket()->getMainRecipient() == "payment") {
            $this->_setupDateProcessed = true;
        }
    }

    public function onStatusSetActive()
    {
        if($this->getTicket()->getMainRecipient() == "payment") {
            $this->getTicket()->setDateProcessed(NULL);
        }
    }

    public function onSetCompleted()
    {
        $this->_setupDateProcessed = true;
    }

    public function onBeforeSave()
    {
        if($this->_setupDateProcessed) {
            $this->getTicket()->setDateProcessed(new Zend_Date());
        }
    }
}
