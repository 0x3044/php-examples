<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;

/**
 * Переводит тикет в завершенные при достижении статуса "отмененный"/"архив"
 */
class App_Finance_Ticket_Component_Observer_Type_SetCompletedOnStatusChange_Observer extends AbstractObserver
{
    public function onStatusSetActive()
    {
        $ticket = $this->getTicket();

        if($ticket->isCompleted()) {
            $ticket->setUncompleted();
        }
    }

    public function onStatusSetArchived()
    {
        $ticket = $this->getTicket();

        if(!($ticket->isCompleted())) {
            $ticket->setCompleted();
        }
    }

    public function onStatusSetCanceled()
    {
        $ticket = $this->getTicket();

        if(!($ticket->isCompleted())) {
            $ticket->setCompleted();
        }
    }

    public function onStatusSetDeleted()
    {
        $ticket = $this->getTicket();

        if(!($ticket->isCompleted())) {
            $ticket->setCompleted();
        }
    }

    public function onStatusSetPendingCancel()
    {
        $ticket = $this->getTicket();

        if($ticket->getMainRecipient() == 'payment') {
            if(!($ticket->isCompleted())) {
                $ticket->setCompleted();
            }
        }else{
            if($ticket->isCompleted()) {
                $ticket->setUncompleted();
            }
        }
    }
}