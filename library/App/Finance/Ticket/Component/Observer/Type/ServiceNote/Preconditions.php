<?php

class App_Finance_Ticket_Component_Observer_Type_ServiceNote_Preconditions
{
    /**
     * Возвращает true, если запрос требует прикрепления служебной записки
     * @param App_Finance_Ticket_TicketInterface $ticket
     * @return bool
     */
    public function isServiceNoteRequired(App_Finance_Ticket_TicketInterface $ticket)
    {
        $isActive = $ticket->getStatusHandler()->equalsToString('active');
        $hasCanceledArchivedTicket = App_Finance_Ticket_Factory::getInstance()->hasCanceledArchivedTicket($ticket->getClaimId());

        return $isActive && $hasCanceledArchivedTicket;
    }
}