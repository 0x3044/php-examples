<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;
use App_Finance_Ticket_Component_Observer_Type_ServiceNote_Preconditions as Preconditions;
use App_Finance_Ticket_Component_Observer_ObserverException as ObserverException;

/**
 * Проверка наличия служебной записки к активным хапросам
 */
class App_Finance_Ticket_Component_Observer_Type_ServiceNote_Observer extends AbstractObserver
{
    /**
     * Из формы можно установить поле, из которого будет загружена служебная записка
     * @var bool
     */
    protected $_uploadServiceNote = false;

    /**
     * Загрузка файла
     * @param boolean $uploadServiceNote
     */
    public function setUploadServiceNote($uploadServiceNote)
    {
        $this->_uploadServiceNote = $uploadServiceNote;
    }

    /**
     * Возврашает поле загрузки файла
     * @return boolean
     */
    public function getUploadServiceNote()
    {
        return $this->_uploadServiceNote;
    }

    /**
     * Проверка наличия служебной записки либо указанного поля загрузки айла
     */
    public function onBeforeSave()
    {
        $ticket = $this->getTicket();

        $hasServiceNote = false;

        if($ticket->isExistedTicket()) {
            $serviceNote = new App_Finance_Ticket_Component_Attachment_ServiceNote_Document($ticket);
            $hasServiceNote = $serviceNote->isUploaded();
        }

        if(!($hasServiceNote)) {
            $hasServiceNote = isset($_FILES[$this->getUploadServiceNote()])
                && isset($_FILES[$this->getUploadServiceNote()]['tmp_name'])
                && strlen($_FILES[$this->getUploadServiceNote()]['tmp_name'])
                && file_exists($_FILES[$this->getUploadServiceNote()]['tmp_name']);
        }

        $preconditions = new Preconditions();

        if($preconditions->isServiceNoteRequired($ticket) && !($hasServiceNote)) {
            throw new ObserverException("Запрос требует прикрепления служебной записки");
        }
    }

    /**
     * Загрузка файла
     */
    public function onAfterSave()
    {
        $postParamName = $this->getUploadServiceNote();

        if($postParamName) {
            $serviceNote = new App_Finance_Ticket_Component_Attachment_ServiceNote_Document($this->getTicket());
            $serviceNote->uploadFromPost($postParamName);
        }
    }
}