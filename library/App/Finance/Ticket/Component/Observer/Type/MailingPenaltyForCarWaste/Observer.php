<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;

/**
 * Рассылка "Прогон машины"
 * Если заявка безнал, по ней отсутствует запрос в бухгалтерский саппорт и по указанному в заявке клиенту отсутствуют
 * договора, то будет выполнена рассылка "прогон машины".
 *
 */
class App_Finance_Ticket_Component_Observer_Type_MailingPenaltyForCarWaste_Observer extends AbstractObserver
{
    /**
     * Данные запроса (кэш)
     * @var App_Finance_Ticket_Component_AdditionalTicketData
     */
    protected $_additionalTicketData;

    /**
     * Флаг "включить рассылку"
     * @var bool
     */
    protected $_performMailing = false;

    /**
     * Рассылка может быть разослана только при создании запроса
     * @var bool
     */
    public function onBeforeSave()
    {
        $this->_additionalTicketData = new App_Finance_Ticket_Component_AdditionalTicketData($this->getTicket());

        if($this->getTicket()->isNewTicket()) {
            $this->_performMailing = true;
        }
    }

    /**
     * Проверяет условия и триггерит при необходимости рассылку
     * @throws Exception
     */
    public function onPerformMailing()
    {
        if($this->sendMailRequired()) {
            /** @var $dbMailListType App_Db_MailListType */
            $dbMailListType = App_Db::get(DB_MAIL_LIST_TYPE);
            $mailListTypeId = $dbMailListType->getMailTypeIdByView('financeSupportCarWaste');

            App_Sendmail_Abstract::sendMail($mailListTypeId, $this->_getMailParams());
        }
    }

    /**
     * Возврашает параметры рассылки
     * @throws Exception
     * @return array
     */
    protected function _getMailParams()
    {
        $ticket = $this->getTicket();

        $httpHost = SITE_NAME . '/';
        $ticketData = $this->_additionalTicketData;
        $emailParams = array(
            'claimId' => $ticket->getClaimId(),
            'claimUrl' => $httpHost.$ticketData->getClaimUrl(),
            'clientName' => $ticketData->getClientName()
        );


        // Рассылка либо по email пользователя, либо по указанным в рассылке адресам
        /** @var $clientEmailsDbTable App_Db_UserEmail */
        $clientEmailsDbTable = App_Db::get(DB_USER_EMAIL);
        $userEmails = $clientEmailsDbTable->getUserEmails((int) $this->_additionalTicketData->getManagerId());

        if(count($userEmails)) {
            $emailParams['recipients'] = $userEmails;
        }

        return $emailParams;
    }

    /**
     * Возвращает true, если требуется рассылка сообщения о готовности менеджера оплаты прогона машины
     * @return bool
     */
    protected function sendMailRequired()
    {
        return $this->_performMailing && $this->_isAccountClaim() && !($this->_hasAccountTicketForCurrentClaim()) && !($this->_hasClientTreaty()) && ($this->_hasAccountTicketForCurrentClient());
    }

    /**
     * Возврашает true, если указанная в запросе заявка является безнальной
     * @return bool
     * @throws Exception
     */
    protected function _isAccountClaim()
    {
        return (int) $this->_additionalTicketData->getClaimData()->cash_type === PAYMENT_TYPE_CASHLESS;
    }

    /**
     * Возвращает true, если по указанной в запросе заявке существует запрос в бухгалтерский саппорт
     * @return bool
     */
    protected function _hasAccountTicketForCurrentClaim()
    {
        $finder = new App_Finance_Ticket_Finder();
        $finder->setRecipients('accounting');
        $finder->setClaimIds($this->getTicket()->getClaimId());
        $finder->setTicketStatus(array(
            App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_ACTIVE,
            App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_PENDING_CANCEL,
            App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_ARCHIVED
        ));

        return $finder->findOne() !== false;
    }

    /**
     * Возвращает true, если по указанному в заявке клиенту уже существуют запросы в бухгалтерский саппорт
     * @return bool
     */
    public function _hasAccountTicketForCurrentClient()
    {
        $finder = new App_Finance_Ticket_Finder();
        $finder->setRecipients('accounting');
        $finder->setClaimIds($this->getTicket()->getClaimId());
        $finder->setTicketStatus(array(

        ));

        $claimId = $this->getTicket()->getClaimId();
        $clientId = $this->_additionalTicketData->getClientId();
        $status = implode(',', array(
            App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_ACTIVE,
            App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_PENDING_CANCEL,
            App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_ARCHIVED
        ));
        $ticketTypeIds = implode(',', App_Finance_Recipient_Factory::getInstance()->getAccountingRecipient()->getTicketTypeIds());

        $sqlQuery = <<<SQL
          SELECT count(ticket.id)
          FROM ticket
          RIGHT JOIN claims ON claims.id = ticket.claim_id AND claims.client_id = {$clientId}
          WHERE
            ticket.`status` IN ({$status}) AND
            ticket.`ticket_type_id` IN ({$ticketTypeIds}) AND
            claims.id != {$claimId}
SQL;

        return (int) App_Db::get()->query($sqlQuery)->fetchColumn() > 0;
    }

    /**
     * Возвращает true, есть у клиента по заявке, указанной в запросе, есть договора
     * @return bool
     */
    protected function _hasClientTreaty()
    {
        /** @var $dbTable App_Db_ClientTreaty */
        $dbTable = App_Db::get(DB_CLIENT_TREATY);
        $clientId = $this->_additionalTicketData->getClientId();

        return $dbTable->hasClientTreaty($clientId);
    }
}