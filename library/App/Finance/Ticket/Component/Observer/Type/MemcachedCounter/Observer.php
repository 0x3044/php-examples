<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;

class App_Finance_Ticket_Component_Observer_Type_MemcachedCounter_Observer extends AbstractObserver
{
    public function onAfterSave()
    {
        $memcached = App_Memcached::get();
        $recipient = App_Finance_Recipient_Factory::getInstance()->createFromRecipientName($this->getTicket()->getMainRecipient());

        $keys = App_Finance_Service::getInstance()->getCountService()->getMemcachedKeys(
            array_shift($recipient->getTicketTypeIds()),
            $this->getTicket()->getMainRecipient(),
            $this->getTicket()->getStatusHandler()->toString()
        );

        $keySession = $keys['session'];
        $keyAccess = $keys['access'];
        $keyValue = $keys['value'];

        $memcached->remove($keySession);
        $memcached->remove($keyAccess);
        $memcached->remove($keyValue);
    }
}