<?php

class App_Finance_Ticket_Component_Observer_Type_ExplanatoryNote_Preconditions
{
    /**
     * Возвращает true, если запрос требует прикрепления пояснительной записки
     * @param App_Finance_Ticket_TicketInterface $ticket
     * @throws Exception
     * @return bool
     */
    public function isExplanationNoteRequired(App_Finance_Ticket_TicketInterface $ticket)
    {
        if(!($ticket->getClaimId())) {
            throw new \Exception('Валидатор может работать только для запросов с указанным Id заявок');
        }

        /** @var $claimDbTable App_Db_Claims */
        $claimDbTable = App_Db::get(DB_CLAIMS);

        return $this->_hasArchivedTicketsFromAug2014($ticket) && !($this->_clientHasContracts($claimDbTable->getClaimClient($ticket->getClaimId())));
    }

    /**
     * Возвращает true, если есть архивный запрос по указанному
     * @param App_Finance_Ticket_TicketInterface $ticket
     * @return bool
     */
    protected function _hasArchivedTicketsFromAug2014(App_Finance_Ticket_TicketInterface $ticket)
    {
        /** @var $claimDbTable App_Db_Claims */
        $claimDbTable = App_Db::get(DB_CLAIMS);
        $clientId = $claimDbTable->getClaimClient($ticket->getClaimId());

        $sqlQuery = <<<SQL
          SELECT
            count(*)
          FROM ticket
          LEFT JOIN claims ON claims.id = ticket.claim_id
          WHERE ticket.ticket_type_id = :ticketTypeId
            AND ticket.status = :status
            AND ticket.date_created >= '2014-08-01 00:00:00'
            AND claims.client_id = :clientId
SQL;

        $result = App_Db::get()->query($sqlQuery, array(
            'ticketTypeId' => $ticket->getTicketTypeId(),
            'status' => App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_ARCHIVED,
            'clientId' => $clientId
        ));

        if(!($result)) {
            return false;
        }


        return (int) $result->fetchColumn() > 0;
    }

    /**
     * Возвращает true, есть у клиента есть договора
     * @param $clientId
     * @return bool
     */
    protected function _clientHasContracts($clientId)
    {
        /** @var $dbTable App_Db_ClientTreaty */
        $dbTable = App_Db::get(DB_CLIENT_TREATY);

        return $dbTable->hasClientTreaty($clientId);
    }
}