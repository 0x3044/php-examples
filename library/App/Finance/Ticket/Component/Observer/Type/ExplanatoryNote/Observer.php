<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;
use App_Finance_Ticket_Component_Observer_Type_ExplanatoryNote_Preconditions as Preconditions;
use App_Finance_Ticket_Component_Observer_ObserverException as ObserverException;

/**
 * Проверка наличия служебной записки к активным хапросам
 */
class App_Finance_Ticket_Component_Observer_Type_ExplanatoryNote_Observer extends AbstractObserver
{

    /**
     * Сохраненный флаг "является новым запросом"
     * @var bool
     */
    protected $_isNewTicket;

    /**
     * Из формы можно установить поле, из которого будет загружена служебная записка
     * @var bool
     */
    protected $_uploadExplanationNote = false;

    /**
     * Загрузка файла
     * @param boolean $uploadServiceNote
     */
    public function setUploadExplanationNote($uploadServiceNote)
    {
        $this->_uploadExplanationNote = $uploadServiceNote;
    }

    /**
     * Возврашает поле загрузки файла
     * @return boolean
     */
    public function getUploadExplanationNote()
    {
        return $this->_uploadExplanationNote;
    }

    /**
     * Проверка наличия пояснительной записки либо указанного поля загрузки айла
     */
    public function onBeforeSave()
    {
        $ticket = $this->getTicket();

        if($ticket->isNewTicket()) {
            $this->_isNewTicket = true;

            $hasExplanationNote = false;

            if($ticket->isExistedTicket()) {
                $attachment = new App_Finance_Ticket_Component_Attachment_ExplanatoryNote_Document($ticket);
                $hasExplanationNote = $attachment->isUploaded();
            }

            if(!($hasExplanationNote)) {
                $hasExplanationNote = isset($_FILES[$this->getUploadExplanationNote()])
                    && isset($_FILES[$this->getUploadExplanationNote()]['tmp_name'])
                    && strlen($_FILES[$this->getUploadExplanationNote()]['tmp_name'])
                    && file_exists($_FILES[$this->getUploadExplanationNote()]['tmp_name']);
            }

            $preconditions = new Preconditions();

            if($preconditions->isExplanationNoteRequired($ticket) && !($hasExplanationNote)) {
                throw new ObserverException("Запрос требует прикрепления пояснительной записки");
            }
        }else{
            $this->_isNewTicket = false;
        }
    }

    /**
     * Загрузка файла
     */
    public function onAfterSave()
    {
        if($this->_isNewTicket) {
            $postParamName = $this->getUploadExplanationNote();

            if($postParamName) {
                $attachment = new App_Finance_Ticket_Component_Attachment_ExplanatoryNote_Document($this->getTicket());
                $attachment->uploadFromPost($postParamName);
            }
        }
    }
}