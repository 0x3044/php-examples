<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;
use App_Finance_Ticket_Component_Observer_Type_UploadDocument_Exception as UploadDocumentException;

/**
 * Запрет прикрепления документов к тикету, если они запрещенны для данного типа тикетов
 */
class App_Finance_Ticket_Component_Observer_Type_UploadDocument_Observer extends AbstractObserver
{
    public function onBeforeDocumentUpload()
    {
        if(!($this->getTicket()->attachmentsAllowed())) {
            throw new UploadDocumentException('Attachments are not allowed for this ticket');
        }
    }
}