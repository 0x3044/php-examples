<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;
use Cash_Model_Planned_State_Change_Spec as ChangeStateSpec;

/**
 * Если установлен флаг, то выполняет следующие действия:
 * 1. Меняет статус планируемого платежа на "отложенный" по текущей заявки
 * 2. Отмечает документы в планируемом платеже как копии
 */
class App_Finance_Ticket_Component_Observer_Type_CashPlanned_Observer extends AbstractObserver
{
    /**
     * Флаг "отложить документы"
     * @var bool
     */
    protected $_postponeDocuments = false;

    /**
     * Статус запроса до изменения статуса
     * @var int
     */
    protected $_currentStatus = null;

    /**
     * Id заявки
     * @var int
     */
    protected $_claimId;

    /**
     * Сохраняет текущий статус запроса перед его изменением
     */
    public function onStatusBeforeChange()
    {
        $this->_currentStatus = $this->getTicket()->getStatusHandler()->toInt();
    }

    /**
     * Если запроса меняет статус с архивного на отмененный, то необходимо отложить документы
     */
    public function onStatusSetCanceled()
    {
        if($this->_currentStatus == App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_ARCHIVED) {
            $this->_postponeDocuments = true;
        }
    }

    /**
     * Запуск процедуры откладывания документы при выполнении ряда условия
     * @throws Exception
     */
    public function onAfterSave()
    {
        $performChangeStateToPostpone = $this->_postponeDocuments
            && $this->getTicket()->getId()
            && $this->getTicket()->getClaimId();

        if($performChangeStateToPostpone) {
            $this->_postponeDocuments();
            $this->_postponeDocuments = false;
        }
    }

    /**
     * Выполняет следующие действия:
     * 1. Меняет статус планируемого платежа на "отложенный" по текущей заявки
     * 2. Отмечает документы в планируемом платеже как копии
     *
     * @throws Cash_Model_Planned_Exception_StateNotFoundException
     * @throws Exception
     */
    private function _postponeDocuments()
    {
        if($this->_checkClaimHasDocuments()) {
            $claimId = $this->_ticket->getClaimId();
            $cashPlannedModel = new Cash_Model_Planned();
            $cashPlannedRecord = $cashPlannedModel->getCashPlannedRecord($claimId);
            $stateManager = $cashPlannedModel->getStateManager();

            $currentState = $stateManager->getPrototypeStateByStateId($cashPlannedRecord->state);
            $postponedState = $stateManager->getPrototypeStateByName('postponed');

            if($currentState->getDowngradeRoutes()->hasRouteRuleWithStateId($postponedState->getStateId())) {
                $changeStateSpec = new ChangeStateSpec($claimId, $postponedState);
                $cashPlannedModel->changeState($changeStateSpec);
            }

            $cashPlannedModel->markDocumentsAsCopies($claimId);
        }
    }

    /**
     * Возвращает true, если по заявке есть планируемый платеж
     * @return bool
     */
    private function _checkClaimHasDocuments()
    {
        /** @var $dbTable App_Db_CashPlannedDocument */
        $dbTable = App_Db::get(DB_CASH_PLANNED_DOCUMENT);

        return (bool) $dbTable->hasDocuments($this->getTicket()->getClaimId());
    }
}