<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;

/**
 * Удаление документов при удалении запросов
 */
class App_Finance_Ticket_Component_Observer_Type_DestroyDocuments_Observer extends AbstractObserver
{
    public function onBeforeDestroy()
    {
        $ticket = $this->getTicket();

        if($ticket->attachmentsAllowed() && $ticket->getDocumentsHandler()->getDocuments()->size()) {
            /** @var $document App_Finance_Ticket_Component_DocumentInterface */
            foreach($ticket->getDocumentsHandler()->getDocuments() as $document) {
                $document->remove();
            }
        }
    }
}