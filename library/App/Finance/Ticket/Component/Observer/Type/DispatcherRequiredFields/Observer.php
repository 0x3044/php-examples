<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;
use App_Finance_Ticket_Component_Observer_ObserverException as ObserverException;

/**
 * При подтверждении запроса в диспетчерский саппорт у заявки должны быть указаны:
 *  - Номер машины
 *  - Диспетчер
 *  - Сумма
 */
class App_Finance_Ticket_Component_Observer_Type_DispatcherRequiredFields_Observer extends AbstractObserver
{
    protected $_enabled = false;

    public function onStatusSetArchived()
    {
        $this->_enabled = true;
    }

    public function onBeforeSave()
    {
        if($this->_enabled) {
            $ticket = $this->getTicket();

            if(!($claim = App_Db::get(DB_CLAIMS)->find($ticket->getClaimId())->current())) {
                throw new \Exception("Заявка `{$ticket->getClaimId()}` не найден");
            }

            $dispatcherId = (int) $claim->dispetcher;
            $carNumber = (int) $claim->car_number;

            if(!($dispatcherId)) {
                throw new ObserverException('Не указан диспетчер');
            }

            if(!($carNumber)) {
                throw new ObserverException('Не указан номер машины');
            }
        }
    }

}