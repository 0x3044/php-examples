<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;
use App_Finance_Ticket_Component_Observer_ObserverException as ObserverException;

class App_Finance_Ticket_Component_Observer_Type_AccountingDocuments_Observer extends AbstractObserver
{
    protected $_enabled = false;

    public function onStatusAfterChange()
    {
        $this->_enabled = false;
    }

    public function onStatusSetArchived()
    {
        $this->_enabled = true;
    }

    public function onBeforeSave()
    {
        $ticket = $this->getTicket();

        if($this->_enabled) {
            if($ticket->getDocumentsHandler()->getDocuments()->size() == 0) {
                throw new ObserverException("Запрос не может быть проведен без прикрепления документов");
            }
        }
    }
}