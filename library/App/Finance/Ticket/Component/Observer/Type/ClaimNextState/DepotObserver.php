<?php
use App_Finance_Ticket_Component_Observer_Type_ClaimNextState_AbstractObserver as AbstractObserver;

class App_Finance_Ticket_Component_Observer_Type_ClaimNextState_DepotObserver extends AbstractObserver
{
    /**
     * Сделать изменения в зявке
     */
    protected function _setUpFlags()
    {
        $this->_performClaimChanges(function (Claim_Model_Abstract $claim) {
            $claim->talibFlag = true;
        });
    }

    /**
     * Откатывает изменения в заявке
     */
    protected function _rollbackFlags()
    {
        $this->_performClaimChanges(function (Claim_Model_Abstract $claim) {
            $claim->talibFlag = false;
        });
    }
}