<?php
use App_Finance_Ticket_Component_Observer_Type_ClaimNextState_Claim_ChangeStatusHandler_Abstract as AbstractHandler;

class App_Finance_Ticket_Component_Observer_Type_ClaimNextState_Claim_ChangeStatusHandler_InOut extends AbstractHandler
{
    /**
     * {@inheritdoc}
     */
    public function updateClaimStatus()
    {
        $claim = App_Claim_Factory::getInstance()->getClaimData($this->getTicket()->getClaimId());

        if($claim->claim_status != App_Claim_Status_InOut_Out::CREATE_STATUS) {
            $inClaim = new App_Claim_Mapper_InOut_In();
            $inClaim = $inClaim->find($claim->connectivity);

            App_Claim_Status_InOut_Out::change($claim->id, $claim, true);
            App_Claim_Status_InOut_In::change($inClaim->id, $inClaim, true);
        }
    }
}