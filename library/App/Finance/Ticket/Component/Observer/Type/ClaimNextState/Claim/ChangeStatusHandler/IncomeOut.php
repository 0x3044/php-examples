<?php
use App_Finance_Ticket_Component_Observer_Type_ClaimNextState_Claim_ChangeStatusHandler_Abstract as AbstractHandler;

class App_Finance_Ticket_Component_Observer_Type_ClaimNextState_Claim_ChangeStatusHandler_IncomeOut extends AbstractHandler
{
    /**
     * {@inheritdoc}
     */
    public function updateClaimStatus()
    {
        $claim = App_Claim_Factory::getInstance()->getClaimData($this->getTicket()->getClaimId());

        if($claim->claim_status != App_Claim_Status_InOut_Out::CREATE_STATUS) {
            $outClaimId = $claim->id;
            $incomeClaimId = $claim->connectivity;

            App_Claim_Status_IncomeOut_Out::change($outClaimId);
            App_Claim_Status_IncomeOut_Income::change($incomeClaimId);
        }
    }
}