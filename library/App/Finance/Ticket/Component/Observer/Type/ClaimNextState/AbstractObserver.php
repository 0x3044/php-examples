<?php
use App_Finance_Ticket_Component_Observer_Type_ClaimNextState_Exception as ClaimNextStateException;
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;
use App_Finance_Ticket_Component_Observer_Type_ClaimNextState_Claim_ChangeStatusHandler_Abstract as ChangeStatusHandler;

/**
 * Перевод заявки на следующий статус
 */
abstract class App_Finance_Ticket_Component_Observer_Type_ClaimNextState_AbstractObserver extends AbstractObserver
{
    protected $_setCompleted = false;

    protected $_setCanceled = false;

    /**
     * Обновлять флаги только один раз
     */
    public function onSetCompleted()
    {
        $this->_setCompleted = true;
    }

    public function onStatusSetCanceled()
    {
        $this->_setCanceled = true;
    }

    /**
     * Переводит заявку на следующий статус при определенных условиях
     */
    public final function onAfterSave()
    {
        try {
            if($this->_setCompleted && $this->getTicket()->getStatusHandler()->equalsToString("archived")) {
                $this->_setUpFlags();
            } else if($this->_setCanceled) {
                $this->_rollbackFlags();
            }
        }
        catch(\Exception $e) {
            throw new ClaimNextStateException("Не удалось сохранить изменения в заявке, причина: ".$e->getMessage());
        }
    }

    /**
     * Сетап флагов
     */
    abstract protected function _setUpFlags();

    /**
     * Откат флагов
     */
    abstract protected function _rollbackFlags();

    /**
     * Выполняет и логирует изменения в заявке
     * @param callable $strategy
     */
    protected function _performClaimChanges(Closure $strategy)
    {
        /** @var $mapper App_Claim_Mapper_Abstract */
        /** @var $claim Claim_Model_Abstract */
        list($mapper, $claim) = App_Claim_Factory::getInstance()->createMapperByClaimId((int) $this->getTicket()->getClaimId());
        $adapter = App_Claim_Factory::getInstance()->createLoggerAdapter($claim);
        $oldClaimObject = clone $claim;

        if(method_exists($mapper, 'disableSaveProducts')) {
            $mapper->disableSaveProducts();
        }

        $strategy($claim);
        $mapper->save($claim);

        if(!($this->_setCanceled)) {
            $changeStatusHandler = ChangeStatusHandler::create($this->getTicket());
            $changeStatusHandler->updateClaimStatus();
        }

        list(, $newClaimObject) = App_Claim_Factory::getInstance()->createMapperByClaimId((int) $this->getTicket()->getClaimId());

        if($oldClaimObject->claimStatus != $newClaimObject->claimStatus) {
            $newClaimObject->changeStatus = true;
        }

        if($adapter) {
            $adapter->setObjectId($newClaimObject->id);
            $adapter->setOldObject($oldClaimObject);
            $adapter->setNewObject($newClaimObject);

            $adapter->writeLog();
        }
    }

    /**
     * Возвращает статус планируемой транзакции
     * @return int|null
     */
    protected function _getCashPlannedState()
    {
        /** @var $cashPlannedTable App_Db_CashPlanned */
        $cashPlannedTable = App_Db::get(DB_CASH_PLANNED);
        $claimId = $this->getTicket()->getClaimId();

        if($payment = $cashPlannedTable->getById($claimId)) {
            return is_null($payment->state) ? -2 : $payment->state;
        } else {
            return null;
        }
    }
}