<?php
use App_Finance_Ticket_Component_Observer_Type_ClaimNextState_Claim_ChangeStatusHandler_Abstract as AbstractHandler;

class App_Finance_Ticket_Component_Observer_Type_ClaimNextState_Claim_ChangeStatusHandler_Default extends AbstractHandler
{
    /**
     * {@inheritdoc}
     */
    public function updateClaimStatus()
    {
        $claimFactory = App_Claim_Factory::getInstance();
        $claim = $claimFactory->getClaimData($this->getTicket()->getClaimId());

        $statusClassName = $claimFactory->getStatusClassNameByClaimTypeId($claim->type_id ? $claim->type_id : $claim->typeId, $claim->claimType);
        $statusClassName::change($this->getTicket()->getClaimId(), $claim, true);
    }
}