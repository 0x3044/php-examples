<?php
use App_Finance_Ticket_Component_Observer_Type_ClaimNextState_Claim_ChangeStatusHandler_Abstract as AbstractHandler;

class App_Finance_Ticket_Component_Observer_Type_ClaimNextState_Claim_ChangeStatusHandler_RepairOut extends AbstractHandler
{
    /**
     * {@inheritdoc}
     */
    public function updateClaimStatus()
    {
        $claimId = $this->getTicket()->getClaimId();
        /** @var $model Claim_Model_RepairOut */
        $model = Claim_Model_RepairOut::mapper()->find($claimId);
        $model->changeStatusFromFinanceSupport();
    }
}