<?php
use App_Finance_Ticket_TicketInterface as Ticket;

/**
 * Хелпер, отвечающий за автоперевод заявки на определенные статусы в случаи присутствия/отсутствия определенных
 * флагов в заявке (dispetcherFlag/talibFlag). Включает в себя фабрику (::create)
 */
abstract class App_Finance_Ticket_Component_Observer_Type_ClaimNextState_Claim_ChangeStatusHandler_Abstract extends App_Finance_Ticket_Extension
{
    /**
     * Возвращает хелпер для заявки данного тикета
     * @param App_Finance_Ticket_TicketInterface $ticket
     * @return App_Finance_Ticket_Component_Observer_Type_ClaimNextState_Claim_ChangeStatusHandler_Default
     * @throws Exception
     */
    public static function create(Ticket $ticket)
    {
        if(!($ticket->hasClaimId())) {
            throw new \Exception('К запросу не указана заявка');
        }

        $claimData = App_Claim_Factory::getInstance()->getClaimData($ticket->getClaimId());

        switch($claimData->type_id) {
            default:
                throw new \Exception('Заявка данного типа не поддерживается наблюдателем ClaimNextState');

            case App_Claim_Factory::TYPE_ID_IN:
            case App_Claim_Factory::TYPE_ID_OUT:
            case App_Claim_Factory::TYPE_ID_RETURN_OUT:
            case App_Claim_Factory::TYPE_ID_SUPPLY:
            case App_Claim_Factory::TYPE_ID_CONVERSION:
            case App_Claim_Factory::TYPE_ID_INCOME:
            case App_Claim_Factory::TYPE_ID_RETURN_SUPPLY:
                return new App_Finance_Ticket_Component_Observer_Type_ClaimNextState_Claim_ChangeStatusHandler_Default($ticket);

            case App_Claim_Factory::TYPE_ID_INCOME_OUT:
                return new App_Finance_Ticket_Component_Observer_Type_ClaimNextState_Claim_ChangeStatusHandler_IncomeOut($ticket);

            case App_Claim_Factory::TYPE_ID_IN_SUPPLY:
                return new App_Finance_Ticket_Component_Observer_Type_ClaimNextState_Claim_ChangeStatusHandler_InSupply($ticket);

            case App_Claim_Factory::TYPE_ID_IN_OUT:
                return new App_Finance_Ticket_Component_Observer_Type_ClaimNextState_Claim_ChangeStatusHandler_InOut($ticket);

            case App_Claim_Factory::TYPE_ID_REPAIR_OUT:
                return new App_Finance_Ticket_Component_Observer_Type_ClaimNextState_Claim_ChangeStatusHandler_RepairOut($ticket);

            case App_Claim_Factory::TYPE_ID_SPARES:
                return new App_Finance_Ticket_Component_Observer_Type_ClaimNextState_Claim_ChangeStatusHandler_Spares($ticket);
        }
    }

    /**
     * Обновление статуса заявки
     */
    abstract public function updateClaimStatus();
}