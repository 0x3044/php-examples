<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;

/**
 * Рассылка "Изменения даты подачи транспорта"
 * Письма отправляются либо менеджеру, если у него заполнены email'ы, либо по указанным в рассылке email'ам в ином случаи
 */
class App_Finance_Ticket_Component_Observer_Type_MailingDispatcher_Observer extends AbstractObserver
{
    protected $_sendMail = false;

    public function onChange($fieldName)
    {
        if($this->getTicket()->getId() > 0 && $fieldName == "dateToProcess") {
            $this->_sendMail = true;
        }
    }

    public function onPerformMailing()
    {
        if($this->_sendMail) {
            /** @var $dbMailListType App_Db_MailListType */
            $dbMailListType = App_Db::get(DB_MAIL_LIST_TYPE);
            $mailListTypeId = $dbMailListType->getMailTypeIdByView('financeDispatcherDateToProcessChanged');

            App_Sendmail_Abstract::sendMail($mailListTypeId, $this->_getMailParams());
        }
    }

    private function _getMailParams()
    {
        $ticket = $this->getTicket();

        $sqlQuery = <<<SQL
            SELECT
                u.`name`, claims.url, claims.manager_id
            FROM ticket t
            LEFT JOIN claims ON claims.`id` = t.`claim_id`
            LEFT JOIN users u ON claims.`dispetcher` = u.`id`
SQL;
        $sqlQuery .= ' WHERE t.`id` = '.(int) $ticket->getId();

        if(!($sqlResult = Zend_Db_Table_Abstract::getDefaultAdapter()->query($sqlQuery)->fetch(Zend_Db::FETCH_ASSOC))) {
            throw new \Exception("Dispatcher not found");
        }

        $managerId = $sqlResult['manager_id'];
        $claimUrl = $sqlResult['url'];
        $dispatcherName = $sqlResult['name'];
        $httpHost = SITE_NAME . '/';

        $emailParams = array(
            'subjectParams' => array('"'.addslashes($ticket->getClaimId()).'"'),
            'claimId' => $ticket->getClaimId(),
            'claimUrl' => $httpHost . $claimUrl,
            'dateToProcess' => $ticket->getDateToProcess()->toString(App_Db::ZEND_DATETIME_RU_FORMAT),
            'dispatcher' => $dispatcherName
        );

        /** @var $clientEmailsDbTable App_Db_UserEmail */
        $clientEmailsDbTable = App_Db::get(DB_USER_EMAIL);
        $userEmails = $clientEmailsDbTable->getUserEmails($managerId);

        if(count($userEmails)) {
            $emailParams['recipients'] = $userEmails;
        }

        return $emailParams;
    }
}
