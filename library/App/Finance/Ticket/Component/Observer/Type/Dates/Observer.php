<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;
use App_Finance_Ticket_Component_Observer_ObserverException as ObserverException;

class App_Finance_Ticket_Component_Observer_Type_Dates_Observer extends AbstractObserver
{
    /**
     * Максимальная допустимая погрешность
     * @const int
     */
    const MAX_MINUTES_DELAY = 3;

    public function onBeforeSave()
    {
        $dateCreated = $this->getTicket()->getDateCreated();
        $dateToProcess = $this->getTicket()->getDateToProcess();
        $dateProcessed = $this->getTicket()->getDateProcessed();

        if(!($dateCreated)) {
            $dateCreated = new Zend_Date();
            $dateCreatedErrorMessage = "Нельзя создавать запрос в прошлом времени";
        } else {
            $dateCreatedErrorMessage = "Дата создания запроса должна быть меньше максимальной даты обработке запроса";
        }

        if($dateToProcess instanceof Zend_Date && $dateCreated instanceof Zend_Date) {
            if(!($this->_compareDates(clone $dateCreated, clone $dateToProcess))) {
                throw new ObserverException($dateCreatedErrorMessage);
            }
        }

        if($dateProcessed instanceof Zend_Date && $dateProcessed->toValue() > 0) {
            if(!($this->_compareDates(clone $dateCreated, clone $dateProcessed))) {
                throw new ObserverException("Дата создания запроса должна быть меньше максимальной даты обработке запроса");
            }
        }
    }

    /**
     * Возвращает true, если разница между leftDate и rightDate отрицательна и превышает допустимую погрешность
     * @param Zend_Date $leftDate
     * @param Zend_Date $rightDate
     * @return bool
     */
    protected function _compareDates(Zend_Date $leftDate, Zend_Date $rightDate)
    {
        $difference = $rightDate->sub($leftDate);

        $measure = new Zend_Measure_Time($difference->toValue(), Zend_Measure_Time::SECOND);
        $measure->convertTo(Zend_Measure_Time::MINUTE);

        $value = $measure->getValue();

        if($value >= 0) {
            return true;
        } else {
            return abs($value) <= self::MAX_MINUTES_DELAY;
        }
    }
}