<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;

/**
 * Заполняет поле processed_user_id при переводе тикета в завершенные
 */
class App_Finance_Ticket_Component_Observer_Type_ProcessedUserId_Observer extends AbstractObserver
{
    protected $_setupProcessedUserId = false;

    public function onSetCompleted()
    {
        $this->_setupProcessedUserId = true;
    }

    public function onBeforeSave()
    {
        if($this->_setupProcessedUserId && !(defined('PHPUNIT_TEST'))) {
            $zendAuth = Zend_Auth::getInstance()->getIdentity();

            if(!($zendAuth && $zendAuth->id > 0)) {
                throw new \Exception("No processedUserId available");
            }

            $this->getTicket()->setProcessedUserId($zendAuth->id);
        }
    }
}
