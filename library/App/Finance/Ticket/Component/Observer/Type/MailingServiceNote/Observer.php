<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;
use App_Finance_Ticket_TicketInterface as Ticket;
use App_Finance_Ticket_Factory as TicketFactory;

/**
 * Рассылка "Служебная записка"
 */
class App_Finance_Ticket_Component_Observer_Type_MailingServiceNote_Observer extends AbstractObserver
{
    /**
     * Флаг "отправлять рассылку"
     * @var bool
     */
    protected $_sendMail = false;

    /**
     * Рассылка будет отправлена после создания запроса, но  после сохранения запроса
     * метод isNewTicket всегда будет возвращать false, т.к. основывается на значении Id запроса
     */
    public function onBeforeSave()
    {
        $ticket = $this->getTicket();

        if($ticket->isNewTicket()) {
            $this->_sendMail = true;
        }
    }

    /**
     * Проверяет, возможно ли рассылка запроса и рассылает, если возможна
     * @param array $postParams
     */
    public function onPerformMailing(Array $postParams = NULL)
    {
        if($this->_sendMail) {
            if($snTicket = $this->findArchivedTicket()) {
                $this->performMailing($snTicket);
            }
        }
    }

    /**
     * Ищет архивный запрос с тем же самым claimId
     * Возвращает запрос либо false в случае отсутствия результатов
     * @return bool|Ticket
     */
    public function findArchivedTicket()
    {
        return TicketFactory::getInstance()->hasCanceledArchivedTicket($this->getTicket()->getClaimId());
    }

    /**
     * Рассылка
     */
    public function performMailing()
    {
        /** @var $dbMailListType App_Db_MailListType */
        $dbMailListType = App_Db::get(DB_MAIL_LIST_TYPE);
        $mailListTypeId = $dbMailListType->getMailTypeIdByView('financeSupportServiceNote');

        App_Sendmail_Abstract::sendMail($mailListTypeId, $this->_getMailParams());
    }

    /**
     * Параметры рассылки
     * @throws Exception
     * @return array
     */
    protected function _getMailParams()
    {
        $ticket = $this->getTicket();

        $httpHost = SITE_NAME . '/';
        $serviceNoteUrl = $httpHost . $ticket->getServiceNoteHandler()->getLink();
        $serviceNoteFileName = $ticket->getServiceNoteHandler()->getFileName();

        $ticketData = new App_Finance_Ticket_Component_AdditionalTicketData($ticket);

        $emailParams = array(
            'subjectParams' => array('"'.addslashes($ticket->getClaimId()).'"'),
            'claimId' => $ticket->getClaimId(),
            'claimUrl' => $httpHost.$ticketData->getClaimUrl(),
            'serviceNoteUrl' => $serviceNoteUrl,
            'serviceNoteFileName' => $serviceNoteFileName,
            'managerName' => $ticketData->getManagerName(),
            'clientName' => $ticketData->getClientName()
        );

        return $emailParams;
    }
}
