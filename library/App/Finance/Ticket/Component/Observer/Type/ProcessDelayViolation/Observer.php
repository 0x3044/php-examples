<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;
use App_Finance_Ticket_Factory as TicketFactory;

/**
 * Фиксирует нарушения просрочки тикета и сохраняет в json-данные тикета.
 * Просрочек может быть более одной.
 */
class App_Finance_Ticket_Component_Observer_Type_ProcessDelayViolation_Observer extends AbstractObserver
{
    public function onBeforeSave()
    {
        $ticket = $this->getTicket();

        if($ticket->isExistedTicket()) {
            $oldTicket = TicketFactory::getInstance()->createFromTicketId($ticket->getId());

            $currentDate = new Zend_Date();
            $oldDateToProcess = $oldTicket->getDateToProcess();
            $newDateToProcess = $ticket->getDateToProcess();

            if($newDateToProcess->compare($oldDateToProcess) !== 0 && $currentDate->compare($oldDateToProcess) >= 0) {
                /** @var $difference Zend_Date */
                $difference = $currentDate->sub($oldDateToProcess);

                $measure = new Zend_Measure_Time($difference->toValue(), Zend_Measure_Time::SECOND);
                $measure->convertTo(Zend_Measure_Time::MINUTE);

                $jsonHandler = $ticket->getJsonDataHandler();

                if(!(isset($jsonHandler['processDelayViolation']))) {
                    $jsonHandler['processDelayViolation'] = array();
                }

                $jsonHandler['processDelayViolation'][] = array(
                    'oldDateToProcess' => $oldDateToProcess->toString(),
                    'newDateToProcess' => $newDateToProcess->toString(),
                    'delay' => (int) $measure->getValue()
                );
            }
        }
    }
}