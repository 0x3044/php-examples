<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;

class App_Finance_Ticket_Component_Observer_Type_Abstract_StatusChangedObserver extends AbstractObserver
{
    protected $_statusChanged = false;

    protected $_oldStatus = NULL;

    public function onStatusBeforeChange()
    {
        if($this->_oldStatus === NULL) {
            $this->_oldStatus = $this->getTicket()->getStatusHandler()->toInt();
        }
    }

    public function onStatusAfterChange()
    {
        $this->_statusChanged = ($this->_oldStatus !== $this->getTicket()->getStatusHandler()->toInt());
    }

    protected final function _statusChanged()
    {
        return $this->_statusChanged;
    }
}