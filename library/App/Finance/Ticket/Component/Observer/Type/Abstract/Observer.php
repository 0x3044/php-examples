<?php
use App_Finance_Ticket_Component_Observer_Component_Observer as Observer;

abstract class App_Finance_Ticket_Component_Observer_Type_Abstract_Observer extends Observer
{
    /**
     * Вызов события
     * @param string $eventName
     * @param mixed $additionalParams
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public final function on($eventName, $additionalParams = NULL)
    {
        $eventName = App_Spl_TypeCheck::getInstance()->underscoreToCamelCase($eventName);

        if(!(is_string($eventName))) {
            throw new \InvalidArgumentException('eventName should have a string type, got '.gettype($eventName));
        }

        if(!(preg_match('/^on(.*)$/', $eventName))) {
            throw new \Exception("Unknown event \"{$eventName}\"");
        }

        $callMethod = $eventName;

        if(method_exists($this, $callMethod)) {
            return $this->$callMethod($additionalParams);
        }

        return true;
    }

    /*
     * -- Методы ниже имеют справочный характер --
     */

    /**
     * Событие вызывается перед изменением статуса тикета
     */
    public function onStatusBeforeChange()
    {
    }

    /**
     * Событие вызывается после изменения статуса заявки
     */
    public function onStatusAfterChange()
    {
    }

    /**
     * Событие вызывается при установке тикету статуса "активный"
     */
    public function onStatusSetActive()
    {
    }

    /**
     * Событие вызывается при установке тикету статуса "отмененный"
     */
    public function onStatusSetCanceled()
    {
    }

    /**
     * Событие вызывается при установке тикету статуса "архивный"
     */
    public function onStatusSetArchived()
    {
    }

    /**
     * Событие вызывается при установке тикету статуса "ожидающий отмены"
     */
    public function onStatusSetPendingCancel()
    {
    }

    /**
     * Событие вызывается при установке тикету статуса "удаленный"
     */
    public function onStatusSetDeleted()
    {
    }

    /**
     * Событие вызывается перед сохранением изменений в тикете
     */
    public function onBeforeSave()
    {
    }

    /**
     * Событие вызывается после сохранения изменений в тикете
     */
    public function onAfterSave()
    {
    }

    /**
     * Событие вызывается перед удалением файлов
     */
    public function onBeforeDestroy()
    {
    }

    /**
     * Событие вызывается после удаление файлов
     */
    public function onAfterDestroy()
    {
    }

    /**
     * Событие вызывается перед сохранением изменений в прикрепленном к тикету документу
     */
    public function onBeforeDocumentSave()
    {
    }

    /**
     * Событие вызывается после сохранения изменений в прикрепленном к тикету документу
     */
    public function onAfterDocumentSave()
    {
    }

    /**
     * Собыите вызывается перед загрузкой документа к запросу
     */
    public function onBeforeDocumentUpload()
    {
    }

    /**
     * Событие вызывается после загрузки документа к запросу
     */
    public function onAfterDocumentUpload()
    {
    }

    /**
     * Событие вызывается при отмечания запроса как выполненый
     */
    public function onSetCompleted()
    {
    }

    /**
     * Событие вызывается при изменении данных тикета
     * @param string $fieldName Поле, которые было изменено
     */
    public function onChange($fieldName)
    {
    }

    /**
     * Событие вызывается перед выполнением рассылки
     */
    public function onPerformMailing()
    {
    }

    /**
     * Событие вызывается после сохранения данных заявки, к которому принадлежит тикет
     * @param array $postParams
     */
    public function onAfterClaimSave(Array $postParams = NULL)
    {
    }

    /**
     * Событие вызывается после выполнения каскада у заявки
     */
    public function onAfterClaimCascade()
    {
    }
}