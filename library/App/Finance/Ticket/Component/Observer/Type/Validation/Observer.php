<?php
use App_Finance_Ticket_Component_Observer_Type_Abstract_Observer as AbstractObserver;
use App_Finance_Ticket_Component_Validation_Handler as ValidationHandler;

/**
 * Валидация - проверка и выброс исключений при создании/сохранении изменений в тикете
 */
class App_Finance_Ticket_Component_Observer_Type_Validation_Observer extends AbstractObserver
{
    /**
     * Валидатор
     * @var ValidationHandler
     */
    protected $_validationHandler;

    /**
     * Запуск валидатор перед каждым сохранением
     */
    public function onBeforeSave()
    {
        $ticket = $this->getTicket();

        if($ticket->isNewTicket() || !($ticket->getStatusHandler()->equalsToString('canceled'))) {
            $validationHandler = $this->getValidationHandler();
            $validationHandler->validateByTicket($this->getTicket());
        }
    }

    /**
     * @return \App_Finance_Ticket_Component_Validation_Handler
     */
    public function getValidationHandler()
    {
        if(!($this->_validationHandler instanceof ValidationHandler)) {
            $this->_validationHandler = new ValidationHandler();
        }

        return $this->_validationHandler;
    }
}