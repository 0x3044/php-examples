<?php
use App_Finance_Ticket_TicketInterface as Ticket;
use App_Finance_Ticket_Component_Observer_ObserverInterface as Observer;

abstract class App_Finance_Ticket_Component_Observer_Component_Observer implements Observer
{
    /**
     * Тикет, к которому прикреплен наблюдатель
     * @var Ticket
     */
    protected $_ticket;

    /**
     * Создает наблюдатель. Если указан тикет, то автоматически начинает наблюдение за ним.
     * @param App_Finance_Ticket_TicketInterface $ticket
     */
    public final function __construct(Ticket $ticket = NULL)
    {
        if($ticket instanceof Ticket) {
            $this->setTicket($ticket);
        }
    }

    /**
     * Указать тикет для наблюдения
     * @param App_Finance_Ticket_TicketInterface $ticket
     */
    public function setTicket(Ticket $ticket)
    {
        $this->_ticket = $ticket;
    }

    /**
     * Возвращает тикет, указанный для наблюдения
     * @return App_Finance_Ticket_TicketInterface
     */
    public final function getTicket()
    {
        return $this->_ticket;
    }
}