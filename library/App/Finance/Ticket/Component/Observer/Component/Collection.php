<?php
use App_Spl_Collection_Abstract as SplCollection;
use App_Finance_Ticket_Component_Observer_ObserverInterface as Observer;

class App_Finance_Ticket_Component_Observer_Component_Collection extends SplCollection
{
    /**
     * Returns true if item is allowed for this collection
     * @param $item
     * @return bool
     */
    function isItemAllowed($item)
    {
        return $item instanceof Observer;
    }
}