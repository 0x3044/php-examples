<?php
use App_Finance_Ticket_Component_Observer_ObserverInterface as ObserverInterface;

interface App_Finance_Ticket_Component_Observer_CollectionInterface extends ObserverInterface
{
    /**
     * Добавить наблюдатель в коллекцию
     * @param App_Finance_Ticket_Component_Observer_ObserverInterface $observer
     * @return mixed
     */
    public function add(ObserverInterface $observer);

    /**
     * Enable triggering observers
     */
    public function enableObservers();

    /**
     * Disable triggering observers
     */
    public function disableObservers();

    /**
     * Remove observer by className
     * @param string $className
     */
    public function removeObserverByClassName($className);

    /**
     * Returns true if observer exists
     * @param $className
     * @return bool
     */
    public function hasObserverWithClassName($className);

    /**
     * Returns true if observers enabled
     */
    public function isEnabledObservers();

    /**
     * Возвращает коллекцию наблюдателей
     * @return \App_Finance_Ticket_Component_Observer_Component_Collection
     */
    public function getObservers();
}