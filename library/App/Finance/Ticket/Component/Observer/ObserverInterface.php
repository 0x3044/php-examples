<?php
use App_Finance_Ticket_TicketInterface as Ticket;

interface App_Finance_Ticket_Component_Observer_ObserverInterface
{
    /**
     * Создает наблюдатель. Если указан тикет, то автоматически начинает наблюдение за ним.
     * @param App_Finance_Ticket_TicketInterface $ticket
     */
    public function __construct(Ticket $ticket = NULL);

    /**
     * Триггер события
     * Вызывает указанный в данном классе.
     * Событие должно начинаться с "on".
     * @param string $eventName
     * @param mixed $additionalParams
     * @return mixed
     */
    public function on($eventName, $additionalParams = NULL);

    /**
     * Указать тикет для наблюдения
     * @param App_Finance_Ticket_TicketInterface $ticket
     */
    public function setTicket(Ticket $ticket);

    /**
     * Возвращает тикет, указанный для наблюдения
     * @return App_Finance_Ticket_TicketInterface
     */
    public function getTicket();
}