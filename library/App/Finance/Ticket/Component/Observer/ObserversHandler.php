<?php
use App_Finance_Ticket_TicketInterface as Ticket;
use App_Finance_Ticket_Component_Observer_Component_Observer as Observer;
use App_Finance_Ticket_Component_Observer_CollectionInterface as ObserverCollectionInterface;
use App_Finance_Ticket_Component_Observer_Component_Collection as ObserverCollection;
use App_Finance_Ticket_Component_Observer_ObserverInterface as ObserverInterface;

class App_Finance_Ticket_Component_Observer_ObserversHandler extends Observer implements ObserverCollectionInterface
{
    /**
     * Включены/отключены наблюдатели
     * @var bool
     */
    protected $_observersEnabled;

    /**
     * Список наблюдателей
     * @var ObserverCollection
     */
    protected $_observers;

    /**
     * Триггер события
     * Вызывает указанный метод во всех наблюдателях данной коллекции.
     * Событие должно начинаться с "on".
     * @param string $eventName
     * @param mixed $additionalParams
     * @return mixed|void
     */
    public final function on($eventName, $additionalParams = NULL)
    {
        $observers = $this->getObservers();

        if($this->isEnabledObservers()) {
            /** @var $observer Observer */
            foreach($observers->getItems() as $observer) {
                $observer->on($eventName, $additionalParams);
            }
        }
    }

    /**
     * Включить наблюдателей
     */
    public final function enableObservers()
    {
        $this->_observersEnabled = true;
    }

    /**
     * Отключить наблюдателей
     */
    public final function disableObservers()
    {
        $this->_observersEnabled = false;
    }

    /**
     * Возвращает true, если наблюдатели сейчас активны
     * @return bool
     */
    public final function isEnabledObservers()
    {
        return (bool) $this->_observersEnabled;
    }

    /**
     * Добавить наблюдатель в коллекцию
     * @param App_Finance_Ticket_Component_Observer_ObserverInterface $observer
     * @return mixed
     */
    public function add(ObserverInterface $observer)
    {
        if($this->getTicket() instanceof Ticket) {
            $observer->setTicket($this->getTicket());
        }

        $this->getObservers()->add($observer);
    }

    /**
     * Возвращает наблюдатель по его имени
     * @param $observerName
     * @return App_Finance_Ticket_Component_Observer_Component_Observer
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function get($observerName)
    {
        if(!(is_string($observerName) && strlen($observerName))) {
            throw new \InvalidArgumentException('Invalid observerName, expected string, got '.var_export($observerName, true));
        }

        if(!($this->getObservers()->size())) {
            throw new \Exception("Observer `{$observerName}` not found");
        }

        /** @var $observer Observer */
        foreach($this->getObservers() as $observer) {
            if(strpos(get_class($observer), $observerName) !== false) {
                return $observer;
            }
        }

        throw new \Exception("Observer `{$observerName}` not found");
    }

    /**
     * Возвращает true, если наблюдатель с указанным именем класса существует в коллекции
     * @param $className
     * @return bool
     * @throws InvalidArgumentException
     */
    public final function hasObserverWithClassName($className)
    {
        $observers = $this->getObservers();

        if(!(is_subclass_of($className, 'App_Finance_Ticket_Component_Observer_ObserverInterface'))) {
            throw new \InvalidArgumentException(var_export($className).' is not a valid observer');
        }

        foreach($observers as $observer) {
            if($observer instanceof $className) {
                return true;
            }
        }

        return false;
    }

    /**
     * Удаляет наблюдатель по имени класса
     * @param string $className
     * @throws InvalidArgumentException
     */
    public final function removeObserverByClassName($className)
    {
        $observers = $this->getObservers();

        if(!(is_subclass_of($className, 'App_Finance_Ticket_Component_Observer_ObserverInterface'))) {
            throw new \InvalidArgumentException(var_export($className).' is not a valid observer');
        }

        /** @var $observer Observer */
        foreach($observers->getItems() as $position => $observer) {
            if($observer instanceof $className) {
                $observers->remove($position);
            }
        }
    }

    /**
     * Возвращает коллекцию наблюдателей
     * @return \App_Finance_Ticket_Component_Observer_Component_Collection
     */
    public final function getObservers()
    {
        if(!($this->_observers instanceof ObserverCollection)) {
            $this->_observers = new ObserverCollection();
        }

        return $this->_observers;
    }

    /**
     * Указать тикет для наблюдения
     * @param App_Finance_Ticket_TicketInterface $ticket
     */
    public function setTicket(Ticket $ticket)
    {
        $this->_ticket = $ticket;

        $observers = $this->getObservers();

        if($observers->size()) {
            /** @var $observer Observer */
            foreach($observers as $observer) {
                $observer->setTicket($ticket);
            }
        }
    }
}