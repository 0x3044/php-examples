<?php
use App_Finance_Ticket_Component_Decorator_AbstractDecorator as AbstractDecorator;
use App_Finance_Ticket_TicketInterface as Ticket;

/*
 * Список дефолтных валидаторов
 */
use App_Finance_Ticket_Component_Validation_Type_ClaimId_Validator as ClaimIdValidator;
use App_Finance_Ticket_Component_Validation_Type_ClaimTypeSupported_Validator as ClaimTypeSupportedValidator;
use App_Finance_Ticket_Component_Validation_Type_WasCompleted_Validator as WasCompletedValidator;
use App_Finance_Ticket_Component_Validation_Type_ClientId_Validator as ClientIdValidator;
use App_Finance_Ticket_Component_Validation_Type_UniqueTicket_Validator as UniqueIdValidator;
use App_Finance_Ticket_Component_Validation_Type_HasPendingCancelTicket_Validator as HasPendingCancelTicketValidator;

class App_Finance_Ticket_Component_Decorator_Type_DefaultValidators extends AbstractDecorator
{
    /**
     * Добавление дефолтных валидаторов
     * @param Ticket $ticket
     * @return mixed
     */
    public function decorate(Ticket $ticket)
    {
        $handler = $ticket->getValidationObserver()->getValidationHandler();

        $handler->add(new ClaimIdValidator());
        $handler->add(new ClaimTypeSupportedValidator());
        $handler->add(new HasPendingCancelTicketValidator());
        $handler->add(new WasCompletedValidator());
        $handler->add(new ClientIdValidator());
        $handler->add(new UniqueIdValidator());
    }
}