<?php
use App_Finance_Ticket_Component_Decorator_AbstractDecorator as AbstractDecorator;
use App_Finance_Ticket_TicketInterface as Ticket;

/*
 * Список дефолтных наблюдателей
 */
use App_Finance_Ticket_Component_Observer_Type_MailingSetCanceled_Observer as MailingObserver;
use App_Finance_Ticket_Component_Observer_Type_UploadDocument_Observer as UploadDocumentObserver;
use App_Finance_Ticket_Component_Observer_Type_CashPlanned_Observer as CashPlannedObserver;
use App_Finance_Ticket_Component_Observer_Type_EndState_Observer as EndStateObserver;
use App_Finance_Ticket_Component_Observer_Type_DateProcessed_Observer as DateProcessedObserver;
use App_Finance_Ticket_Component_Observer_Type_ProcessedUserId_Observer as ProcessedUserIdObserver;
use App_Finance_Ticket_Component_Observer_Type_MailingTicketNotViewed_Observer as MailingTicketNotViewedObserver;
use App_Finance_Ticket_Component_Observer_Type_Dates_Observer as DatesObserver;
use App_Finance_Ticket_Component_Observer_Type_WasArchivedFlag_Observer as WasArchivedFlagObserver;
use App_Finance_Ticket_Component_Observer_Type_SetCompletedOnStatusChange_Observer as SetCompletedOnStatusChangeObserver;
use App_Finance_Ticket_Component_Observer_Type_MemcachedCounter_Observer as MemcachedCounterObserver;

class App_Finance_Ticket_Component_Decorator_Type_DefaultObservers extends AbstractDecorator
{
    /**
     * Декорирует тикет
     * @param Ticket $ticket
     * @return mixed
     */
    public function decorate(Ticket $ticket)
    {
        $observers = $ticket->getObserversHandler();

        $observers->add(new DatesObserver());
        $observers->add(new SetCompletedOnStatusChangeObserver());
        $observers->add(new UploadDocumentObserver());
        $observers->add(new CashPlannedObserver());
        $observers->add(new DateProcessedObserver());
        $observers->add(new DateProcessedObserver());
        $observers->add(new EndStateObserver());
        $observers->add(new ProcessedUserIdObserver());
        $observers->add(new WasArchivedFlagObserver());
        $observers->add(new MemcachedCounterObserver());

        $observers->add(new MailingObserver());
        $observers->add(new MailingTicketNotViewedObserver());
    }
}