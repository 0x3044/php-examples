<?php
use App_Finance_Ticket_TicketInterface as Ticket;

/**
 * Декоратор тикетов
 */
abstract class App_Finance_Ticket_Component_Decorator_AbstractDecorator
{
    /**
     * Декорирует тикет
     * @param Ticket $ticket
     * @return mixed
     */
    abstract public function decorate(Ticket $ticket);
}