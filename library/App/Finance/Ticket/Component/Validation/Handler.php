<?php
use App_Spl_Collection_Abstract as SplCollection;
use App_Finance_Ticket_TicketInterface as Ticket;
use App_Finance_Ticket_Component_Validation_Validator as Validator;
use App_Finance_Ticket_Component_Validation_Exception as ValidationException;

class App_Finance_Ticket_Component_Validation_Handler extends SplCollection
{
    /**
     * Returns true if item is allowed for this collection
     * @param $item
     * @return bool
     */
    public function isItemAllowed($item)
    {
        return $item instanceof Validator;
    }

    /**
     * Валидация по Id заявки
     * @param int $claimId
     * @throws ValidationException
     */
    public function validateByClaimId($claimId)
    {
        /** @var $validator Validator */
        foreach($this->getItems() as $validator) {
            $validator->create($claimId);
            $validator->validate();
        }
    }

    /**
     * Валидация по существующему объекту запроса
     * @param App_Finance_Ticket_TicketInterface $ticket
     * @throws ValidationException
     */
    public function validateByTicket(Ticket $ticket)
    {
        /** @var $validator Validator */
        foreach($this->getItems() as $validator) {
            $validator->create($ticket->getClaimId(), $ticket);
            $validator->validate();
        }
    }
}