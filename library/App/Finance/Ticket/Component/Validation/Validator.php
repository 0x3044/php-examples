<?php
use App_Finance_Ticket_TicketInterface as Ticket;
use App_Finance_Ticket_Component_Validation_Exception as ValidationException;

interface App_Finance_Ticket_Component_Validation_Validator
{
    /**
     * Валидация (возможно, еще не существующих) запросов по claimId
     * В случаи, если валидация запускается для уже существующего тикета, также передается тикет
     * @param int $claimId
     * @param App_Finance_Ticket_TicketInterface $ticket
     * @throws Exception
     */
    public function create($claimId, Ticket $ticket = NULL);

    /**
     * Провести валидацию
     * @throws ValidationException
     */
    public function validate();

    /**
     * Возвращает указанный при создании валидатора Id заявки
     * @return int
     */
    public function getClaimId();

    /**
     * Возвращает указанный при создании валидатора запрос
     * @return Ticket
     */
    public function getTicket();

    /**
     * Возврашает true, если имеется информация о тикете, над которым проводится валидация
     * @return bool
     */
    public function hasTicket();
}