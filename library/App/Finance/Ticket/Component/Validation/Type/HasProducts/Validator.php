<?php
use App_Finance_Ticket_Component_Validation_Type_Abstract_Validator as Validator;
use App_Finance_Ticket_Component_Validation_Exception as ValidationException;

class App_Finance_Ticket_Component_Validation_Type_HasProducts_Validator extends Validator
{
    /**
     * Запрещает создавать запрос, если по заявке нет товаров
     * @throws \App_Finance_Ticket_Component_Validation_Exception
     */
    public function validate()
    {
        /** @var $dbClaimProductTable App_Db_ClaimProducts */
        $dbClaimProductTable = App_Db::get(DB_CLAIM_PRODUCTS);

        if(!($dbClaimProductTable->getMaxNumber($this->getClaimId()))) {
            throw new ValidationException("Нельзя создать запрос в это направление при отсутствии товаров в заявке");
        }
    }
}