<?php
use App_Finance_Ticket_Component_Validation_Type_Abstract_Validator as Validator;
use App_Finance_Ticket_Component_Validation_Type_InvoicesAvailable_Exception as ValidationException;

class App_Finance_Ticket_Component_Validation_Type_InvoicesAvailable_Validator extends Validator
{
    /**
     * Запрещает создавать запрос в бухгалерский саппорт, если к заявке не прикреплены счета
     * @throws \App_Finance_Ticket_Component_Validation_Exception
     */
    public function validate()
    {
        $ticket = $this->getTicket();
        $claimId = $ticket->getClaimId();

        /** @var $claimInvoicesDbTable App_Db_ClaimInvoices */
        $claimInvoicesDbTable = App_Db::get(DB_CLAIM_INVOICES);

        if(!($claimInvoicesDbTable->hasSetInvoices($claimId)) || $claimInvoicesDbTable->hasSetNoDocumentsOptions($claimId)) {
            throw new ValidationException("Нельзя создавать запросы в бухгалтерский саппорт к заявкам, у которых не указаны счета либо их отсутствие");
        }
    }
}