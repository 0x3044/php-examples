<?php
use App_Finance_Ticket_Component_Validation_Type_Abstract_Validator as Validator;
use App_Finance_Ticket_Component_Validation_Type_ClientId_Exception as ClientNotFoundException;

class App_Finance_Ticket_Component_Validation_Type_ClientId_Validator extends Validator
{
    /**
     * Запрещает создавать запрос для заявок, у которых не указан Id клиента
     * @throws \App_Finance_Ticket_Component_Validation_Exception
     */
    public function validate()
    {
        $claimId = $this->getClaimId();

        /** @var $dbClaimTable App_Db_Claims */
        $dbClaimTable = App_Db::get(DB_CLAIMS);
        $claim = $dbClaimTable->getById($claimId);

        if(!($claim)) {
            throw new \Exception("Claim #{$claimId} not found");
        }

        if(!(isset($claim->client_id) && $claim->client_id)) {
            throw new ClientNotFoundException("Запрещено создать запрос в заявках, в которых не указан клиент");
        }
    }
}