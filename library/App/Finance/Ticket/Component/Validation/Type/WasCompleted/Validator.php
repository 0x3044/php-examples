<?php
use App_Finance_Ticket_Component_Validation_Type_Abstract_Validator as Validator;
use App_Finance_Ticket_Component_Validation_Type_WasCompleted_Exception as ValidationException;
use App_Finance_Ticket_Factory as TicketFactory;

class App_Finance_Ticket_Component_Validation_Type_WasCompleted_Validator extends Validator
{
    /**
     * Запрещает создавать запрос по указанной заявке, если уже существует архивный запрос в указанное направление
     * @throws \App_Finance_Ticket_Component_Validation_Exception
     */
    public function validate()
    {
        $claimId = $this->getClaimId();
        $ticket = $this->getTicket();
        $tickets = TicketFactory::getInstance()->getTicketsByClaimId($claimId);

        if($tickets->size()) {
            /** @var $cTicket App_Finance_Ticket_TicketInterface */
            foreach($tickets as $cTicket) {
                $sameTicket = $ticket->getId() == $cTicket->getId();
                $isCompleted = $cTicket->getStatusHandler()->equalsToString("archived");
                $sameTicketType = $ticket->getTicketTypeId() == $cTicket->getTicketTypeId();

                if(!($sameTicket) && $sameTicketType && $isCompleted) {
                    throw new ValidationException('Запрос в данное направление уже был рассмотрен');
                }
            }
        }
    }
}