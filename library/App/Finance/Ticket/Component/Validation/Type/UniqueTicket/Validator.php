<?php
use App_Finance_Ticket_Component_Validation_Type_Abstract_Validator as Validator;
use App_Finance_Ticket_Component_Validation_Type_UniqueTicket_Exception as ValidationException;
use App_Finance_Ticket_Factory as TicketFactory;

class App_Finance_Ticket_Component_Validation_Type_UniqueTicket_Validator extends Validator
{
    /**
     * Запрещает создавать запрос данного типа, если уже есть активный запрос в данное направление
     * @throws \App_Finance_Ticket_Component_Validation_Exception
     */
    public function validate()
    {
        $claimId = $this->getClaimId();
        $ticket = $this->getTicket();
        $tickets = TicketFactory::getInstance()->getTicketsByClaimId($claimId);

        if($tickets->size()) {
            /** @var $cTicket App_Finance_Ticket_TicketInterface */
            foreach($tickets as $cTicket) {
                $sameTicket = $ticket->getId() == $cTicket->getId();
                $isCanceled = $cTicket->getStatusHandler()->toString() == 'canceled' || $ticket->getStatusHandler()->toString() == 'canceled';
                $sameTicketType = $ticket->getTicketTypeId() == $cTicket->getTicketTypeId();

                if(!($sameTicket) && $sameTicketType && !($isCanceled)) {
                    throw new ValidationException('Запрос в данное направление уже существует и находится на рассмотрении');
                }
            }
        }
    }
}