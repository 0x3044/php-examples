<?php
use App_Finance_Ticket_Component_Validation_Type_Abstract_Validator as Validator;
use App_Finance_Ticket_Component_Validation_Type_ClaimTypeSupported_Exception as ClaimTypeSupportedException;

/**
 * Запрет создавать запросы для типов заявок, которые не поддерживаются саппортом
 */
class App_Finance_Ticket_Component_Validation_Type_ClaimTypeSupported_Validator extends Validator
{
    /**
     * Карта "тип запроса" => список поддерживаемых типов запросов
     * @var array
     */
    protected $_supportMap = array(
        App_Claim_Factory::TYPE_ID_OUT => array(
            "accounting_waybill" => true,
            "dispatcher_verify" => true,
            "depot_verify" => true,
        ),

        App_Claim_Factory::TYPE_ID_IN => array(
            "accounting_waybill" => false,
            "dispatcher_verify" => true,
            "depot_verify" => false,
        ),

        App_Claim_Factory::TYPE_ID_IN_OUT => array(
            "accounting_waybill" => true,
            "dispatcher_verify" => true,
            "depot_verify" => false,
        ),

        App_Claim_Factory::TYPE_ID_RETURN_OUT => array(
            "accounting_waybill" => false,
            "dispatcher_verify" => true,
            "depot_verify" => false,
        ),

        App_Claim_Factory::TYPE_ID_SUPPLY => array(
            "accounting_waybill" => false,
            "dispatcher_verify" => true,
            "depot_verify" => false,
        ),

        App_Claim_Factory::TYPE_ID_CONVERSION => array(
            "accounting_waybill" => false,
            "dispatcher_verify" => false,
            "depot_verify" => false,
        ),

        App_Claim_Factory::TYPE_ID_INCOME => array(
            "accounting_waybill" => false,
            "dispatcher_verify" => true,
            "depot_verify" => false,
        ),

        App_Claim_Factory::TYPE_ID_INCOME_OUT => array(
            "accounting_waybill" => true,
            "dispatcher_verify" => true,
            "depot_verify" => false,
        ),

        App_Claim_Factory::TYPE_ID_IN_SUPPLY => array(
            "accounting_waybill" => false,
            "dispatcher_verify" => true,
            "depot_verify" => false,
        ),

        App_Claim_Factory::TYPE_ID_RETURN_SUPPLY => array(
            "accounting_waybill" => false,
            "dispatcher_verify" => true,
            "depot_verify" => false,
        ),

        App_Claim_Factory::TYPE_ID_REPAIR_OUT => array(
            "accounting_waybill" => false,
            "dispatcher_verify" => true,
            "depot_verify" => false,
        ),

        App_Claim_Factory::TYPE_ID_SPARES => array(
            "accounting_waybill" => true,
            "dispatcher_verify" => true,
            "depot_verify" => true,
        ),
    );

    /**
     * Выбрасывает исключение, если запрос данного типа не поддерживается данным типом заявки
     * @throws App_Finance_Ticket_Component_Validation_Type_ClaimTypeSupported_Exception
     */
    public function validate()
    {
        $ticket = $this->getTicket();
        $typeId = App_Claim_Factory::getInstance()->getTypeId($ticket->getClaimId());

        if(!($this->isClaimTypeSupported($typeId))) {
            throw new ClaimTypeSupportedException("Запрещено отправлять запрос данного типа для заявок данного типа");
        }
    }

    /**
     * Возвращает карту поддержки запросов
     * @return array
     */
    public function getSupportMap()
    {
        return $this->_supportMap;
    }

    /**
     * Возвращает true, если запрос данного типа поддерживается данным типом заявки
     * @param $claimType
     * @return bool
     * @throws Exception
     */
    public function isClaimTypeSupported($claimType)
    {
        $supportMap = $this->getSupportMap();
        $ticketType = $this->getTicket()->getTicketTypeName();

        if(!(array_key_exists($claimType, $this->getSupportMap()))) {
            throw new \Exception("Заявки данного типа не поддерживаются финансовым саппортом");
        }

        return isset($supportMap[$claimType][$ticketType]) && $supportMap[$claimType][$ticketType];
    }
}