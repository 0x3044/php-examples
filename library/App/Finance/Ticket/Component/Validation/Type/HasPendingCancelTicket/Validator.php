<?php
use App_Finance_Ticket_Component_Validation_Type_Abstract_Validator as Validator;
use App_Finance_Ticket_Component_Validation_Type_HasPendingCancelTicket_Exception as ValidationException;

class App_Finance_Ticket_Component_Validation_Type_HasPendingCancelTicket_Validator extends Validator
{
    /**
     * Запрещает создавать запрос, если по данному типу запроса уже есть ожидающий отмены запрос
     * @throws \App_Finance_Ticket_Component_Validation_Exception
     */
    public function validate()
    {
        $ticket = $this->getTicket();

        if($ticket->getStatusHandler()->isActive()) {
            $finder = new App_Finance_Ticket_Finder();
            $finder->setClaimIds($ticket->getClaimId());
            $finder->setTicketStatus(App_Finance_Ticket_Component_Status_Type_Default_Status::STATUS_PENDING_CANCEL);
            $finder->setTicketTypeIds($ticket->getTicketTypeId());

            if($ticket->isExistedTicket()) {
                $finder->setAdditionalWhere('id not in ('.$ticket->getId().')');
            }

            if($finder->findOne()) {
                throw new ValidationException('По заявке есть ожидающий отмены запрос');
            }
        }
    }
}