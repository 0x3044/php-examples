<?php
use App_Finance_Ticket_Component_Validation_Type_Abstract_Validator as Validator;
use App_Finance_Ticket_Component_Validation_Type_CashAccountOnly_Exception as ValidationException;

class App_Finance_Ticket_Component_Validation_Type_CashAccountOnly_Validator extends Validator
{
    /**
     * Запрещает создавать запросы в бухгалтерию, если заявка не является безнал
     * @throws \App_Finance_Ticket_Component_Validation_Exception
     */
    public function validate()
    {
        $claimId = $this->getClaimId();

        /** @var $dbClaimTable App_Db_Claims */
        $dbClaimTable = App_Db::get(DB_CLAIMS);
        $claim = $dbClaimTable->getById($claimId);

        if(!($claim)) {
            throw new \Exception("Claim #{$claimId} not found");
        }

        if(!($this->getTicket()->getStatusHandler()->equalsToString("canceled"))) {
            if(!(isset($claim->payment) && $claim->payment == PAYMENT_TYPE_CASHLESS)) {
                throw new ValidationException("Запросы в бухгалтерию можно создавать только для заявок, оплачиваемых по безналичному расчету");
            }
        }
    }
}