<?php
use App_Finance_Ticket_Component_Validation_Type_Abstract_Validator as Validator;
use App_Finance_Ticket_Component_Validation_Type_ExplanatoryNote_Exception as ExplanatoryNoteException;

class App_Finance_Ticket_Component_Validation_Type_ExplanatoryNote_Validator extends Validator
{
    /**
     * Валидация не проходит в случае, если от запроса требуется прикрепление пояснительной записки, но у пользователя
     * нет права ее прикрепить.
     */
    public function validate()
    {
        if($this->getTicket()->isNewTicket() && !($this->getTicket()->getAcl()->hasAccessExplanatoryNote())) {
            $preconditions = new App_Finance_Ticket_Component_Observer_Type_ExplanatoryNote_Preconditions();

            if($preconditions->isExplanationNoteRequired($this->getTicket())) {
                throw new ExplanatoryNoteException('Запрещено отправлять запрос в бухгалтерский саппорт без заключения договора с клиентом.');
            }
        }

        return true;
    }
}