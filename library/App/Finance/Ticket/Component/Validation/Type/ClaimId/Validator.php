<?php
use App_Finance_Ticket_Component_Validation_Type_Abstract_Validator as Validator;

class App_Finance_Ticket_Component_Validation_Type_ClaimId_Validator extends Validator
{
    /**
     * Запрещает создавать запрос, если не указан Id заявки
     * @throws \App_Finance_Ticket_Component_Validation_Exception
     */
    public function validate()
    {
        $claimId = $this->getTicket()->getClaimId();

        if(!(is_numeric($claimId) && $claimId > 0)) {
            throw new App_Finance_Ticket_Component_Validation_Type_ClaimId_Exception("No claimId available");
        }
    }
}