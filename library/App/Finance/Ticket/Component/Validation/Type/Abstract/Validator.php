<?php
use App_Finance_Ticket_TicketInterface as Ticket;
use App_Finance_Ticket_Component_Validation_Validator as Validator;
use App_Finance_Ticket_Component_Validation_Exception as ValidationException;

abstract class App_Finance_Ticket_Component_Validation_Type_Abstract_Validator implements Validator
{
    /**
     * Указанный при создании валидатора Id заявки
     * @var
     */
    protected $_claimId;

    /**
     * В случаи, если тикет не создается, а сохраняются в нем изменения, имеется возможность доступа к тикету
     * @var Ticket
     */
    protected $_ticket;

    /**
     * Валидация (возможно, еще не существующих) запросов по claimId
     * В случаи, если валидация запускается для уже существующего тикета, также передается тикет
     * @param int $claimId
     * @param App_Finance_Ticket_TicketInterface $ticket
     * @throws Exception
     */
    public function create($claimId, Ticket $ticket = NULL)
    {
        $this->_claimId = $claimId;
        $this->_ticket = $ticket;
    }

    /**
     * Возвращает указанный при создании валидатора Id заявки
     * @return int
     */
    public function getClaimId()
    {
        return $this->_claimId;
    }

    /**
     * Возвращает указанный при создании валидатора запрос
     * @throws App_Finance_Ticket_Component_Validation_Exception
     * @return Ticket
     */
    public function getTicket()
    {
        if(!($this->hasTicket())) {
            throw new ValidationException("Ticket is not available for this validator");
        }

        return $this->_ticket;
    }

    /**
     * Возврашает true, если имеется информация о тикете, над которым проводится валидация
     * @return bool
     */
    public function hasTicket()
    {
        return $this->_ticket instanceof Ticket;
    }
}