<?php
use App_Finance_Ticket_Component_Validation_Type_Abstract_Validator as Validator;
use App_Finance_Ticket_Component_Validation_Type_OnlyOwnCar_Exception as ValidationException;

class App_Finance_Ticket_Component_Validation_Type_OnlyOwnCar_Validator extends Validator
{
    /**
     * Запрещает создавать запрос к заявка, у которой не указана машина "наша"
     * @throws ValidationException
     */
    public function validate()
    {
        $claimId = $this->getClaimId();

        /** @var $dbClaimTable App_Db_Claims */
        $dbClaimTable = App_Db::get(DB_CLAIMS);
        $claim = $dbClaimTable->getById($claimId);

        if(!($claim)) {
            throw new \Exception("Claim #{$claimId} not found");
        }

        if(!($this->getTicket()->getStatusHandler()->equalsToString("canceled"))) {
            if(!(isset($claim->car) && $claim->car == 1)) { // 0 - Не наша, 1 - наша
                throw new ValidationException("Запросы к диспетчеру можно создавать только для заявок, к которым указана наша машина");
            }
        }
    }
}