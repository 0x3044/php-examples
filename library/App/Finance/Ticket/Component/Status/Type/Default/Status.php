<?php
use App_Finance_Ticket_Component_Status_Type_Abstract_Status as AbstractStatus;

class App_Finance_Ticket_Component_Status_Type_Default_Status extends AbstractStatus
{
    /**
     * Активный статус - тикет активен и готов к обработке
     */
    const STATUS_ACTIVE = 1;

    /**
     * Архив - тикет перемещен в архив и отмечен как завершенный
     */
    const STATUS_ARCHIVED = 2;

    /**
     * Отмененный - тикет перемещен в архим и отмечен как архивный
     */
    const STATUS_CANCELED = 3;

    /**
     * Запрос на отмену - тикет перемещен в "ожидающие отмену" и отмечен как ожидающие отмены
     */
    const STATUS_PENDING_CANCEL = 4;

    /**
     * Запрос "удален"
     */
    const STATUS_DELETED = 5;

    /**
     * Список доступных статусов
     * @var array
     */
    protected static $_availableStatuses = array(
        'active' => self::STATUS_ACTIVE,
        'archived' => self::STATUS_ARCHIVED,
        'canceled' => self::STATUS_CANCELED,
        'pending-cancel' => self::STATUS_PENDING_CANCEL,
        'deleted' => self::STATUS_DELETED
    );

    /**
     * Возвращает список всех доступных статусов в формате "название статуса" => "значение"
     * @return int[]
     */
    public function getAvailableStatuses()
    {
        return self::$_availableStatuses;
    }

    /**
     * Возвращает список доступных статусов (статический метод)
     * @return array
     */
    public static function getDefaultAvailableStatuses()
    {
        return self::$_availableStatuses;
    }

    /**
     * Возвращает название статуса по его id
     * @param $requiredId
     * @return int|string
     * @throws Exception
     */
    public static function getStatusNameById($requiredId)
    {
        foreach(self::getDefaultAvailableStatuses() as $statusName => $statusId) {
            if($requiredId == $statusId) {
                return $statusName;
            }
        }

        throw new \Exception("Unknown status `{$requiredId}`");
    }

    /**
     * Дефолтный статус тикета - активный
     */
    public function setDefaultStatus()
    {
        $this->setActive();
    }

    /**
     * Сменить статус тикета на "активный"
     */
    public function setActive()
    {
        $this->set(self::STATUS_ACTIVE);
    }

    /**
     * Сменить статус тикета на "архив"
     */
    public function setArchived()
    {
        $this->set(self::STATUS_ARCHIVED);
    }

    /**
     * Сменить статус тикета на "отмененный"
     */
    public function setCanceled()
    {
        $this->set(self::STATUS_CANCELED);
    }

    /**
     * Сменить статус тикета на "ожидающий отмены"
     */
    public function setPendingCancel()
    {
        $this->set(self::STATUS_PENDING_CANCEL);
    }

    /**
     * Сменить статус тикета на "удаленный"
     */
    public function setDeleted()
    {
        $this->set(self::STATUS_DELETED);
    }

    /**
     * Возвращает true, если тикет находится в статусе "активный"
     * @return bool
     */
    public function isActive()
    {
        return $this->toInt() === self::STATUS_ACTIVE;
    }

    /**
     * Возвращает true, если тикет находится в статусе "архив"
     * @return bool
     */
    public function isArchived()
    {
        return $this->toInt() === self::STATUS_ARCHIVED;
    }

    /**
     * Возвращает true, если тикет находится в статусе "отмененный"
     * @return bool
     */
    public function isCanceled()
    {
        return $this->toInt() === self::STATUS_CANCELED;
    }

    /**
     * Возвращает true, если тикет находится в статусе "ожидающий отмену"
     * @return bool
     */
    public function isPendingCancel()
    {
        return $this->toInt() === self::STATUS_PENDING_CANCEL;
    }

    /**
     * Возврашает true, если тикет находился в статусе "отмененный"
     * @return bool
     */
    public function isDeleted()
    {
        return $this->toInt() == self::STATUS_DELETED;
    }
}