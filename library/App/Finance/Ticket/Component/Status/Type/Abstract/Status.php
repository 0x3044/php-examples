<?php
use App_Finance_Ticket_Component_Status_StatusInterface as StatusInterface;
use App_Finance_Ticket_TicketInterface as Ticket;

/**
 * Class App_Finance_Ticket_Component_Status_Type_Abstract_Status
 * Observers: onStatusBeforeChange, onStatusAfterChange, onStatusSet[statusName]
 */
abstract class App_Finance_Ticket_Component_Status_Type_Abstract_Status implements StatusInterface
{
    /**
     * Текущий статус
     * @var int
     */
    protected $_status;

    /**
     * Переданный конструктору тикет
     * @var Ticket
     */
    protected $_ticket;

    /**
     * Управление статусом тикета
     * @param App_Finance_Ticket_TicketInterface $ticket
     */
    public final function __construct(Ticket $ticket)
    {
        $this->_ticket = $ticket;
    }

    /**
     * Устанавливает текущий статус тикета
     * @param $status
     * @throws InvalidArgumentException
     */
    public function set($status)
    {
        $this->getTicket()->getObserversHandler()->on('onStatusBeforeChange');

        if(!(is_numeric($status))) {
            throw new \InvalidArgumentException("Expected int, got ".gettype($status));
        }

        if(!(in_array($status, $this->getAvailableStatuses()))) {
            throw new \InvalidArgumentException("Invalid status");
        }

        $this->_status = $status;
        $this->getTicket()->getObserversHandler()->on('onStatusAfterChange');
        $this->getTicket()->getObserversHandler()->on('onStatusSet'.ucfirst($this->toString()));
    }

    /**
     * Возвращает текущий статус тикета как числовое значение
     * @throws Exception
     * @return int
     */
    public function toInt()
    {
        if(!($this->hasStatus())) {
            throw new \Exception('No status has been set');
        }

        return (int) $this->_getStatus();
    }

    /**
     * Возвращает текущий статус тикета как строковое значение
     * @throws Exception
     * @return string
     */
    public function toString()
    {
        if(!($this->hasStatus())) {
            throw new \Exception('No status has been set');
        }

        return $this->getStatusNameByInt($this->toInt());
    }

    /**
     * Возвращает true, если тикету установлен какой-либо статус
     * @return bool
     */
    public final function hasStatus()
    {
        return !(is_null($this->_getStatus()));
    }

    /**
     * Возвращает числовое значение статуса по его названию
     * @param string $statusName
     * @throws InvalidArgumentException
     * @return int
     */
    public function getStatusIntByName($statusName)
    {
        $statuses = $this->getAvailableStatuses();

        if(!(in_array($statusName, array_keys($statuses)))) {
            throw new \InvalidArgumentException('Unknown status '.var_export($statusName, true));
        }

        return $statuses[$statusName];
    }

    /**
     * Возвращает название статуса по его числовому значению
     * @param int $statusInt
     * @throws InvalidArgumentException
     * @return string
     */
    public function getStatusNameByInt($statusInt)
    {
        $statuses = $this->getAvailableStatuses();

        if(!($arrKey = array_search($statusInt, $statuses))) {
            throw new \InvalidArgumentException('Unknown status '.var_export($statusInt, true));
        }

        return $arrKey;
    }

    /**
     * Возвращает внутреннее представление текущего статуса тикета
     * @return int
     */
    protected function _getStatus()
    {
        return $this->_status;
    }

    /**
     * Возвращает true, если статусы индентичны
     * @param App_Finance_Ticket_Component_Status_Type_Abstract_Status $compareStatus
     * @return bool
     */
    public function equalsTo(App_Finance_Ticket_Component_Status_Type_Abstract_Status $compareStatus)
    {
        return $compareStatus->toInt() === $this->toInt();
    }

    /**
     * Возвращает true, если текущий статус идентичен указанному числу
     * @param int $statusInt
     * @return bool
     */
    public function equalsToInt($statusInt)
    {
        return $statusInt === $this->toInt();
    }

    /**
     * Возвращает true, если текущий статус идентичен указанному строковому значению
     * @param string $statusString
     * @return bool
     */
    public function equalsToString($statusString)
    {
        return $statusString === $this->toString();
    }

    /**
     * @see App_Finance_Ticket_Component_Status_StatusInterface::toString
     * @return string
     */
    public function __toString()
    {
        return $this->toString();
    }

    /**
     * Возвращает переданный конструктору тикет
     * @return Ticket
     */
    public function getTicket()
    {
        return $this->_ticket;
    }
}