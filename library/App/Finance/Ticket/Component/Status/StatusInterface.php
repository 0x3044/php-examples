<?php
use App_Finance_Ticket_TicketInterface as Ticket;

interface App_Finance_Ticket_Component_Status_StatusInterface
{
    /**
     * Управление статусом тикета
     * @param App_Finance_Ticket_TicketInterface $ticket
     */
    public function __construct(Ticket $ticket);

    /**
     * Устанавливает текущий статус тикета
     * @param int $status
     */
    public function set($status);

    /**
     * Возвращает текущий статус тикета как числовое значение
     * @return int
     */
    public function toInt();

    /**
     * Возвращает текущий статус тикета как строковое значение
     * @return string
     */
    public function toString();

    /**
     * Возвращает true, если тикету установлен какой-либо статус
     * @return bool
     */
    public function hasStatus();

    /**
     * Устанавливает тикету дефолтный статус
     */
    public function setDefaultStatus();

    /**
     * Возвращает название статуса по его числовому значению
     * @param int $statusInt
     * @return string
     */
    public function getStatusNameByInt($statusInt);

    /**
     * Возвращает числовое значение статуса по его названию
     * @param string $statusName
     * @return int
     */
    public function getStatusIntByName($statusName);

    /**
     * Возвращает список всех доступных статусов в формате "название статуса" => "значение"
     * @return int[]
     */
    public function getAvailableStatuses();

    /**
     * Возвращает true, если статусы индентичны
     * @param App_Finance_Ticket_Component_Status_Type_Abstract_Status $compareStatus
     * @return bool
     */
    public function equalsTo(App_Finance_Ticket_Component_Status_Type_Abstract_Status $compareStatus);

    /**
     * Возвращает true, если текущий статус идентичен указанному числу
     * @param int $statusInt
     * @return bool
     */
    public function equalsToInt($statusInt);

    /**
     * Возвращает true, если текущий статус идентичен указанному строковому значению
     * @param string $statusString
     * @return bool
     */
    public function equalsToString($statusString);

    /**
     * @see App_Finance_Ticket_Component_Status_StatusInterface::toString
     * @return string
     */
    public function __toString();

    /**
     * Возвращает переданный конструктору тикет
     * @return Ticket
     */
    public function getTicket();
}