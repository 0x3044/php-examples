<?php
use App_Finance_Ticket_Component_Acl_Default as AbstractType;

/**
 * Права для диспетчерского саппорта
 */
class App_Finance_Ticket_Component_Acl_Dispatcher extends AbstractType
{
    /**
     * Возвращает список прав в виде массива
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), array('dispatcher' => array(
            'dispatcher' => $this->getDispatcherModule()->hasAccessClaimDispatcher(),
            'carNumber' => $this->getDispatcherModule()->hasAccessClaimCarNumber(),
            'dateToProcess' => $this->getDispatcherModule()->hasAccessClaimDateToProcess(),
            'rentPrice' => $this->getDispatcherModule()->hasAccessClaimRentPrice(),
            'transportDeliveryTime' => $this->getDispatcherModule()->hasAccessClaimTransportDeliveryTime()
        )));
    }
}