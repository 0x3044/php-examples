<?php
use App_Finance_Ticket_TicketInterface as Ticket;
use App_Finance_Ticket_Component_Acl_Default as AclDefault;
use App_Finance_Ticket_Component_Acl_Dispatcher as AclDispatcher;
use App_Finance_Ticket_Component_Acl_Accounting as AclAccounting;

class App_Finance_Ticket_Component_Acl_Factory
{
    /**
     * Возвращает дерево прав для тикета
     * @param App_Finance_Ticket_TicketInterface $ticket
     * @return App_Finance_Ticket_Component_Acl_Default|App_Finance_Ticket_Component_Acl_Dispatcher
     */
    public function createAclTree(Ticket $ticket)
    {
        switch($ticket->getMainRecipient()) {
            default:
                return new AclDefault($ticket);

            case 'dispatcher':
                return new AclDispatcher($ticket);
            case 'accounting':
                return new AclAccounting($ticket);
        }
    }
}