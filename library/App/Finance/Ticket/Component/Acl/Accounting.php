<?php
use App_Finance_Ticket_Component_Acl_Default as AbstractType;

/**
 * Права для бухгалтерского саппорта
 */
class App_Finance_Ticket_Component_Acl_Accounting extends AbstractType
{
    /**
     * Возвращает список прав в виде массива
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), array(
                'setnotviewed' => true,
                'setviewed' => true
            )
        );
    }
}