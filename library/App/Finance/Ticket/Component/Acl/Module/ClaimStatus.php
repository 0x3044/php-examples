<?php
use App_Claim_Reference as ClaimTypeReference;

class App_Finance_Ticket_Component_Acl_Module_ClaimStatus
{
    /**
     * Дерево прав
     * @var stdClass
     */
    protected $_permissions;

    /**
     * Кастомный статус
     * @var int
     */
    protected $_customClaimStatus;

    /**
     * Тип заявки
     * @var ClaimTypeReference
     */
    protected $_claimTypeReference;

    /**
     * Права финансового саппорта относительно заявки/статуса
     * @param App_Claim_Reference $claimTypeReference
     */
    public final function __construct(ClaimTypeReference $claimTypeReference)
    {
        $this->_claimTypeReference = $claimTypeReference;
    }

    /**
     * Возвращает переданный конструктору ID заявки
     * @return int
     */
    public function getClaimId()
    {
        return $this->getClaimTypeReference()->getClaimId();
    }

    /**
     * Возвращает тип заявки (парный)
     * @throws Exception
     * @return int
     */
    public function getTypeId()
    {
        return $this->getClaimTypeReference()->getTypeId();
    }

    /**
     * Возвращает тип заявки
     * @return int
     */
    public function getClaimTypeId()
    {
        return $this->getClaimTypeReference()->getClaimTypeId();
    }

    /**
     * Возвращает claimTypeReference
     * @return \App_Claim_Reference
     */
    public function getClaimTypeReference()
    {
        return $this->_claimTypeReference;
    }

    /**
     * Получение данных о типе заявки исходя из Id заявки
     * @throws Exception
     */
    protected function _fetchClaimTypeData()
    {
        if(!($claimId = $this->getClaimId())) {
            throw new \Exception("No claimId/claimTypeId available");
        }

        $claimFactory = App_Claim_Factory::getInstance();
        $claimData = $claimFactory->getClaimData($claimId);

        $this->_typeId = $claimData->typeId;
        $this->_claimTypeId = $claimData->claimType;
    }

    /**
     * Загрузка прав для заявки с указанным статусом
     * @param $claimStatus
     */
    public function setCustomClaimStatus($claimStatus)
    {
        App_Spl_TypeCheck::getInstance()->numeric($claimStatus);

        $this->_customClaimStatus = $claimStatus;
        $this->_permissions = null;

        $this->_getClaimPermissions();
    }

    /**
     * Загрузка прав для заявки без кастомного статуса заявки
     */
    public function disableCustomClaimStatus()
    {
        $this->_customClaimStatus = null;
        $this->_permissions = null;

        $this->_getClaimPermissions();
    }

    /**
     * Возвращает дерево прав
     * @return stdClass
     * @throws Exception
     */
    protected function _getClaimPermissions()
    {
        if(is_null($this->_permissions)) {
            $claimType = App_Claim_Factory::getInstance()->typeIdToString($this->getTypeId());
            $claimStatus = $this->_customClaimStatus;

            switch($this->getTypeId()) {
                default:
                    $path = 'claim>'.$claimType.'>finance';
                    $this->_permissions = App_Access::get('finance', $path, array(
                        'claimId' => $this->getClaimId(),
                        'type' => $this->getTypeId(),
                        'customStatus' => $claimStatus
                    ));

                    break;

                case App_Claim_Factory::TYPE_ID_IN_OUT:
                    $path = 'claim>inout>out>finance';
                    $this->_permissions = App_Access::get('finance', $path, array(
                        'claimId' => $this->getClaimId(),
                        'customStatus' => $claimStatus,
                        'type' => 12 // Волшебное число: @see App_Access_DataWrapper::$_statusClassArray
                    ));

                    break;

                case App_Claim_Factory::TYPE_ID_INCOME_OUT:
                    $path = 'claim>incomeout>out>finance';
                    $this->_permissions = App_Access::get('finance', $path, array(
                        'claimId' => $this->getClaimId(),
                        'type' => $this->getTypeId(),
                        'customStatus' => $claimStatus
                    ));

                    break;

                case App_Claim_Factory::TYPE_ID_IN_SUPPLY:
                    $path = 'claim>insupply>in>finance';
                    $this->_permissions = App_Access::get('finance', $path, array(
                        'claimId' => $this->getClaimId(),
                        'type' => $this->getTypeId(),
                        'customStatus' => $claimStatus
                    ));

                    break;
            }
        }

        if(!($this->_permissions instanceof stdClass)) {
            throw new \Exception("Отсутствует дерево прав для данной заявки/статуса");
        }

        return $this->_permissions;
    }

    /**
     * Возвращает true, если пользователю разрешено действие {$key}
     * @param $key
     * @return bool
     * @throws Exception
     */
    protected function _hasAccess($key)
    {
        if(!(isset($this->_getClaimPermissions()->{$key}))) {
            return false;
        }

        return $this->_getClaimPermissions()->{$key} == App_Access_Field_Type1::ACCESS_ALLOWED;
    }

    /**
     * Возвращает true, если пользователь имеет доступ к просмотру статуса запросов
     * @return bool
     */
    public function hasAccessStatus()
    {
        return $this->_hasAccess('status');
    }

    /**
     * Возвращает true, если пользователь имеет право создавать запрос данного типа
     * @param App_Finance_Ticket_TicketInterface $ticket
     * @return bool
     * @throws Exception
     */
    public function hasAccessCreate(App_Finance_Ticket_TicketInterface $ticket)
    {
        switch($ticket->getMainRecipient()) {
            default:
                throw new \Exception("Unknown recipient `{$ticket->getMainRecipient()}`");

            case 'accounting':
                return $this->hasAccessCreateAccounting();

            case 'dispatcher':
                return $this->hasAccessCreateDispatcher();

            case 'depot':
                return $this->hasAccessCreateDepot();
        }
    }

    /**
     * Возврашает true, если пользователь имеет право создать запрос в хоть какое-либо направление
     * @return bool
     */
    public function hasAccessCreateAnyTicket()
    {
        return $this->hasAccessCreateAccounting() || $this->hasAccessCreateDepot() || $this->hasAccessCreateDispatcher();
    }

    /**
     * Возвращает true, если пользователь имеет право на создания запросов в бухгалтерский саппорт
     * @return bool
     */
    public function hasAccessCreateAccounting()
    {
        return $this->_hasAccess('create_accounting');
    }

    /**
     * Возвращает true, если пользователь имеет право на создания запросов в диспетчерский саппорт
     * @return bool
     */
    public function hasAccessCreateDispatcher()
    {
        return $this->_hasAccess('create_dispatcher');
    }

    /**
     * Возвращает true, если пользователь имеет право на создания запросов в складской саппорт
     * @return bool
     */
    public function hasAccessCreateDepot()
    {
        return $this->_hasAccess('create_depot');
    }

    /**
     * Возвращает true, если пользователь имеет право создавать запрос данного типа
     * @param App_Finance_Ticket_TicketInterface $ticket
     * @return bool
     * @throws Exception
     */
    public function hasAccessCancel(App_Finance_Ticket_TicketInterface $ticket)
    {
        switch($ticket->getMainRecipient()) {
            default:
                throw new \Exception("Unknown recipient `{$ticket->getMainRecipient()}`");

            case 'accounting':
                return $this->hasAccessCancelAccounting();

            case 'dispatcher':
                return $this->hasAccessCancelDispatcher();

            case 'depot':
                return $this->hasAccessCancelDispatcher();

            case 'payment':
                return true;
        }
    }

    /**
     * Возвращает true, если пользователь имеет право на отмену запросов в саппорт
     * @return bool
     */
    public function hasAccessCancelAccounting()
    {
        return $this->_hasAccess('cancel_accounting');
    }

    /**
     * Возвращает true, если пользователь имеет право на отмену запросов в саппорт
     * @return bool
     */
    public function hasAccessCancelDispatcher()
    {
        return $this->_hasAccess('cancel_dispatcher');
    }

    /**
     * Возвращает true, если пользователь имеет право на отмену запросов в саппорт
     * @return bool
     */
    public function hasAccessCancelDepot()
    {
        return $this->_hasAccess('cancel_depot');
    }
}