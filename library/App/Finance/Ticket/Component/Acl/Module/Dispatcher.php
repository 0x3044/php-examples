<?php

class App_Finance_Ticket_Component_Acl_Module_Dispatcher extends App_Finance_Ticket_Extension
{
    /**
     * Возвращает права по ключу
     * @param $subPath
     * @return mixed
     */
    protected function hasAccess($subPath)
    {
        return $this->getTicket()->getAcl()->hasAccess($subPath);
    }

    /**
     * Возвращает true, если пользователь имеет права на finance>...>claim>key
     * @param $subPath
     * @return bool
     */
    public function hasAccessClaim($subPath)
    {
        return $this->hasAccess("claim>{$subPath}");
    }

    /**
     * Возвращает true, если пользователь имеет права на изменения даты обработки запроса
     * @return bool
     */
    public function hasAccessClaimDateToProcess()
    {
        return $this->hasAccessClaim("date_to_process");
    }

    /**
     * Возвращает true, если пользователь имеет права на изменение диспетчера в заявке
     * @return bool
     */
    public function hasAccessClaimDispatcher()
    {
        return $this->hasAccessClaim("dispatcher");
    }

    /**
     * Возвращает true, если пользователь имеет право на изменение номер машины в заявке
     * @return bool
     */
    public function hasAccessClaimCarNumber()
    {
        return $this->hasAccessClaim("car_number");
    }

    /**
     * Возвращает true, если пользователь имеет право на изменение стоимости найма машины в заявке
     * @return bool
     */
    public function hasAccessClaimRentPrice()
    {
        return $this->hasAccessClaim("rent_price");
    }

    /**
     * Возврашает true, если пользователь имеет право на изменение времени подачи транспорта в заявке
     * @return bool
     */
    public function hasAccessClaimTransportDeliveryTime()
    {
        return $this->hasAccessClaim("transport_delivery_time");
    }
}