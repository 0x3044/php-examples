<?php
use App_Finance_Ticket_TicketInterface as Ticket;
use App_Access_Field_Type2 as CrudAccess;
use App_Finance_Ticket_Component_Acl_Module_ClaimStatus as ClaimStatusModule;
use App_Finance_Ticket_Component_Acl_Module_Dispatcher as DispatcherModule;

/**
 * Общие для всех запросов права
 */
class App_Finance_Ticket_Component_Acl_Default
{
    /**
     * Переданный конструктору тике
     * @var Ticket
     */
    protected $_ticket;

    /**
     * Права доступа (относительно заявки/статуса)
     * @var ClaimStatusModule
     */
    protected $_claimStatusModule;

    /**
     * Права доступа (модуль диспетчера)
     * @var DispatcherModule
     */
    protected $_dispatcherModule;

    /**
     * Дерево прав для запроса
     * @param App_Finance_Ticket_TicketInterface $ticket
     */
    public final function __construct(Ticket $ticket)
    {
        $this->_ticket = $ticket;
    }

    /**
     * Возвращает права заявки относительно запроса/статуса
     * @return ClaimStatusModule
     */
    public function getClaimModule()
    {
        if(is_null($this->_claimStatusModule)) {
            $this->_claimStatusModule = new ClaimStatusModule(new App_Claim_Reference($this->_getTicket()->getClaimId()));
        }

        return $this->_claimStatusModule;
    }

    /**
     * Возвращает права на изменения полей в заявке (диспетчер)
     * @return App_Finance_Ticket_Component_Acl_Module_Dispatcher
     */
    public function getDispatcherModule()
    {
        if(is_null($this->_dispatcherModule)) {
            $this->_dispatcherModule = new DispatcherModule($this->_getTicket());
        }

        return $this->_dispatcherModule;
    }

    /**
     * Возвращает дерево прав тикета в виде массива
     * @return array
     */
    public function toArray()
    {
        if(defined('EXCEL_CONVERTER') && EXCEL_CONVERTER) {
            return array();
        } else {
            return array(
                'access' => $this->hasAccessView(),
                'create' => $this->hasAccessCreate(),
                'archive' => $this->hasAccessArchive(),
                'cancel' => $this->hasAccessCancel(),
                'pending_cancel' => $this->hasAccessPendingCancel(),
                'active_pending_cancel' => $this->hasAccessActivePendingCancel(),
                'cancel_archived' => $this->hasAccessCancelArchived(),
                'documents' => $this->hasAccessListDocuments(),
                'attachment' => $this->hasAccessAttachDocuments(),
                'history' => $this->hasAccessHistory(),
                'print' => $this->hasAccessPrint(),
                'service_note' => $this->hasAccessServiceNote(),
                'destroy' => $this->hasAccessDestroy()
            );
        }
    }

    /**
     * Возвращает права для данного тикета по суб-пути
     * @param $subPath
     * @return mixed
     */
    public final function hasAccess($subPath)
    {
        $ticket = $this->_getTicket();
        $recipient = $ticket->getMainRecipient();
        $ticketTypeName = $ticket->getTicketTypeName();

        return App_Access::get('access', "finance>{$recipient}>{$ticketTypeName}>{$subPath}");
    }

    /**
     * Возвращает true, если у пользователя есть права на просмотр запроса
     * @return mixed
     */
    public final function hasAccessView()
    {
        return $this->hasAccess('access');
    }

    /**
     * Возвращает true, если у пользователя есть права на создание запроса
     * @return mixed
     */
    public final function hasAccessCreate()
    {
        return $this->hasAccess('create');
    }

    /**
     * Возвращает true, если у пользователя есть права на перевод запроса в архив
     * @return mixed
     */
    public final function hasAccessArchive()
    {
        return $this->hasAccess('archive');
    }

    /**
     * Возвращает true, если у пользователя есть права на отмену запроса
     * @return mixed
     */
    public final function hasAccessCancel()
    {
        return $this->hasAccess('cancel');
    }

    /**
     * Возвращает true, если у пользователя есть права на отмену архивного запроса
     * @return bool
     */
    public final function hasAccessCancelArchived()
    {
        return $this->hasAccess('cancel_archived');
    }

    /**
     * Возвращает true, если у пользователя есть права на перевод запроса в статус "ожидающие отмены"
     * @return bool
     */
    public final function hasAccessPendingCancel()
    {
        return $this->hasAccess('pending_cancel');
    }

    /**
     * Возвращает true, если у пользователя есть права на удаление запроса
     * @return bool
     */
    public final function hasAccessDestroy()
    {
        return $this->hasAccess('destroy');
    }

    /**
     * Возвращает true, если у пользователя есть права на перевод запроса в активные из статуса "ожидающие отмены"
     * @return bool
     */
    public final function hasAccessActivePendingCancel()
    {
        return $this->hasAccess('active_pending_cancel');
    }

    /**
     * Возвращает true, если у пользователя есть права на прикрепление документов
     * @return bool
     */
    public final function hasAccessAttachDocuments()
    {
        return $this->hasAccess('attachment') == CrudAccess::ACCESS_EDIT;
    }

    /**
     * Возвращает true, если у пользователя есть права на просмотр списка документов
     * @return bool
     */
    public final function hasAccessListDocuments()
    {
        return $this->hasAccess('attachment') == CrudAccess::ACCESS_VIEW
        || $this->hasAccess('attachment') == CrudAccess::ACCESS_EDIT;
    }

    /**
     * Возвращает true, если у пользователя есть права на просмотр истории
     * @return mixed
     */
    public final function hasAccessHistory()
    {
        return $this->hasAccess('history');
    }

    /**
     * Возвращает true, если у пользователя есть права на печать
     * @return mixed
     */
    public final function hasAccessPrint()
    {
        return $this->hasAccess('print');
    }

    /**
     * Возвращает права пользователя на прикрепление служебной записки
     * @return int
     */
    public final function hasAccessServiceNote()
    {
        return $this->hasAccess('service_note');
    }

    /**
     * Возвращает права пользователя на прикрепление пояснительной записки
     * @return int
     */
    public final function hasAccessExplanatoryNote()
    {
        return $this->hasAccess('wanocontractcheck');
    }

    /**
     * Возврашает переданный конструктору тикет
     * @return Ticket
     */
    protected final function _getTicket()
    {
        return $this->_ticket;
    }
}