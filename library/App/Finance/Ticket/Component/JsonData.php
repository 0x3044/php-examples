<?php

class App_Finance_Ticket_Component_JsonData implements ArrayAccess
{
    protected $_jsonData = array();

    /**
     * Дополнительные данные запроса, которая хранятся в базе в виде JSON
     * @param array $jsonData
     */
    public function __construct(Array $jsonData = NULL)
    {
        if($jsonData) {
            $this->set($jsonData);
        }
    }

    /**
     * Задать JSON-данные запроса
     * @param array $jsonData
     */
    public function set(Array $jsonData)
    {
        $this->_jsonData = $jsonData;
    }

    /**
     * Возвращает JSON-данные запроса
     * @return array
     */
    public function get()
    {
        return $this->_jsonData;
    }

    /**
     * Возвращает JSON-данные в виде JSON-строки
     * @return string
     */
    public function __toString()
    {
        return json_encode($this->_jsonData);
    }

    /**
     * Возвращает true, если JSON-данные идентичный
     * @param App_Finance_Ticket_Component_JsonData $compareObject
     * @return bool
     */
    public function equals(App_Finance_Ticket_Component_JsonData $compareObject)
    {
        return (string) $this == (string) $compareObject;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @throws OutOfRangeException
     * @return mixed Can return all value types.
     */
    public function &offsetGet($offset)
    {
        if(!($this->offsetExists($offset))) {
            throw new \OutOfRangeException("Key \"{$offset}\" not found");
        }

        return $this->_jsonData[$offset];
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     */
    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->_jsonData);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->_jsonData[$offset] = $value;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @throws OutOfRangeException
     * @return void
     */
    public function offsetUnset($offset)
    {
        if(!($this->offsetExists($offset))) {
            throw new \OutOfRangeException("Key \"{$offset}\" not found");
        }

        unset($this->_jsonData[$offset]);
    }
}