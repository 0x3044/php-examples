<?php
use App_Finance_Ticket_Component_DocumentInterface as DocumentInterface;
use App_Finance_Ticket_TicketInterface as Ticket;
use App_Db_Row_TicketDocument as DocumentRow;
use App_Finance_Ticket_Component_Document_PathStrategy_Abstract as PathStrategyFactory;
use App_Finance_Ticket_Component_Document_FileNotFoundException as FileNotFoundException;

/**
 * Class App_Finance_Ticket_Component_Document
 * Observers: onBeforeDocumentSave, onAfterDocumentSave, onBeforeDocumentUpload, onAfterDocumentUpload
 */
class App_Finance_Ticket_Component_Document implements DocumentInterface
{
    /**
     * @var string
     */
    protected $_mode;

    /**
     * @var DocumentRow
     */
    protected $_row;

    /**
     * @var Ticket
     */
    protected $_uploadTicket;

    /**
     * В случаи, если передан аргумент, будет создан readonly-объект, представляющий документ и попытка вызова метода
     * upload вызовет исключение.
     * @param App_Db_Row_TicketDocument $documentRow
     */
    public function __construct(DocumentRow $documentRow = NULL)
    {
        if(!($documentRow)) {
            $dbTable = App_Db::get(DB_TICKET_DOCUMENT);
            $documentRow = $dbTable->createRow();
            $this->_setMode('create');
        } else {
            $this->_setMode('read');
        }

        $this->_setRow($documentRow);
    }

    /**
     * Возвращает TRUE, если файл существует
     * @return bool
     */
    public function fileExists()
    {
        return file_exists($this->getFilePath());
    }

    /**
     * Возвращает директорию, в которой сохранен/будет сохранен файл
     * @throws Exception
     * @return string
     */
    public function getFileDir()
    {
        $documentRow = $this->getRow();

        if(!($documentRow->getTicketId())) {
            throw new \Exception('ticket_id should be not empty');
        }

        if(!($documentRow->getDirName())) {
            $pathStrategy = PathStrategyFactory::create($this->_getUploadTicket());

            $documentRow->setDirName($pathStrategy->getPath(array(
                'allocate' => true
            )));
        }

        return APPLICATION_PATH.'/../public/uploads/finance/documents/'.($this->getRow()->getDirName());
    }

    /**
     * Возвращает имя файла
     * @return string
     */
    public function getFileName()
    {
        return $this->getRow()->getFileName();
    }

    /**
     * Возвращает размер файла
     */
    public function getSize()
    {
        return filesize($this->getFilePath());
    }

    /**
     * Возвращает MIME файла
     * @return string
     */
    public function getMime()
    {
        return mime_content_type($this->getFilePath());
    }

    /**
     * Возвращает ссылку на скачивание файла
     * @return string
     */
    public function getDownloadLink()
    {
        return '/finance/ticket/document/download?documentId='.$this->getRow()->getId().'&ticketId='.$this->getRow()->getTicketId();
    }

    /**
     * Возвращает прямую ссылку на файл
     * @return string
     */
    public function getPublicLink()
    {
        return sprintf('/uploads/finance/documents/%s/%s', $this->getRow()->getDirName(), $this->getRow()->getFileName());
    }

    /**
     * Возвращает ссылку на открытие файла сторонним сервисом
     * @return string
     */
    public function getVendorLink()
    {
        return sprintf('/filemanager/preview/view/type/ticket-document/?ticketId=%d&documentId=%d', $this->getRow()->getTicketId(), $this->getRow()->getId());
    }

    /**
     * Возвращает полный путь к файлу
     * @return string
     */
    public function getFilePath()
    {
        return $this->getFileDir().'/'.$this->getFileName();
    }

    /**
     * Возвращает путь к файлу (настоящий)
     * @return string
     */
    public function getRealPath()
    {
        return ($this->getRow()->getDirName()).'/'.$this->getFileName();
    }

    /**
     * Возвращает информацию о файле(path_info)
     * @throws Exception
     * @return array
     */
    public function getFileInfo()
    {
        if(!(file_exists($this->getFilePath()))) {
            throw new FileNotFoundException("Файл `{$this->getFileName()}` не найден");
        }

        return pathinfo($this->getFilePath());
    }

    /**
     * При вызове метода upload() переданный тикет
     * @param \App_Finance_Ticket_TicketInterface $uploadTicket
     */
    public function setUploadTicket(Ticket $uploadTicket)
    {
        $this->_uploadTicket = $uploadTicket;
    }

    /**
     * @return \App_Finance_Ticket_TicketInterface
     */
    protected function _getUploadTicket()
    {
        return $this->_uploadTicket;
    }

    /**
     * Сохраняет изменения
     */
    public function save()
    {
        $mode = $this->_getMode();

        switch($mode) {
            default:
                throw new \Exception('Unknown mode '.var_export($mode));

            case 'create':
                break;

            case 'read':
                $this->getRow()->save();
                break;
        }
    }

    /**
     * Сохраняет файл
     * @param $tmpFile
     * @param string $customFileName
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function upload($tmpFile, $customFileName = NULL)
    {
        $ticket = $this->_getUploadTicket();
        $ticket->getObserversHandler()->on('onBeforeDocumentUpload');

        if(!($ticket && ($ticket instanceof Ticket))) {
            throw new \Exception('Call method `setUploadTicket` before upload file');
        }

        if(!(is_string($tmpFile)) || empty($tmpFile)) {
            throw new \InvalidArgumentException('tmpFile is empty');
        }

        if(!(file_exists($tmpFile))) {
            throw new \InvalidArgumentException("File `{$tmpFile}` not exists`");
        }

        if(!($ticket->getId())) {
            throw new \Exception('Ticket(Id) required');
        }

        $documentRow = $this->getRow();
        $documentRow->setTicketId($ticket->getId());
        $this->_setupFileName(strlen($customFileName) > 0 ? $customFileName : $tmpFile);

        if(is_uploaded_file($tmpFile)) {
            if(!(move_uploaded_file($tmpFile, $this->getFilePath()))) {
                throw new \Exception('Failed to move uploaded file:');
            }
        } else {
            if(!(copy($tmpFile, $this->getFilePath()))) {
                throw new \Exception('Failed to copy file');
            }
        }

        $documentRow->save();
        $this->_setMode('read');

        $ticket->getObserversHandler()->on('onAfterDocumentUpload');
    }

    /**
     * Выгрузка документа
     * @throws Exception
     */
    public function download()
    {
        $fileName = $this->getFilePath();

        if(!(file_exists($fileName))) {
            throw new \Exception("Файл `{$this->getFileName()}` не найден");
        }

        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header('Content-Disposition: attachment; filename='.$this->getFileName());
        header('Content-Length: '.filesize($fileName));
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);

        exit(file_get_contents($fileName));
    }

    /**
     * Обработка имени файла
     * @param $fileName
     */
    protected function _setupFileName($fileName)
    {
        $dirName = $this->getFileDir();

        if(!(file_exists($dirName))) {
            mkdir($dirName);
        } else if(is_file($dirName)) {
            $this->getRow()->setDirName(uniqid());
            $dirName = $this->getFileDir();
        }

        $count = 0;

        $pathInfo = pathinfo($fileName);
        $extension = $pathInfo['extension'];
        $translit = new App_Translit();

        $fileName = $pathInfo['filename'];
        $fileName = $translit->filterFileName($fileName);
        $fileName = $this->_getFileName($extension, $fileName, $count);

        while(file_exists($dirName.'/'.$fileName)) {
            $count += 1;
            $fileName = $pathInfo['filename'];
            $fileName = $translit->filterFileName($fileName);
            $fileName = $this->_getFileName($extension, $fileName, $count);
        }

        $this->getRow()->setFileName($fileName);
    }

    /**
     * Возвращает имя файла в формате claimId_counter.ext
     * @param $extension
     * @param $fileName
     * @param int $count
     * @throws Exception
     * @return string
     */
    protected function _getFileName($extension, $fileName, $count = 0)
    {
        if($this->_getUploadTicket()->getMainRecipient() == 'payment') {
            if($count) {
                return "{$fileName}_({$count}).{$extension}";
            }else{
                return "{$fileName}.{$extension}";
            }
        }else{
            $claimId = $this->_getUploadTicket()->getClaimId() ? $this->_getUploadTicket()->getClaimId() : $this->_getUploadTicket()->getId();

            if($count) {
                return "{$claimId}_({$count}).{$extension}";
            } else {
                return "{$claimId}.{$extension}";
            }
        }
    }

    /**
     * Удаляет файл с жесткого диска и базы данных
     */
    public function remove()
    {
        if(!($this->getRow()->getId())) {
            throw new \Exception('rowId required');
        }

        $filePath = $this->getFilePath();

        if(file_exists($filePath)) {
            unlink($filePath);
        }

        $this->getRow()->delete();
    }

    /**
     * Сериализовать в массив
     * @return array
     */
    public function toArray()
    {
        try {
            return array(
                'rowId' => (int) $this->getRow()->getId(),
                'fileExists' => true,
                'ticketId' => (int) $this->getRow()->getTicketId(),
                'dateCreated' => $this->getRow()->getDateCreated(),
                'fileName' => $this->getFileName(),
                'fileUrl' => $this->getDownloadLink(),
                'fileDir' => $this->getRow()->getDirName(),
                'fileInfo' => $this->getFileInfo(),
                'md5' => md5_file($this->getFilePath())
            );
        }
        catch(FileNotFoundException $e) {
            return array(
                'rowId' => (int) $this->getRow()->getId(),
                'fileExists' => false,
                'ticketId' => (int) $this->getRow()->getTicketId(),
                'dateCreated' => $this->getRow()->getDateCreated(),
                'fileName' => $this->getFileName(),
                'fileDir' => $this->getRow()->getDirName(),
                'fileUrl' => $this->getDownloadLink(),
                'fileInfo' => array(),
                'md5' => NULL
            );
        }
    }

    /**
     * @param mixed $mode
     * @throws Exception
     */
    protected function _setMode($mode)
    {
        if($mode != 'create' && $mode != 'read') {
            throw new \Exception('Unknown mode '.var_export($mode, true));
        }

        $this->_mode = $mode;
    }

    /**
     * @return mixed
     */
    protected function _getMode()
    {
        return $this->_mode;
    }

    /**
     * @param \App_Db_Row_TicketDocument $row
     */
    protected function _setRow(DocumentRow $row)
    {
        $this->_row = $row;
    }

    /**
     * @return \App_Db_Row_TicketDocument
     */
    public function getRow()
    {
        return $this->_row;
    }
}