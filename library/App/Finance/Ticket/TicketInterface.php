<?php
use App_Finance_Ticket_Component_Observer_ObserversHandler as ObserverHandler;
use App_Finance_Ticket_Component_Status_Type_Default_Status as StatusHandler;
use App_Finance_Ticket_Component_JsonData as JsonDataHandler;
use App_Finance_Ticket_Component_Document_Handler as DocumentsHandler;
use App_Finance_Ticket_Component_Observer_Type_Validation_Observer as ValidationObserver;
use App_Finance_Ticket_Component_Acl_Default as Acl;
use App_Finance_Ticket_Component_Modification_Handler as ModificationHandler;
use App_Finance_Recipient_Collection as RecipientCollection;
use App_Finance_Ticket_Component_Attachment_ServiceNote_Document as ServiceNoteHandler;

interface App_Finance_Ticket_TicketInterface
{
    /**
     * Специальный код для желаемой даты обработки запроса
     * @const int
     */
    const DATE_CLIENT_DELIVERY_FACT = -1;

    /**
     * Создает тикет
     * При указании zendRow заполняет объект данными из zendRow
     * @param App_Db_Row_Ticket $zendRow
     */
    public function __construct(App_Db_Row_Ticket $zendRow = NULL);

    /**
     * Возвращает true, если тикет является новым и при сохранении будет создана новая запись в базе данных
     * @return bool
     */
    public function isNewTicket();

    /**
     * Возвращает true, если тикет представляет уже существующую запись в базе данных
     * @return bool
     */
    public function isExistedTicket();

    /**
     * Возвращает объект для получения информации о правах доступа к тем или иным операциям с тикетом
     * @return Acl
     */
    public function getAcl();

    /**
     * Возвращает true, если тикеты идентичны
     * @param App_Finance_Ticket_TicketInterface $compareObject
     * @return bool
     */
    public function equals(App_Finance_Ticket_TicketInterface $compareObject);

    /**
     * Возвращает Zend_Row, представляющей тикет
     * @return App_Db_Row_Ticket
     */
    public function getZendRow();

    /**
     * Возвращает Id тикета либо 0, если не указан
     * @return int
     */
    public function getId();

    /**
     * Возвращает URL тикета (соотвествующая страница саппорта+ticketId=?)
     * @param null|string $specifiedRecipient Указать получателя(если у тикетов данного типа может быть несколько получателей)
     * @throws Exception
     * @return string
     */
    public function getUrl($specifiedRecipient = NULL);

    /**
     * Возвращает дату создания тикета
     * @return Zend_Date|null
     */
    public function getDateCreated();

    /**
     * Устанавливает дату создания тикета
     * @param Zend_Date $dateCreated
     */
    public function setDateCreated(Zend_Date $dateCreated);

    /**
     * Возвращает дату, к которой должен быть обработан тикет
     * @return Zend_Date|null
     */
    public function getDateToProcess();

    /**
     * Устанавливает дату, к которой должен быть обработан тикет
     * @param Zend_Date $dateToProcess
     */
    public function setDateToProcess(Zend_Date $dateToProcess = NULL);

    /**
     * Устанавливает дату, к которой должен быть обработан тикет
     * Допускается:
     *  - Zend_Date
     *  - int(0-60): стартовая дата + n минут
     *  - int(>60): UNIX timestamp
     *  - string: попытка парсинга даты средствами Zend_Date
     * @param $dateToProcess
     */
    public function setDateToProcessFromMixed($dateToProcess);

    /**
     * Устанавливает дату обработки запроса
     * @param Zend_Date $dateProcessed
     */
    public function setDateProcessed(Zend_Date $dateProcessed = NULL);

    /**
     * Возвращает дату обработки запроса
     * @return Zend_Date|null
     */
    public function getDateProcessed();

    /**
     * Возвращает Id типа тикета
     * @return int
     */
    public function getTicketTypeId();

    /**
     * Возвращает название(name) типа тикета
     * @return string
     */
    public function getTicketTypeName();

    /**
     * Возвращает Id пользователя, создавшего тикет
     * @return int|null
     */
    public function getAuthorUserId();

    /**
     * Устанавливает Id пользователя, создавшего тикет
     * @param $authorUserId
     */
    public function setAuthorUserId($authorUserId);

    /**
     * Возвращает Id пользователя, обработавшего тикет
     * @return int|null
     */
    public function getProcessedUserId();

    /**
     * Устанавливает Id пользователя, обработавшего тикет
     * @param $processedUserId
     */
    public function setProcessedUserId($processedUserId);

    /**
     * Возвращает Id заявки, к которой принадлежит тикет
     * @return int
     */
    public function getClaimId();

    /**
     * Возвращает true, если к запросу прикреплена заявка
     * @return bool
     */
    public function hasClaimId();

    /**
     * Устанавливает Id заявки, к которой принадлежит тикет
     * @param $claimId
     */
    public function setClaimId($claimId);

    /**
     * Возвращает true, если тикет отмечен как завершенный
     * @return bool
     */
    public function isCompleted();

    /**
     * Отмечает тикет как завершенный
     */
    public function setCompleted();

    /**
     * Отмечает тикет как незавершенный
     */
    public function setUncompleted();

    /**
     * Возвращает true, если к данному тикету разрешено прикреплять файлы
     * @return bool
     */
    public function attachmentsAllowed();

    /**
     * Возвращает объект для работы с наблюдателями тикета
     * @return ObserverHandler
     */
    public function getObserversHandler();

    /**
     * Возвращает валидатор
     * @return ValidationObserver
     */
    public function getValidationObserver();

    /**
     * Возвращает объект для управления статусом тикета
     * @return StatusHandler
     */
    public function getStatusHandler();

    /**
     * Возвращает объект для управления дополнительными JSON-данными тикета
     * @return JsonDataHandler
     */
    public function getJsonDataHandler();

    /**
     * Возвращает объект для управления файлами, прикрепленными к тикету
     * @return DocumentsHandler
     */
    public function getDocumentsHandler();

    /**
     * Возвращает объект для получения информации о модифицированных полях в заявке
     * @return ModificationHandler
     */
    public function getModificationHandler();

    /**
     * Возвращает служебную записку данного запроса
     * @return ServiceNoteHandler
     */
    public function getServiceNoteHandler();

    /**
     * Возвращает список всех получателей, к которым может быть адресован данный тикет
     * @return RecipientCollection
     */
    public function getRecipients();

    /*
     * Возвращает список всех получателей(в виде массива строк), к которым может быть адресован данный тикет
     * @return string[]
     */
    public function getRecipientNames();

    /**
     * Возвращает "главного" получателя тикета в виде строкового значения
     * @return string
     */
    public function getMainRecipient();

    /**
     * Сохраняет изменения в тикете
     * @param array $options
     */
    public function save(array $options = null);

    /**
     * Удаляет тикет и всю связанную с ним информацию
     */
    public function destroy();

    /**
     * Рассылка почтовых сообщений
     */
    public function performMailing();
}