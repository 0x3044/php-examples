<?php
use App_Spl_Collection_Abstract as SplCollection;
use App_Finance_Ticket_TicketInterface as Ticket;

class App_Finance_Ticket_Collection extends SplCollection
{
    /**
     * Returns true if item is allowed for this collection
     * @param $item
     * @return bool
     */
    public function isItemAllowed($item)
    {
        return $item instanceof Ticket;
    }
}