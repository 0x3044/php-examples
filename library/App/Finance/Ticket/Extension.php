<?php
use App_Finance_Ticket_TicketInterface as Ticket;

class App_Finance_Ticket_Extension
{
    /**
     * Запрос
     * @var Ticket
     */
    protected $_ticket;

    /**
     * Модуль для запроса
     * @param App_Finance_Ticket_TicketInterface $ticket
     */
    public final function __construct(Ticket $ticket)
    {
        $this->_ticket = $ticket;
    }

    /**
     * Возвращает переданный конструктору запрос
     * @return \App_Finance_Ticket_TicketInterface
     */
    public final function getTicket()
    {
        return $this->_ticket;
    }
}