<?php
use App_Db_Row_Ticket as TicketRow;
use App_Finance_Ticket_Type_Abstract_Ticket as Ticket;
use App_Finance_Ticket_Collection as TicketCollection;
use App_Finance_Ticket_Finder as Finder;
use App_Finance_Ticket_Factory_Registry as Registry;

class App_Finance_Ticket_Factory
{
    /**
     * @var App_Finance_Ticket_Factory
     */
    protected static $_instance;

    /**
     * Массив "Тип тикета => Инстанцируемый класс"
     * @var array
     */
    protected $_ticketTypes = array();

    /**
     * Реестр уже загруженных записей
     * @var Registry
     */
    protected $_registry;

    /**
     * Фабрика тикетов (синглтон)
     * Создает существующие тикеты, зная их Id либо создает новые тикеты, зная их тип
     */
    public function __construct()
    {
        $this->_registry = new Registry();
        $this->_loadTicketTypes();
    }

    /**
     * Синглтон
     * @return App_Finance_Ticket_Factory
     */
    public static function getInstance()
    {
        if(is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * Возвращает тикет по его Id
     * @param int $ticketId
     * @return App_Finance_Ticket_Type_Abstract_Ticket
     */
    public function createFromTicketId($ticketId)
    {
        /** @var $ticketDbTable App_Db_Ticket */
        $ticketDbTable = App_Db::get(DB_TICKET);

        /** @var $ticketRow App_Db_Row_Ticket */
        $ticketRow = $ticketDbTable->getTicketById($ticketId);

        $ticket = $this->createFromZendRow($ticketRow);
        $this->getRegistry()->save($ticket);

        return $ticket;
    }

    /**
     * Возвращает реестр уже загруженных записей
     */
    public function getRegistry()
    {
        return $this->_registry;
    }

    /**
     * Создает новый тикет по указанному typeId
     * @param int $ticketTypeId
     * @return Ticket
     * @throws Exception
     */
    public function createFromTicketTypeId($ticketTypeId)
    {
        $ticketTypeDbTable = $this->_getTicketTypeTable();

        if(!(is_numeric($ticketTypeId))) {
            $ticketTypeId = $ticketTypeDbTable->getTicketTypeByName($ticketTypeId)->getId();
        }

        $ticketTypeName = $ticketTypeDbTable->getTicketTypeNameById($ticketTypeId);
        $ticketClassName = $this->getTicketClassNameByTicketType($ticketTypeName);

        if(!(class_exists($ticketClassName) && (is_subclass_of($ticketClassName, 'App_Finance_Ticket_TicketInterface')))) {
            throw new \Exception("Class \"{$ticketClassName}\" not found or not implementation of App_Finance_Ticket_TicketInterface");
        }

        return new $ticketClassName;
    }

    /**
     * Создает новый тикет по указанному ticketType
     * @param string $ticketTypeName
     * @return Ticket
     */
    public function createFromTicketTypeName($ticketTypeName)
    {
        $ticketTypeDbTable = $this->_getTicketTypeTable();

        return $this->createFromTicketTypeId($ticketTypeDbTable->getTicketTypeIdByName($ticketTypeName));
    }

    /**
     * Создает тикет, заполняя его данными из Zend_Row
     * @param App_Db_Row_Ticket $zendRow
     * @return App_Finance_Ticket_Type_Abstract_Ticket
     * @throws Exception
     */
    public function createFromZendRow(TicketRow $zendRow)
    {
        $ticketTypeDbTable = $this->_getTicketTypeTable();
        $ticketTypeName = $ticketTypeDbTable->getTicketTypeNameById($zendRow->getTicketTypeId());
        $ticketClassName = $this->getTicketClassNameByTicketType($ticketTypeName);

        if(!(class_exists($ticketClassName) && (is_subclass_of($ticketClassName, 'App_Finance_Ticket_TicketInterface')))) {
            throw new \Exception("Class \"{$ticketClassName}\" not found or not implementation of App_Finance_Ticket_TicketInterface");
        }

        /** @var $ticket Ticket */
        $ticket = new $ticketClassName($zendRow);

        return $ticket;
    }

    /**
     * Возвращает все тикеты, которые так или иначе привязаны к указанной заявке
     * @param $claimId
     * @return App_Finance_Ticket_Collection
     */
    public function createSameClaimIdTickets($claimId)
    {
        App_Spl_TypeCheck::getInstance()->positiveNumeric($claimId);

        /** @var $dbTicket App_Db_Ticket */
        $dbTicket = $this->_getTicketTable();
        $tickets = new TicketCollection();

        $ticketRows = $dbTicket->getTicketsByClaimId($claimId, 'date_created desc');

        if(count($ticketRows) > 0) {
            foreach($ticketRows as $ticket) {
                $tickets->add($this->createFromZendRow($ticket));
            }
        }

        return $tickets;
    }

    /**
     * Возвращает коллекцию тикетов, привязанных к указанной заявке
     * @param int $claimId
     * @return TicketCollection
     */
    public function getTicketsByClaimId($claimId)
    {
        $ticketTable = $this->_getTicketTable();
        $ticketRows = $ticketTable->getTicketsByClaimId($claimId);

        return $this->_convertZendRowsToTickets($ticketRows);
    }

    /**
     * Возвращает коллекцию активных тикетов диспетчерского саппорта, привязанных к указанной заявке
     * @param $claimId
     * @return App_Finance_Ticket_Collection
     */
    public function getActiveDispatcherTicketsByClaimId($claimId)
    {
        $ticketTable = $this->_getTicketTable();
        $ticketTypeTable = $this->_getTicketTypeTable();
        $ticketRows = $ticketTable->getActiveTicketsByTicketTypeIdAndClaimId($ticketTypeTable->getTicketTypeIdByName("dispatcher_verify"), $claimId);

        return $this->_convertZendRowsToTickets($ticketRows);
    }

    /**
     * Возвращает коллекцию активных тикетов складского саппорта, привязанных к указанной заявке
     * @param $claimId
     * @return App_Finance_Ticket_Collection
     */
    public function getActiveDepotTicketsByClaimId($claimId)
    {
        $ticketTable = $this->_getTicketTable();
        $ticketTypeTable = $this->_getTicketTypeTable();
        $ticketRows = $ticketTable->getActiveTicketsByTicketTypeIdAndClaimId($ticketTypeTable->getTicketTypeIdByName("depot_verify"), $claimId);

        return $this->_convertZendRowsToTickets($ticketRows);
    }

    /**
     * Возвращает true, если по заявке есть отмененный запрос, который когда-то был в архиве
     * @param int $claimId
     * @return bool
     */
    public function hasCanceledArchivedTicket($claimId)
    {
        $tickets = $this->getTicketsByClaimId($claimId);

        /** @var $ticket Ticket */
        foreach($tickets as $ticket) {
            $isCanceled = $ticket->getStatusHandler()->isCanceled();
            $wasArchived = $ticket->getJsonDataHandler()->offsetExists('wasArchived') && $ticket->getJsonDataHandler()->offsetGet('wasArchived');

            if($isCanceled && $wasArchived) {
                return true;
            }
        }

        return false;
    }

    /**
     * Конвертирует результаты запроса в тикеты
     * @param Zend_Db_Table_Rowset_Abstract $ticketRows
     * @return App_Finance_Ticket_Collection
     */
    protected function _convertZendRowsToTickets(\Zend_Db_Table_Rowset_Abstract $ticketRows)
    {
        $ticketCollection = new TicketCollection();

        if($ticketRows->count()) {
            foreach($ticketRows as $ticketRow) {
                $ticketCollection->add($this->createFromZendRow($ticketRow));
            }
        }

        return $ticketCollection;
    }

    /**
     * @return App_Db_Ticket
     */
    protected function  _getTicketTable()
    {
        return App_Db::get(DB_TICKET);
    }

    /**
     * @return App_Db_TicketType
     */
    protected function  _getTicketTypeTable()
    {
        return App_Db::get(DB_TICKET_TYPE);
    }

    /**
     * Возвращает имя класса, соответствующее указанному типу тикетов
     * @param string $ticketType
     * @return string
     * @throws Exception
     */
    public function getTicketClassNameByTicketType($ticketType)
    {
        if(!($this->hasTicketType($ticketType))) {
            throw new \Exception("TicketType {$ticketType} not exists");
        }

        return $this->_ticketTypes[$ticketType];
    }

    /**
     * Загружает карту "тип тикета => класс" из ticket_types.php
     * @throws Exception
     */
    protected function _loadTicketTypes()
    {
        $fileName = __DIR__.'/Factory/ticket_types.php';

        if(!(is_readable($fileName))) {
            throw new \Exception('Cannot open factory configuration file');
        }

        /** @var $ticketTypesConfig array */
        $ticketTypesConfig = include $fileName;

        if(!(is_array($ticketTypesConfig))) {
            throw new \Exception('Invalid factory configuration file');
        }

        if(count($ticketTypesConfig)) {
            foreach($ticketTypesConfig as $ticketType => $ticketClassName) {
                $this->addTicketType($ticketType, $ticketClassName);
            }
        }
    }

    /**
     * Добавить новую связь "тип тикета => класс" в фабрику
     * @param string $ticketType
     * @param string $ticketClassName
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function addTicketType($ticketType, $ticketClassName)
    {
        if(!(is_string($ticketType)) || !(is_string($ticketClassName))) {
            throw new \InvalidArgumentException('ticketType/ticketClassName should have a string type, got '.gettype($ticketType));
        }

        if(!(class_exists($ticketClassName) && (is_subclass_of($ticketClassName, 'App_Finance_Ticket_TicketInterface')))) {
            throw new \Exception("Class {$ticketClassName} not exists or is not implementation of App_Finance_Ticket_TicketInterface interface");
        }

        $this->_ticketTypes[$ticketType] = $ticketClassName;
    }

    /**
     * Возвращает true, если фабрика знает, как создать тикет с указанным типом
     * @param string $ticketType
     * @return bool
     * @throws InvalidArgumentException
     */
    public function hasTicketType($ticketType)
    {
        if(!(is_string($ticketType))) {
            throw new \InvalidArgumentException('ticketType should have a string type, got '.gettype($ticketType));
        }

        return isset($this->_ticketTypes[$ticketType]);
    }

    /**
     * Удаляет связь "тип тикета => класс" из фабрики
     * @param string $ticketType
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function removeTicketType($ticketType)
    {
        if(!(is_string($ticketType))) {
            throw new \InvalidArgumentException('ticketType should have a string type, got '.gettype($ticketType));
        }

        if(!($this->hasTicketType($ticketType))) {
            throw new \Exception("TicketType {$ticketType} not exists");
        }

        unset($this->_ticketTypes[$ticketType]);
    }
}