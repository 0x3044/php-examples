<?php
use App_Finance_Ticket_TicketInterface as Ticket;
use App_Finance_Ticket_Factory as TicketFactory;
use App_Finance_Recipient_Factory as RecipientFactory;
use App_Finance_Ticket_Collection as TicketCollection;

/**
 * Поиск запросов в финансовый саппорт по определенным критериям
 */
class App_Finance_Ticket_Finder
{
    /**
     * Указанные ID заявок
     * @var array|null
     */
    protected $_claimIds;

    /**
     * Указанные получатели
     * @var array|null
     */
    protected $_recipients;

    /**
     * Указанные типы запросов
     * @var array|null
     */
    protected $_ticketTypeIds;

    /**
     * Указанные статусы запросов
     * @var array|null
     */
    protected $_ticketStatus;

    /**
     * Дополнительные where-условия
     * @var string|array
     */
    protected $_additionalWhere;

    /**
     * Сортировка
     * @var array
     */
    protected $_order;

    /**
     * Искать запросы указанных заявок
     * @param array|int $claimIds
     * @return $this
     */
    public function setClaimIds($claimIds)
    {
        if(!(is_array($claimIds))) {
            $claimIds = array($claimIds);
        }

        $this->_claimIds = $claimIds;

        return $this;
    }

    /**
     * Возвращает указанные ID заявок
     * @return array|int
     */
    protected function _getClaimIds()
    {
        return $this->_claimIds;
    }

    /**
     * Искать запросы указанных получателей
     * @param array|string $recipients
     * @return $this
     */
    public function setRecipients($recipients)
    {
        if(!(is_array($recipients))) {
            $recipients = array($recipients);
        }

        $this->_recipients = $recipients;

        return $this;
    }

    /**
     * Возврашает указанных получателей
     * @return array|int
     */
    protected function _getRecipients()
    {
        return $this->_recipients;
    }

    /**
     * Искать запросы указанных статусов
     * @param array|int $status
     * @return $this
     */
    public function setTicketStatus($status)
    {
        if(!(is_array($status))) {
            $status = array($status);
        }

        $this->_ticketStatus = $status;

        return $this;
    }

    /**
     * Возвращает указанные статусы запросов
     * @return array|int
     */
    protected function _getTicketStatus()
    {
        return $this->_ticketStatus;
    }

    /**
     * Искать запросы указанных типов
     * @param array|int|string $ticketTypeIds
     * @return $this
     */
    public function setTicketTypeIds($ticketTypeIds)
    {
        if(!(is_array($ticketTypeIds))) {
            $ticketTypeIds = array($ticketTypeIds);
        }

        $this->_ticketTypeIds = $ticketTypeIds;

        return $this;
    }

    /**
     * Возвращает указанные типы запросов
     * @return array|int|string
     */
    public function getTicketTypeIds()
    {
        return $this->_ticketTypeIds;
    }

    /**
     * Устанавливает дополнительные WHERE-условия
     * @param array|string $additionalWhere
     */
    public function setAdditionalWhere($additionalWhere)
    {
        $this->_additionalWhere = $additionalWhere;
    }

    /**
     * Возвращает дополнительные WHERE-условия
     * @return array|string
     */
    public function getAdditionalWhere()
    {
        return $this->_additionalWhere;
    }

    /**
     * Сортировка по выбранному полю
     * @param string $fieldName
     * @param string $direction asc, desc
     * @throws Exception
     */
    public function sortBy($fieldName, $direction)
    {
        $direction = strtolower($direction);

        if($direction != 'desc' && $direction != 'asc') {
            throw new \Exception('Unknown order direction');
        }

        $this->_order[$fieldName] = $direction;
    }

    /**
     * Удалить сортировку по выбранному полю
     * @param string $fieldName
     */
    public function removeSort($fieldName)
    {
        if(isset($this->_order[$fieldName])) {
            unset($this->_order[$fieldName]);
        }
    }

    /**
     * Возвращает первый доступный результат либо FALSE в случаи отсутствия запросов по указанным критериям
     * @return bool|Ticket
     */
    public function findOne()
    {
        $result = $this->find();

        if($result->size()) {
            return $result[0];
        } else {
            return false;
        }
    }

    /**
     * Возвращает сгененрированный селект
     * @return Zend_Db_Table_Select
     */
    public function getSelect()
    {
        return $this->_generateSqlQuery();
    }

    /**
     * Возвращает результат запроса
     * @return App_Finance_Ticket_Collection
     */
    public function find()
    {
        $dbTable = App_Db::get(DB_TICKET);
        $selectQuery = $this->_generateSqlQuery();
        $tickets = new TicketCollection();

        if($result = $dbTable->fetchAll($selectQuery)) {
            /** @var $ticketRow App_Db_Row_Ticket */
            foreach($result as $ticketRow) {
                $tickets->add(TicketFactory::getInstance()->createFromZendRow($ticketRow));
            }
        }

        return $tickets;
    }

    /**
     * Генерация запроса
     * @return Zend_Db_Table_Select
     */
    protected function _generateSqlQuery()
    {
        $dbTable = App_Db::get(DB_TICKET);

        $selectQuery = $dbTable->select();

        $this->_generateWhereClaimIds($selectQuery);
        $this->_generateWhereTicketTypes($selectQuery);
        $this->_generateWhereTicketStatus($selectQuery);
        $this->_generateSort($selectQuery);

        if($this->getAdditionalWhere()) {
            $selectQuery->where($this->getAdditionalWhere());
        }

        return $selectQuery;
    }

    /**
     * Генерация запроса - ID заявок
     * @param Zend_Db_Table_Select $selectQuery
     */
    protected function _generateWhereClaimIds(Zend_Db_Table_Select $selectQuery)
    {
        $claimIds = $this->_getClaimIds();

        if(is_array($claimIds) && count($claimIds) > 0) {
            foreach($claimIds as $claimId) {
                App_Spl_TypeCheck::getInstance()->positiveNumeric($claimId);
            }

            $selectQuery->where('claim_id IN ('.implode(',', $claimIds).')');
        }
    }

    /**
     * Генерация запросов - тип запросов
     * @param Zend_Db_Table_Select $selectQuery
     */
    protected function _generateWhereTicketTypes(Zend_Db_Table_Select $selectQuery)
    {
        $ticketTypeIds = $this->getTicketTypeIds();
        $recipients = $this->_getRecipients();
        /** @var $ticketTypeTable App_Db_TicketType */
        $ticketTypeTable = App_Db::get(DB_TICKET_TYPE);

        if(is_array($recipients) && count($recipients) > 0) {
            if(!(is_array($ticketTypeIds))) {
                $ticketTypeIds = array();
            }

            foreach($recipients as $recipient) {
                $recipient = RecipientFactory::getInstance()->createFromRecipientName($recipient);
                $ticketTypeIds = array_merge($ticketTypeIds, $recipient->getTicketTypeIds());
            }
        }

        if(is_array($ticketTypeIds) && count($ticketTypeIds)) {
            $sqlIds = array();

            foreach($ticketTypeIds as $ticketTypeId) {
                if(!(is_numeric($ticketTypeId))) {
                    $ticketTypeId = $ticketTypeTable->getTicketTypeByName($ticketTypeId)->getId();
                }

                App_Spl_TypeCheck::getInstance()->positiveNumeric($ticketTypeId);

                $sqlIds[] = $ticketTypeId;
            }

            $selectQuery->where('ticket_type_id IN ('.implode(',', $sqlIds).')');
        }
    }

    /**
     * Генерация запроса - статус запросов
     * @param Zend_Db_Table_Select $selectQuery
     * @throws Exception
     */
    protected function _generateWhereTicketStatus(Zend_Db_Table_Select $selectQuery)
    {
        $ticketStatus = $this->_getTicketStatus();

        if(is_array($ticketStatus) && count($ticketStatus)) {
            $sqlIds = array();
            $defaultStatuses = App_Finance_Ticket_Component_Status_Type_Default_Status::getDefaultAvailableStatuses();

            foreach($ticketStatus as $status) {
                if(is_string($status)) {
                    if(!(isset($defaultStatuses[$status]))) {
                        throw new \Exception("Status `{$status}` not found");
                    }

                    $status = $defaultStatuses[$status];
                }

                App_Spl_TypeCheck::getInstance()->positiveNumeric($status);

                $sqlIds[] = $status;
            }

            $selectQuery->where('status IN ('.implode(',', $sqlIds).')');
        }
    }

    /**
     * Генерация запроса - сортировка
     * @param Zend_Db_Table_Select $selectQuery
     */
    protected function _generateSort(Zend_Db_Table_Select $selectQuery)
    {
        $order = $this->_order;
        $specs = array();

        if(is_array($order) && count($order)) {
            foreach($order as $fieldName => $direction) {
                $specs[] = $fieldName.' '.$direction;
            }
        }

        $selectQuery->order($specs);
    }
}